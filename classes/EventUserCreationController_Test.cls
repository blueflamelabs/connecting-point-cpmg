/*****************************************/
/**
*  @Who     Rajnish Kumar
*  @When    26/06/2018
*  @What    Test Class for cover EventUserCreationController,ChangePasswordController1,ChangePasswordCTRL,ChangePasswordController,
*           
*/
/****************************************/
@isTest
private class EventUserCreationController_Test {

    @isTest
    private static void supplier_Contact_Test1() {
        TestDataGenerator.createInactiveTriggerSettings();
        Community_Settings__c commSett = TestDataGenerator.createCommSettings();
        
        Id suppAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Account a = TestDataGenerator.createTestAccount(true, 'Test Account',suppAccRtype);
        Id suppConRtype =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Contact c = TestDataGenerator.createTestContact(true,'Test','Last Name','testAcc@account.com',a,suppConRtype);

        Contact attendContact = TestDataGenerator.createTestContact(true,'Test','Attending Con', 'testAttendingCon@account.com',a,suppConRtype);
        
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event Active',true,Date.today().addDays(10),Date.today().addDays(15),Date.today().addMonths(-2), Date.today().addMonths(-1));

        Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Event Active Opp',Date.today().addMonths(-4),a,'Closed Won',prod);

        Id execPartRecordType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Suppliers').getRecordTypeId();
        Event_Account_Details__c partRecord = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c, execPartRecordType);
        partRecord.Registration_Part_1_Submitted__c = true;
        update partRecord;
//Complete
        Order conf = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod);
        Order attendingConf = TestDataGenerator.createConfirmation(true,a,attendContact,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod);
        
        

        PageReference pageRef = Page.SupplierEventCreationPage;
        pageRef.getParameters().put('id', prod.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(prod);
        Test.setCurrentPageReference(pageRef);
        
        Test.startTest();
            EventUserCreationController eventUserController = new EventUserCreationController(sc);
            eventUserController.executiveUserCreationProcess();
            eventUserController.supplierUserCreationProcess();
            prod.IsActive = false;
            eventUserController.executiveUserCreationProcess();
            eventUserController.supplierUserCreationProcess();
        Test.stopTest();
    }

    @isTest
    private static void supplier_Contact_Test2() {
        TestDataGenerator.createInactiveTriggerSettings();
        Community_Settings__c commSett = TestDataGenerator.createCommSettings();
        
        Id suppAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Account a = TestDataGenerator.createTestAccount(true, 'Test Account',suppAccRtype);
        Id suppConRtype =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Contact c = TestDataGenerator.createTestContact(true,'Test','Last Name','testAcc@account.com',a,suppConRtype);

        Contact attendContact = TestDataGenerator.createTestContact(true,'Test','Attending Con', 'testAttendingCon@account.com',a,suppConRtype);
        
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event Active',true,Date.today().addDays(10),Date.today().addDays(15),Date.today().addMonths(-2), Date.today().addMonths(-1));

        Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Event Active Opp',Date.today().addMonths(-4),a,'Closed Won',prod);

        Id execPartRecordType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Suppliers').getRecordTypeId();
        Event_Account_Details__c partRecord = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c, execPartRecordType);
        partRecord.Registration_Part_1_Submitted__c = true;
        update partRecord;

        Order conf = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod,true);
        Order attendingConf = TestDataGenerator.createConfirmation(true,a,attendContact,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod);
        
     

        PageReference pageRef = Page.SupplierEventCreationPage;
        pageRef.getParameters().put('id', prod.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(prod);
        Test.setCurrentPageReference(pageRef);
        
        Test.startTest();
            EventUserCreationController eventUserController = new EventUserCreationController(sc);
           prod.IsActive = false;
            update prod;
            eventUserController.executiveUserCreationProcess();
            eventUserController.supplierUserCreationProcess();
        Test.stopTest();
    }
  
   @isTest
      private static void executive_Contact_Test3() {
            TestDataGenerator.createInactiveTriggerSettings();
            Community_Settings__c commSett = TestDataGenerator.createCommSettings();
            
            Id suppAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
            Account a = TestDataGenerator.createTestAccount(true, 'Test Account',suppAccRtype);
            Id suppConRtype =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
            Contact c = TestDataGenerator.createTestContact(true,'Test','Last Name','testAcc@account.com',a,suppConRtype);

            Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event Active',true,Date.today().addDays(10),Date.today().addDays(15),Date.today().addMonths(-2), Date.today().addMonths(-1));
    
            Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Event Active Opp',Date.today().addMonths(-4),a,'Closed Won',prod);
    
            Id execPartRecordType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
            Event_Account_Details__c partRecord = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c, execPartRecordType);
            partRecord.Registration_Part_1_Submitted__c = true;
            update partRecord;
    
            Order conf = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod);
           
            PageReference pageRef1 = Page.ResendPage;
            pageRef1.getParameters().put('id', conf.Id);
            ApexPages.StandardController sc1 = new ApexPages.StandardController(conf);
            Test.setCurrentPageReference(pageRef1);

            Profile profileId = [SELECT Id FROM Profile WHERE Name= 'CPMG Community Portal User' LIMIT 1];
            Id pricebookId = Test.getStandardPricebookId();
            PricebookEntry PBE= new PricebookEntry();
                PBE.Pricebook2Id=pricebookId;
                PBE.Product2Id=prod.id;
                PBE.UnitPrice=100;
                PBE.UseStandardPrice=false;
                PBE.isActive = true;
                Insert PBE;   
            Test.startTest();
                EventUserCreationController eventUserController = new EventUserCreationController(sc1);
                eventUserController.ResendMailndUserCreationProcess();
                eventUserController.RegenerateLinkMailndUserCreationProcess();
            Test.stopTest();
        }

        @isTest
        private static void executive_Contact_Test4() {
            TestDataGenerator.createInactiveTriggerSettings();
            Community_Settings__c commSett = TestDataGenerator.createCommSettings();
            
            Id suppAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
            Account a = TestDataGenerator.createTestAccount(true, 'Test Account',suppAccRtype);
            Id suppConRtype =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
            Contact c = TestDataGenerator.createTestContact(true,'Test','Last Name','testAcc@account.com',a,suppConRtype);
    
            Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event Active',true,Date.today().addDays(10),Date.today().addDays(15),Date.today().addMonths(-2), Date.today().addMonths(-1));
    
            Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Event Active Opp',Date.today().addMonths(-4),a,'Closed Won',prod);
    
            Id execPartRecordType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
            Event_Account_Details__c partRecord = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c, execPartRecordType);
            partRecord.Registration_Part_1_Submitted__c = true;
            update partRecord;
    
            Order conf = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod);
            
            prod.IsActive = false;
            prod.Supplier_Users_Created__c = true;
            update prod;

            PageReference pageRef1 = Page.ResendPage;
            pageRef1.getParameters().put('id', conf.Id);
            ApexPages.StandardController sc1 = new ApexPages.StandardController(conf);
            Test.setCurrentPageReference(pageRef1);
             
            Profile profileId = [SELECT Id FROM Profile WHERE Name= 'CPMG Community Portal User' LIMIT 1];
            Id pricebookId = Test.getStandardPricebookId();
            PricebookEntry PBE= new PricebookEntry();
                PBE.Pricebook2Id=pricebookId;
                PBE.Product2Id=prod.id;
                PBE.UnitPrice=100;
                PBE.UseStandardPrice=false;
                PBE.isActive = true;
                Insert PBE;   
            Test.startTest();
                EventUserCreationController eventUserController = new EventUserCreationController(sc1);
                eventUserController.ResendMailndUserCreationProcess();
                eventUserController.RegenerateLinkMailndUserCreationProcess();
            Test.stopTest();
        }

        @isTest
        private static void executive_Contact_Test5() {
            TestDataGenerator.createInactiveTriggerSettings();
            Community_Settings__c commSett = TestDataGenerator.createCommSettings();
            
            Id suppAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
            Account a = TestDataGenerator.createTestAccount(true, 'Test Account',suppAccRtype);
            Id suppConRtype =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
            Contact c = TestDataGenerator.createTestContact(true,'Test','Last Name','testAcc@account.com',a,suppConRtype);

            Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event Active',true,Date.today().addDays(10),Date.today().addDays(15),Date.today().addMonths(-2), Date.today().addMonths(-1));
    
            Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Event Active Opp',Date.today().addMonths(-4),a,'Closed Won',prod);
    
            Id execPartRecordType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
            Event_Account_Details__c partRecord = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c, execPartRecordType);
            partRecord.Registration_Part_1_Submitted__c = true;
            update partRecord;
    
            Order conf = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod);
            prod.Allow_Welcome_Emails_for_Executives__c = false;
            update prod;

            PageReference pageRef1 = Page.ResendPage;
            pageRef1.getParameters().put('id', conf.Id);
            ApexPages.StandardController sc1 = new ApexPages.StandardController(conf);
            Test.setCurrentPageReference(pageRef1);
                
            Profile profileId = [SELECT Id FROM Profile WHERE Name= 'CPMG Community Portal User' LIMIT 1];
            Id pricebookId = Test.getStandardPricebookId();
            PricebookEntry PBE= new PricebookEntry();
                PBE.Pricebook2Id=pricebookId;
                PBE.Product2Id=prod.id;
                PBE.UnitPrice=100;
                PBE.UseStandardPrice=false;
                PBE.isActive = true;
                Insert PBE; 

            Test.startTest();
                EventUserCreationController eventUserController = new EventUserCreationController(sc1);
                eventUserController.ResendMailndUserCreationProcess();
                eventUserController.RegenerateLinkMailndUserCreationProcess();
        
                ForgotPasswordCtrl forgtPassCtrl = New ForgotPasswordCtrl();
                forgtPassCtrl.sendEmail();
            Test.stopTest();
                
        }
   
    @isTest
        private static void executive_Contact_Test7() {
            TestDataGenerator.createInactiveTriggerSettings();
            Community_Settings__c commSett = TestDataGenerator.createCommSettings();
            
            Id suppAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
            Account a = TestDataGenerator.createTestAccount(true, 'Test Account',suppAccRtype);
            Id suppConRtype =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
            Contact c = TestDataGenerator.createTestContact(true,'Test','Last Name','testAcc@account.com',a,suppConRtype);
    
            Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event Active',true,Date.today().addDays(10),Date.today().addDays(15),Date.today().addMonths(-2), Date.today().addMonths(-1));
    
            Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Event Active Opp',Date.today().addMonths(-4),a,'Closed Won',prod);
    
            Id execPartRecordType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
            Event_Account_Details__c partRecord = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c, execPartRecordType);
            partRecord.Registration_Part_1_Submitted__c = true;
            update partRecord;
    
            Order conf = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod);
            
            prod.Executive_Users_Created__c = true;
            update prod;
            
            PageReference pageRef1 = Page.ResendPage;
            pageRef1.getParameters().put('id', conf.Id);
            ApexPages.StandardController sc1 = new ApexPages.StandardController(conf);
            Test.setCurrentPageReference(pageRef1);
                
            Profile profileId = [SELECT Id FROM Profile WHERE Name= 'CPMG Community Portal User' LIMIT 1];
            Id pricebookId = Test.getStandardPricebookId();
            PricebookEntry PBE= new PricebookEntry();
                PBE.Pricebook2Id=pricebookId;
                PBE.Product2Id=prod.id;
                PBE.UnitPrice=100;
                PBE.UseStandardPrice=false;
                PBE.isActive = true;
                Insert PBE; 

                User u = new User();
                    u.ProfileId = profileId.Id;
                    u.FirstName = 'FirstName';
                    u.Phone = '5151511';
                    u.Email = 'email11@google.com';
                    u.EmailEncodingKey = 'UTF-8';
                    u.LastName = 'LastName';
                    u.LanguageLocaleKey = 'en_US';
                    u.LocalesIdKey = 'en_US';
                    u.ContactId = c.id;
                    u.TimezonesIdKey = 'America/Los_Angeles';
                    u.Alias = 'uhsda';
                    u.isActive = true;
                    u.Username = 'test@gmail.com';
                    u.CommunityNickname = 'test';
                    if (u.CommunityNickname.length() > 37) {
                        u.CommunityNickname = u.CommunityNickname.substring(0, 36);
                    }
                    u.CommunityNickname = u.CommunityNickname + 'amc';
                    Insert u;
           
            Test.startTest();
                EventUserCreationController eventUserController = new EventUserCreationController(sc1);
                //ApexPages.currentPage().getParameters().put('ContactId',c.Id);
                ApexPages.currentPage().getParameters().put('Token','20180211');

                ForgotPasswordCtrl forgtPassCtrl = New ForgotPasswordCtrl();
                forgtPassCtrl.sendEmail();
                
                ChangePasswordCTRL changPassCONT = New ChangePasswordCTRL();
                Contact c1 = TestDataGenerator.createTestContact(true,'Tes1t','Last1Name','testAcc1@account.com',a,suppConRtype);
                
                prod.Year__c = '2019';
                prod.Family = 'HotelPoint';
                update prod;
                
                Blob cryptoKey = Blob.valueOf(system.label.cryptoKey);
                String CurrentDate = '20180726';
                Blob tokenData = Blob.valueOf('{"contactId":"'+c1.Id+'","confirmationId":"'+conf.id+'","token":"'+CurrentDate+'"}');
                Blob encryptedData = Crypto.encryptWithManagedIV('AES128',cryptoKey,tokenData);
                String token = EncodingUtil.base64Encode(encryptedData);
                
                
                PageReference pageRef11 = Page.CPMG_SetPassword;
                pageRef11.getParameters().put('contactId', token);
                ApexPages.StandardController sc11 = new ApexPages.StandardController(conf);
                Test.setCurrentPageReference(pageRef11);
                
                ChangePasswordController1 changPassContr1 = new ChangePasswordController1();
                 
                 changPassContr1.passwordPolicy= null;
                 changPassContr1.newPassword='12o3tyy299';
                 changPassContr1.sendEmail();
                 changPassContr1.passwordPolicy='123//.4';
                 changPassContr1.newPassword='1233';
                 changPassContr1.confirmPassword='1233';
                 changPassContr1.sendEmail();
                 changPassContr1.newPassword='12o3tyy2';
                 changPassContr1.confirmPassword='12o3tyy2';
                 changPassContr1.sendEmail();
                 changPassContr1.newPassword='12345678';
                 changPassContr1.sendEmail();
                 changPassContr1.newPassword='12345678';
                 changPassContr1.redirectUrl();
                
            Test.stopTest();      
        }
        private static testMethod void executive_Contact_Test8() { 
                 ChangePasswordCTRL changPassCONT = new ChangePasswordCTRL();
                 changPassCONT.passwordPolicy='123//.4';
                 changPassCONT.newPassword='1233';
                 changPassCONT.confirmPassword='1233';
                 changPassCONT.sendEmail();
                 changPassCONT.newPassword='12o3tyy2';
                 changPassCONT.confirmPassword='12o3tyy2';
                 changPassCONT.sendEmail();
                 changPassCONT.newPassword='12345678';
                 changPassCONT.sendEmail();
                 
                 ChangePasswordController objChangePass = new ChangePasswordController();
                 objChangePass.passwordPolicy='123//.4';
                 objChangePass.newPassword='1233';
                 objChangePass.changePassword();
                 objChangePass.newPassword='123312345';
                 objChangePass.verifyNewPassword='123312345';
                 objChangePass.oldPassword='12345678';
                 objChangePass.changePassword();
        }
}