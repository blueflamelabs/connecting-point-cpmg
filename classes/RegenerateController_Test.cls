/*
*Created By     : 
*Created Date   : 19/07/2019 
*Description    : 
*Test Class Name: RegenerateController
Modification History :
============================================================================================
V.No    Date            By                  Description
============================================================================================
*/
@IsTest
public class RegenerateController_Test{
    static testMethod void RegenerateController_testMethod(){
         Id execAccountRtype = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
         Id execConRtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
         Account a = TestDataGenerator.createTestAccount(true,'Test Account',execAccountRtype);
         Account objacc = [Select id,RecordTypeId,RecordType.Name from Account where Id =:a.id];
         
         Contact objCon = TestDataGenerator.createTestContact(true,'Test','Exec','t@t.com',objacc,(String) execConRtype);
      
         Profile profilId = [SELECT Id FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        
         User objUser = TestDataGenerator.CreateUser(true,profilId.Id,'Test','Exec','2225252525','test1Exec1234@cpmg.com',objCon.Id);
        
         Product2 objProd = TestDataGenerator.createTestProduct(true,'Test Name',True);
          
         Event_Staff__c objEventStf = TestDataGenerator.createEventStaff(true,objProd,objUser,'Support');
        
         Opportunity objOpp = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),objacc,'Closed Won',objProd,'Onsite Renewal');
         Opportunity objOpp1 = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),objacc,'Closed Won',objProd,'Onsite Renewal');
        
         Id execPartRecordType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        
         Event_Account_Details__c partRecord = TestDataGenerator.createParticipationRecord(true,objProd,objacc,objOpp,objCon, execPartRecordType);
        
         Opportunity objOppQuery = [select id,Account_Record_Type__c,CloseDate,AccountId from opportunity where id =:objOpp.id];
         objOppQuery.AccountId = a.id;
         update objOppQuery;
         
         System.debug('#########objOppQueryobjOppQuery############# '+objOppQuery);
         Order objconf = TestDataGenerator.createConfirmation(true,objacc,objCon,'Attending Event Contact',objOppQuery,objOppQuery.CloseDate,partRecord,objProd,true);
        
        objconf  = [Select Welcome_Email_Link_Expiration__c,id,Opportunity.RecordTypeId,Opportunity.Account_Record_Type__c from Order where id=:objconf.id];
        objconf.Welcome_Email_Link_Expiration__c  = Date.toDay().addDays(-2);
        update objconf;
        System.debug('########objconfobjconfobjconf######## '+objconf);
        System.debug('##########objOrListobjAccount_Record_Type__cOrList####### '+objconf.Opportunity.Account_Record_Type__c);
        Community_Welcome_Emails__mdt cwem = new Community_Welcome_Emails__mdt();
           cwem.Sender_Name__c = 'test';
           cwem.Channel__c = ' Supplier';
           cwem.Default_Email_Sender__c = 'test@123';
           cwem.Email_Template_ID__c ='test';
           cwem.Status__c = 'Attending Event Contact';
           cwem.User_Type__c= 'New User';
        
        EmailTemplate emailTemp = new EmailTemplate();
            emailTemp.isActive = true;
            emailTemp.Name = 'name';
            emailTemp.DeveloperName = 'unique_name_addSomethingSpecialHere';
            emailTemp.TemplateType = 'text';
            emailTemp.FolderId = UserInfo.getUserId();
          
        Test.startTest();
            ApexPages.StandardController controller = new ApexPages.StandardController(objProd);
            RegenerateController extension = new RegenerateController(controller);
            extension.RegenerateProcessExecutive();
            extension.RegenerateProcessSupplier();
            extension.RegenerateProcess('Executive');
            extension.createEmailMessage(objCon.Id,objProd.Id,emailTemp.Id,objUser.id);
        Test.stopTest();
    }
 }