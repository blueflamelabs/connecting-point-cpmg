public class PreventEventQuestionCTRL{
    public void preventInsert(List<Event_Question__c> objEventQuestionList){
       Set<Id> objEventId = new Set<Id>();
       for(Event_Question__c objEventQu : objEventQuestionList){
           objEventId.add(objEventQu.Event__c);
       }
       List<Event_Question__c> objEventQuestionListDetails = [select id,RecordTypeId,Event__c,Phase__c,Display_Type__c from Event_Question__c where Event__c IN : objEventId];
       Map<Id,List<Event_Question__c>> objEventIdEventQuList = new Map<Id,List<Event_Question__c>>();
       for(Event_Question__c objEventQu : objEventQuestionListDetails){
            if(!objEventIdEventQuList.keyset().contains(objEventQu.Event__c)){
                objEventIdEventQuList.put(objEventQu.Event__c,new List<Event_Question__c>{objEventQu});
            }else{
                List<Event_Question__c> objEventQuList = objEventIdEventQuList.get(objEventQu.Event__c);
                objEventQuList.add(objEventQu);
                objEventIdEventQuList.put(objEventQu.Event__c,objEventQuList);
            }
       }
       for(Event_Question__c objEventQu : objEventQuestionList){
         if(objEventIdEventQuList.get(objEventQu.Event__c) != null){
          List<Event_Question__c> objEventQuList = objEventIdEventQuList.get(objEventQu.Event__c);
          for(Event_Question__c objEventQuestion : objEventQuList){
              if(objEventQu.Display_Type__c == objEventQuestion.Display_Type__c && objEventQu.RecordTypeId == objEventQuestion.RecordTypeId){
                  objEventQu.addError('There is already an event question for this display type. You cannot use it again');
              }
          }
        }
      }   
    }
}