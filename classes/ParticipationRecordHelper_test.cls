@isTest
private class ParticipationRecordHelper_test {
    
    @isTest static void testGetEventAccountIdAndParticipationRecordMap() {

        Account acc = new Account(Name='test');
        insert acc;

        Product2 myEvent = new Product2(Name='testEvent',User_Link_Expiration_for_Suppliers__c= 1,User_Link_Expiration_for_Executives__c=2);
        insert myEvent;

        Event_Account_Details__c myEventAccountDetail = new Event_Account_Details__c(Event__c=myEvent.Id, Account__c=acc.Id);
        insert myEventAccountDetail;

        test.startTest();


        Set<Id> eventIds = new Set<Id>();
        Set<Id> accountIDs = new Set<Id>();


        eventIds.add(myEvent.Id);
        accountIDs.add(acc.Id);

        Map<String,Event_Account_Details__c> result = 
        ParticipationRecordHelper.getEventAccountIdAndParticipationRecordMap(
            eventIds, 
            accountIDs
            );
        System.assert(result.size() > 0);
        test.stopTest();
    }
    
    @isTest static void testGetEventParticipationRecords() {
        Account acc = new Account(Name='test');
        insert acc;

        Product2 myEvent = new Product2(Name='testEvent',User_Link_Expiration_for_Suppliers__c= 1,User_Link_Expiration_for_Executives__c=2);
        insert myEvent;

        Event_Account_Details__c myEventAccountDetail = new Event_Account_Details__c(Event__c=myEvent.Id, Account__c=acc.Id);
        insert myEventAccountDetail;

        Set<Id> accountIDs = new Set<Id>();
        accountIDs.add(acc.RecordTypeId);

        Order_Statuses__c orderStatus = new Order_Statuses__c(Event_Contact_picklist_values__c='Event Contact', Attending_Picklist_values__c='Attending');
        insert orderStatus;

        test.startTest();

        List<Event_Account_Details__c> result = ParticipationRecordHelper.getEventParticipationRecords(myEvent.Id, accountIDs);

        System.assertNotEquals(result, null);

        test.stopTest();
    }

    static testmethod void getALLParticipationRecordsByEvent_Test(){
        // Prepare Test Data.
        TestDataGenerator.createInactiveTriggerSettings();

        //Event Setup
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Active Event',true,Date.today().addDays(40),Date.today().addDays(45),Date.today().addMonths(-2), Date.today().addMonths(-1));
        prod.Start_Date__c = Date.today().addDays(40);
        prod.Family ='BuildPoint';
        prod.Event_Navigation_Color__c = '90135d';
        prod.Event_Tint_Color__c = 'EEEEEE';
        update prod;

        Profile standardUser = [SELECT Id FROM Profile where Name ='Standard User' LIMIT 1];
        User execUserCPMG = TestDataGenerator.CreateUser(true,standardUser.Id,'Test','Exec',null,'testExec1234@cpmg.com',null);
        User supplierUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Supplier',null,'testSupplier1234@cpmg.com',null);
        User supportUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Support',null,'testSupport1234@cpmg.com',null);

        //Set Up Executive User
        Id execAccRType =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Id execConRType =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Id suppAccRType =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Id suppConRType =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();

        Account execAcc = TestDataGenerator.createTestAccount(true,'Test Exec Account', execAccRType);
        Account suppAcc = TestDataGenerator.createTestAccount(true,'Test Supp Account', suppAccRType);

        
        Contact execCon = TestDataGenerator.createTestContact(true,'Test Exec','Last Account','testexec@test.com',execAcc, execConRType);
        Contact suppCon = TestDataGenerator.createTestContact(true,'Test Supp','Test Account','testexec1@test.com',suppAcc, suppConRType);

        Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        User execUser = TestDataGenerator.CreateUser(true,commUserProfId.id,'Test Exec','Last Account','603-555-5555','testexec@test.com', execCon.Id);

        Opportunity execOpp = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),execAcc,'Closed Won',prod,'Renewal');
        Opportunity suppOpp = TestDataGenerator.createTestOpp(true,'Supplier Test Opp',Date.today(),suppAcc,'Closed Won',prod,'Renewal');

        Id execPRRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Id suppPRRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Suppliers').getRecordTypeId();
        Event_Account_Details__c execPRecod = TestDataGenerator.createParticipationRecord(true,prod,execAcc,execOpp,execCon,execPRRType);
        Event_Account_Details__c suppPRecod = TestDataGenerator.createParticipationRecord(true,prod,suppAcc,suppOpp,suppCon,suppPRRType);

        Test.startTest();
            Event_Account_Details__c[] prList = ParticipationRecordHelper.getAllParticipationRecordsByEvent(new Set<Id>{prod.id});
        Test.stopTest();
            System.assertEquals(prList.size(),2);
            System.assertEquals(prList[0].Account__r.Name,'Test Exec Account' );
        
    }

    static testMethod void getParticipationRecord_Test_Executive(){
        // Prepare Test Data.
        TestDataGenerator.createInactiveTriggerSettings();

        //Event Setup
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Active Event',true,Date.today().addDays(40),Date.today().addDays(45),Date.today().addMonths(-2), Date.today().addMonths(-1));
        prod.Start_Date__c = Date.today().addDays(40);
        prod.Family ='BuildPoint';
        prod.Event_Navigation_Color__c = '90135d';
        prod.Event_Tint_Color__c = 'EEEEEE';
        update prod;

        Profile standardUser = [SELECT Id FROM Profile where Name ='Standard User' LIMIT 1];
        User execUserCPMG = TestDataGenerator.CreateUser(true,standardUser.Id,'Test','Exec',null,'testExec1234@cpmg.com',null);
        User supplierUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Supplier',null,'testSupplier1234@cpmg.com',null);
        User supportUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Support',null,'testSupport1234@cpmg.com',null);


        //Set Up Executive User
        Id execAccRType =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Id execConRType =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Id suppAccRType =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Id suppConRType =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();

        Account execAcc = TestDataGenerator.createTestAccount(true,'Test Exec Account', execAccRType);
        Account suppAcc = TestDataGenerator.createTestAccount(true,'Test Supp Account', suppAccRType);

        
        Contact execCon = TestDataGenerator.createTestContact(true,'Test Exec','Last Account','testexec@test.com',execAcc, execConRType);
        Contact suppCon = TestDataGenerator.createTestContact(true,'Test Exec','Test Account','testexec1@test.com',suppAcc, suppConRType);

        Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        User execUser = TestDataGenerator.CreateUser(true,commUserProfId.id,'Test Exec','Last Account','603-555-5555','testexec@test.com', execCon.Id);

        Opportunity execOpp = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),execAcc,'Closed Won',prod,'Renewal');
        Opportunity suppOpp = TestDataGenerator.createTestOpp(true,'Supplier Test Opp',Date.today(),suppAcc,'Closed Won',prod,'Renewal');

        Id execPRRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Id suppPRRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Suppliers').getRecordTypeId();
        Event_Account_Details__c execPRecod = TestDataGenerator.createParticipationRecord(true,prod,execAcc,execOpp,execCon,execPRRType);
        Event_Account_Details__c suppPRecod = TestDataGenerator.createParticipationRecord(true,prod,suppAcc,suppOpp,suppCon,suppPRRType);

        Test.startTest();
            Event_Account_Details__c prRecord = ParticipationRecordHelper.getParticipationRecord(prod.Id, execCon);
        Test.stopTest();
            System.assertEquals(execPRecod.Id, prRecord.Id);
    }

    static testMethod void getParticipationRecord_Test_Supplier(){
        // Prepare Test Data.
        TestDataGenerator.createInactiveTriggerSettings();

        //Event Setup
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Active Event',true,Date.today().addDays(40),Date.today().addDays(45),Date.today().addMonths(-2), Date.today().addMonths(-1));
        prod.Start_Date__c = Date.today().addDays(40);
        prod.Family ='BuildPoint';
        prod.Event_Navigation_Color__c = '90135d';
        prod.Event_Tint_Color__c = 'EEEEEE';
        update prod;

        Profile standardUser = [SELECT Id FROM Profile where Name ='Standard User' LIMIT 1];
        User execUserCPMG = TestDataGenerator.CreateUser(true,standardUser.Id,'Test','Exec',null,'testExec1234@cpmg.com',null);
        User supplierUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Supplier',null,'testSupplier1234@cpmg.com',null);
        User supportUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Support',null,'testSupport1234@cpmg.com',null);


        //Set Up Executive User
        Id execAccRType =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Id execConRType =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Id suppAccRType =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Id suppConRType =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();

        Account execAcc = TestDataGenerator.createTestAccount(true,'Test Exec Account', execAccRType);
        Account suppAcc = TestDataGenerator.createTestAccount(true,'Test Supp Account', suppAccRType);

        
        Contact execCon = TestDataGenerator.createTestContact(true,'Test Exec','Last Account','testexec@test.com',execAcc, execConRType);
        Contact suppCon = TestDataGenerator.createTestContact(true,'Test Exec','Test Account','testexec1@test.com',suppAcc, suppConRType);

        Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        User execUser = TestDataGenerator.CreateUser(true,commUserProfId.id,'Test Exec','Last Account','603-555-5555','testexec@test.com', execCon.Id);

        Opportunity execOpp = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),execAcc,'Closed Won',prod,'Renewal');
        Opportunity suppOpp = TestDataGenerator.createTestOpp(true,'Supplier Test Opp',Date.today(),suppAcc,'Closed Won',prod,'Renewal');

        Id execPRRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Id suppPRRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Suppliers').getRecordTypeId();
        Event_Account_Details__c execPRecod = TestDataGenerator.createParticipationRecord(true,prod,execAcc,execOpp,execCon,execPRRType);
        Event_Account_Details__c suppPRecod = TestDataGenerator.createParticipationRecord(true,prod,suppAcc,suppOpp,suppCon,suppPRRType);

        Test.startTest();
            Event_Account_Details__c prRecord = ParticipationRecordHelper.getParticipationRecord(prod.Id, suppAcc.Id);
        Test.stopTest();
            System.assertEquals(suppPRecod.Id, prRecord.Id);
    }

    static testMethod void getAccountUserSpecifiedFields_Test(){
        TestDataGenerator.createInactiveTriggerSettings();

        Test.startTest();
            ParticipationRecordHelper.getAccountUserSpecifiedFields();
        Test.stopTest();
    }

    static testMethod void getContactUserSpecifiedFields_test(){
        TestDataGenerator.createInactiveTriggerSettings();

        Test.startTest();
            ParticipationRecordHelper.getContactUserSpecifiedFields();
        Test.stopTest();
    }

    static testMethod void getAllParticipationRecordsByEventAndRType_Test(){

        TestDataGenerator.createInactiveTriggerSettings();

        //Event Setup
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Active Event',true,Date.today().addDays(40),Date.today().addDays(45),Date.today().addMonths(-2), Date.today().addMonths(-1));
        prod.Start_Date__c = Date.today().addDays(40);
        prod.Family ='BuildPoint';
        prod.Event_Navigation_Color__c = '90135d';
        prod.Event_Tint_Color__c = 'EEEEEE';
        update prod;

        Profile standardUser = [SELECT Id FROM Profile where Name ='Standard User' LIMIT 1];
        User execUserCPMG = TestDataGenerator.CreateUser(true,standardUser.Id,'Test','Exec',null,'testExec1234@cpmg.com',null);
        User supplierUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Supplier',null,'testSupplier1234@cpmg.com',null);
        User supportUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Support',null,'testSupport1234@cpmg.com',null);


        //Set Up Executive User
        Id execAccRType =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Id execConRType =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Id suppAccRType =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Id suppConRType =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();

        Account execAcc = TestDataGenerator.createTestAccount(true,'Test Exec Account', execAccRType);
        Account suppAcc = TestDataGenerator.createTestAccount(true,'Test Supp Account', suppAccRType);

        
        Contact execCon = TestDataGenerator.createTestContact(true,'Test Exec','Last Account','testexec@test.com',execAcc, execConRType);
        Contact suppCon = TestDataGenerator.createTestContact(true,'Test Exec','Test Account','testexec1@test.com',suppAcc, suppConRType);

        Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        User execUser = TestDataGenerator.CreateUser(true,commUserProfId.id,'Test Exec','Last Account','603-555-5555','testexec@test.com', execCon.Id);

        Opportunity execOpp = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),execAcc,'Closed Won',prod,'Renewal');
        Opportunity suppOpp = TestDataGenerator.createTestOpp(true,'Supplier Test Opp',Date.today(),suppAcc,'Closed Won',prod,'Renewal');

        Id execPRRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Id suppPRRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Suppliers').getRecordTypeId();

        Event_Package_Inventory__c inventory = TestDataGenerator.createEventPackage(true, prod, 10, 10, 5, 5, 4);


        Event_Account_Details__c execPRecod = TestDataGenerator.createParticipationRecord(true,prod,execAcc,execOpp,execCon,execPRRType);
        Event_Account_Details__c suppPRecod = TestDataGenerator.createParticipationRecord(true,prod,suppAcc,suppOpp,suppCon,suppPRRType,inventory);

        Test.startTest();
            List<Event_Account_Details__c> allPrsForRType = ParticipationRecordHelper.getAllParticipationRecordsByEventAndRType(new Set<Id>{prod.Id}, 'Supplier');
        Test.stopTest();
    }

    static testMethod void getAllPRRecordsByEventForMeetingRequests_Test(){

        TestDataGenerator.createInactiveTriggerSettings();

        //Event Setup
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Active Event',true,Date.today().addDays(40),Date.today().addDays(45),Date.today().addMonths(-2), Date.today().addMonths(-1));
        prod.Start_Date__c = Date.today().addDays(40);
        prod.Family ='BuildPoint';
        prod.Event_Navigation_Color__c = '90135d';
        prod.Event_Tint_Color__c = 'EEEEEE';
        update prod;

        Profile standardUser = [SELECT Id FROM Profile where Name ='Standard User' LIMIT 1];
        User execUserCPMG = TestDataGenerator.CreateUser(true,standardUser.Id,'Test','Exec',null,'testExec1234@cpmg.com',null);
        User supplierUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Supplier',null,'testSupplier1234@cpmg.com',null);
        User supportUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Support',null,'testSupport1234@cpmg.com',null);


        //Set Up Executive User
        Id execAccRType =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Id execConRType =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Id suppAccRType =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Id suppConRType =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();

        Account execAcc = TestDataGenerator.createTestAccount(true,'Test Exec Account', execAccRType);
        Account suppAcc = TestDataGenerator.createTestAccount(true,'Test Supp Account', suppAccRType);

        
        Contact execCon = TestDataGenerator.createTestContact(true,'Test Exec','Last Account','testexec@test.com',execAcc, execConRType);
        Contact suppCon = TestDataGenerator.createTestContact(true,'Test Exec','Test Account','testexec1@test.com',suppAcc, suppConRType);

        Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        User execUser = TestDataGenerator.CreateUser(true,commUserProfId.id,'Test Exec','Last Account','603-555-5555','testexec@test.com', execCon.Id);

        Opportunity execOpp = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),execAcc,'Closed Won',prod,'Renewal');
        Opportunity suppOpp = TestDataGenerator.createTestOpp(true,'Supplier Test Opp',Date.today(),suppAcc,'Closed Won',prod,'Renewal');

        Id execPRRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Id suppPRRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Suppliers').getRecordTypeId();

        Event_Package_Inventory__c inventory = TestDataGenerator.createEventPackage(true, prod, 10, 10, 5, 5, 4);


        Event_Account_Details__c execPRecod = TestDataGenerator.createParticipationRecord(true,prod,execAcc,execOpp,execCon,execPRRType);
        Event_Account_Details__c suppPRecod = TestDataGenerator.createParticipationRecord(true,prod,suppAcc,suppOpp,suppCon,suppPRRType,inventory);

        Test.startTest();
            Map<Id,Event_Account_Details__c> allPrsForRType = ParticipationRecordHelper.getAllPRRecordsByEventForMeetingRequests(new Set<Id>{prod.Id}, 'Supplier');
        Test.stopTest();
    }
    
}