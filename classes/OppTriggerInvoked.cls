public with sharing class OppTriggerInvoked {
    public Opportunity opp;
    public boolean showError{get;set;}
    public string errorMessage{get;set;}
    public OppTriggerInvoked( ApexPages.StandardController stdController ) {
        opp = ( Opportunity )stdController.getRecord();        
    }
    public void updateoppty(){         
        list<opportunity> opplist  = [select id,Primary_Contact__c,Account_Record_Type__c ,(select id,contactid from opportunitycontactroles where isprimary = true limit 1) from opportunity 
        where id =:opp.id];
        try{
            
            if(opplist.size()>0){
                if(opplist[0].opportunitycontactroles.size() >0 && opplist[0].Primary_Contact__c != opplist[0].opportunitycontactroles[0].contactid){
                    list<opportunitycontactrole> opprole1 = [select id,contactid,opportunityId ,isprimary,role  from opportunitycontactrole where contactId =:opplist[0].Primary_Contact__c and opportunityId =:opplist[0].id];
                    opplist[0].Primary_Contact__c = opplist[0].opportunitycontactroles[0].contactid;
                    if(opplist[0].Account_Record_Type__c =='Supplier')
                        opplist[0].opportunitycontactroles[0].role = 'Sales Rep';
                    if(opplist[0].Account_Record_Type__c =='Executive')
                        opplist[0].opportunitycontactroles[0].role = 'Event Contact';
                    update opplist[0].opportunitycontactroles[0];
                    update opplist[0];
                    if(opprole1.size()>0){
                        opprole1[0].role= 'Other';
                        update opprole1[0];
                        
                    }
                }
            }
             
        }catch(exception e){
            showError = true; 
            errorMessage = e.getMessage().substringAfter(',').substringBeforeLast(':');
            list<opportunitycontactrole > updateOpprole = new list<opportunitycontactrole >();
            list<opportunitycontactrole > opprole1 = [select id,contactid,opportunityId ,isprimary,role  from opportunitycontactrole where contactId =:opplist[0].Primary_Contact__c and opportunityId =:opplist[0].id];
            if(opprole1 .size()>0){
                opprole1[0].role= 'Other';
                opprole1[0].isprimary = false;
                updateOpprole.add(opprole1[0]);
            }
            opplist  = [select id,Primary_Contact__c,(select id,contactid from opportunitycontactroles where isprimary = true limit 1) from opportunity 
                        where id =:opp.id];
            list<opportunitycontactrole > opprole = [select id,contactid,opportunityId ,isprimary,role  from opportunitycontactrole where contactId =:opplist[0].Primary_Contact__c and opportunityId =:opplist[0].id];
            if(opprole.size()>0){
                opprole[0].isprimary = true;
               updateOpprole.add(opprole[0]);
            }
            if(updateOpprole.size()>0)
                update updateOpprole;
        }
    } 
}