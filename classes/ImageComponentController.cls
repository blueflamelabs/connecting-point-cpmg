public without sharing class ImageComponentController {
 public Contact currentContact {get;set;}
 public Product2 currentEvent {get;set;}
 public Order currentConfirmation {get;set;}
 public String userID {get;set;}
 public String imageSection {get;set;}
 
 private static String publicFolderName = 'Public Photo';
 private List<Image__c> currentImages {get;set;}
 private Set<Id> docIds {get;set;}
 private Map<Id,Document> docMap {get;set;}
 private static Folder publicFolder {
    get {
        if (publicFolder == NULL) {
           // publicFolder = new UserPermissions().getPublicPhotoFolder(publicFolderName);
           publicFolder = getPublicPhotoFolder(publicFolderName);
        }
        return publicFolder;
    } set;
  }    
  //For holding a new Photo
  public Blob contactPhotoFile {get; set;}
  public String contactPhotoFileName {get; set;}
  public String contactPhotoFileContentType {get; set;}
  public List<List<AttachmentDto>> currentUploadPhotos {get{
		  if(currentUploadPhotos == null){
		  	try{
		  	currentUploadPhotos = getCurrentContactPhotos();
		  	}catch(Exception ex){
		  		System.debug('Error: '+ ex.getMessage()+'. Stack Trace: '+ ex.getStackTraceString());
		  	}
		  }
		  return currentUploadPhotos;	
  	} set;}
  public MultiComponentForm parentForm {
    get;
    set {
      try {
       // System.debug('parentForm being set to ' + value);
        if(value != null) {
          parentForm = value;
          value.registerImageCompController(this);
		  System.debug('IMAGE COMPONENT NUMBERS: '+parentForm.getImageControllersSize());
        }
      } catch (Exception ex) {
        System.debug('Exception setting parentForm');
        System.debug(ex.getMessage());
        System.debug(ex.getStackTraceString());
      }
      //System.debug('parentForm is ' + parentForm);
    }
  }

	public ImageComponentController() {
		//System.debug('IMAGE CONTROLLER!!!!!');
	}
	public List<List<AttachmentDto>> getCurrentUploadPhotos(){
	//public void getCurrentUploadPhotos(){
	//	System.debug('SHOULD BE EXECUTED ON COMPONENT LOAD');

		currentUploadPhotos = getCurrentContactPhotos();

		return currentUploadPhotos;
	}
		 //GET THE Images associated with this contact.
	private List<List<AttachmentDto>> getCurrentContactPhotos() {
    //We need to access Documents
	//	System.debug('TRYING TO FIND THE CURRENT CONTACT PHOTOS: ');
		getImages();
		getExistingDocumentReferences();
		getDocumentMap();

        List<AttachmentDto> photosSublist = new List<AttachmentDto>();
        For(Image__c img: currentImages){
        //	System.debug('The Image: '+ img);
        	if(docMap != null && docMap.containsKey(img.File_Id__c)){
        		Document d = docMap.get(img.File_Id__c);
        		Boolean selected = (img.Selected__c != null && img.Selected__c)?true:false;
        		photosSublist.add(new AttachmentDto(d,selected,img));
        	}
        }
      //  System.debug('PHOTO SUBLIST: '+ photosSublist);
		List<List<AttachmentDto>> photos = new List<List<AttachmentDto>>();
        Integer i = 0;

        photos = chunkArrayOfObjects(photosSublist, 2);
      //  SYstem.debug('THe Photos: '+photos);
        return photos;
	}
	//Get the Image records for the contact
	private void getImages(){
		if(imageSection =='Contact'){
			currentImages = ImageHelper.getImagesByContact(currentContact.Id);
		} else {
			currentImages = ImageHelper.getImagesByAccount(currentContact.AccountId);
		}
	}

	/*private void getImagesForAccount(){
		currentImages = ImageHelper.getImagesByAccount(currentContact.AccountId);
	} */

	private void getExistingDocumentReferences(){
		docIds = new Set<Id>();

		for(Image__c img : currentImages){

			if(!docIds.contains(img.File_Id__c)) docIds.add(img.File_Id__c);
		}
	}

	private void getDocumentMap(){
		if(docIds.size() > 0){
			docMap = DocumentHelper.getDocumentMap(docIds);
		}
	}

	private static Folder  getPublicPhotoFolder(String p_publicFolderName) {
      Folder publicFolderResult;
      List<Folder> publicFolders = [SELECT Id, Name FROM Folder Where Name =:p_publicFolderName];
            if(publicFolders.size() != 0){
                publicFolderResult = publicFolders[0];
            }

            return publicFolderResult;
    }

    //THis method commits the Save to the Database
    public PageReference selectImage(){
    	upsertImages();
	    PageReference pageRef = new PageReference('/EventRegistration2');

	    pageRef.getParameters().put('eId', this.currentEvent.Id);
     	pageRef.setRedirect(true);
     	return pageRef;

    }

  public void upsertImages(){
  		    	List<Image__c> imagesUpsert = new List<Image__c>();
    //	System.debug('UPLOADED IMAGE: '+contactPhotoFile );
    //	System.debug('UPLOADED IMAGE NAME: '+ contactPhotoFileName);
    //	System.debug('UPLOADED IMAGE TYPE:'+ contactPhotoFileContentType);

    	//THIS WILL Create the uploaded document. All uploaded documents are stored in the 'Public Photo folder'
    	try{
	    	if(contactPhotoFile != null){
	    		Document d = DocumentHelper.createDoc(contactPhotoFile, contactPhotoFileContentType, publicFolder.Id, contactPhotoFileName);
	    		insert d;
	    		System.debug('The Document: '+ d);
	    		//Now that we have the Document Inserted, we need to create the Image__c record. We are assuming an uploaded file should be the "Selected File"
	    		Image__c newImage = ImageHelper.createImage(currentEvent.Name+' '+d.Name,
	    											 currentContact.accountId,
	    											  currentContact.Id,
	    											  currentEvent,
	    											  currentConfirmation.Event_Account_Detail__c,
	    											  currentConfirmation.Id,
	    											  d,
	    											  (imageSection=='Contact')?'Headshot':'Logo',
	    											  true);
	    		imagesUpsert.add(newImage);
	    		//insert newImage;
	    		//System.debug('The New Image File: '+ newImage);
	    	}
	    	//We have split the Images into groups of three
	    	System.debug('Current Uploads: '+ currentUploadPhotos);
	    	for(List<AttachmentDto> attachments: currentUploadPhotos){

	    		for(AttachmentDto img: attachments){
	    			Image__c currentImage = img.potentialImage;
	    			//THis determines which current Image has been selected.
	    			if(img.selected && contactPhotoFile == null){
	    				
	    				if(img.potentialImage.Event__c != currentEvent.Id){
	    					Image__c newImage = ImageHelper.createImage(currentEvent.Name+' '+img.doc.Name, 
	    																currentContact.AccountId,
	    																currentContact.Id,
	    																currentEvent,
	    															    CurrentConfirmation.Event_Account_Detail__c,
	    															    currentConfirmation.Id,
	    															    img.doc,
	    															    (imageSection=='Contact')?'Headshot':'Logo',
	    															    true);
	    					imagesUpsert.add(newImage);
	    				}else{
	    					currentImage.Selected__c = true;
	    					imagesUpsert.add(currentImage);
	    				}
	       			} else{
	       				currentImage.Selected__c = false;
	       				imagesUpsert.add(currentImage);
	       			}

	       			//imagesUpsert.add(currentImage);
	    		}

	    	}
	    	//COMMIT THEM TO THE DATABASE
	    	if(!imagesUpsert.isEmpty()){

	    		upsert imagesUpsert;
	    		System.debug('Images Upsert: '+ imagesUpsert);
	    	}
	    	}catch(Exception ex){
	    		System.debug('Error: '+ ex.getMessage()+'. Stack Trace: '+ex.getStackTraceString());
	    	}

  }

	//Image Attachment Wrapper Class
  public class AttachmentDto {
    	public Document doc { get; set; }
        public Boolean selected { get; set; }
        public Image__c potentialImage {get;set;}

        public AttachmentDto(Document att, Boolean p_selected, Image__c img ) {
            selected = p_selected;
            doc = att;
            potentialImage = img;
        }
  } 
  
  //Used to separate the identified attachments into groups of 3.
  public static List<List<AttachmentDto>> chunkArrayOfObjects(List<AttachmentDto> p_listToSplit, Integer chunkSize) {
    List<List<AttachmentDto>> resultList = new List<List<AttachmentDto>>();

    Integer numberOfChunks = p_listToSplit.size() / chunkSize;
    //System.debug('THe passed in AttachmentDTO: '+ p_listToSplit);
    for(Integer j = 0; j < numberOfChunks; j++ ){
         List<AttachmentDto> someList = new List<AttachmentDto>();
        for(Integer i = j * chunkSize; i < (j+1) * chunkSize; i++){
            someList.add(p_listToSplit[i]);
        }
        resultList.add(someList);
    }

    if(numberOfChunks * chunkSize < p_listToSplit.size()){
        List<AttachmentDto> aList = new List<AttachmentDto>();
        for(Integer k = numberOfChunks * chunkSize ; k < p_listToSplit.size(); k++){
            aList.add(p_listToSplit[k]);
        }
        resultList.add(aList);
    }
 //   System.debug('--------------------->The Result LIST: '+ resultList);
    return resultList;
  }

  

}