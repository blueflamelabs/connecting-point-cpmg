/*
*Created By     : 
*Created Date   : 22/07/2019 
*Description    : 
*Class Name     : CampaignController
Modification History :
============================================================================================
V.No    Date            By                  Description
============================================================================================
*/
@IsTest
public class CampaignController_Test{
@IsTest
       static void CampaignController_TestMethod(){
            Id execAccountRtype = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
            Id execConRtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
            Id CampaignConRtype = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
            Account a = TestDataGenerator.createTestAccount(true,'Test Account',execAccountRtype);
        
            Contact c = TestDataGenerator.createTestContact(true,'Test','Exec','t@t.com',a,(String) execConRtype);
            
            Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event',true,Date.today().addDays(2), Date.today().addDays(5), Date.today().addMonths(-3), Date.today().addMonths(-2));
            
             
            User userRecord = new User(Alias = 'new123',  
                                              EmailEncodingKey='UTF-8',
                                              LastName='Testing',
                                              LanguageLocaleKey='en_US', 
                                              LocaleSidKey='en_US', 
                                              ProfileId = UserInfo.getProfileId(),
                                              Email = 'test1.liveston@asdf.com',
                                              Username = 'test.liveston@asdf.com', 
                                              TimeZoneSidKey='Asia/Dubai'
                                              );
            insert userRecord ;
            Campaign camp = new Campaign();
            camp.Name = 'test';
            camp.isReopen__c = false;
            camp.IsActive = true;
            camp.Event__c = prod.id;
            camp.OwnerId = userRecord.id;
            camp.Status = 'Planned';
            camp.RecordTypeId = CampaignConRtype;
            insert camp;
            
            Lead lead = new Lead();
            lead.FirstName ='test';
            lead.LastName = 'test';
            lead.Company ='syn';
            insert lead;
            
            
            CampaignMember cm = new CampaignMember();
            //cm.Name = 'test';
            cm.CampaignId = camp.id;
            cm.ContactId = c.id;
            cm.LeadId = lead.id;
            insert cm;
              
              //List<String> strList = new List<String>();
              //strList.add(camp.id);
             //ApexPages.currentPage().getParameters().put('Id',c.Id);
            //ApexPages.currentPage().getParameters().put('ReTypeId','Supplier');
            CampaignController cc = new CampaignController();
            //cc.SelectedCampaign = 'My Campaigns'
            //cc.SelectedCampaignEvent = 'Upcoming Event Campaigns';
             //CampaignController.insertRecord(strList,c.id);
             CampaignController.fetchCampaignWrapper(c.id,'My Campaigns','Upcoming Event Campaigns','Name',true,'tab1');
             CampaignController.fetchCampaignWrapper(c.id,'My Campaigns','Upcoming Event Campaigns','Name',true,'tab2');
             CampaignController.findByName('abc',c.id,camp.id,'abc','tab1');
             CampaignController.fetchRemovefromCampaigns(c.id);
             CampaignController.getUIThemeDescription();
             CampaignController.getBaseUrl();
            
            
            
            
     }
      @IsTest
       static void CampaignController_TestMethod1(){
            Id execAccountRtype = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
            Id execConRtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
            Account a = TestDataGenerator.createTestAccount(true,'Test Account',execAccountRtype);
        
            Contact c = TestDataGenerator.createTestContact(true,'Test','Exec','t@t.com',a,(String) execConRtype);
            
            Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event',true,Date.today().addDays(2), Date.today().addDays(5), Date.today().addMonths(-3), Date.today().addMonths(-2));
            
             
            User userRecord = new User(Alias = 'new123',  
                                              EmailEncodingKey='UTF-8',
                                              LastName='Testing',
                                              LanguageLocaleKey='en_US', 
                                              LocaleSidKey='en_US', 
                                              ProfileId = UserInfo.getProfileId(),
                                              Email = 'test1.liveston@asdf.com',
                                              Username = 'test.liveston@asdf.com', 
                                              TimeZoneSidKey='Asia/Dubai'
                                              );
            insert userRecord ;
            Campaign camp = new Campaign();
            camp.Name = 'test';
            camp.isReopen__c = false;
            camp.Event__c = prod.id;
            insert camp;
            
            Lead lead = new Lead();
            lead.FirstName ='test';
            lead.LastName = 'test';
            lead.Company ='syn';
            insert lead;
            
            
            CampaignMember cm = new CampaignMember();
            //cm.Name = 'test';
            cm.CampaignId = camp.id;
            cm.ContactId = c.id;
            cm.LeadId = lead.id;
            //insert cm;
              
              List<String> strList = new List<String>();
              strList.add(camp.id);
             //ApexPages.currentPage().getParameters().put('Id',c.Id);
            //ApexPages.currentPage().getParameters().put('ReTypeId','Supplier');
            CampaignController cc = new CampaignController();
             CampaignController.insertRecord(strList,c.id);
             CampaignController.removeRecord(strList,c.id);
             
            
            
            
     }  
       
}