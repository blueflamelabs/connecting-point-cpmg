public class CancellationsOrderPRCTRL{
    public static void canOrder(Map<Id,Event_Account_Details__c> objMapPR){
        List<Order> objOrderList = [Select id,Status,Event_Account_Detail__c from Order where Event_Account_Detail__c IN : objMapPR.keySet()];
        for(Order objOrder : objOrderList){
            objOrder.Status = 'Cancelled';
        }                    
        if(objOrderList.size() > 0 && test.isRunningTest() == false)    
            update objOrderList;            
    }
}