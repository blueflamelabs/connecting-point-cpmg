public class SingleMailMessage {

	public static Messaging.SingleEmailMessage createSingleMailMessage(String[] toEmails, String body, String Subject){
		Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.setToAddresses(toEmails);
        //message.setTargetObjectId(currentContact.Id); //Sets the Who Id to the logged in User
       
        message.setSubject(Subject);
        message.setHtmlBody(body);
       return message;  
	}
}