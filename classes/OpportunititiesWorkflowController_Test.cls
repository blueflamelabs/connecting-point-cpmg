@isTest
private class OpportunititiesWorkflowController_Test {
    
   @isTest static void test_OppWorkflowController_Redirect_Exec_No_EAD() {
        // Implement test code
        PageReference pageRef = Page.OpportunitiesWorkflow;
        
        Product2 prod = new Product2(Name = 'Test Event', User_Link_Expiration_for_Executives__c = 2, User_Link_Expiration_for_Suppliers__c = 2, Start_Date__c =System.Today(),  ProductCode =' TE 17');  //
        insert prod;
        Id AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Account a = new Account(Name = 'Test Account 1', RecordTypeId = AccountRecordTypeId);
        insert a;
        Id ConRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Contact c = new Contact(FirstName ='Test', LastName ='Executive', AccountId = a.Id,email ='test@test.com',RecordTypeId = ConRecordTypeId);
        insert c;

        Campaign camp = new Campaign(Name ='Test Event Campaign', StartDate = System.Today(), Event__c = prod.Id );
        insert camp;

        Opportunity opp = new Opportunity(Name = 'Test Opp',AccountId = a.Id,Product__c = prod.Id, stageName = 'Closed Won', CloseDate =System.Today(), CampaignId = camp.Id);
        opp.Returning_Customer__c ='Onsite Renewal';
        insert opp;
        
        
        OpportunityContactRole ocr = new OpportunityContactRole(OpportunityId = opp.Id, ContactId =c.Id, Role ='Primary', isPrimary =true);
        insert ocr;
        
        Campaign_Opp_Settings__c campSettings = new Campaign_Opp_Settings__c(Days_To_Event__c = 21, StageName__c ='Prospect');
        insert campSettings;

        Field_References__c fRef = new Field_References__c (EAD_Account__c = '001', EAD_Event__c = '01t', EAD_Opportunity__c='006', Order_Product__c ='01t',Order_EAD__c='a01');
        fRef.Hotel_Confirmation__c='Pending';
        fRef.Primary_Event_Contact__c='1'; 
        insert fRef;

        test.StartTest();
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('id', opp.Id);
            //ApexPages.StandardController sc = new ApexPages.StandardController(opp);
            OpportunitiesWorkflowController owc = new OpportunitiesWorkflowController();
            
            owc.redirect();
        test.StopTest();
    }
  
    @isTest static void test_OppWorkflowController_Redirect_Exec_No_EADPR() {
        // Implement test code
        PageReference pageRef = Page.OpportunitiesWorkflow;
        
        Product2 prod = new Product2(Name = 'Test Event', User_Link_Expiration_for_Executives__c = 2, User_Link_Expiration_for_Suppliers__c = 2, Start_Date__c =System.Today(),  ProductCode =' TE 17');  
        insert prod;
        Id AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Account a = new Account(Name = 'Test Account 1', RecordTypeId = AccountRecordTypeId);
        insert a;
        Contact c = new Contact(FirstName ='Test', LastName ='Executive', AccountId = a.Id,RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId(), email ='test@test.com');
        insert c;

        Campaign camp = new Campaign(Name ='Test Event Campaign', StartDate = System.Today(), Event__c = prod.Id );
        insert camp;

        Opportunity opp = new Opportunity(Name = 'Test Opp',AccountId = a.Id,Product__c = prod.Id, stageName = 'Closed Won', CloseDate =System.Today(), CampaignId = camp.Id);
        opp.Returning_Customer__c ='Onsite Renewal';
        insert opp;
        
        
        OpportunityContactRole ocr = new OpportunityContactRole(OpportunityId = opp.Id, ContactId =c.Id, Role ='Primary', isPrimary =true);
        insert ocr;
        
        Campaign_Opp_Settings__c campSettings = new Campaign_Opp_Settings__c(Days_To_Event__c = 21, StageName__c ='Prospect');
        insert campSettings;

        Field_References__c fRef = new Field_References__c (EAD_Account__c = '001', EAD_Event__c = '01t', EAD_Opportunity__c='006', Order_Product__c ='01t',Order_EAD__c='a01');
        fRef.Hotel_Confirmation__c='Pending';
        fRef.Primary_Event_Contact__c='1'; 
        fRef.Registration_Reason__c = 'Renewal';
        insert fRef;

        test.StartTest();
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('id', opp.Id);
            //ApexPages.StandardController sc = new ApexPages.StandardController(opp);
            OpportunitiesWorkflowController owc = new OpportunitiesWorkflowController();
            
            owc.redirect();
        test.StopTest();
    }
 }