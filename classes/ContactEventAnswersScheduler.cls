global class ContactEventAnswersScheduler implements Schedulable {
    global void execute(SchedulableContext sc) {
        ContactEventAnswers objContactEvent = new ContactEventAnswers();
        objContactEvent.CreateExecutiveEventAnswers();
    }
}