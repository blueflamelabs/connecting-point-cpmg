public without sharing class SpeakerSessionHelper {
    /*public SpeakerSessionHelper() {
        
    } */
    //THIS IS USED IN THE EVENT DIRECTORY. IT WILL BE THE 
    public static List<Speaker_Session__c> getSpeakersForEvent(Set<Id> eventIds){
        return [SELECT Date_and_Time_Display__c, Date_and_Time_Order__c, Event__c, Name, Overview_Text__c, Overview_URL__c,
                       Presenter__c, Title__c
                FROM Speaker_Session__c
                WHERE Event__c in :eventIds ORDER BY Date_and_Time_Order__c ASC];
    }

    public static Map<String,Object> getJSONSpeakerMap(Speaker_Session__c speaker){
        Map<String,Object> jsonSpeaker = new Map<String,Object>();
        jsonSpeaker.put('overview',speaker.Overview_Text__c);
        jsonSpeaker.put('overviewURL',speaker.Overview_URL__c);
        jsonSpeaker.put('title',speaker.Title__c);
        jsonSpeaker.put('name',speaker.Name);
        //jsonSpeaker.put('presenterTitle',speaker.Title__c);
        jsonSpeaker.put('presenter',speaker.Presenter__c);
        jsonSpeaker.put('datedisplay',speaker.Date_and_Time_Display__c);
        //jsonSpeaker.put('startdate','');
        return jsonSpeaker;
    }
}