//THe purpose of this class is to assist with generating test data for the Apex Unit Tests.
@isTest
public with sharing class TestDataGenerator {
    public static User CreateUser(Boolean hasInsert, Id ProfileId, String firstName, String LastName, String Phone, String email, Id contactId) {
        //System.debug('Enter CreateUser');
        List<string> split = Email.split('@', 0);

        User u = new User();
        
        //System.debug('Mid CreateUser');
        u.ProfileId = profileId;
        u.Alias = split[0].length() > 5 ? split[0].substring(0, 5) : split[0];
        u.FirstName = FirstName;
        u.Phone = Phone;
        u.Email = Email;
        u.EmailEncodingKey = 'UTF-8';
        u.LastName = LastName;
        u.LanguageLocaleKey = 'en_US';
        u.LocalesIdKey = 'en_US';
        u.ContactId = ContactId;
        u.TimezonesIdKey = 'America/Los_Angeles';
        //u.Username = UtilClass.GenerateRandomString(10) + '@' + split[1]; //We used a random number generator
        u.Username = Email;
        u.CommunityNickname = split[0] + split[1];
        if (u.CommunityNickname.length() > 37) {
            u.CommunityNickname = u.CommunityNickname.substring(0, 36);
        }
        u.CommunityNickname = u.CommunityNickname + 'amc';
        
        //This controls whether the email gets sent, set to true to trigger the SFDC default email
        Database.DMLOptions dmo = new Database.DMLOptions();
        dmo.EmailHeader.triggerUserEmail = false;
        u.setOptions(dmo);
        //System.debug('Exit CreateUser');
        if(hasInsert) insert u;
        return u;
    }

    public static Account createTestAccount(Boolean hasInsert, String name){
        Id rtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        return TestDataGenerator.createTestAccount(hasInsert,name,rType);
    }
    public static Account createTestAccount(Boolean hasInsert, String name, Id rTypeId){
        Account a = new Account(
            Name = name
            ,RecordTypeId = (rTypeId != null)? rTypeId:null
            );
        if(hasInsert) insert a;
        return a;
    }
    
    public static Contact createTestContact(Boolean hasInsert, String fName, String lName, String email,Account a){
        Id rtype =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        return TestDataGenerator.createTestContact(hasInsert,fName,lName,email,a,rtype);
    }
    public static Contact createTestContact(Boolean hasInsert, String fName, String lName, String email,Account a, Id rTypeId){
        Contact c = new Contact(
                firstName = fName
                ,LastName = lName
                ,email=email
                ,AccountId = (a != null)?a.Id:null
                ,RecordTypeId = (rTypeId != null)? rTypeId:null
            );
        if(hasInsert) insert c;
        return c;
    }

    public static Product2 createTestProduct(Boolean hasInsert, String name, Boolean isActive){
                return TestDataGenerator.createTestProduct(hasInsert,name, isActive, Date.valueOf('2018-01-01'),Date.valueOf('2018-01-30'));
    }

    public static Product2 createTestProduct(Boolean hasInsert, String name,  Boolean isActive, Date startDate, Date endDate){
        return TestDataGenerator.createTestProduct(hasInsert,name,isActive,startDate,endDate,null,null);
    }
    public static Product2 createTestProduct(Boolean hasInsert, String name,  Boolean isActive, Date startDate, Date endDate, Date reg1Start, Date reg1End){
        return TestDataGenerator.createTestProduct(hasInsert, name, isActive,startDate,endDate,reg1Start,reg1End,null,null,null, null,null,null,null,null,null);
    }
    public static Product2 createTestProduct(Boolean hasInsert, String name,  Boolean isActive, Date startDate, Date endDate, Date reg1Start, Date reg1End
            ,String venue, String venueLocation, String location, String linkedIn, String twitter, String year, String headDateRange, String headLogo, String family){
        Product2 prod = new Product2(
            Name='testProduct'
            ,IsActive = isActive
            ,Start_Date__c = startDate
            ,End_Date__c = endDate
            //,Registration_Part_1_Start__c = reg1Start
            ,Registration_Part_1_Open_For_Executive__c = reg1Start
            ,Registration_Part_1_Open_for_Supplier__c = reg1Start
            ,Registration_Part_1_Close_For_Executive__c = reg1End
            ,Registration_Part_1_Close_For_Supplier__c = reg1End
            ,User_Link_Expiration_for_Executives__c= 2
            ,User_Link_Expiration_for_Suppliers__c = 2
            ,Venue__c=venue
            ,Venue_and_Location__c =venueLocation
            ,Location__c =location
            ,LinkedIn__c =linkedIn
            ,Twitter__c =twitter
            ,Year__c =year
            ,Header_Date_Range__c = headDateRange
            ,EventHeaderLogo__c= headLogo
            ,Family=family
            );
            if(hasInsert) insert prod;
            return prod;
    }

    public static Event_Sponsorships__c createEventSponsorship(Boolean hasInsert, String name, Product2 prod, Decimal price, Boolean purchased, Boolean portalDisplay){
        Event_Sponsorships__c eSponsor = new Event_Sponsorships__c(
            Name = name
            ,Event__c = prod.Id
            ,Price__c = price
            ,Purchased__c = purchased
            ,Display_on_Portal__c = portalDisplay
        );
        if(hasInsert) insert eSponsor;
        return eSponsor;
    }
    
    public static Opportunity createTestOpp(Boolean hasInsert, String name, Date closeDate, Account a, String stage, Product2 prod,String ReturningCustomer){
        Opportunity o = new Opportunity(
                name = name
                ,CloseDate = closeDate
                ,AccountId = (a != null)?a.Id:null
                ,StageName = stage
                ,Product__c = (prod != null)?prod.Id:null
                ,Returning_Customer__c = ReturningCustomer
            );
        if(hasInsert) insert o;
        return o;
    }

    public static Opportunity createTestOpp(Boolean hasInsert, String name, Date closeDate, Account a, String stage, Product2 prod){
        Opportunity o = new Opportunity(
                name = name
                ,CloseDate = closeDate
                ,AccountId = (a != null)?a.Id:null
                ,StageName = stage
                ,Product__c = (prod != null)?prod.Id:null
                ,Returning_Customer__c = 'ReturningCustomer'
            );
        if(hasInsert) insert o;
        return o;
    }
    public static Event_Account_Details__c createParticipationRecord(Boolean hasInsert, Product2 event, Account a, Opportunity o, Contact primaryCon, Id rTypeId){
        
        return TestDataGenerator.createParticipationRecord(hasInsert,event,a,o,primaryCon,rTypeId,null);
    }
    public static Event_Account_Details__c createParticipationRecord(Boolean hasInsert, Product2 event, Account a, Opportunity o, Contact primaryCon, Id rTypeId, Event_Package_Inventory__c option){
        Event_Account_Details__c partRecord = new Event_Account_Details__c(
            Account__c = (a != null)?a.id:null
            ,Opportunity__c = (o != null)?o.Id:null
            ,Event__c =(event !=null)?event.Id:null
            ,Primary_Contact__c = (primaryCon != null)?primaryCon.Id:null
            ,RecordTypeId = (rTypeId !=null)?rTypeId:null
            ,Package_Selection__c = (option != null)?option.Id :null
            
            );
        if(hasInsert) insert partRecord;
        return partRecord;
    }

    public static Event_Package_Inventory__c createEventPackage(Boolean hasInsert, Product2 event, Integer numBadges, Integer numBoardrooms, Integer numOneOnOnes, Integer numSelectedOneOnOnes, Integer numSelectedBoardrooms){
        Event_Package_Inventory__c inventory = new Event_Package_Inventory__c(
            Event__c = event.id
            ,Number_of_Badges__c = numBadges
            ,Number_of_Board_rooms__c = numBoardrooms
            , Number_of_One_on_Ones__c = numOneOnOnes
            ,Number_Of_One_On_One_Selections_Needed__c = numSelectedOneOnOnes
            ,Number_of_Boardroom_Selections_Needed__c =numSelectedBoardrooms
            );
        if(hasInsert) insert inventory;
        return inventory;
    }


    public static Purchased_Sponsorship__c createPurchasedSponsorship(Boolean hasInsert, Product2 prod, Event_Account_Details__c pRecord, Decimal uPrice, Event_Sponsorships__c eSponsor){
        Purchased_Sponsorship__c pSponsor = new Purchased_Sponsorship__c(
            Event__c = prod.Id
            ,Event_Account_Detail__c = pRecord.Id
            ,Unit_PRice__c = uPrice
            ,Sponsorship__c =eSponsor.id
        );
        if(hasInsert) insert pSponsor;
        return pSponsor;
    } 

    public static Order createConfirmation(Boolean hasInsert, Account a, Contact billCon, String status, Opportunity o, Date effectiveDate, Event_Account_Details__c partRecord){
      
        return TestDataGenerator.createConfirmation(hasInsert,a,billCon,status,o,effectiveDate,partRecord,null,false);
    }
    public static Order createConfirmation(Boolean hasInsert, Account a, Contact billCon, String status, Opportunity o, Date effectiveDate, Event_Account_Details__c partRecord, Product2 prod,boolean isPrimary){
        Order ord = new Order(
            AccountId = (a != null)?a.Id:null
            ,BillToContactId = (billCon != null)?billCon.Id:null
            ,Status = status
            ,OpportunityId =(o != null)?o.Id:null
            ,effectiveDate = effectiveDate
            ,Event_Account_Detail__c = (partRecord != null)?partRecord.Id:null
            ,Order_Event__c = (prod != null)?prod.Id:null
            ,Primary_Event_Contact__c = isPrimary
        );
        if(hasInsert) insert ord;
        return ord;
    }
public static Order createConfirmation(Boolean hasInsert, Account a, Contact billCon, String status, Opportunity o, Date effectiveDate, Event_Account_Details__c partRecord, Product2 prod){
        opportunity opp = [select id,Account_Record_Type__c,Returning_Customer__c from opportunity where id =: o.id];
        boolean status_New = true;
        if((status=='Cancelled' || status=='Complete') && opp.Account_Record_Type__c == 'Supplier'){
            status_New = false;
        }
        Order ord = new Order(
            AccountId = (a != null)?a.Id:null
            ,BillToContactId = (billCon != null)?billCon.Id:null
            ,Status = status
            ,OpportunityId =(o != null)?o.Id:null
            ,effectiveDate = effectiveDate
            ,Event_Account_Detail__c = (partRecord != null)?partRecord.Id:null
            ,Order_Event__c = (prod != null)?prod.Id:null,            
            Primary_Event_Contact__c = status_New           
            
        );
        
        if(hasInsert) insert ord;
        return ord;
    }
    public static Question__c createQuestion(Boolean hasInsert, String name, Integer maxChar, Boolean picklist, String qType, Id rTypeId, Boolean multiSelect, Boolean includesGolf){
            Question__c question = new Question__c(
                Name = name
                ,Max_Char_Count__c = (maxChar != null)? maxChar: null
                ,Picklist__c = picklist
                ,Question_Type__c = (qType != null)?qType:null
                ,RecordTypeId =(rTypeId != null)?rTypeId:null
                ,Multi_Select__c = multiSelect
                ,Includes_Golf__c = includesGolf
            );
            if(hasInsert) insert question;
            return question;
    }

    public static Question_Answers_Values__c createQuestionOption(Boolean hasInsert, Question__c question, String name, String value){
        Question_Answers_Values__c questionOption = new Question_Answers_Values__c(
                Question__c = question.Id
                ,Name = name
                ,Value__c = (value != null)?value:null
            );
        if(hasInsert) insert questionOption;
        return questionOption;
    }
    public static Event_Question__c createEventQuestion(Boolean hasInsert, String name, Product2 event, String phase, Question__c question, Boolean required){
        
        return TestDataGenerator.createEventQuestion(hasInsert, name, event, phase, question, required, null, null);
    }
    public static Event_Question__c createEventQuestion(Boolean hasInsert, String name,  Product2 event, String phase, Question__c question, Boolean required, Integer displayOrder, String displayType){
        Event_Question__c eventQuestion = new Event_Question__c(
                Question__c = question.Id
                ,Event__c = event.Id
                ,Name = name
                ,Display_Order__c = (displayOrder != null)?displayOrder:null
                ,Display_Type__c = (displayType != null)?displayType:null
                ,Phase__c = phase
                ,Required__c = required
            );
        if(hasInsert) insert eventQuestion;
        return eventQuestion;
    }

    public static Contact_Event_Answers__c createQuestionAnswers(Boolean hasInsert,Order conf, Event_Account_Details__c pRecord, Event_Question__c eQuestion, Contact con, String ans, String picklistIds, String SubAnwer){
        Contact_Event_Answers__c eventAnswer = new Contact_Event_Answers__c(
            Answer__c = ans
            ,Confirmation__c = conf.Id
            ,Contact__c = con.Id
            ,Participation_Record__c = pRecord.Id
            ,Picklist_Ids__c = picklistIds
            ,Question__c =eQuestion.Id
            ,Sub_Question_Answer__c = SubAnwer
        );
        if(hasInsert) insert eventAnswer;
        return eventAnswer;
    }

    public static Event_Staff__c createEventStaff(Boolean hasInsert, Product2 prod, User cpmgUser, String role){
        
        return TestDataGenerator.createEventStaff(hasInsert, prod, cpmgUser, role, false, null);
    }
    public static Event_Staff__c createEventStaff(Boolean hasInsert, Product2 prod, User cpmgUser, String role, Boolean directoryDisplay, Integer directoryOrder){
        Event_Staff__c eStaff = new Event_Staff__c(
            CPMG_Event__c = prod.Id
            ,User__c = cpmgUser.Id
            ,Role__c = role
            ,Directory_Staff_Order__c = directoryOrder
            ,Display_on_Staff_Directory__c = directoryDisplay
        );
        
        if(hasInsert) insert eStaff;
        return eStaff;
    }

    public static Speaker_Session__c createSpeakerSessions(Boolean hasInsert, String dateTimeDisplay, Datetime dateTimeOrder, String name, Product2 prod, String overText, String overURL, String presenter, String title){
        Speaker_Session__c speakSession = new Speaker_Session__c(
            Name = name
            ,Date_and_Time_Display__c = dateTimeDisplay
            ,Date_and_Time_Order__c = dateTimeOrder
            ,Event__c = prod.Id
            ,Overview_Text__c = overText
            ,Overview_URL__c = overURL
            ,Presenter__c = presenter
            ,Title__c =  title
        );
        if(hasInsert) insert speakSession;
        return speakSession;
    }

    public static Event_Map_URL__c createEventMapUrl(Boolean hasInsert,String name, Integer dispOrder, String title, String type, string mapUrl, Product2 prod){
        Event_Map_URL__c eMapUrl = new Event_Map_URL__c(
            Name = name
            ,Event__c = prod.Id
            ,Display_Order__c = dispOrder
            ,Map_Title__c = title
            ,Map_Url__c = mapUrl
            ,Map_Type__c = type
        );
        if(hasInsert) insert eMapURL;
        return eMapUrl;
    } 

    public static Document createDocument(String testBody, String contType, String name){
        Folder FolderId = [SELECT Id FROM Folder WHERE Name = 'Public Photo' LIMIT 1];
        Blob body = Blob.valueOf(testBody);
        Document result = DocumentHelper.createDoc(body, contType, (String) folderID.Id, name);
        insert result;
        return result;
    }

    public static Image__c createImage(Boolean hasInsert, Account a, Contact con, Order conf, Product2 Event, String docId, Event_Account_Details__c partRecord, Boolean sponsorImage, Boolean isLogo, Boolean isSelected){
        Image__c img = new Image__c(
        Company__c = a.Id
        ,Contact__c =con.Id
        ,Confirmation__c = conf.Id
        ,Event__c = Event.Id
        ,File_Id__c = docId
        ,Participation_Record__c = partRecord.Id
        ,Image_Type__c = (sponsorImage)?'Logo':'Headshot'
        ,Selected__c = isSelected
        );
        if(hasInsert) insert img;
        return img;
    }

    public static Order_Statuses__c createOrderStatusCustomSetting(){
        Order_Statuses__c ordStatusSetting = new Order_Statuses__c(
            Attendee_Remove_Action__c = 'Attending Event Contact->Preliminary'
            ,Attending_Picklist_Values__c = 'Complete|Confirmed|Attending Event Contact'
            ,Event_Contact_Picklist_Values__c = 'Preliminary|Attending Event Contact'
        );
        insert ordStatusSetting;
        return ordStatusSetting;
    }

    public static Trigger_Settings__c createInactiveTriggerSettings(){
        Trigger_Settings__c tSet = new Trigger_Settings__c(
            Event_Account_Detail_Trigger__c = true
            ,Order_Trigger__c = true
            ,Purchased_Sponsorship_Trigger__c = true
        );
        insert tSet;
        return tSet;
    }
    public static Trigger_Settings__c createActiveTriggerSettings(){
        Trigger_Settings__c tSet = new Trigger_Settings__c(
            Event_Account_Detail_Trigger__c = false
            ,Order_Trigger__c = false
            ,Purchased_Sponsorship_Trigger__c = false
        );
        insert tSet;
        return tSet;
    }

    public static Community_Settings__c createCommSettings(){
        Profile comProfile = [SELECT Id FROM Profile where Name ='CPMG Community Portal User'];
        User defOwner = [SELECT Id FROM User where Profile.Name ='System Administrator' and isActive = true  LIMIT 1];
        Community_Settings__c comSett = new Community_Settings__c(
            Attendee_Confirmation_Status__c = 'Complete'
            ,Community_Notification_Email__c = 'test1234@test.com'
            ,Community_User_Profile__c = comProfile.Id
            ,Default_Owner__c = defOwner.Id
            ,First_Event_Date__c = System.today()
            ,Test_User__c = defOwner.Id
        );
        insert comSett;
        return comSett;
    }

    public static List<Account_Form_Display_Settings__c> createAccountFormSettings(){
        List<Account_Form_Display_Settings__c> afds = new List<Account_Form_Display_Settings__c>();
        
        afds.add(new Account_Form_Display_Settings__c(Name='BillingCity', Field_Display_Name__c ='City' , Read_Only__c =false ));
        afds.add(new Account_Form_Display_Settings__c(Name='BillingCountry', Field_Display_Name__c ='Country' , Read_Only__c =false ));
        afds.add(new Account_Form_Display_Settings__c(Name='BillingPostalCode', Field_Display_Name__c = 'Zip Code' , Read_Only__c =false ));
        afds.add(new Account_Form_Display_Settings__c(Name='BillingState', Field_Display_Name__c = 'State', Read_Only__c =false ));
        afds.add(new Account_Form_Display_Settings__c(Name='BillingStreet', Field_Display_Name__c = 'Corporate Street', Read_Only__c =false ));
        afds.add(new Account_Form_Display_Settings__c(Name='Name', Field_Display_Name__c = 'Name', Read_Only__c =false ));
        afds.add(new Account_Form_Display_Settings__c(Name='Phone', Field_Display_Name__c = 'Phone' , Read_Only__c =false ));  //REMOVE ONE TO HAVE THE EVENT REG2 Create one
        
        insert afds;
        return afds;
    }

    public static List<Contact_Form_Display_Settings__c> createContactFormSettings(){
        List<Contact_Form_Display_Settings__c> cfds = new List<Contact_Form_Display_Settings__c>();
        
        cfds.add(new Contact_Form_Display_Settings__c(Name ='Company_Name__c' , Field_Display_Name__c='Company Name' , Read_Only__c =true ));
        cfds.add(new Contact_Form_Display_Settings__c(Name ='MailingCity' , Field_Display_Name__c='City' , Read_Only__c =false ));
        cfds.add(new Contact_Form_Display_Settings__c(Name ='MailingCountry' , Field_Display_Name__c='Country' , Read_Only__c =false ));
        cfds.add(new Contact_Form_Display_Settings__c(Name ='MailingPostalCode' , Field_Display_Name__c='Zip Code' , Read_Only__c =false ));
        cfds.add(new Contact_Form_Display_Settings__c(Name ='MailingState' , Field_Display_Name__c='State' , Read_Only__c =false ));
        cfds.add(new Contact_Form_Display_Settings__c(Name ='MailingStreet' , Field_Display_Name__c='My Office Address' , Read_Only__c =false ));
        cfds.add(new Contact_Form_Display_Settings__c(Name ='MobilePhone' , Field_Display_Name__c='Mobile' , Read_Only__c =false ));
        cfds.add(new Contact_Form_Display_Settings__c(Name ='Phone' , Field_Display_Name__c='My Work Phone' , Read_Only__c =false )); //REMOVE ONE TO CREATE BLANK
        
        insert cfds;
        return cfds;    
    }

    public static List<Other_Contact_Specified_Fields__c> createOtheContactFormSettings(){
        List<Other_Contact_Specified_Fields__c> cfds = new List<Other_Contact_Specified_Fields__c>();
        
        cfds.add(new Other_Contact_Specified_Fields__c(Name ='Email' , Field_Display_Name__c='Email Address' , Read_Only__c =true ));
        cfds.add(new Other_Contact_Specified_Fields__c(Name ='FirstName' , Field_Display_Name__c='First' , Read_Only__c =true ));
        cfds.add(new Other_Contact_Specified_Fields__c(Name ='LastName' , Field_Display_Name__c='Last' , Read_Only__c =false ));
        
        insert cfds;
        return cfds;    
    }

    public static List<Order_Form_Display_Settings__c> createOrderFormSettings(){
        List<Order_Form_Display_Settings__c> ofds = new List<Order_Form_Display_Settings__c>();

        ofds.add(new Order_Form_Display_Settings__c(Name='ADA_Information__c' , Field_Display_Name__c ='ADA needs relating to ground transport or hotel accommodations', Read_Only__c=false));
        ofds.add(new Order_Form_Display_Settings__c(Name='BadgeName__c' , Field_Display_Name__c ='Badge Name', Read_Only__c=false));

        insert ofds;
        return ofds;
    }

    public static Registration_Settings__c createRegistrationSettings(){
        Registration_Settings__c regSetting = new Registration_Settings__c(Notification_User__c ='test@test.com');
        insert regSetting;
        return regSetting;
    }

    public static List<CPMG_Notified_Executives__c> createCPMGNotifiedUsers(){
        List<CPMG_Notified_Executives__c> notifiedUsers = new List<CPMG_Notified_Executives__c>();
        notifiedUsers.add(new CPMG_Notified_Executives__c(Name = 'Kim Haulk' , Email__c = 'kim.haulk@cpmginc.com'));

        insert notifiedUsers;
        return notifiedUsers;
    }

    public static Event_Ranking__c createEventRanking(Boolean hasInsert, Product2 event, Event_Account_Details__c prRegistrant, Event_Account_Details__c prMatch, Integer rank, Contact registrant, String rankType, String comment ){
        Event_Ranking__c ranking = new Event_Ranking__c(
            Event__c = event.Id
            ,Participation_Record_Registrant__c = prRegistrant.Id
            ,Participation_Record_Registrant_Match__c = prMatch.Id
            ,Rank__c = rank
            ,Registrant__c = registrant.Id
            ,Type__c = rankType
            ,Comment__c = comment
        );
        if(hasInsert) insert ranking;
        return ranking;
    }

}