public class SaveResultEmailClass {
    

    //private String objType {get;set;}

    public static String  processSaveResults(Database.SaveResult[] saveResults,List<SObject> attempt){

        Map<Integer,String> errorResults;
        Integer x = 0;
        Integer y = 0;
        errorResults = new Map<Integer,String>();
        String successResult;
        for(Database.SaveResult sr : saveResults){
            System.debug('THE Save result: '+sr);
            Sobject tempObj = attempt[y];
            
            if(sr.isSuccess()){
                x++;
            }
            else{
            
                for(Database.Error err :sr.getErrors()){
                    errorResults.put(y,'<br/>'+'\r\n'+'The Error Message: '+err.getMessage()+'\r\n'+' Creating: '+ String.valueOf(tempObj)+'\r\n'+'<br/>');
                    //System.debug(s);
                    System.debug('Fields: '+ err.getFields());
                }
            }
            y++;
        }

        successResult = String.valueOf(x)+ ' out of ' + String.valueOf(saveResults.size())+' records successfully created.'+'<br/>';
        System.debug('THE ERROR RESULT: '+ errorResults);
        if(errorResults != null){
            for(Integer z =0; z<errorResults.size(); z++){
                if(errorResults.containsKey(z)){
                    String error = errorResults.get(z);
                    System.debug('The Current Error: '+ error);
                    successResult += ('\r\n'+error);
                }
            }
        }
        System.debug('Result Message: '+ successResult);
        return successResult;
    }
}