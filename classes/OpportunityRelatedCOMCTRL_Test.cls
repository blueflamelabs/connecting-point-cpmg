/*
*Created By     : 
*Created Date   : 19/07/2019 
*Description    : 
*Test Class Name: OpportunityRelatedCOMCTRL
Modification History :
============================================================================================
V.No    Date            By                  Description
============================================================================================
*/
@IsTest
public class OpportunityRelatedCOMCTRL_Test{
    static testMethod void OpportunityRelatedCOMCTRL_testMethod(){
        
         Id execAccountRtype = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
         Id execConRtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
         Account a = TestDataGenerator.createTestAccount(true,'Test Account',execAccountRtype);
        
         Contact objCon = TestDataGenerator.createTestContact(true,'Test','Exec','t@t.com',a,(String) execConRtype);
      
         Profile profilId = [SELECT Id FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        
         User objUser = TestDataGenerator.CreateUser(true,profilId.Id,'Test','Exec','2225252525','test1Exec1234@cpmg.com',objCon.Id);
        
         Product2 objProd = TestDataGenerator.createTestProduct(true,'Test Name',True);
       
         Event_Staff__c objEventStf = TestDataGenerator.createEventStaff(true,objProd,objUser,'Support');
        
         Opportunity objOpp = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),a, 'Closed Won',objProd,'Onsite Renewal');
         
         Test.startTest();
          OpportunityRelatedCOMCTRL oppRel = new OpportunityRelatedCOMCTRL();
          OpportunityRelatedCOMCTRL.showContactRecord(objCon.Id);
         Test.stopTest(); 
    }
}