@isTest
public class EmailSeriesController_test{
       public static testMethod void EmailSeriesControllertest(){
       Id execAccountRtype = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
       Id execConRtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
       Account a = TestDataGenerator.createTestAccount(true,'Test Account',execAccountRtype );
       Contact c = testdatagenerator.createtestcontact(true,'test','test','test@gmail.com',a,execConRtype);
       
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'CPMG Community Portal User' LIMIT 1];
        User objUser=TestDataGenerator.CreateUser(true ,profileId.id, 'Test', 'test','bc123','test@gmail.com',c.id);
       // User objUserTest=TestDataGenerator.CreateUser(true ,profileId.id, 'Test', 'test','bc123','test@gmail.com');
        
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event',true,Date.today().addDays(2), Date.today().addDays(5), Date.today().addMonths(-3), Date.today().addMonths(-2));
        Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Event Active Opp',Date.today().addMonths(-4),a,'Closed Won',prod ,'Renewal');
        Id execPartRecordType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c partRecord = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c, execPartRecordType);
        Order conf = TestDataGenerator.createConfirmation(true,a,c,'Preliminary',opp,opp.CloseDate,partRecord,prod,true);
        
       Id execConRtype1 = Schema.SObjectType.Planning_Series__c.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
           Planning_Series__c ps = new Planning_Series__c();
          // ps.Written_By__c =objUser.id;
           ps.Event__c = prod.id;
           ps.Title__c = 'test';
           ps.Posted_Date__c =system.Today();
           ps.Event__c = prod.id;
           ps.RecordTypeId =execConRtype1;
           insert ps;
           
           Planning_Series__c ps1 = new Planning_Series__c();
           //ps1.Written_By__c =objUser.id;
           ps1.Event__c = prod.id;
           ps1.Title__c = 'test';
           ps1.Posted_Date__c =system.Today();
           ps1.Event__c = prod.id;
           ps1.RecordTypeId =execConRtype1;
           insert ps1;
           
           Planning_Series__c ps2 = new Planning_Series__c();
           //ps2.Written_By__c =objUser.id;
           ps2.Event__c = prod.id;
           ps2.Title__c = 'test';
           ps2.Posted_Date__c =system.Today();
           ps2.Event__c = prod.id;
           ps2.RecordTypeId =execConRtype1;
           insert ps2;
          
           Planning_Series__c ps3 = new Planning_Series__c();
          // ps3.Written_By__c =objUser.id;
           ps3.Event__c = prod.id;
           ps3.Title__c = 'test';
           ps3.Posted_Date__c =system.Today();
           ps3.Event__c = prod.id;
           ps3.RecordTypeId =execConRtype1;
           insert ps3;
           
           Planning_Series__c ps4 = new Planning_Series__c();
        //   ps4.Written_By__c =objUser.id;
           ps4.Event__c = prod.id;
           ps4.Title__c = 'test';
           ps4.Posted_Date__c =system.Today();
           ps4.Event__c = prod.id;
           ps4.RecordTypeId =execConRtype1;
           insert ps4;
           
           Planning_Series__c ps5 = new Planning_Series__c();
          // ps5.Written_By__c =objUser.id;
           ps5.Event__c = prod.id;
           ps5.Title__c = 'test';
           ps5.Posted_Date__c =system.Today();
           ps5.Event__c = prod.id;
           ps5.RecordTypeId =execConRtype1;
           insert ps5;
           
           Planning_Series__c ps6 = new Planning_Series__c();
          // ps6.Written_By__c =objUser.id;
           ps6.Event__c = prod.id;
           ps6.Title__c = 'test';
           ps6.Posted_Date__c =system.Today();
           ps6.Event__c = prod.id;
           ps6.RecordTypeId =execConRtype1;
           insert ps6;
           
           Planning_Series__c ps7 = new Planning_Series__c();
          // ps7.Written_By__c =objUser.id;
           ps7.Event__c = prod.id;
           ps7.Title__c = 'test';
           ps7.Posted_Date__c =system.Today();
           ps7.Event__c = prod.id;
           ps7.RecordTypeId =execConRtype1;
           insert ps7;
           
           
           
           System.runAs (objUser){ 
              
       Test.startTest();
         ApexPages.currentPage().getParameters().put('eId',prod.id);
         EmailSeriesController escon = new EmailSeriesController();
         escon.getTotalPages();
         escon.getNumbers();
         escon.getMyCommandButtons();
         escon.refreshGrid();
         escon.Previous();
         escon.Next();
         escon.End();
         escon.getDisablePrevious();
         escon.getDisableNext();
         escon.getTotal_size();
         escon.getPageNumber();
         escon.updateOrderAlertNewPost();
       Test.stopTest();
       } 
    }
    
       
}