@isTest
private class QuestionsHelper_test {
    
    @isTest static void getQuestions_test() {

        RecordType rt = [SELECT Id,Name FROM RecordType WHERE SobjectType='Question__c' AND Name='Question' LIMIT 1];

        Question__c question = new Question__c(Name='testQuestion', RecordTypeId=rt.Id);
        insert question;

        Set<Id> questionIds = new Set<Id>();
        questionIds.add(question.Id);


        Map<Id, Question__c> result = QuestionsHelper.getQuestions(questionIds);

        System.assertEquals(result.get(question.Id).Id, question.Id);
    }
    
    @isTest static void getCategoryOptions_Test() {
        // Implement test code

        RecordType rt = [SELECT Id,Name FROM RecordType WHERE SobjectType='Question__c' AND Name='Question' LIMIT 1];

        Question__c question = new Question__c(Name='testQuestion', RecordTypeId=rt.Id, Picklist__c = true);
        insert question;

        Set<Id> questionIds = new Set<Id>();
        questionIds.add(question.Id);

        Question_Answers_Values__c option1 = TestDataGenerator.createQuestionOption(true, question, 'Option 1', 'Option 1');
        Question_Answers_Values__c option2 = TestDataGenerator.createQuestionOption(true, question, 'Option 2', 'Option 2');
        Question_Answers_Values__c option3 = TestDataGenerator.createQuestionOption(true, question, 'Option 3', 'Option 3');

        test.startTest();
            List<SelectOption> questionOptions = QuestionsHelper.getCategoryOptions(questionIds);
        test.stopTest();

    }
    
}