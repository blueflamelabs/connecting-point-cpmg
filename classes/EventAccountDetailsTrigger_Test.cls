@IsTest
public class EventAccountDetailsTrigger_Test{

    static testMethod void EventAccountDetailsTrigger_testMethod(){
     Id conRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        
        Account objAcc = TestDataGenerator.createTestAccount(true,'Test Supplier Account');
        
        Contact objCon = TestDataGenerator.createTestContact(true,'Test FName','Last Name','testexec@test.com',objAcc,conRecType);

        
        Product2 objProd = TestDataGenerator.createTestProduct(true,'Test Name',True);
        
        Opportunity objOpp = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),ObjAcc,'Closed Won',objProd,'Onsite Renewal');
        
        Id execPartRecordType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c partRecord = TestDataGenerator.createParticipationRecord(true,objProd,objAcc,objOpp,objCon, execPartRecordType);
        partRecord.Status__c = 'Cancelled';
        update partRecord;
        
    }
}