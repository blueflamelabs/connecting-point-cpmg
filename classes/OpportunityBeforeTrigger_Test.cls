/*
*Created By     : 
*Created Date   : 22/07/2019 
*Description    : 
*Trigger Name     : OpportunityBeforeTrigger
Modification History :
============================================================================================
V.No    Date            By                  Description
============================================================================================
*/
@IsTest
public class OpportunityBeforeTrigger_Test{

       static testmethod void OpportunityBeforeTrigger_TestMethod(){
       Id conRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        
        Account objAcc = TestDataGenerator.createTestAccount(true,'Test Supplier Account');
        
        Contact objCon = TestDataGenerator.createTestContact(true,'Test FName','Last Name','testexec@test.com',objAcc,conRecType);

        
        Product2 objProd = TestDataGenerator.createTestProduct(true,'Test Name',True);
        
        Opportunity objOpp = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),ObjAcc,'Closed Won',objProd,'Onsite Renewal');
        
        objOpp.StageName = 'Closed Lost';
        objOpp.Lost_Reason__c = 'Cancelled';
        objOpp.Lost_Reason_Details__c = 'test';
        update objOpp;
        
       }
      
}