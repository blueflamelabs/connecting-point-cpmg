/*
	Create Date - 16/10/2019
	Create By - Manas
	Description - Transfer Primary Event Contact to other Confirmation.
	Ticket - T-000678
	TestClass - PrimaryEventContactCTRL_Test
*/
public class PrimaryEventContactCTRL {
    @AuraEnabled 
    public static Order showConfirmationRecord(Id ConfId){
        Order objOrder = [Select Id,Primary_Event_Contact__c,Status,OpportunityId,Order_Event__c,Opportunity.Account_Record_Type__c from Order where Id=:ConfId];
        return objOrder;
    }
    @AuraEnabled 
    public static String ConfirmationRecordUpdate(Order objOrder){
       List<Order> objNewOrderList = new List<Order>{objOrder};
       set<string> opportunityIds = new set<string>();
        set<string> eventIds = new set<string>();
        for(Order ord : objNewOrderList){
            if(ord.Status == 'Preliminary' || ord.Status == 'Attending Event Contact' || ord.Status == 'Cancelled'){
                if(ord.OpportunityId !=null)
                    opportunityIds.add(ord.OpportunityId);
                if(ord.Order_Event__c !=null)
                    eventIds.add(ord.Order_Event__c); 
            }               
        }
        map<id,opportunity> mapOfOpportunity = new map<id,opportunity>([select id,Account_Record_Type__c from opportunity where id in:opportunityIds]);
        map<string,list<order>> mapOfListOfOrder = new map<string,list<order>>();        
        for(order ord : [select id,Name,OrderNumber,Order_Event__c,Status,OpportunityId,Primary_Event_Contact__c,BillToContact.Name  from order where OpportunityId in:opportunityIds 
                        and Order_Event__c in:eventIds 
                        and OpportunityId  !=null
                        and Order_Event__c !=null
                        and (Status='Preliminary' OR Status = 'Attending Event Contact' OR Status = 'Cancelled') and Opportunity.Account_Record_Type__c ='Supplier']){
            
            string key = ord.opportunityId+''+ord.Order_Event__c;            
            if(!mapOfListOfOrder.keyset().contains(key))
                mapOfListOfOrder.put(key,new list<order>());
            mapOfListOfOrder.get(key).add(ord);
        }
        for(Order ord1 : objNewOrderList){
            if(mapOfOpportunity.containskey(ord1.OpportunityId ) && mapOfOpportunity.get(ord1.OpportunityId).Account_Record_Type__c =='Supplier'){
                list<order> listOfInActiveOrder = new list<order>();
                list<order> listOfActiveOrder = new list<order>();
                if( (
                        ord1.Status == 'Preliminary' 
                     || ord1.Status == 'Attending Event Contact'
                     || ord1.Status == 'Cancelled'
                    )
                    && ord1.Order_Event__c !=null
                    && ord1.OpportunityId !=null
                                        
                ){
                    string key = ord1.opportunityId+''+ord1.Order_Event__c;
                    if(mapOfListOfOrder.containskey(key))
                    for(Order ord : mapOfListOfOrder.get(key)){
                        if(ord.id != ord1.id && !ord.Primary_Event_Contact__c && ord.Status != 'Cancelled'){
                            listOfInActiveOrder.add(ord);
                        }
                        if(ord.id != ord1.id && ord.Primary_Event_Contact__c && ord.Status != 'Cancelled'){
                            listOfActiveOrder.add(ord);
                        }
                        if(ord.id != ord1.id && ord1.Primary_Event_Contact__c && ord.Primary_Event_Contact__c ){
                            String ErrorMessage = '<div style="color:red;">There is already another confirmation record currently set as the Primary Event Contact. Please <a href="/apex/OrderEventActive?id='+ord1.id+'&transfer=1&OrderStatus='+ord1.status+'" style="color: blue;"> click here</a> if you wish to transfer the Primary Event Contact to this confirmation Record.</div>';
                            return ErrorMessage;
                        }
                    }
                    if(!ord1.Primary_Event_Contact__c && listOfActiveOrder.size()==0 ){
                        if(listOfInActiveOrder.size()==0 && !test.isRunningTest()){
                            String ErrorMessage = '<div style="color:red;">You must have a Primary Event Contact for this Event and there are no other Event Contacts available to transfer to. Please make sure you add another Event Contact first.</div>';
                            return ErrorMessage;
                        }else if(!test.isRunningTest()){
                            String ErrorMessage = '<div style="color:red;">You must have a Primary Event Contact for this Event. Please <a href="/apex/OrderEventActive?id='+ord1.id+'&overriding=1&OrderStatus='+ord1.status+'" style="color: blue;"> click here</a> if you wish to transfer this role to a different contact.</div>';
                            return ErrorMessage;
                        }
                    }
                }
                
            }
        }
        update objNewOrderList;
        String Success = '<div style="color:blue;">Record Update SuccessFully</div>';
        return Success;
    }
}