public class RegenerateController {

    private final sObject mysObject;
    public Id eventId{get;set;}
    Public String SuccessMessage{set;get;}
    Private list<Order> listOrder{get;set;}
    
    public RegenerateController(ApexPages.StandardController stdController) {
        this.mysObject = (sObject)stdController.getRecord();
        this.eventId = (Id)stdController.getId();
    }
    public PageReference RegenerateProcessExecutive(){
        RegenerateProcess('Executive');
        return null;
    }
    public PageReference RegenerateProcessSupplier(){
        RegenerateProcess('Supplier');
        return null;
    }
    public void RegenerateProcess(String userType){
         listOrder = [select Order_Event__r.User_Link_Expiration_for_Suppliers__c,Order_Event__r.User_Link_Expiration_for_Executives__c,id,Welcome_Email_Link__c,BillToContactId,Opportunity.Account_Record_Type__c,status,Welcome_Email_Link_Expiration__c,
                         Order_Event__r.Allow_Welcome_Emails_for_Suppliers__c,Order_Event__r.Allow_Welcome_Emails_for_Executives__c
                         from order where Order_Event__c =:eventId and opportunity.Account_Record_Type__c =:userType and 
                         Welcome_Email_Link_Expiration__c < today];
                         
         Map<Id,Order> objMapUserIdOrder = new Map<Id,Order>();
         for(Order objOrder : listOrder){
             if(String.isNotBlank(objOrder.BillToContactId))
                 objMapUserIdOrder.put(objOrder.BillToContactId,objOrder);
         }
         Blob cryptoKey = Blob.valueOf(system.label.cryptoKey);
         String ChangePassword = Label.ChangePassword;
         List<Order> objOrderList = new List<Order>();
         List<String> objMailId = new List<String>();
         
         String RoleName = '';
         if(userType == 'Supplier')
             RoleName = 'Support';
         else if(userType == 'Executive')
             RoleName = 'Recruiter';
         
         String EventStaffEmail = '';
         String OwnerName = '';
         List<Event_Staff__c> objEventStaff = [select id,User__r.IsActive,User__r.Email,User__r.Name,Role__c,CPMG_Event__c,Display_to_Suppliers__c  from Event_Staff__c where CPMG_Event__c =: eventId and Role__c =: RoleName and User__r.IsActive=true limit 1];
         if(objEventStaff.size() > 0){             
            EventStaffEmail = objEventStaff[0].User__r.Email;
            OwnerName = objEventStaff[0].User__r.Name;
         }
         Map<Id,Id> objContactorOrderId = new Map<Id,Id>();
         Map<string,OrgWideEmailAddress> mapOfOrgWideEmailAddress = new Map<string,OrgWideEmailAddress>();
         for(OrgWideEmailAddress owea : [select Id,Address,DisplayName from OrgWideEmailAddress ]){
             mapOfOrgWideEmailAddress.put(owea.Address,owea);
         }
         
         List<Messaging.SingleEmailMessage> listOfEmails = new List<Messaging.SingleEmailMessage>();
         List<User> objUserList = [SELECT id,LastLoginDate,ContactId,Contact.Email FROM User where ContactId IN : objMapUserIdOrder.keySet() and LastLoginDate = null];
         
         for(User objUser : objUserList){
             Order objOrder = objMapUserIdOrder.get(objUser.ContactId);
             Integer Dates = 0;
             if(userType == 'Executive'){
                 Dates = Integer.valueOf(objOrder.Order_Event__r.User_Link_Expiration_for_Executives__c);
             }else{
                 Dates = Integer.valueOf(objOrder.Order_Event__r.User_Link_Expiration_for_Suppliers__c);
             }
             objOrder.Welcome_Email_Link_Expiration__c = System.today().addDays(Integer.valueOf(Dates));
             Blob tokenData = Blob.valueOf('{"contactId":"'+objOrder.BillToContactId+'","confirmationId":"'+objOrder.Id+'","token":"'+Dates+'"}');
             Blob encryptedData = Crypto.encryptWithManagedIV('AES128',cryptoKey,tokenData);
             String token = EncodingUtil.base64Encode(encryptedData);
             token  = EncodingUtil.urlEncode(token, 'UTF-8');
             objOrder.Welcome_Email_Link__c = ChangePassword+token;
             objOrderList.add(objOrder);
         }
         if(objOrderList.size() > 0){
             for(Community_Welcome_Emails__mdt communitywel : [select id,Sender_Name__c,Channel__c,Default_Email_Sender__c,Email_Template_ID__c,Status__c,User_Type__c from Community_Welcome_Emails__mdt]){
                 for(Order ord : objOrderList){
                     Boolean isAllowMail = false;
                     if(userType == 'Supplier'){
                         isAllowMail = ord.Order_Event__r.Allow_Welcome_Emails_for_Suppliers__c;
                     }
                     if(userType == 'Executive'){
                         isAllowMail = ord.Order_Event__r.Allow_Welcome_Emails_for_Executives__c;
                     }
                   if(isAllowMail == true){
                     if(ord.Opportunity.Account_Record_Type__c == communitywel.Channel__c &&
                         communitywel.User_Type__c =='New User' &&
                         ord.status == communitywel.Status__c
                      ){
                          string owaId;
                          if(mapOfOrgWideEmailAddress.keyset().contains(EventStaffEmail))
                              owaId = mapOfOrgWideEmailAddress.get(EventStaffEmail).id;    
                          else if(mapOfOrgWideEmailAddress.keyset().contains(communitywel.Default_Email_Sender__c)){
                              owaId = mapOfOrgWideEmailAddress.get(communitywel.Default_Email_Sender__c).id;
                              OwnerName = communitywel.Sender_Name__c;
                          }else{
                              OwnerName = userinfo.getName(); 
                          }    
                        listOfEmails.add(createEmailMessage(ord.BillToContactID,(string)ord.id,communitywel.Email_Template_ID__c,owaId));
                      }
                    }  
                 }
             }
             System.debug('@@@@@@@@ listOfEmails %%%%%%%%% '+listOfEmails);
             if(listOfEmails.size() > 0){
               try{
                Messaging.sendEmail(listOfEmails,false);
                SuccessMessage = 'The links were regenerated successfully and the welcome emails resent';
               }catch(Exception ex){
                   System.debug('$$$$ '+ex.getMessage());
               } 
             }else{
                 SuccessMessage = 'The links have been successfully regenerated';
             }
             update objOrderList;
         } else{
             SuccessMessage = 'There are no expired links to reset';
         } 
          
         
    }
    
    public Messaging.SingleEmailMessage createEmailMessage(string contactId,string orderId,string TemplateId,string owaId){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setSaveAsActivity(true);
        if(owaId !=null)
            mail.setOrgWideEmailAddressId(owaId);
        mail.setTargetObjectId(contactId);
        mail.setWhatId(orderId);
        mail.setTemplateId(TemplateId);
        return mail;
    }
}