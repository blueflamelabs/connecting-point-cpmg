public class QuestionsHelper {


   public static Map<Id, Question__c> getQuestions(Set<Id> p_questionIds) {
    
    System.debug('Get Questions Called: ');
    return new Map<Id, Question__c>([SELECT Id, Name, Max_Char_Count__c, Picklist__c, Question_Type__c,
          RecordTypeId, RecordType.Name, Multi_Select__c, Includes_Golf__c, (SELECT Id,Name, Value__c,Golf__c,Sort_Order__c 
              FROM Question_Answers_Values__r ORDER BY Sort_Order__c ASC nulls last,Name ASC)
          FROM Question__c
          WHERE Id IN :p_questionIds
        ]);
  }

  public static List<SelectOption> getCategoryOptions(Set<Id> categoryQuestionId){
        List<SelectOption> categoryOptions = new List<SelectOption>();
        for(Question__c quest : [SELECT Id, (SELECT Id, Value__c,Name FROM Question_Answers_Values__r ORDER By Value__c) FROM Question__c Where Id IN :categoryQuestionId]){
            for(Question_Answers_Values__c qav : quest.Question_Answers_Values__r){
                categoryOptions.add(new SelectOption(qav.Name, qav.Value__c));
            }
        }
        return categoryOptions;
  }
}