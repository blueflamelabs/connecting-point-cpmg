public class ForgotPasswordCtrl{
    public String email{set;get;}
    public boolean isMessageShow{set;get;}
    public boolean isExpiry{set;get;}
    
    public ForgotPasswordCtrl(){
        Date Token;
        isMessageShow = false;
        isExpiry = false;
        String BillToUserContactId = ApexPages.currentPage().getParameters().get('ContactId');
        string ConfmId = ApexPages.currentPage().getParameters().get('ConfirmationId');
        if(ApexPages.currentPage().getParameters().get('Token') != null){
            String dataformatchange = ApexPages.currentPage().getParameters().get('Token');
            String dataformatyear = dataformatchange.substring(0,4)+'-';
            String dateformatmonth = dataformatchange.substring(4,6)+'-';
            String dateformatday = dataformatchange.substring(6,dataformatchange.length());
            Token = date.valueof(dataformatyear+dateformatmonth+dateformatday);
        }    
        if(BillToUserContactId != null){
            email = [Select id,ContactId,Email from user where ContactId =:BillToUserContactId limit 1].Email;
             list<Order> listObjOrder = [select id,Welcome_Email_Link_Expiration__c,BillToContactId from Order where BillToContactId =: BillToUserContactId
             And Id =:ConfmId And Welcome_Email_Link_Expiration__c =:Token limit 1];
             if(listObjOrder.size()==0){                    
                    isExpiry = true;                  
             } 
        }
        
    }
    public void sendEmail(){
       Site.forgotPassword(email);
       isMessageShow = true;
    }
}