public class ContactEventAnswersOnConfirmations{
    public void CreateExecutiveEventAnswers(set<id> orderId){
        System.debug('#####orderId#####3 '+orderId);
        Set<Id> objProId = new Set<Id>();
        Set<Id> objConId = new Set<Id>();
        Set<string> objProCompanyName = new Set<string>();
        list<Product2> objProduct = new list<Product2>();
        List<Order> objOrderList = [select id,Event_Account_Detail__c,Order_Event__c,Company_Display_Name__c,Status,BillToContactId,Primary_Event_Contact__c from Order 
                    where Id IN : orderId];
        for(Order objOrder : objOrderList){
            objConId.add(objOrder.BillToContactId);
            objProId.add(objOrder.Order_Event__c);
            objProCompanyName.add(objOrder.Company_Display_Name__c);
        }        
        List<Contact_Event_Answers__c> ContactEventAnsList = [select Question__r.Question__c,Name,Answer__c,Sub_Question_Answer__c,Picklist_IDs__c,Participation_Record__c,Confirmation__c,id,Contact__c,Contact__r.Gone__c,Question__r.Display_Type__c from Contact_Event_Answers__c 
                    where Contact__c IN : objConId and ((Question__r.Display_Type__c = 'Professional Biography') OR (((Question__r.Display_Type__c = 'Company Short Description' Or Question__r.Display_Type__c = 'Company Long Description') And (Confirmation__r.Company_Display_Name__c IN : objProCompanyName and Contact__r.Gone__c = true)) OR 
                    ((Question__r.Display_Type__c = 'Company Short Description' Or Question__r.Display_Type__c = 'Company Long Description') And (Contact__r.Gone__c = false)))) 
                    and Question__r.RecordType.Name = 'Executive' and Question__r.Event__r.Test_Event__c !=true and Question__r.Event__r.IsActive = true ORDER BY createdDate DESC];
       
        System.debug('#ContactEventAnsList## '+ContactEventAnsList); 
        List<Event_Question__c> objEvenQuestionList = [select id,Display_Type__c,Event__c,RecordType.Name,Question__c,Question__r.name from Event_Question__c where Event__c IN : objProId and RecordType.Name = 'Executive'];
        map<string,list<Event_Question__c>> mapOfEventQuestion = new map<string,list<Event_Question__c>>();
        for(Event_Question__c evn : objEvenQuestionList ){
            if(!mapOfEventQuestion.keyset().contains(evn.Event__c))
                mapOfEventQuestion.put(evn.Event__c,new list<Event_Question__c>());
            mapOfEventQuestion.get(evn.Event__c).add(evn);
        } 
        List<Contact_Event_Answers__c> objContactEventAns = new List<Contact_Event_Answers__c>();
        
        for(Order ord : objOrderList){
        System.debug('####ord.Id##########3 '+ord.Id);
        if(mapOfEventQuestion.keySet().Contains(ord.Order_Event__c)){
            for(Event_Question__c objEventQu : mapOfEventQuestion.get(ord.Order_Event__c)){
                if(ContactEventAnsList.size()>0 && ( objEventQu.Display_Type__c=='Professional Biography'
                    || objEventQu.Display_Type__c=='Company Short Description'
                    || objEventQu.Display_Type__c=='Company Long Description' )
                    ){
                    boolean flagAns = true;
                    for(Contact_Event_Answers__c contactAns : ContactEventAnsList){
                        if(ord.BillToContactId==contactAns.contact__c && objEventQu.Display_Type__c== contactAns.Question__r.Display_Type__c){
                            Contact_Event_Answers__c objContactEvent = new Contact_Event_Answers__c();  
                            objContactEvent.Confirmation__c = ord.id;                      
                            objContactEvent.Name =  objEventQu.Question__r.name;
                            objContactEvent.Contact__c = ord.BillToContactId;
                            objContactEvent.Answer__c =  contactAns.Answer__c;
                            objContactEvent.Sub_Question_Answer__c =  contactAns.Sub_Question_Answer__c;
                            objContactEvent.Participation_Record__c = ord.Event_Account_Detail__c;
                            objContactEvent.Picklist_IDs__c =  contactAns.Picklist_IDs__c;  
                            objContactEvent.Question__c = objEventQu.id;                       
                            objContactEventAns.add(objContactEvent);
                            flagAns = false;
                            break;
                             System.debug('#objContactEvent525252525## '+objContactEvent); 
                        }
                    }
                    if(flagAns){
                        Contact_Event_Answers__c objContactEvent = new Contact_Event_Answers__c();
                        objContactEvent.Contact__c = ord.BillToContactId;
                        objContactEvent.Confirmation__c = ord.id;
                        objContactEvent.name = objEventQu.Question__r.name;
                        objContactEvent.Participation_Record__c = ord.Event_Account_Detail__c;
                        objContactEvent.Question__c = objEventQu.id;
                        objContactEventAns.add(objContactEvent);
                        System.debug('#objContactEve6363636363## '+objContactEvent); 
                    }
                }else{
                    Contact_Event_Answers__c objContactEvent = new Contact_Event_Answers__c();
                    objContactEvent.Contact__c = ord.BillToContactId;
                    objContactEvent.Confirmation__c = ord.id;
                    objContactEvent.name = objEventQu.Question__r.name;
                    objContactEvent.Participation_Record__c = ord.Event_Account_Detail__c;
                    objContactEvent.Question__c = objEventQu.id;
                    objContactEventAns.add(objContactEvent);
                    System.debug('#objContactEvent57272727737375## '+objContactEvent); 
                }
            }
           }
        }
        System.debug('$$$$$$$$objContactEventAnsobjContactEventAns$$$4444444 '+objContactEventAns);
        if(objContactEventAns.size() > 0)
            insert objContactEventAns;
            
    }
    
    public void CreateSupplierEventAnswers(set<Id> OrderId){
    
        Set<Id> objConId = new Set<Id>();
        Set<Id> objProId = new Set<Id>();
        Set<string> objProCompanyName = new Set<string>();
        Set<String> objProductFamily  = new Set<String>();
        
        List<Order> objOrderList = [select id,Primary_Event_Contact__c,Company_Display_Name__c,Event_Account_Detail__c,Order_Event__c,Order_Event__r.Family,Order_Event__r.Parent_Event__c,Status,BillToContactId from Order 
                    where Id IN : OrderId  and (Status = 'Preliminary' OR Status = 'Attending Event Contact') and Primary_Event_Contact__c = true and Opportunity.Account_Record_Type__c = 'Supplier'];
        for(Order objOrder : objOrderList){
            objConId.add(objOrder.BillToContactId);
            objProId.add(objOrder.Order_Event__c);
            objProCompanyName.add(objOrder.Company_Display_Name__c);
        }
        
        List<Contact_Event_Answers__c> ContactEventAnsList = [select Confirmation__r.Company_Display_Name__c,Question__r.Question__c,Name,Answer__c,Sub_Question_Answer__c,Picklist_IDs__c,Participation_Record__c,Confirmation__c,id,Contact__c,Question__r.Display_Type__c,Question__r.Event__r.Parent_Event__c,Question__r.Event__r.Family from Contact_Event_Answers__c 
                    where Confirmation__r.Company_Display_Name__c IN : objProCompanyName and ( Question__r.Display_Type__c = 'Company Short Description' Or Question__r.Display_Type__c = 'Company Long Description')
                     and Question__r.RecordType.Name = 'Supplier' and Question__r.Event__r.Test_Event__c !=true and Question__r.Event__r.IsActive = true  ORDER BY createdDate DESC];
        
        List<Event_Question__c> objEvenQuestionList = [select id,Display_Type__c,Event__c,RecordType.Name,Question__c,Question__r.name from Event_Question__c where Event__c IN : objProId and RecordType.Name = 'Supplier'];
        map<string,list<Event_Question__c>> mapOfEventQuestion = new map<string,list<Event_Question__c>>();
        for(Event_Question__c evn : objEvenQuestionList ){
            if(!mapOfEventQuestion.keyset().contains(evn.Event__c))
                mapOfEventQuestion.put(evn.Event__c,new list<Event_Question__c>());
            mapOfEventQuestion.get(evn.Event__c).add(evn);
        }        
        List<Contact_Event_Answers__c> objContactEventAns = new List<Contact_Event_Answers__c>();
        
        for(Order ord : objOrderList){
        if(mapOfEventQuestion.keySet().Contains(ord.Order_Event__c)){
            for(Event_Question__c objEventQu : mapOfEventQuestion.get(ord.Order_Event__c)){
                if(ContactEventAnsList.size()>0 && ( objEventQu.Display_Type__c=='Company Short Description' || objEventQu.Display_Type__c=='Company Long Description' )){
                    boolean flagAns = true;
                    for(Contact_Event_Answers__c contactAns : ContactEventAnsList){
                        if(ord.Company_Display_Name__c == contactAns.Confirmation__r.Company_Display_Name__c)
                        if( (Ord.Order_Event__r.Parent_Event__c !='N/A' && Ord.Order_Event__r.Parent_Event__c == contactAns.Question__r.Event__r.Parent_Event__c) 
                            || (Ord.Order_Event__r.Parent_Event__c =='N/A' && Ord.Order_Event__r.Family == contactAns.Question__r.Event__r.Family )
                         )
                        if(objEventQu.Display_Type__c== contactAns.Question__r.Display_Type__c){
                            Contact_Event_Answers__c objContactEvent = new Contact_Event_Answers__c();  
                            objContactEvent.Confirmation__c = ord.id;                      
                            objContactEvent.Name =  objEventQu.Question__r.name;
                            objContactEvent.Contact__c = ord.BillToContactId;
                            objContactEvent.Answer__c =  contactAns.Answer__c;
                            objContactEvent.Participation_Record__c = ord.Event_Account_Detail__c;
                            objContactEvent.Sub_Question_Answer__c =  contactAns.Sub_Question_Answer__c;
                            objContactEvent.Picklist_IDs__c =  contactAns.Picklist_IDs__c;  
                            objContactEvent.Question__c = objEventQu.id;                       
                            objContactEventAns.add(objContactEvent);
                            flagAns = false;
                            break;
                        }
                    }
                    if(flagAns){
                        Contact_Event_Answers__c objContactEvent = new Contact_Event_Answers__c();
                        objContactEvent.Contact__c = ord.BillToContactId;
                        objContactEvent.Name = objEventQu.Question__r.name;
                        objContactEvent.Confirmation__c = ord.id;
                        objContactEvent.Participation_Record__c = ord.Event_Account_Detail__c;
                        objContactEvent.Question__c = objEventQu.id;
                        objContactEventAns.add(objContactEvent);
                    }
                }else{
                    Contact_Event_Answers__c objContactEvent = new Contact_Event_Answers__c();
                    objContactEvent.Contact__c = ord.BillToContactId;
                    objContactEvent.Confirmation__c = ord.id;
                    objContactEvent.Participation_Record__c = ord.Event_Account_Detail__c;
                    objContactEvent.name = objEventQu.Question__r.name;
                    objContactEvent.Question__c = objEventQu.id;
                    objContactEventAns.add(objContactEvent);
                }
            }
           }
        }
        System.debug('$$$$$$$$objContactEventAnsobjContactEventAns$$$4444444 '+objContactEventAns);
        if(objContactEventAns.size() > 0)
            insert objContactEventAns;
            
    }
}