@isTest
private class EventQuestionHelper_test {
    
    @isTest static void test_method_one() { 
        test.startTest();

        Product2 event = new Product2(Name='testProduct',User_Link_Expiration_for_Executives__c= 2,User_Link_Expiration_for_Suppliers__c = 2);
        insert event;

        RecordType rt = [SELECT Id,Name FROM RecordType WHERE SobjectType='Question__c' AND Name='Question' LIMIT 1];

        System.assertNotEquals(rt, null);

        Question__c question = new Question__c(Name='testQuestion', RecordTypeId=rt.Id);
        insert question;

        System.assertNotEquals(question.Id, null);

        string phase = 'testPhase';
        Event_Question__c eq = new Event_Question__c(Event__c=event.Id, Phase__c=phase, Question__c=question.Id);
        insert eq;

        Map<Id,Event_Question__c> eqMap = EventQuestionHelper.getEventQuestions(phase, question.RecordType.Name, question.Question_Type__c, event.Id);

        //System.assertEquals(eq.id, eqMap.get(eq.Id).id);

        test.stopTest();
    }
    
    @isTest static void test_method_two() {
        // Implement test code
    }
    
}