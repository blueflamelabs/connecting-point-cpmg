@isTest
private class UserHelper_test {

	@TestSetup
	private static void createTestData(){
		Id accSupplierRType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
		Id accExecRType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
		Id conSupplierRType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
		Id conExecRType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
		
		Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];

		Account a = TestDataGenerator.createTestAccount(true,'Test Account',accSupplierRType);
		Account a2 = TestDataGenerator.createTestAccount(true,'Test Account2',accExecRType);
		List<Contact> testCons = new List<Contact>();
		for(integer x =0; x < 100; ++x){
			Account conAcc = Math.mod(x,2) == 0?a:a2;
			Id conRType = Math.mod(x,2) == 0 ?conSupplierRType:conExecRType;
			testCons.add(TestDataGenerator.createTestContact(false,'First'+String.valueOf(x),'Last'+String.valueOf(x),'first.last'+String.valueOf(x)+'@testaccountSObjectHelper.com',conAcc,conRType));
		}
		
		insert testCons;
		List<User> users = new List<User>();
		for(Contact c : testCons)
		{
			users.add(TestDataGenerator.CreateUser(false,commUserProfId.Id,c.FirstName,c.LastName,c.Phone, c.Email,c.Id));

		}

		insert users;		
	}
	
	@isTest static void testGetContactIdUserMap() {
		
	 	Profile profileId = [SELECT Id FROM Profile WHERE Name = 'CPMG Community Portal User' LIMIT 1];
  
	 	Account acc = new Account(Name='test');
	 	insert acc;

		Contact newContact = new Contact (
		FirstName = 'firstName',
		LastName = 'lastName',
		AccountId = acc.Id,
		Email = 'test@testmail.com'
		);

		insert newContact;

		User usr = new User(LastName = 'TestLast',
                     FirstName='TestFirst',
                     Alias = 'test',
                     Email = 'testFirst.testLast@asdf.com',
                     Username = 'testFirst.testLast@asdf.com',
                     ProfileId = profileId.id,
                     TimeZoneSidKey = 'GMT',
                     LanguageLocaleKey = 'en_US',
                     EmailEncodingKey = 'UTF-8',
                     LocaleSidKey = 'en_US',
                     ContactId = newContact.Id
                     );
			
		insert usr;


		Set<Id> contactIds = new Set<Id>();
		contactIds.add(newContact.Id);

		test.startTest();
		Map<Id,User> result = UserHelper.getContactIdUserMap(contactIds);

		System.assertEquals(result.get(newContact.Id).Id, usr.Id);
		test.stopTest();

	}
	
	@isTest static void testCreateUser() {

		Profile profileId = [SELECT Id FROM Profile WHERE Name = 'CPMG Community Portal User' LIMIT 1];

	 	Account acc = new Account(Name='test');
	 	insert acc;
	 	
		Contact newContact = new Contact (
			FirstName = 'FirstName',
			LastName = 'LastName',
			AccountId = acc.Id,
			Email = 'test@testmail.com'
		);

		insert newContact;

		String LastName = 'TestLast';
     	String FirstName='TestFirst';
        String Email = 'ReallyQuiteIncrediblySuperLongEmailAddressForTesting@asdf.com';
        Id UserProfileId = profileId.id;
        String Phone = '(555)555-5555';
        Id ContactId = newContact.Id;


        test.startTest();
		User result =  UserHelper.CreateUser(UserProfileId, FirstName, LastName, Phone, Email, ContactId);
		System.assertNotEquals(result, null);
		test.stopTest();
	}

	@IsTest
	private static void updateUserNameOnContact_Test()
	{
		Map<Id,User> contactIDUserMap = new Map<Id,User>();
		integer count = 0;
		for(User u : [SELECT Id, UserName, ContactId, Contact.COmmunity_User_Name__c FROM User where ContactId <> null and userName like '%@testaccountSObjectHelper.com']){
			//System.assertEquals(u.UserName,u.Contact.Community_User_Name__c);
			u.put('Username',u.Username + String.valueOf(count));
			contactIDUserMap.put(u.ContactId,u);
			++count;
			
		}
	
		Test.startTest();
			UserHelper.updateUserNameOnContact(contactIDUserMap.values());
		Test.stopTest();
	}
	
}