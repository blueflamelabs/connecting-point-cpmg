public class ChangePasswordCTRL {
    public boolean isMinLength{set;get;}
    public boolean isExpiry{set;get;}
    public String passwordPolicy{set;get;}
    public String confirmPassword { get; set; }
    public String newPassword { get; set; }    
     
    public ChangePasswordCTRL(){
       passwordPolicy = String.valueOf(site.getPasswordPolicyStatement());
    } 
    public PageReference sendEmail(){  
        if(newPassword == confirmPassword){
            passwordPolicy = '';
            try{
                isMinLength = true;
                pageReference pr = site.changePassword(newPassword ,confirmPassword ,null);            
                if(pr != null){
                    return pr;
                }else{
                    
                    passwordPolicy = String.valueOf(ApexPages.getMessages()).substringBetween('"','\"');
                }    
            }catch(Exception ex){
                 // if(ex.getMessage().contains('INVALID_NEW_PASSWORD')){
                       //passwordPolicy = String.valueOf(ex.getMessage()).split(':')[1];
                      // passwordPolicy =  ex.getMessage();
                       //isMinLength = true;
                  //  }
                    return null;
            }             
        }else{
            passwordPolicy = 'Passwords do not match. Please enter a matching password in both rows.';
            isMinLength = true;
        }
        return null;
    }


  
}