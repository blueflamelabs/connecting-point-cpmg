@isTest
public class ContactEventAnswers_Test{

    public static testMethod void CreateExecutiveEventAnswersTest(){
         Test.startTest();
        String CRON_EXP = '0 0 0 15 3 ? *';
        Datetime dt = Datetime.now().addMinutes(1); 
        Date todaydate = Date.today().adddays(1);
        Product2 prod = new Product2();prod.Name='testProduct';
        prod.IsActive = true;prod.Start_Date__c = Date.today();prod.Default_Executive_Event_Answers_Created__c = false;prod.End_Date__c = Date.today();prod.Registration_Part_1_Open_For_Executive__c = todaydate;
            prod.Registration_Part_1_Open_for_Supplier__c = todaydate;prod.Registration_Part_1_Close_For_Executive__c = Date.today();
            prod.Registration_Part_1_Close_For_Supplier__c = Date.today();prod.User_Link_Expiration_for_Executives__c= 2;prod.User_Link_Expiration_for_Suppliers__c = 2;
            prod.Venue__c='test';prod.Venue_and_Location__c ='test';prod.Location__c ='test';prod.LinkedIn__c ='test';prod.Twitter__c ='test';
            prod.Year__c ='2018';prod.Header_Date_Range__c = String.valueOf(Date.today());prod.EventHeaderLogo__c= 'BuildPoint';prod.Family='StorePoint Retail';
        insert prod;   
       Id execAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
       Account a = TestDataGenerator.createTestAccount(true, 'Test Account',execAccRtype);
       Id execConRtype =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
       Contact c = TestDataGenerator.createTestContact(true,'Test','Last Name','testAcc@account.com',a,execConRtype);
       Opportunity execOpp = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),a,'Closed Won',prod,'Renewal');
       Id execPRRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
       Event_Account_Details__c execPRecord = TestDataGenerator.createParticipationRecord(true,prod,a,execOpp,c,execPRRType);
       Order execConf = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',execOpp,execOpp.CloseDate,execPRecord,prod,true);  
       Map<String,RecordTypeInfo> questionRTypeMap = Schema.SObjectType.Question__c.getRecordTypeInfosByName();
       Question__c mulitPicklistQuestion = TestDataGenerator.createQuestion(true,'Test Multi Pickilist',200,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
       Id exeEventQuRtype =  Schema.SObjectType.Event_Question__c.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
       Event_Question__c eventQuestion = new Event_Question__c();
       eventQuestion.Question__c = mulitPicklistQuestion.Id;
       eventQuestion.Event__c = prod.Id;
       eventQuestion.Name = 'The Picklist Event Quest';
       eventQuestion.Display_Order__c =1;
       eventQuestion.Display_Type__c = 'Company Short Description';
       eventQuestion.Phase__c = 'Company';
       eventQuestion.Required__c = false;
       eventQuestion.RecordTypeId  = exeEventQuRtype;
       insert eventQuestion;
       Event_Question__c eventQuestion1 = new Event_Question__c();
       eventQuestion1.Question__c = mulitPicklistQuestion.Id;
       eventQuestion1.Event__c = prod.Id;
       eventQuestion1.Name = 'The Picklist Event Quest';
       eventQuestion1.Display_Order__c =1;
       eventQuestion1.Display_Type__c = 'Company Long Description';
       eventQuestion1.Phase__c = 'Company';
       eventQuestion1.Required__c = false;
       eventQuestion1.RecordTypeId  = exeEventQuRtype;
       insert eventQuestion1;
       Contact_Event_Answers__c categoryAnswer = TestDataGenerator.createQuestionAnswers(true,execConf,execPRecord,eventQuestion,c,'Select2;Select3',null,null);
       String jobId = System.schedule('ScheduleApexClassTest',  CRON_EXP, new ContactEventAnswersScheduler());
       
       Test.stopTest();
       }
   public static testMethod void CreateExecutiveEventAnswersTest1(){
    Test.startTest();
        String CRON_EXP = '0 0 0 15 3 ? *';
        Datetime dt = Datetime.now().addMinutes(1); 
        Date todaydate = Date.today().adddays(1);
        Product2 prod = new Product2();prod.Name='testProduct';
        prod.IsActive = true;prod.Start_Date__c = Date.today();prod.Default_Executive_Event_Answers_Created__c = false;prod.End_Date__c = Date.today();prod.Registration_Part_1_Open_For_Executive__c = todaydate;
            prod.Registration_Part_1_Open_for_Supplier__c = todaydate;prod.Registration_Part_1_Close_For_Executive__c = Date.today();
            prod.Registration_Part_1_Close_For_Supplier__c = Date.today();prod.User_Link_Expiration_for_Executives__c= 2;prod.User_Link_Expiration_for_Suppliers__c = 2;
            prod.Venue__c='test';prod.Venue_and_Location__c ='test';prod.Location__c ='test';prod.LinkedIn__c ='test';prod.Twitter__c ='test';
            prod.Year__c ='2018';prod.Header_Date_Range__c = String.valueOf(Date.today());prod.EventHeaderLogo__c= 'BuildPoint';prod.Family='StorePoint Retail';
        insert prod;   
       Id execAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
       Account a = TestDataGenerator.createTestAccount(true, 'Test Account',execAccRtype);
       Id execConRtype =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
       Contact c = TestDataGenerator.createTestContact(true,'Test','Last Name','testAcc@account.com',a,execConRtype);
       Opportunity execOpp = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),a,'Closed Won',prod,'Renewal');
       Id execPRRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
       Event_Account_Details__c execPRecord = TestDataGenerator.createParticipationRecord(true,prod,a,execOpp,c,execPRRType);
       Order execConf = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',execOpp,execOpp.CloseDate,execPRecord,prod,true);  
       Map<String,RecordTypeInfo> questionRTypeMap = Schema.SObjectType.Question__c.getRecordTypeInfosByName();
       Question__c mulitPicklistQuestion = TestDataGenerator.createQuestion(true,'Test Multi Pickilist',200,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
       Id exeEventQuRtype =  Schema.SObjectType.Event_Question__c.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
       Event_Question__c eventQuestion = new Event_Question__c();
       eventQuestion.Question__c = mulitPicklistQuestion.Id;
       eventQuestion.Event__c = prod.Id;
       eventQuestion.Name = 'The Picklist Event Quest';
       eventQuestion.Display_Order__c =1;
       eventQuestion.Display_Type__c = 'Company Short Description';
       eventQuestion.Phase__c = 'Company';
       eventQuestion.Required__c = false;
       eventQuestion.RecordTypeId  = exeEventQuRtype;
       insert eventQuestion;
       Event_Question__c eventQuestion1 = new Event_Question__c();
       eventQuestion1.Question__c = mulitPicklistQuestion.Id;
       eventQuestion1.Event__c = prod.Id;
       eventQuestion1.Name = 'The Picklist Event Quest';
       eventQuestion1.Display_Order__c =1;
       eventQuestion1.Display_Type__c = 'Company Long Description';
       eventQuestion1.Phase__c = 'Company';
       eventQuestion1.Required__c = false;
       eventQuestion1.RecordTypeId  = exeEventQuRtype;
       insert eventQuestion1;
       Contact_Event_Answers__c categoryAnswer = TestDataGenerator.createQuestionAnswers(true,execConf,execPRecord,eventQuestion,c,'Select2;Select3',null,null);
       String jobId = System.schedule('ScheduleApexClassTest',  CRON_EXP, new ContactEventAnswersScheduler());
       
       Test.stopTest();
       }
}