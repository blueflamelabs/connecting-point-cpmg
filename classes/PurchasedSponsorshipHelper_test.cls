@isTest
private class PurchasedSponsorshipHelper_test {
	
	@isTest static void test_method_one() {

		Event myEvent = new Event(ActivityDate=Date.today().addDays(3), IsAllDayEvent = true);
		//Event_Category_Inventory__c category = new Event_Category_Inventory__c(Name='test', Total_Available_Spots__c=4 ,Category__c='Communications');
		//insert category;
		Opportunity opp = new Opportunity(Name='test', StageName ='stagetest', CloseDate = Date.today());
		insert opp;
		Event_Account_Details__c record = new Event_Account_Details__c(Event__c=myEvent.Id, Opportunity__c=opp.Id);
		insert record;

		Event_Sponsorships__c sponsorship = new Event_Sponsorships__c(Display_On_Portal__c=true, Event__c=myEvent.Id);
		insert sponsorship;

		Purchased_Sponsorship__c purchasedSponsorship = new Purchased_Sponsorship__c(
			Event__c=myEvent.Id, 
			Event_Account_Detail__c=record.Id,
			Sponsorship__c = sponsorship.Id);
		insert purchasedSponsorship;

		test.startTest();		
		List<Purchased_Sponsorship__c> result = PurchasedSponsorshipHelper.getPurchasedSponsorshipsForPortalImages((String)myEvent.Id);

		boolean containsPS = false;
		
		for(Purchased_Sponsorship__c ps : result){
			if(ps.Id == purchasedSponsorship.Id){
				containsPS = true;
				break;
			}
		}

		System.assert(containsPS);
		test.stopTest();
	}
}