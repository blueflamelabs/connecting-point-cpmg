public class OrderEventActive {
    public list<Order> listOrder{get;set;}
    public list<Order> listOrderUncheck{get;set;}
    public string overriding{get;set;}
    public string orderId{get;set;}
    public string transfer{get;set;}
    public integer repIndex{get;set;}
    public String currentOrderStatus{set;get;}
    public OrderEventActive(ApexPages.StandardController controller) {
        orderId = ApexPages.currentPage().getParameters().get('id');
        overriding = ApexPages.currentPage().getParameters().get('overriding');
        transfer= ApexPages.currentPage().getParameters().get('transfer');
        currentOrderStatus = ApexPages.currentPage().getParameters().get('OrderStatus');
        listOrder = [select Id,Status,Primary_Event_Contact__c,Order_Event__c,OpportunityId,BillToContactId from Order where id =:orderId];
        if(listOrder!=null && listOrder.size()>0){
            listOrderUncheck = [select Id,Primary_Event_Contact__c,Order_Event__c,OpportunityId,BillToContact.Name,BillToContactId 
                                        from Order where id !=:orderId and Order_Event__c =: listOrder[0].Order_Event__c 
                                        and OpportunityId  =: listOrder[0].OpportunityId
                                        and OpportunityId  !=null
                                        and Order_Event__c !=null
                                        and (Status='Preliminary' OR Status = 'Attending Event Contact')
                                        and Primary_Event_Contact__c = false and Opportunity.Account_Record_Type__c ='Supplier' 
                                        ];
                           
        }
    }
    public void updateOrder(){
        list<Order> listUpdateOrder = new list<Order>();
        listOrder[0].Primary_Event_Contact__c = true;
        //listOrder[0].status = currentOrderStatus;
        listUpdateOrder.add(listOrder[0]);
        list<string> setOfEventAns = new list<string>();
        list<Contact_Event_Answers__c> updateConEvnt = new list<Contact_Event_Answers__c>();
        for(order ord : [select Id,Primary_Event_Contact__c,Order_Event__c,OpportunityId,BillToContactId 
                        from Order where id !=:listOrder[0].id and Order_Event__c =: listOrder[0].Order_Event__c 
                        and OpportunityId  =: listOrder[0].OpportunityId
                        and OpportunityId  !=null
                        and Order_Event__c !=null
                        and (Status='Preliminary' OR Status = 'Attending Event Contact')
                        and Primary_Event_Contact__c = true
                        ]){
                            ord.Primary_Event_Contact__c = false;
                            listUpdateOrder.add(ord);
                            setOfEventAns.add(ord.id);
                        }
       for(Contact_Event_Answers__c conEvenAns : [select Id,Contact__c,Confirmation__c from Contact_Event_Answers__c where Confirmation__c =:setOfEventAns] ){
            conEvenAns.Confirmation__c = listOrder[0].id;
            conEvenAns.Contact__c = listOrder[0].BillToContactId;
            updateConEvnt.add(conEvenAns);
        }       
       
       if(listUpdateOrder.size()>0){
           RecursiveVariable.orderActiveVariable = false;
           update listUpdateOrder;
           if(updateConEvnt.size()>0)
               Update updateConEvnt;
       }
    }
    public void replaceOrder(){
        list<Order> listUpdateOrder = new list<Order>();        
        listOrder[0].Primary_Event_Contact__c = false;
        listOrder[0].status = currentOrderStatus;
        listUpdateOrder.add(listOrder[0]);
        list<Contact_Event_Answers__c> updateConEvnt = new list<Contact_Event_Answers__c>();
        for(Contact_Event_Answers__c conEvenAns : [select Id,Contact__c,Confirmation__c from Contact_Event_Answers__c where Confirmation__c =:listOrder[0].id] ){
            conEvenAns.Confirmation__c = listOrderUncheck[repIndex].id;
            conEvenAns.Contact__c = listOrderUncheck[repIndex].BillToContactId;
            updateConEvnt.add(conEvenAns);
        }        
        listOrderUncheck[repIndex].Primary_Event_Contact__c = true;
        listUpdateOrder.add(listOrderUncheck[repIndex]);
        
        
        if(listUpdateOrder.size()>0){
           RecursiveVariable.orderActiveVariable = false;
           update listUpdateOrder;
           if(updateConEvnt.size()>0)
               Update updateConEvnt;
       }
    }
    public void updateOrderCancel(){
        update listOrder[0];
    }
}