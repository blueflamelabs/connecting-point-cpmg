@isTest
private class EventStaffHelper_Test {
    //private static final String email = 'test5@example.com';
    
   /* @isTest static void testIncorrectRole() {
        User user = insertUser('testdev251@example.com');
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Prod',true);
        insertEventStaff(user, 'Salesperson',prod);

        Test.startTest();
        Set<Id> eventStaffUsers = EventStaffHelper.getEventStaffUsers(prod.Id, new Set<String>{'Support'});
        System.assertEquals(0, eventStaffUsers.size());

        Test.stopTest();
    }*/
    
    @isTest static void testCorrectRole() {
        User user = insertUser('test6@example.com');
                Product2 prod = TestDataGenerator.createTestProduct(true,'Test Prod',true);
        insertEventStaff(user, 'Salesperson', prod);

        Test.startTest();
        Set<Id> eventStaffUsers = EventStaffHelper.getEventStaffUsers(prod.Id, new Set<String>{'Salesperson'});
        System.assertEquals(1, eventStaffUsers.size());
        
        Set<String> emails = EventStaffHelper.getEmails(new Set<Id>{user.Id});
        
        System.assertEquals(1, emails.size());
                
        System.assertEquals('test6@example.com', new list<string>(emails)[0]); 
        Test.stopTest();       
    }

    static testmethod void getEventStaffMapByRoles_Test(){
        User user = insertUser('test71@example.com');
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Prod',true);
        insertEventStaff(user, 'Salesperson',prod);
        Set<Id> eventIds = new Set<Id>{prod.Id};
        Test.startTest();
        Map<String, Event_Staff__c> eventStaffRoleMap = EventStaffHelper.getEventStaffMapByRoles(eventIds);
        System.assertEquals(1, eventStaffRoleMap.size());

        Test.stopTest();
    }

    static testmethod void getStaffByEvent_Test(){
        // Prepare Test Data.
        TestDataGenerator.createInactiveTriggerSettings();

        //Event Setup
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Active Event',true,Date.today().addDays(-10),Date.today().addDays(-5),Date.today().addMonths(-2), Date.today().addMonths(-1));
        prod.Start_Date__c = Date.today().addDays(10);
        prod.Family ='BuildPoint';
        prod.Event_Navigation_Color__c = '90135d';
        prod.Event_Tint_Color__c = 'EEEEEE';
        update prod;

        Profile standardUser = [SELECT Id FROM Profile where Name ='Standard User' LIMIT 1];
        User execUserCPMG = TestDataGenerator.CreateUser(true,standardUser.Id,'Test','Exec',null,'testExec1234@cpmg.com',null);
        User supplierUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Supplier',null,'testSupplier1234@cpmg.com',null);
        User supportUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Support',null,'testSupport1234@cpmg.com',null);


        Event_Staff__c execStaff = TestDataGenerator.createEventStaff(true,prod,execUserCPMG,'Recruiter',true,3);
        Event_Staff__c supplierStaff = TestDataGenerator.createEventStaff(true,prod,supplierUserCPMG,'Salesperson',true,2);
        Event_Staff__c supportStaff = TestDataGenerator.createEventStaff(true,prod,supportUserCPMG,'Support',true,1);

        Test.startTest();
            Event_Staff__c[] eStaffList = EventStaffHelper.getEventStaffByEvent(new Set<Id>{prod.Id});
        Test.stopTest();
            System.assertEquals(eStaffList[0].Id, supportStaff.Id);
            System.assertEquals(eStaffList[1].Id, supplierStaff.Id);
            System.assertEquals(eStaffList[2].Id, execStaff.Id);
    }

    static testmethod void eventStaffJSONMap_Test(){
        // Prepare Test Data.
        TestDataGenerator.createInactiveTriggerSettings();

        //Event Setup
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Active Event',true,Date.today().addDays(-10),Date.today().addDays(-5),Date.today().addMonths(-2), Date.today().addMonths(-1));
        prod.Start_Date__c = Date.today().addDays(10);
        prod.Family ='BuildPoint';
        prod.Event_Navigation_Color__c = '90135d';
        prod.Event_Tint_Color__c = 'EEEEEE';
        update prod;

        Profile standardUser = [SELECT Id FROM Profile where Name ='Standard User' LIMIT 1];
        User execUserCPMG = TestDataGenerator.CreateUser(true,standardUser.Id,'Test','Exec',null,'testExec1234@cpmg.com',null);
        execUserCPMG.Department ='Test Department';
        execUserCPMG.Title ='CEO';
        update execUserCPMG;

        Event_Staff__c execStaff = TestDataGenerator.createEventStaff(true,prod,execUserCPMG,'Recruiter',true,3);

        Event_Staff__c eStaffFromDB =[SELECT Id, CPMG_Event__c, Directory_Staff_Order__c, Display_on_Staff_Directory__c, Role__c, User__c, User__r.Title, User__r.FirstName, User__r.LastName, User__r.Email,
                        User__r.FullPhotoUrl, User__r.Department 
                     FROM Event_Staff__c WHERE id =:execStaff.Id];
        System.assertEquals(eStaffFromDB.User__r.Department,execUserCPMG.Department);


        Test.startTest();
            Map<String,Object> eStaffJsonMap = EventStaffHelper.eventStaffJSON(eStaffFromDB);
        Test.stopTest();
            System.assertEquals(eStaffFromDB.User__r.Department, eStaffJsonMap.get('department'));
            System.assertEquals(eStaffFromDB.User__r.Title, eStaffJsonMap.get('title'));
            System.assertEquals(eStaffFromDB.User__r.email, eStaffJsonMap.get('email'));
            System.assertEquals(eStaffFromDB.User__r.FirstName+' '+execUserCPMG.LastName, eStaffJsonMap.get('name'));
            System.assertEquals(eStaffFromDB.User__r.FullPhotoUrl, eStaffJsonMap.get('picture'));

    }
    
    private static User insertUser(string email) {
        User test = new User(Email=email, Username=email, LastName='lastname', Alias='alias', CommunityNickname='CommunityNickname',
                             localesidkey='en_US',emailencodingkey='UTF-8', languagelocalekey='en_US', TimeZoneSidKey='America/New_York',
                             profileid = [select id from profile where name='Standard User' limit 1].Id );
        insert test;
        
        return test;
    }   
    private static void insertEventStaff(User user, String role, Product2 prod) {
        Event_Staff__c test = new Event_Staff__c(Role__c = role, User__r = user, CPMG_Event__c = prod.id );
        insert test;
    }
    
}