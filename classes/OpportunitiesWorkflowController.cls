public with sharing class OpportunitiesWorkflowController {
    public string returncustomer {get;set;}
    private Id oppId;
    private Opportunity opp;
    private OpportunityContactRole oppContactRole;
    private Map<Id, RecordType> contactRecordTypes;

    public OpportunitiesWorkflowController() {
        oppId =ApexPages.CurrentPage().getparameters().get('id');
        getOpportunity();
        getPrimaryContactRole();
        getContactRecordType();
    }

   public void getOpportunity() {
        
        opp = [SELECT Id, Account_Record_Type__c,Returning_Customer__c,Primary_Contact__r.Name,Primary_Contact__c,AccountId, Account.Name, Product__c,Product__r.Name, Name FROM Opportunity WHERE Id =: oppId];
           
         
   }
    
    public void getPrimaryContactRole() {
        oppContactRole = [SELECT ContactId, Contact.RecordTypeId FROM OpportunityContactRole WHERE IsPrimary = true and OpportunityId = :oppId];
    }
    public void getContactRecordType() { //not used
        //Callout to Contact get RecordType Id (getContactRecordType)
        contactRecordTypes = new Map<Id, RecordType>([SELECT Id, Name FROM RecordType WHERE SobjectType='Contact']);
    }

    public PageReference redirect() {
      opp = [SELECT Id, Account_Record_Type__c,Returning_Customer__c,Primary_Contact__r.Name,Primary_Contact__c,AccountId, Account.Name, Product__c,Product__r.Name,Product__r.Supplier_Renewal_Badge_Add_On_Rule__c,Product__r.Start_Date__c, Name FROM Opportunity WHERE Id =: oppId];
         
        Field_References__c fieldReferences = Field_References__c.getInstance();
        System.debug(LoggingLevel.WARN, oppId);

        Boolean isSupplier = contactRecordTypes.containsKey(oppContactRole.Contact.RecordTypeId) && contactRecordTypes.get(oppContactRole.Contact.RecordTypeId).Name == 'Supplier';
        Event_Account_Details__c ead = NULL;
        //Added 4.3.2017 by PR, used to determine if the Supplier already has an Event_Account_Detail__c record. If they do, re-direct to the create Order (Confirmation URL)
        try {
            List<Event_Account_Details__c> eads = [SELECT Id,Name, Account__c, Opportunity__c, Event__c FROM Event_Account_Details__c WHERE Opportunity__c =:oppId LIMIT 1];
            if (eads.size() > 0) {
                ead = eads.get(0);
            }
        } catch(Exception e) {
            System.debug(LoggingLevel.ERROR, e);
        }

        String keyPrefix = Event_Account_Details__c.sObjectType.getDescribe().getKeyPrefix();
        PageReference pr;

        if (isSupplier && ead == NULL) { //redirect to create ead record
            Id PRRecordTypeId = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Suppliers').getRecordTypeId();
            pr = new PageReference('/' + keyPrefix + '/e');
            pr.setRedirect(true);
            pr.getParameters().put('retURL', '/'+opp.Id);
            pr.getParameters().put('RecordType', PRRecordTypeId);
            pr.getParameters().put(fieldReferences.EAD_Account__c, opp.Account.Name);
            pr.getParameters().put(fieldReferences.EAD_Account__c+'_lkid', opp.AccountId);

            pr.getParameters().put(fieldReferences.EAD_Event__c, opp.Product__r.Name);
            pr.getParameters().put(fieldReferences.EAD_Event__c+'_lkid', opp.Product__c);
            pr.getParameters().put(fieldReferences.EAD_Opportunity__c, opp.Name);
            if(opp.Returning_Customer__c=='Renewal'||opp.Returning_Customer__c=='Onsite Renewal'){
                if(opp.Product__r.Supplier_Renewal_Badge_Add_On_Rule__c == 'Always Receive Renewal Badge Add On'){
                    pr.getParameters().put(fieldReferences.Badge_Add_Ons__c, '1');  
                }
                if(opp.Product__r.Supplier_Renewal_Badge_Add_On_Rule__c == 'Must Attend Another Event'){
                    // and CALENDAR_YEAR(Event__r.Start_Date__c) =: CALENDAR_YEAR(opp.Product__r.Start_Date__c)
                    List<Event_Account_Details__c> objEvenAcDetails = [Select id,Event__r.Start_Date__c from Event_Account_Details__c where Status__c != 'Cancelled' 
                                                                        and Account__c =:opp.AccountId];
                    if(objEvenAcDetails.size() > 0){
                        Boolean isAvailable = false;
                        for(Event_Account_Details__c objEventAcc : objEvenAcDetails){
                            if(objEventAcc.Event__r.Start_Date__c.Year() == opp.Product__r.Start_Date__c.Year()){
                                isAvailable = true;
                                break;
                            }
                        }
                        if(isAvailable == true){
                            pr.getParameters().put(fieldReferences.Badge_Add_Ons__c, '1'); 
                        }
                    }
                }
            }
            if(opp.Returning_Customer__c=='Renewal'||opp.Returning_Customer__c=='Onsite Renewal'){
                pr.getParameters().put(fieldReferences.Registration_Reason__c, 'Renewal');    
            }
        } else {
            if(!isSupplier && ead == NULL){ //if exec without ead - create ead record auto....
                Id PRRecordTypeId = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
                ead = new Event_Account_Details__c();
                ead.Account__c = opp.AccountId;
                //ead.Badge_Add_Ons__c = 1;
                ead.Opportunity__c = opp.Id;
                ead.Event__c = opp.Product__c;
                ead.RecordTypeId = PRRecordTypeId;
                if(oppContactRole != null)ead.Primary_Contact__c = oppContactRole.ContactId;

                insert ead;

                ead = [SELECT Id, Name, Account__c, Opportunity__c, Event__c FROM Event_Account_Details__c WHERE Id =: ead.Id];
            } else {
              ead = [SELECT Id, Name, Account__c, Opportunity__c, Event__c FROM Event_Account_Details__c WHERE Id =: ead.Id];
            }
           // String status = (isSupplier)?'Event Contact':'Attending Event Contact')
            keyPrefix = Order.sObjectType.getDescribe().getKeyPrefix();
            pr = new PageReference('/' + keyPrefix + '/e');
            pr.setRedirect(true);
            pr.getParameters().put('retURL', '/'+opp.Id);


            pr.getParameters().put(fieldReferences.Order_Product__c, opp.Product__r.Name);
            pr.getParameters().put(fieldReferences.Order_Product__c+'_lkid', opp.Product__c);

            pr.getParameters().put(fieldReferences.Order_EAD__c, ead.Name);
            pr.getParameters().put(fieldReferences.Order_EAD__c+'_lkid', ead.Id);

            pr.getParameters().put('Account', opp.Account.Name);
            pr.getParameters().put('AccountId', opp.AccountId);

            pr.getParameters().put('Opportunity', opp.Name);
            pr.getParameters().put('OpportunityId', opp.Id);
            
            pr.getParameters().put('oid', opp.Id);
            pr.getParameters().put('Status', (isSupplier)?'Preliminary':'Attending Event Contact');
            pr.getParameters().put('EffectiveDate', System.today().format());
            if(opp.Primary_Contact__c != null && opp.Account_Record_Type__c == 'Executive'){
                pr.getParameters().put('BillToContact', opp.Primary_Contact__r.Name);
                pr.getParameters().put('BillToContactId', opp.Primary_Contact__c);
            }
            if(opp.Account_Record_Type__c == 'Executive'){
                pr.getParameters().put(fieldReferences.Primary_Event_Contact__c, '1');
                pr.getParameters().put(fieldReferences.Hotel_Confirmation__c, 'Pending');
            }
            
        }
        return pr;
    }
}