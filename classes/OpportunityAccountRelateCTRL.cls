public class OpportunityAccountRelateCTRL{

    Public String AccId{set;get;}
    Public String AccRecId{set;get;}
    public String AccName{set;get;}
    Public String RecTypeName{set;get;}
    public OpportunityAccountRelateCTRL(){
        AccId = ApexPages.currentPage().getParameters().get('Id');
        AccName = ApexPages.currentPage().getParameters().get('AccName');
        RecTypeName = ApexPages.currentPage().getParameters().get('ReTypeId');
        AccRecId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(ApexPages.currentPage().getParameters().get('ReTypeId')).getRecordTypeId();
    }
}