@isTest
private class PurchasedSponsorshipTrigger_Test {
    
    @isTest static void test_Insert_PurchasedSponsorship() {
        // Implement test code
        Account a = new Account(Name = 'Test Account 1', RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId());
        insert a;

        Product2 prod = new Product2(Name = 'Test Event', Start_Date__c =System.Today(), ProductCode =' TE 17',User_Link_Expiration_for_Executives__c= 2,User_Link_Expiration_for_Suppliers__c = 2);
        insert prod;
        //Create Event Sponsorship Records
        Event_Sponsorships__c eSponsor = new Event_Sponsorships__c(Name='TE Sponsor 1', Event__c = prod.Id, Price__c =1500, Purchased__c =false);
        Event_Sponsorships__c eSponsor2 = new Event_Sponsorships__c(Name='TE Sponsor 2', Event__c = prod.Id, Price__c =3000, Purchased__c =false);
        insert eSponsor;
        insert eSponsor2;

        Event_Package_Inventory__c package1 = new Event_Package_Inventory__c(Event__c = prod.Id, Name ='Package TE A', Price__c= 1000.00, Number_of_Badges__c =1,Number_of_Board_Rooms__c = 1, Number_of_Theatres__c=1 );
        insert package1;

        Event_Account_Details__c ead = new Event_Account_Details__c(Account__c = a.Id, Event__c = prod.Id, Package_Selection__c = package1.Id );
        insert ead;

        Purchased_Sponsorship__c pSponsor = new Purchased_Sponsorship__c(Event__c = prod.Id, Event_Account_Detail__c = ead.Id, Sponsorship__c = eSponsor.Id, Unit_Price__c = 1499.99);

        test.startTest();
        insert pSponsor;

        Event_Sponsorships__c eVerify = [SELECT Id, Purchased__c FROM Event_Sponsorships__c where Id = :eSponsor.Id];
        System.assertEquals(true, eVerify.Purchased__c);

        test.StopTest();

    }
    
    @isTest static void test_update_ChangePurchased_Sponsorship() {
        // Implement test code
                Account a = new Account(Name = 'Test Account 1', RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId());
        insert a;

        Product2 prod = new Product2(Name = 'Test Event', Start_Date__c =System.Today(), ProductCode =' TE 17',User_Link_Expiration_for_Executives__c= 2,User_Link_Expiration_for_Suppliers__c = 2);
        insert prod;
        //Create Event Sponsorship Records
        Event_Sponsorships__c eSponsor = new Event_Sponsorships__c(Name='TE Sponsor 1', Event__c = prod.Id, Price__c =1500, Purchased__c =false);
        Event_Sponsorships__c eSponsor2 = new Event_Sponsorships__c(Name='TE Sponsor 2', Event__c = prod.Id, Price__c =3000, Purchased__c =false);
        insert eSponsor;
        insert eSponsor2;

        Event_Package_Inventory__c package1 = new Event_Package_Inventory__c(Event__c = prod.Id, Name ='Package TE A', Price__c= 1000.00, Number_of_Badges__c =1,Number_of_Board_Rooms__c = 1, Number_of_Theatres__c=1 );
        insert package1;

        Event_Account_Details__c ead = new Event_Account_Details__c(Account__c = a.Id, Event__c = prod.Id, Package_Selection__c = package1.Id );
        insert ead;

        Purchased_Sponsorship__c pSponsor = new Purchased_Sponsorship__c(Event__c = prod.Id, Event_Account_Detail__c = ead.Id, Sponsorship__c = eSponsor.Id, Unit_Price__c = 1499.99);
        insert pSponsor;


        Event_Sponsorships__c eVerify = [SELECT Id, Purchased__c FROM Event_Sponsorships__c where Id = :eSponsor.Id];
        System.assertEquals(true, eVerify.Purchased__c);

        test.startTest();
        Purchased_Sponsorship__c pSponsorChange = new Purchased_Sponsorship__c(Id = pSponsor.Id, Sponsorship__c = eSponsor2.Id, Unit_Price__c = 2500.99);
        update pSponsorChange;

        Event_Sponsorships__c eVerify1 = [SELECT Id, Purchased__c FROM Event_Sponsorships__c where Id = :eSponsor.Id];
        Event_Sponsorships__c eVerify2 = [SELECT Id, Purchased__c FROM Event_Sponsorships__c where Id = :eSponsor2.Id];
        System.assertEquals(false, eVerify1.Purchased__c);
        System.assertEquals(true, eVerify2.Purchased__c);

        test.StopTest();
    }
    @isTest static void test_update_RemovePurchased_Sponsorship() {
        // Implement test code
                Account a = new Account(Name = 'Test Account 1', RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId());
        insert a;

        Product2 prod = new Product2(Name = 'Test Event', Start_Date__c =System.Today(), ProductCode =' TE 17',User_Link_Expiration_for_Executives__c= 2,User_Link_Expiration_for_Suppliers__c = 2);
        insert prod;
        //Create Event Sponsorship Records
        Event_Sponsorships__c eSponsor = new Event_Sponsorships__c(Name='TE Sponsor 1', Event__c = prod.Id, Price__c =1500, Purchased__c =false);
        Event_Sponsorships__c eSponsor2 = new Event_Sponsorships__c(Name='TE Sponsor 2', Event__c = prod.Id, Price__c =3000, Purchased__c =false);
        insert eSponsor;
        insert eSponsor2;

        Event_Package_Inventory__c package1 = new Event_Package_Inventory__c(Event__c = prod.Id, Name ='Package TE A', Price__c= 1000.00, Number_of_Badges__c =1,Number_of_Board_Rooms__c = 1, Number_of_Theatres__c=1 );
        insert package1;

        Event_Account_Details__c ead = new Event_Account_Details__c(Account__c = a.Id, Event__c = prod.Id, Package_Selection__c = package1.Id );
        insert ead;

        Purchased_Sponsorship__c pSponsor = new Purchased_Sponsorship__c(Event__c = prod.Id, Event_Account_Detail__c = ead.Id, Sponsorship__c = eSponsor.Id, Unit_Price__c = 1499.99);
        insert pSponsor;


        Event_Sponsorships__c eVerify = [SELECT Id, Purchased__c FROM Event_Sponsorships__c where Id = :eSponsor.Id];
        System.assertEquals(true, eVerify.Purchased__c);

        test.startTest();
        Purchased_Sponsorship__c pSponsorChange = new Purchased_Sponsorship__c(Id = pSponsor.Id, Sponsorship__c = null, Unit_Price__c = 2500.99);
        update pSponsorChange;

        Event_Sponsorships__c eVerify1 = [SELECT Id, Purchased__c FROM Event_Sponsorships__c where Id = :eSponsor.Id];
        //Event_Sponsorships__c eVerify2 = [SELECT Id, Purchased__c FROM Event_Sponsorships__c where Id = :eSponsor2.Id];
        System.assertEquals(false, eVerify1.Purchased__c);
        //System.assertEquals(true, eVerify2.Purchased__c);

        test.StopTest();
    }

    @isTest static void test_Delete_Purchased_Sponsorship() {
        // Implement test code
                Account a = new Account(Name = 'Test Account 1', RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId());
        insert a;

        Product2 prod = new Product2(Name = 'Test Event', Start_Date__c =System.Today(), ProductCode =' TE 17',User_Link_Expiration_for_Executives__c= 2,User_Link_Expiration_for_Suppliers__c = 2);
        insert prod;
        //Create Event Sponsorship Records
        Event_Sponsorships__c eSponsor = new Event_Sponsorships__c(Name='TE Sponsor 1', Event__c = prod.Id, Price__c =1500, Purchased__c =false);
        Event_Sponsorships__c eSponsor2 = new Event_Sponsorships__c(Name='TE Sponsor 2', Event__c = prod.Id, Price__c =3000, Purchased__c =false);
        insert eSponsor;
        insert eSponsor2;

        Event_Package_Inventory__c package1 = new Event_Package_Inventory__c(Event__c = prod.Id, Name ='Package TE A', Price__c= 1000.00, Number_of_Badges__c =1,Number_of_Board_Rooms__c = 1, Number_of_Theatres__c=1 );
        insert package1;

        Event_Account_Details__c ead = new Event_Account_Details__c(Account__c = a.Id, Event__c = prod.Id, Package_Selection__c = package1.Id );
        insert ead;

        Purchased_Sponsorship__c pSponsor = new Purchased_Sponsorship__c(Event__c = prod.Id, Event_Account_Detail__c = ead.Id, Sponsorship__c = eSponsor.Id, Unit_Price__c = 1499.99);
        insert pSponsor;


        Event_Sponsorships__c eVerify = [SELECT Id, Purchased__c FROM Event_Sponsorships__c where Id = :eSponsor.Id];
        System.assertEquals(true, eVerify.Purchased__c);

        test.startTest();
        Purchased_Sponsorship__c pSponsorChange = new Purchased_Sponsorship__c(Id = pSponsor.Id);
        delete pSponsorChange;

        Event_Sponsorships__c eVerify1 = [SELECT Id, Purchased__c FROM Event_Sponsorships__c where Id = :eSponsor.Id];
        //Event_Sponsorships__c eVerify2 = [SELECT Id, Purchased__c FROM Event_Sponsorships__c where Id = :eSponsor2.Id];
        System.assertEquals(false, eVerify1.Purchased__c);
        //System.assertEquals(true, eVerify2.Purchased__c);

        test.StopTest();
    }
    
}