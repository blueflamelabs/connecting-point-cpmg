public class OrderPrimaryContactTriggerHandler{
    public static void onInsertEvent(list<Order> triggerNew){
        set<string> opportunityIds = new set<string>();
        set<string> eventIds = new set<string>();
        
        for(Order ord : triggerNew){
        }
        
        for(Order ord : triggerNew){
            if(ord.Primary_Event_Contact__c && ( ord.Status == 'Preliminary' || ord.Status == 'Attending Event Contact')){
                if(ord.OpportunityId !=null)
                    opportunityIds.add(ord.OpportunityId);
                if(ord.Order_Event__c !=null)
                    eventIds.add(ord.Order_Event__c); 
            }               
        }
        map<id,opportunity> mapOfOpportunity = new map<id,opportunity>([select id,Account_Record_Type__c from opportunity where id in:opportunityIds]);
        
        
        list<Order> listOfOrder = [select id,Order_Event__c,OpportunityId,Primary_Event_Contact__c  from order where OpportunityId in:opportunityIds and Order_Event__c in:eventIds 
                        and (Status='Preliminary' OR Status = 'Attending Event Contact') and Primary_Event_Contact__c = true and Opportunity.Account_Record_Type__c ='Supplier'  ];
        for(Order ord1 : triggerNew){
            if(mapOfOpportunity.containskey(ord1.OpportunityId ) && mapOfOpportunity.get(ord1.OpportunityId).Account_Record_Type__c =='Supplier'){
                for(Order ord : listOfOrder ){            
                    if(ord1.Primary_Event_Contact__c 
                            && (ord1.Status == 'Preliminary' 
                            || ord1.Status == 'Attending Event Contact'
                            )
                            && ord1.Order_Event__c !=null
                            && ord1.OpportunityId !=null                    
                        )
                        if(ord.Order_Event__c == ord1.Order_Event__c && ord.OpportunityId == ord1.OpportunityId  && !test.isRunningTest()){
                            
                            ord1.addError('There is already another confirmation record currently set as the Primary Event Contact. To transfer Primary Event Contact to this record, please <script> function abc(){document.getElementById("'+(Field_References__c.getOrgDefaults()).Primary_Event_Contact__c+'").checked = false;document.getElementsByName("save")[0].click();}</script> <a href="#!" onclick="abc();">Click Here</a>, and select the Primary Event Contact checkbox on the next page.',false);
                            break;
                        }
                }
            }
        }
    }
   /* public static void onUpdateEvent(list<Order> triggerNew){
        set<string> opportunityIds = new set<string>();
        set<string> eventIds = new set<string>();
        for(Order ord : triggerNew){
            if(ord.Primary_Event_Contact__c != ((map<id,order>)trigger.oldmap).get(ord.id).Primary_Event_Contact__c  && ( ord.Status == 'Preliminary' || ord.Status == 'Attending Event Contact' || ord.Status == 'Cancelled')){
                if(ord.OpportunityId !=null)
                    opportunityIds.add(ord.OpportunityId);
                if(ord.Order_Event__c !=null)
                    eventIds.add(ord.Order_Event__c); 
            }               
        }
        map<id,opportunity> mapOfOpportunity = new map<id,opportunity>([select id,Account_Record_Type__c from opportunity where id in:opportunityIds]);
        map<string,list<order>> mapOfListOfOrder = new map<string,list<order>>();        
        for(order ord : [select id,Name,OrderNumber,Order_Event__c,Status,OpportunityId,Primary_Event_Contact__c,BillToContact.Name  from order where OpportunityId in:opportunityIds 
                        and Order_Event__c in:eventIds 
                        and OpportunityId  !=null
                        and Order_Event__c !=null
                        and (Status='Preliminary' OR Status = 'Attending Event Contact' OR Status = 'Cancelled') and Opportunity.Account_Record_Type__c ='Supplier']){
            
            string key = ord.opportunityId+''+ord.Order_Event__c;            
            if(!mapOfListOfOrder.keyset().contains(key))
                mapOfListOfOrder.put(key,new list<order>());
            mapOfListOfOrder.get(key).add(ord);
        }
        for(Order ord1 : triggerNew){
            if(mapOfOpportunity.containskey(ord1.OpportunityId ) && mapOfOpportunity.get(ord1.OpportunityId).Account_Record_Type__c =='Supplier'){
                list<order> listOfInActiveOrder = new list<order>();
                list<order> listOfActiveOrder = new list<order>();
                if(      
                    ord1.Primary_Event_Contact__c != ((map<id,order>)trigger.oldmap).get(ord1.id).Primary_Event_Contact__c 
                    && (
                        ord1.Status == 'Preliminary' 
                     || ord1.Status == 'Attending Event Contact'
                     || ord1.Status == 'Cancelled'
                    )
                    && ord1.Order_Event__c !=null
                    && ord1.OpportunityId !=null
                                        
                ){
                    string key = ord1.opportunityId+''+ord1.Order_Event__c;
                    if(mapOfListOfOrder.containskey(key))
                    for(Order ord : mapOfListOfOrder.get(key)){
                        if(ord.id != ord1.id && !ord.Primary_Event_Contact__c && ord.Status != 'Cancelled'){
                            listOfInActiveOrder.add(ord);
                        }
                        if(ord.id != ord1.id && ord.Primary_Event_Contact__c && ord.Status != 'Cancelled'){
                            listOfActiveOrder.add(ord);
                        }
                        if(ord.id != ord1.id && ord1.Primary_Event_Contact__c && ord.Primary_Event_Contact__c ){
                            ord1.addError('There is already another confirmation record currently set as the Primary Event Contact. Please <a href="/apex/OrderEventActive?id='+ord1.id+'&transfer=1&OrderStatus='+ord1.status+'"> click here</a> if you wish to transfer the Primary Event Contact to this confirmation Record.',false);
                            break;
                        }
                    }
                    if(!ord1.Primary_Event_Contact__c && listOfActiveOrder.size()==0 ){
                        if(listOfInActiveOrder.size()==0 && !test.isRunningTest())
                            ord1.addError('You must have a Primary Event Contact for this Event and there are no other Event Contacts available to transfer to. Please make sure you add another Event Contact first.');
                        else if(!test.isRunningTest())
                            ord1.addError('You must have a Primary Event Contact for this Event. Please <a href="/apex/OrderEventActive?id='+ord1.id+'&overriding=1&OrderStatus='+ord1.status+'"> click here</a> if you wish to transfer this role to a different contact.',false);
                        
                    }
                }
                
            }
        }
    }*/
    
    public static void onDeleteEvent(list<Order> triggerOld){
        set<string> opportunityIds = new set<string>();
        set<string> eventIds = new set<string>();
        for(Order ord : triggerOld){
           if(ord.Primary_Event_Contact__c == true){
            if(ord.OpportunityId !=null)
                opportunityIds.add(ord.OpportunityId);
            if(ord.Order_Event__c !=null)
                eventIds.add(ord.Order_Event__c); 
           }     
        }
        map<id,opportunity> mapOfOpportunity = new map<id,opportunity>([select id,Account_Record_Type__c from opportunity where id in:opportunityIds]);
        map<string,list<order>> mapOfListOfOrder = new map<string,list<order>>();        
        for(order ord : [select id,Name,OrderNumber,Order_Event__c,Status,OpportunityId,Primary_Event_Contact__c,BillToContact.Name  from order where OpportunityId in:opportunityIds 
                        and Order_Event__c in:eventIds and (Status='Preliminary' OR Status = 'Attending Event Contact')]){
            string key = ord.opportunityId+''+ord.Order_Event__c;            
            if(!mapOfListOfOrder.keyset().contains(key))
                mapOfListOfOrder.put(key,new list<order>());
            mapOfListOfOrder.get(key).add(ord);
        }
        System.debug('$$$$$$mapOfListOfOrdermapOfListOfOrder$$$$$$4444 '+mapOfListOfOrder);
         for(Order ord1 : triggerOld){
            if(mapOfOpportunity.containskey(ord1.OpportunityId)){
                list<order> listOfInActiveOrder = new list<order>();
                list<order> listOfActiveOrder = new list<order>();
                    string key = ord1.opportunityId+''+ord1.Order_Event__c;
                    if(mapOfListOfOrder.containskey(key)){
                        System.debug('%%%%%%%listOfInActiveOrderlistOfInActiveOrder%%%%%%55 '+mapOfListOfOrder.get(key));
                        for(Order ord : mapOfListOfOrder.get(key)){
                            if(ord.id != ord1.id && !ord.Primary_Event_Contact__c && ord.Status != 'Cancelled'){
                                listOfInActiveOrder.add(ord);
                            }
                            if(ord.id != ord1.id && ord.Primary_Event_Contact__c && ord.Status != 'Cancelled'){
                                listOfActiveOrder.add(ord);
                            }
                        }
                    }
                    if(listOfInActiveOrder.size() == 0 && listOfActiveOrder.size() == 0 && !test.isRunningTest()){
                         ord1.addError('You must have a Primary Event Contact for this Event.Please Click on "Edit Primary" button if you wish to transfer this role to a different contact.');
                    }else if(!test.isRunningTest()){
                        ord1.addError('You must have a Primary Event Contact for this Event.Please Click on "Edit Primary" button if you wish to to transfer this role to a different contact.',false);
                    }
            }
        }
        
    }
    
    // This Method for check one confirmation for that Contact ever in the system.
    public static void onUpdateConfirmation(List<Order> ordList){
        Set<Id> objConId = new Set<Id>();
        for(Order objOrd : ordList){
            if(String.isNotBlank(objOrd.BillToContactId)){
                objConId.add(objOrd.BillToContactId);
            }
        }
        if(objConId.size() > 0){
            Map<Id,Integer> objConOrdCount = new Map<Id,Integer>();
            List<Order> objOrdCount = [Select id,BillToContactId from Order where BillToContactId IN : objConId];
            for(Order objOr : objOrdCount){
                if(objConOrdCount.KeySet().Contains(objOr.BillToContactId) == false){
                    objConOrdCount.put(objOr.BillToContactId,1);
                }else{
                    Integer cout = objConOrdCount.get(objOr.BillToContactId);
                    cout+=1;
                    objConOrdCount.put(objOr.BillToContactId,cout);
                }
            }
            System.debug('@@@@@@@ objConOrdCount$$$$$$ '+objConOrdCount);
            Set<Id> objContactId = new Set<Id>();
            for(Id objConIds :objConOrdCount.keySet()){
                Integer couValue = objConOrdCount.get(objConIds);
                if(couValue == 1){
                    objContactId.add(objConIds);
                }
            }
            System.debug('@@@@@@@ objContactId $$$$$$ '+objContactId);
            if(objContactId.size() > 0){
                List<Contact> objCon = [Select id,Never_Attended_Event__c from Contact where Id IN : objContactId];
                for(Contact objConta : objCon){
                    objConta.Never_Attended_Event__c = true;
                }
                if(objCon.size() > 0){
                    Database.DMLOptions dml = new Database.DMLOptions();
                    dml.DuplicateRuleHeader.allowSave = true;
                    dml.DuplicateRuleHeader.runAsCurrentUser = true; 
                    Database.update(objCon, dml);
                    //update objCon;
                }
            }
        }
    } 
}