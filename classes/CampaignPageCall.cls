public class CampaignPageCall {
    public Boolean shouldRedirect {set;get;}
    public String redirectUrl{set;get;}
    public String ContactId{set;get;}
    public CampaignPageCall(ApexPages.StandardController controller) {
        List<Contact> conRec = [select id,isCheckPage__c from contact where id = :controller.getRecord().id];
         if(conRec.size() > 0){
             if(conRec[0].isCheckPage__c == true){
                 ContactId = conRec[0].id;
                 shouldRedirect = true;
                 redirectUrl = '/apex/ActiveCampaigns?Id='+conRec[0].id;
             }
         }
         
    }
}