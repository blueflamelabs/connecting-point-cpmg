public with sharing class EventUserCreationController {

    private final sObject mysObject;
    public Id eventId{get;set;}
    private UserAndEmailCreationProcess eventUserCreator {get;set;}
    Public String SuccessMessage{set;get;}
    public Boolean executiveErrorMessage{set;get;}
    Blob cryptoKey = Blob.valueOf(system.label.cryptoKey);
    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public EventUserCreationController(ApexPages.StandardController stdController) {
        this.mysObject = (sObject)stdController.getRecord();
        this.eventId = (Id)stdController.getId();
    }

    public PageReference executiveUserCreationProcess(){
        Product2 objProduct = (Product2)mysObject;
        Product2 objPro = [select User_Link_Expiration_for_Executives__c,IsActive,Id,Executive_Users_Created__c,Supplier_Users_Created__c,Allow_Welcome_Emails_for_Suppliers__c,Allow_Welcome_Emails_for_Executives__c  from product2 where id =:objProduct.id];
        
        if(objPro.IsActive == false){
            SuccessMessage  = 'You cannot create users or send welcome emails until you activate the event';
        }else if(objPro.Executive_Users_Created__c){
            SuccessMessage = 'Users and Welcome Emails have already been sent. You may only do this once per CPMG Event';
        }else if(objPro.User_Link_Expiration_for_Executives__c == null){
            SuccessMessage = 'You may not create users without populating the User Link Expiration Date, please set this field and try again.';
        }else{
            this.eventUserCreator = new UserAndEmailCreationProcess(objProduct.id,'Executive');
            Product2 objProdu = [select Id,Executive_Users_Created__c,Supplier_Users_Created__c,Allow_Welcome_Emails_for_Suppliers__c,Allow_Welcome_Emails_for_Executives__c  from product2 where id =:objProduct.id];
            if(!objProdu.Allow_Welcome_Emails_for_Executives__c)
                SuccessMessage = 'Users successfully Created';
            else
                SuccessMessage = 'Users successfully created and welcome emails sent';
        } 
        return null; 
    }
    public PageReference supplierUserCreationProcess(){
        Product2 objProduct = (Product2)mysObject;
        Product2 objPro = [select User_Link_Expiration_for_Suppliers__c,IsActive,Id,Executive_Users_Created__c,Supplier_Users_Created__c,Allow_Welcome_Emails_for_Suppliers__c,Allow_Welcome_Emails_for_Executives__c  from product2 where id =:objProduct.id];
       
        if(objPro.IsActive == false){
            SuccessMessage  = 'You cannot create users or send welcome emails until you activate the event';       
        }else if(objPro.Supplier_Users_Created__c){
            SuccessMessage = 'Users and Welcome Emails have already been sent. You may only do this once per CPMG Event';
        }else if(objPro.User_Link_Expiration_for_Suppliers__c == null){
            SuccessMessage = 'You may not create users without populating the User Link Expiration Date, please set this field and try again.';
        }else{
            this.eventUserCreator = new UserAndEmailCreationProcess(objProduct.id,'Supplier');
            Product2 objProdu = [select Id,Executive_Users_Created__c,Supplier_Users_Created__c,Allow_Welcome_Emails_for_Suppliers__c,Allow_Welcome_Emails_for_Executives__c  from product2 where id =:objProduct.id];
            if(!objProdu.Allow_Welcome_Emails_for_Suppliers__c)
                SuccessMessage = 'Users successfully Created';
            else
                SuccessMessage = 'Users successfully created and welcome emails sent';
         
         }
        return null;    
    }
   public PageReference ResendMailndUserCreationProcess(){
       boolean isCheck = false;
       boolean isUserCreate = false;
       Order objorder = [select BillToContactId,Order_Event__r.User_Link_Expiration_for_Suppliers__c,Order_Event__r.User_Link_Expiration_for_Executives__c,Order_Event__r.IsActive,id,Order_Event__r.Executive_Users_Created__c,Order_Event__r.Supplier_Users_Created__c,Order_Event__r.Allow_Welcome_Emails_for_Executives__c,Order_Event__r.Allow_Welcome_Emails_for_Suppliers__c,Opportunity.Record_Type__c from order where id =: eventId];
       List<User> objuser = [SELECT id, ContactId, UserName, Email,LastLoginDate FROM User where contactId =: objorder.BillToContactId];
       if(objuser.size() > 0)
           isUserCreate = true;
       if(objorder.Order_Event__r.IsActive == false){
           isCheck = true;
           SuccessMessage = 'You cannot create users or send welcome emails until you activate the event';
       }
       if(objorder.Opportunity.Record_Type__c == 'Executive'){ 
           if(!objorder.Order_Event__r.Executive_Users_Created__c){
               isCheck = true;
               SuccessMessage = 'You cannot Resend Email because the initial Emails for the event have not been sent yet.';
           }else if(!objorder.Order_Event__r.Allow_Welcome_Emails_for_Executives__c){
               isCheck = true;
               SuccessMessage = 'You cannot Resend Email because the automated Welcome Email is not allowed on this Event';
           }   
       }
       if(objorder.Opportunity.Record_Type__c == 'Supplier'){
           if(!objorder.Order_Event__r.Supplier_Users_Created__c){
               isCheck = true;
               SuccessMessage = 'You cannot Resend Email because the initial Emails for the event have not been sent yet.';
           }    
           else if(!objorder.Order_Event__r.Allow_Welcome_Emails_for_Suppliers__c){
               isCheck = true;
               SuccessMessage = 'You cannot Resend Email because the automated Welcome Email is not allowed on this Event';
           }      
       }
       if(isCheck == false || test.isrunningtest()){
            this.eventUserCreator = new UserAndEmailCreationProcess(eventId);
            if(isUserCreate == false)
                  SuccessMessage = 'Users successfully created and welcome Email sent'; 
              else
                  SuccessMessage = 'Welcome Email Sent';
        }
        return null;
   } 
   
    public PageReference RegenerateLinkMailndUserCreationProcess(){
       String SetUserPassword = Label.ChangePassword;
       String LoginUserPage = Label.LoginPage;
       String UserInfo='';
       boolean isCheck = false;
       boolean isUserCreate = false;
       Order objorder = [select Opportunity.Record_Type__c,Order_Event__r.IsActive,Event_Support__c,Order_Event__r.Executive_Users_Created__c,Order_Event__r.Supplier_Users_Created__c,BillToContactId,Welcome_Email_Link_Expiration__c,Order_Event__r.User_Link_Expiration_for_Suppliers__c,Order_Event__r.User_Link_Expiration_for_Executives__c,Opportunity.Account_Record_Type__c,status from order where id =: eventId];
       List<User> objuserCreate = [SELECT id, ContactId, UserName, Email,LastLoginDate FROM User where contactId =: objorder.BillToContactId];
       if(objuserCreate.size() > 0)
           isUserCreate = true;
       if(objorder.Order_Event__r.IsActive == false){
               isCheck = true;
               SuccessMessage = 'You cannot create users until you activate the event.';
       }
      if(objorder.Opportunity.Record_Type__c == 'Executive' && objorder.Order_Event__r.Executive_Users_Created__c == false){ 
              isCheck = true;
              SuccessMessage = 'You cannot generate link because the users have not been created for the event yet.';
       }
      else if(objorder.Opportunity.Record_Type__c == 'Supplier' && objorder.Order_Event__r.Supplier_Users_Created__c == false){
               isCheck = true;
               SuccessMessage = 'You cannot generate link because the users have not been created for the event yet.';
      }
      else if(objorder.Opportunity.Account_Record_Type__c == 'Executive' && objorder.Order_Event__r.User_Link_Expiration_for_Executives__c == null){
           isCheck = true;
           SuccessMessage = 'You may not generate the link without populating the User Link Expiration Date, please set this field and try again.';
      }
      else if(objorder.Opportunity.Account_Record_Type__c == 'Supplier' && objorder.Order_Event__r.User_Link_Expiration_for_Suppliers__c == null){
           isCheck = true;
           SuccessMessage = 'You may not generate the link without populating the User Link Expiration Date, please set this field and try again.';
      }  
     if(isCheck == false){
         this.eventUserCreator = new UserAndEmailCreationProcess(eventId,true);
         if(isUserCreate == false)
             SuccessMessage = 'Users successfully created and link has been generated successfully.'; 
          else
             SuccessMessage = 'Link has been generated successfully';  
      } 
       return null;
   } 
}