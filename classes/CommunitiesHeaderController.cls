public with sharing class CommunitiesHeaderController {
    
    //private PageReference pr {get;set;}
    public Product2 passedEvent {get;set;}
    

    /*public CommunitiesHeaderController(){
        
    } */
    public HeaderWrapper getHead() {
    try{
        return new HeaderWrapper();
        }catch(exception ex){
            System.debug('Error: '+ ex.getMessage()+'. '+ ex.getStackTraceString());
            return null;
        }
    }

    public class HeaderWrapper {
        //public Community_Settings__c commSettings {get;set;}
        //public String header_logo { get; set; }
        public String start_end_dates { get; set; }
        public String venue { get; set; }
        public String venueUrl { get; set; }
        public String location { get; set; }
        public Boolean use_default_logo { get; set; }
        public String nav_color { get; set; }
        public String event_year {get;set;}
        //public String logo_path {get;set;}
        public String LinkedIn {get;set;}
        public String Twitter {get;set;}
        //public String header_path {get;set;}
        public String header_date_range {get;set;}
        public ProductWrapper eventWrapper {get;set;}


        public HeaderWrapper() {
           
            start_end_dates = 'Passionate People Inspiring Growth';
            use_default_logo = true;
            
            try {
                Id event_id_param = ApexPages.currentPage().getParameters().get('eId');
                Product2 target_product = NULL;
                //target_product = NULL;
                if (event_id_param != null) {
                    target_product = EventHelper.getEvent(event_id_param);

                } else {
                    Id userId = UserInfo.getUserId();
                    User currentUser = [SELECT Id, Name, ContactId, Contact.RecordTypeId FROM User where Id = :userId];
                    if(currentUser.contactId != null){
                      
                        List<Order> orders = [SELECT Id, Order_Event__c FROM Order WHERE (Order_Event__r.Start_Date__c >= Today AND BillToContactId =:currentUser.ContactId and Status != 'Cancelled') ORDER BY Order_Event__r.Start_Date__c ASC LIMIT 1];
                        if (orders.size() > 0) {
                            Id pId = orders.get(0).Order_Event__c;
                            target_product = EventHelper.getEvent(pId);
                        }
                    } else {
                        target_product = EventHelper.getClosestEventByDate();
                        //[SELECT Id, EventHeaderLogo__c, Event_Navigation_Color__c, Venue__c, Venue_Url__c, Start_Date__c, End_Date__c, Location__c,Year__c, Twitter__c, LinkedIn__c, Header_Date_Range__c
                          // FROM Product2 WHERE Start_Date__c >= Today ORDER BY Start_Date__c ASC LIMIT 1]; 

                    } 
                }
                    if (target_product != NULL) {
                        set<string> setStaticResource = new set<string>();
                        for(StaticResource  sr: [Select id, name from StaticResource where Name like 'CPMG_Events_Page%'
                            OR Name like 'CPMG_Events_Page_Hotel%'
                            OR Name like 'CPMG_Header_Transparent%'
                        ]){
                            setStaticResource.add(sr.name);
                        }
                        
                    eventWrapper = new ProductWrapper(target_Product);
                    
                      if(!setStaticResource.contains(eventWrapper.CPMG_Events_Page_Label)){
                          eventWrapper.isCPMG_Events_Page_Label  = false;
                      }
                      if(!setStaticResource.contains(eventWrapper.CPMG_Events_Page_Hotel_Label )){
                          eventWrapper.isCPMG_Events_Page_Hotel_Label  = false;
                      }
                      if(!setStaticResource.contains(eventWrapper.CPMG_Header_Transparent_Label )){
                          eventWrapper.isCPMG_Header_Transparent_Label  = false;
                      }
                    header_date_range = target_product.Header_Date_Range__c;
                    nav_color = target_product.Event_Navigation_Color__c;
                    event_year=target_product.Year__c;
                    LinkedIn = target_product.LinkedIn__c;
                    Twitter = target_product.Twitter__c;
                   
                    venue = target_product.Venue__c;
                    venueUrl = target_product.Venue_Url__c;
                    location = target_product.Location__c;
                
                    use_default_logo = false; 
                }
            }
            catch (Exception e) {
                System.debug('Error: '+e.getMessage() +'Stack Trace: '+ e.getStackTraceString());
            }
        }
    }

    /*public PageReference determineNavigation(){
        pr = new PageReference('/'+commSettings.Community_name__c+header_path);
        pr.setRedirect(true);
        //pr.getParameters(eId,)
    } */
}