@isTest
private with sharing class ProfilePageController_Test {
    
    static testMethod void buildProfileWrappers_Test(){
        TestDataGenerator.createInactiveTriggerSettings();
        Id execAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Account a = TestDataGenerator.createTestAccount(true, 'Test Account',execAccRtype);
        Id execConRtype =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Contact c = TestDataGenerator.createTestContact(true,'Test','Last Name','testAcc@account.com',a,execConRtype);
        
        Id suppAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Account suppAcc = TestDataGenerator.createTestAccount(true, 'Test Account Supplier',suppAccRtype);
        Account suppAcc2 = TestDataGenerator.createTestAccount(true, 'Test Account Supplier',suppAccRtype);

        Id suppConRtype =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Contact suppCon = TestDataGenerator.createTestContact(true,'Test','Last Name','testAccSupp@account.com',suppAcc,suppConRtype);
        Contact suppCon2 = TestDataGenerator.createTestContact(true,'Test','Last Name','testAccSupp2@account.com',suppAcc2,suppConRtype);

        Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        
        User execCommUser = TestDataGenerator.CreateUser(true,commUserProfId.Id,'Test','Exec',null,'testExecUser@cpmg.com',suppCon.Id);
        
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event Active',true,Date.today().addDays(40),Date.today().addDays(45),Date.today().addMonths(-2), Date.today().addMonths(-1));

        Profile standUserProfId = [SELECT ID FROM Profile where Name ='Standard User' LIMIT 1];
        
        User execUser = TestDataGenerator.CreateUser(true,standUserProfId.Id,'Test','Exec',null,'testExec1234@cpmg.com',null);
        User supplierUser = TestDataGenerator.CreateUser(true, standUserProfId.Id,'Test','Supplier',null,'testSupplier1234@cpmg.com',null);

        Opportunity execOpp = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),a,'Closed Won',prod);
        Opportunity suppOpp1 = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),suppAcc,'Closed Won',prod);
        Opportunity suppOpp2= TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),suppAcc2,'Closed Won',prod);

        Id execPRRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c execPRecord = TestDataGenerator.createParticipationRecord(true,prod,a,execOpp,c,execPRRType);

        Id supplierPRRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Suppliers').getRecordTypeId();
        Event_Account_Details__c suppPRecord1 = TestDataGenerator.createParticipationRecord(true,prod,suppAcc,suppOpp1,suppCon,execPRRType);
        Event_Account_Details__c suppPRecord2 = TestDataGenerator.createParticipationRecord(true,prod,suppAcc2,suppOpp2,suppCon2,execPRRType);
        
        Event_Account_Details__c suppPRecord3 = new Event_Account_Details__c();
        suppPRecord3.Account__c = suppAcc.id;
        suppPRecord3.Opportunity__c = suppOpp1.id;
        suppPRecord3.Event__c = prod.id;
        suppPRecord3.Primary_Contact__c = suppCon.id;
        suppPRecord3.RecordTypeId = supplierPRRType;
        suppPRecord3.Company_Display_Name__c = 'test';
        insert suppPRecord3;
        
        Order execConf = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',execOpp,execOpp.CloseDate,execPRecord,prod,true);
        Order suppConf1 = TestDataGenerator.createConfirmation(false,suppAcc,suppCon,'Attending Event Contact',suppOpp1,execOpp.CloseDate,suppPRecord1,prod,true);
        Order suppConf2 = TestDataGenerator.createConfirmation(true,suppAcc2,suppCon2,'Attending Event Contact',suppOpp2,execOpp.CloseDate,suppPRecord2,prod,true);
        Order suppConf3 = TestDataGenerator.createConfirmation(true,suppAcc,suppCon,'Attending Event Contact',suppOpp1,execOpp.CloseDate,suppPRecord3,prod,true);

        PageReference pageRef = Page.profilePage;
        pageRef.getParameters().put('eId', prod.Id);
        pageRef.getParameters().put('prId', suppPRecord1.Id);
        pageRef.getParameters().put('currentPrId', suppPRecord1.Id);
        

        //Create Question
        Map<String,RecordTypeInfo> questionRTypeMap = Schema.SObjectType.Question__c.getRecordTypeInfosByName();
        Question__c picklistQuestion = TestDataGenerator.createQuestion(true,'Test Category Pickilist',200,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
            Question_Answers_Values__c qav1Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option','Select');
            Question_Answers_Values__c qav2Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option2','Select2');
            Question_Answers_Values__c qav3Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option3','Select3');
        Question__c longQuestion = TestDataGenerator.createQuestion(true,'Long Description',2000,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
        Question__c shortQuestion = TestDataGenerator.createQuestion(true,'Short Description',200,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
            
        //Create the 3 Event Questions
        Event_Question__c categoryEQ = TestDataGenerator.createEventQuestion(true,'Category',prod,'Category',picklistQuestion,true,1,null);
        Event_Question__c longEQ = TestDataGenerator.createEventQuestion(true,'Company Long Description',prod,'Company',longQuestion,true,1,'Company Long Description');
        Event_Question__c shortEQ = TestDataGenerator.createEventQuestion(true,'Company Short Description',prod,'Company',shortQuestion,true,1,'Company Short Description');

        //Create Contact Answer Questions, Executive
        Contact_Event_Answers__c categoryAnswer = TestDataGenerator.createQuestionAnswers(true,execConf,execPRecord,categoryEQ,c,'Select2;Select3',null,null);
        Contact_Event_Answers__c longAnswer = TestDataGenerator.createQuestionAnswers(true,execConf,execPRecord,longEQ,c,'Long Answer',null,null);
        Contact_Event_Answers__c shortAnswer = TestDataGenerator.createQuestionAnswers(true,execConf,execPRecord,shortEQ,c,'Short Answer',null,null);
        //Suppliers
        Contact_Event_Answers__c categoryAnswerSupp1 = TestDataGenerator.createQuestionAnswers(true,suppConf1,suppPRecord1,categoryEQ,c,'Select2;Select',null,null);
        Contact_Event_Answers__c longAnswerSupp1 = TestDataGenerator.createQuestionAnswers(true,suppConf1,suppPRecord1,longEQ,c,'Long Answer Supp 1',null,null);
        Contact_Event_Answers__c shortAnswerSupp1 = TestDataGenerator.createQuestionAnswers(true,suppConf1,suppPRecord1,shortEQ,c,'Short Answer Supp 1',null,null);
        //Supplier 2 
        Contact_Event_Answers__c categoryAnswerSupp2 = TestDataGenerator.createQuestionAnswers(true,suppConf2,suppPRecord2,categoryEQ,c,'Select;Select3',null,null);
        Contact_Event_Answers__c longAnswerSupp2 = TestDataGenerator.createQuestionAnswers(true,suppConf2,suppPRecord2,longEQ,c,'Long Answer Supp 2',null,null);
        Contact_Event_Answers__c shortAnswerSupp2 = TestDataGenerator.createQuestionAnswers(true,suppConf2,suppPRecord2,shortEQ,c,'Short Answer Supp 2',null,null);
        
        Contact_Event_Answers__c categoryAnswerSupp3 = TestDataGenerator.createQuestionAnswers(true,suppConf3,suppPRecord3,categoryEQ,suppCon,'Select;Select3',null,null);
        Contact_Event_Answers__c longAnswerSupp3 = TestDataGenerator.createQuestionAnswers(true,suppConf3,suppPRecord3,longEQ,suppCon,'Long Answer Supp 2',null,null);
        Contact_Event_Answers__c shortAnswerSupp3 = TestDataGenerator.createQuestionAnswers(true,suppConf3,suppPRecord3,shortEQ,suppCon,'Short Answer Supp 2',null,null);



        ProfilePageController  mreqController = new ProfilePageController();
        test.startTest();
            Test.setCurrentPageReference(pageRef);
            System.runAs(execCommUser){
                mreqController.buildProfileWrappers(prod.Id, execPRecord.Id);
            }
        test.stopTest();
    }

    static testMethod void profilePageController_Constructor_Test(){
        TestDataGenerator.createInactiveTriggerSettings();
        Id execAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Account a = TestDataGenerator.createTestAccount(true, 'Test Account',execAccRtype);
        Id execConRtype =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Contact c = TestDataGenerator.createTestContact(true,'Test','Last Name','testAcc@account.com',a,execConRtype);

        Id suppAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Account suppAcc = TestDataGenerator.createTestAccount(true, 'Test Account Supplier',suppAccRtype);
        Account suppAcc2 = TestDataGenerator.createTestAccount(true, 'Test Account Supplier',suppAccRtype);

        Id suppConRtype =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Contact suppCon = TestDataGenerator.createTestContact(true,'Test','Last Name','testAccSupp@account.com',suppAcc,suppConRtype);
        Contact suppCon2 = TestDataGenerator.createTestContact(true,'Test','Last Name','testAccSupp2@account.com',suppAcc2,suppConRtype);

        Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        
        User execCommUser = TestDataGenerator.CreateUser(true,commUserProfId.Id,'Test','Exec',null,'testExecUser@cpmg.com',c.Id);
        
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event Active',true,Date.today().addDays(40),Date.today().addDays(45),Date.today().addMonths(-2), Date.today().addMonths(-1));

        Profile standUserProfId = [SELECT ID FROM Profile where Name ='Standard User' LIMIT 1];
        
        User execUser = TestDataGenerator.CreateUser(true,standUserProfId.Id,'Test','Exec',null,'testExec1234@cpmg.com',null);
        User supplierUser = TestDataGenerator.CreateUser(true, standUserProfId.Id,'Test','Supplier',null,'testSupplier1234@cpmg.com',null);

        Opportunity execOpp = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),a,'Closed Won',prod);
        Opportunity suppOpp1 = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),suppAcc,'Closed Won',prod);
        Opportunity suppOpp2= TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),suppAcc2,'Closed Won',prod);

        Id execPRRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c execPRecord = TestDataGenerator.createParticipationRecord(true,prod,a,execOpp,c,execPRRType);

        Id supplierPRRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Suppliers').getRecordTypeId();
        Event_Account_Details__c suppPRecord1 = TestDataGenerator.createParticipationRecord(true,prod,suppAcc,suppOpp1,suppCon,execPRRType);
        Event_Account_Details__c suppPRecord2 = TestDataGenerator.createParticipationRecord(true,prod,suppAcc2,suppOpp2,suppCon2,execPRRType);

        Order execConf = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',execOpp,execOpp.CloseDate,execPRecord,prod,true);
        Order suppConf1 = TestDataGenerator.createConfirmation(true,suppAcc,suppCon,'Attending Event Contact',suppOpp1,execOpp.CloseDate,suppPRecord1,prod,true);
        Order suppConf2 = TestDataGenerator.createConfirmation(true,suppAcc2,suppCon2,'Attending Event Contact',suppOpp2,execOpp.CloseDate,suppPRecord2,prod,true);

        PageReference pageRef = Page.profilePage;
        pageRef.getParameters().put('eId', prod.Id);
        pageRef.getParameters().put('prId', suppPRecord1.Id);
        pageRef.getParameters().put('currentPrId', suppPRecord1.Id);
        

        //Create Question
        Map<String,RecordTypeInfo> questionRTypeMap = Schema.SObjectType.Question__c.getRecordTypeInfosByName();
        Question__c picklistQuestion = TestDataGenerator.createQuestion(true,'Test Category Pickilist',200,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
            Question_Answers_Values__c qav1Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option','Select');
            Question_Answers_Values__c qav2Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option2','Select2');
            Question_Answers_Values__c qav3Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option3','Select3');
        Question__c longQuestion = TestDataGenerator.createQuestion(true,'Long Description',2000,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
        Question__c shortQuestion = TestDataGenerator.createQuestion(true,'Short Description',200,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
            
        //Create the 3 Event Questions
        Event_Question__c categoryEQ = TestDataGenerator.createEventQuestion(true,'Category',prod,'Category',picklistQuestion,true,1,null);
        Event_Question__c longEQ = TestDataGenerator.createEventQuestion(true,'Company Long Description',prod,'Company',longQuestion,true,1,'Company Long Description');
        Event_Question__c shortEQ = TestDataGenerator.createEventQuestion(true,'Company Short Description',prod,'Company',shortQuestion,true,1,'Company Short Description');

        //Create Contact Answer Questions, Executive
        Contact_Event_Answers__c categoryAnswer = TestDataGenerator.createQuestionAnswers(true,execConf,execPRecord,categoryEQ,c,'Select2;Select3',null,null);
        Contact_Event_Answers__c longAnswer = TestDataGenerator.createQuestionAnswers(true,execConf,execPRecord,longEQ,c,'Long Answer',null,null);
        Contact_Event_Answers__c shortAnswer = TestDataGenerator.createQuestionAnswers(true,execConf,execPRecord,shortEQ,c,'Short Answer',null,null);
        //Suppliers
        Contact_Event_Answers__c categoryAnswerSupp1 = TestDataGenerator.createQuestionAnswers(true,suppConf1,suppPRecord1,categoryEQ,c,'Select2;Select',null,null);
        Contact_Event_Answers__c longAnswerSupp1 = TestDataGenerator.createQuestionAnswers(true,suppConf1,suppPRecord1,longEQ,c,'Long Answer Supp 1',null,null);
        Contact_Event_Answers__c shortAnswerSupp1 = TestDataGenerator.createQuestionAnswers(true,suppConf1,suppPRecord1,shortEQ,c,'Short Answer Supp 1',null,null);
        //Supplier 2 
        Contact_Event_Answers__c categoryAnswerSupp2 = TestDataGenerator.createQuestionAnswers(true,suppConf2,suppPRecord2,categoryEQ,c,'Select;Select3',null,null);
        Contact_Event_Answers__c longAnswerSupp2 = TestDataGenerator.createQuestionAnswers(true,suppConf2,suppPRecord2,longEQ,c,'Long Answer Supp 2',null,null);
        Contact_Event_Answers__c shortAnswerSupp2 = TestDataGenerator.createQuestionAnswers(true,suppConf2,suppPRecord2,shortEQ,c,'Short Answer Supp 2',null,null);



        
        test.startTest();
            Test.setCurrentPageReference(pageRef);
            System.runAs(execCommUser){
                ProfilePageController  mreqController = new ProfilePageController();
            }
        test.stopTest();
    }
}