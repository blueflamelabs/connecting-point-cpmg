@isTest
private class emailPdfReport_Test {

    @isTest
    private static void emailPdfReport_Instantiate_Test() {
        TestDataGenerator.createInactiveTriggerSettings();
        Community_Settings__c commSett = TestDataGenerator.createCommSettings();

        //AccountFormSettings
        TestDataGenerator.createAccountFormSettings();

        //Contact Form Settings:
        TestDataGenerator.createContactFormSettings();

        //Order Form Display settings
        TestDataGenerator.createOrderFormSettings();

        //Registration Settings
        TestDataGenerator.createRegistrationSettings();

        //Need the Order Status Custom Setting
        TestDataGenerator.createOrderStatusCustomSetting();
        
        Id execAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Account a = TestDataGenerator.createTestAccount(true, 'Test Account',execAccRtype);
        Id execConRtype =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Contact c1 = TestDataGenerator.createTestContact(true,'Test1','Last Name1','testAcc@account.com',a,execConRtype);
        Contact c = TestDataGenerator.createTestContact(true,'Test','Last Name','testAc2c@account.com',a,execConRtype);
       

        Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        
        User execCommUser = TestDataGenerator.CreateUser(true,commUserProfId.Id,'Test','Exec',null,'testExecUser@cpmg.com',c.Id);
        
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event Active',true,Date.today().addDays(40),Date.today().addDays(45),Date.today().addMonths(-2), Date.today().addMonths(-1));

        Profile standUserProfId = [SELECT ID FROM Profile where Name ='Standard User' LIMIT 1];
        
        User execUser = TestDataGenerator.CreateUser(true,standUserProfId.Id,'Test','Exec',null,'testExec1234@cpmg.com',null);
        User supplierUser = TestDataGenerator.CreateUser(true, standUserProfId.Id,'Test','Supplier',null,'testSupplier1234@cpmg.com',null);

        Event_Staff__c execStaff = TestDataGenerator.createEventStaff(true,prod,execUser,'Recruiter');
        Event_Staff__c supplierStaff = TestDataGenerator.createEventStaff(true,prod,supplierUser,'Salesperson');


        Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Event Active Opp',Date.today().addMonths(-4),a,'Closed Won',prod,'Renewal');
      
        Id execPartRecordType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c partRecord = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c, execPartRecordType);

        Order conf = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod,true);
        List<Order> ordersJustCreated = new List<Order>{conf};

        Document d1 = TestDataGenerator.createDocument('test1','text/plain','test1');
        Document d2 = TestDataGenerator.createDocument('test2','text/plain','test2');
        Document d3 = TestDataGenerator.createDocument('test3','text/plain','test3');
        Document d4 = TestDataGenerator.createDocument('test4','text/plain','test4');

        Image__c img1 = TestDataGenerator.createImage(true,a,c,conf,prod,(String) d1.Id,partRecord,true,false,true);
        Image__c img2 = TestDataGenerator.createImage(true,a,c,conf,prod,(String) d2.Id,partRecord,true,false,true);
        Image__c img3 = TestDataGenerator.createImage(true,a,c,conf,prod,(String) d3.Id,partRecord,true,false,true);
        Image__c img4= TestDataGenerator.createImage(true,a,c,conf,prod,(String) d4.Id,partRecord,true,false,true);

        Map<String,RecordTypeInfo> questionRTypeMap = Schema.SObjectType.Question__c.getRecordTypeInfosByName();
        Question__c picklistQuestion = TestDataGenerator.createQuestion(true,'Test Pickilist',200,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),false,false);
            Question_Answers_Values__c qav1Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option','Select');
            Question_Answers_Values__c qav2Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option2','Select2');
            Question_Answers_Values__c qav3Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option3','Select3');
        Question__c mulitPicklistQuestion = TestDataGenerator.createQuestion(true,'Test Multi Pickilist',200,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
            Question_Answers_Values__c qav4Picklist = TestDataGenerator.createQuestionOption(true,mulitPicklistQuestion,'Select Option4','Select4');
            Question_Answers_Values__c qav5Picklist = TestDataGenerator.createQuestionOption(true,mulitPicklistQuestion,'Select Option5','Select5');
            Question_Answers_Values__c qav6Picklist = TestDataGenerator.createQuestionOption(true,mulitPicklistQuestion,'Select Option6','Select6');
        //Supplier
        Question__c supplierQuestion = TestDataGenerator.createQuestion(true,'Test Supplier Question',2000,false,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);

        //Executive Golf
        Question__c execActivityQuestion = TestDataGenerator.createQuestion(true,'Test Golf Pickilist',200,true,'Activity',questionRTypeMap.get('Question').getRecordTypeId(),false,true);
        
        //Create Event Questions
        Event_Question__c eQuestion = TestDataGenerator.createEventQuestion(true,'The Picklist Event Quest',prod,'Category',mulitPicklistQuestion,true,null,null);

        UserPermissions uPermissions = new UserPermissions('Executive',true);
        uPermissions.getEventStaff(prod.Id);
         
        
        BrandTemplate  objBrandTem = [select id,name from BrandTemplate limit 1];
        EmailTemplate et = new EmailTemplate();  
        et.isActive = true;  
        et.Name = 'Registration_Summary_PDF1';  
        et.DeveloperName = 'Registration_Summary_PDF1';  
        et.TemplateType = 'HTML'; 
        et.FolderId = execCommUser.id; 
        et.BrandTemplateId = objBrandTem.id;
        et.TemplateStyle = 'Products';
        et.Body = 'Hi {!Contact.FirstName},A member of the team has updated Registration. The most recent edits were submitted by {!CurrentContact.Name} Please see attached Registration Summary.Link To Registration Summary'; 
       System.runAs(execCommUser){ 
        insert et;
       }
        PageReference pageRef = Page.pdf_RegistrationPage;
        pageRef.getParameters().put('eId', prod.Id);
        
        Test.startTest();
            System.runAs(execCommUser){
            Test.setCurrentPageReference(pageRef);
            List<Contact> objConList = new List<Contact>();
            objConList.Add(c1);
            Set<String> objPrimaryMailId = new Set<String>();
            emailPdfReport pdfReport = new EmailPdfReport(prod,c,Site.getBaseUrl()+'apex/pdf_RegistrationPage?', uPermissions,null,objConList,objPrimaryMailId);
            pdfReport.sendEmailForEvent();
            pdfReport.sendMeetingRequestEmail();
            }
        Test.stopTest();

    }

    static testMethod void sendMeetingRequestEmail_Test(){

        Id execAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Account a = TestDataGenerator.createTestAccount(true, 'Test Account',execAccRtype);
        Id execConRtype =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Contact c = TestDataGenerator.createTestContact(true,'Test','Last Name','testAcc@account.com',a,execConRtype);

        Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        
        User execCommUser = TestDataGenerator.CreateUser(true,commUserProfId.Id,'Test','Exec',null,'testExecUser@cpmg.com',c.Id);
        
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event Active',true,Date.today().addDays(40),Date.today().addDays(45),Date.today().addMonths(-2), Date.today().addMonths(-1));

        Profile standUserProfId = [SELECT ID FROM Profile where Name ='Standard User' LIMIT 1];
        
        User execUser = TestDataGenerator.CreateUser(true,standUserProfId.Id,'Test','Exec',null,'testExec1234@cpmg.com',null);
        User supplierUser = TestDataGenerator.CreateUser(true, standUserProfId.Id,'Test','Supplier',null,'testSupplier1234@cpmg.com',null);

        Event_Staff__c execStaff = TestDataGenerator.createEventStaff(true,prod,execUser,'Recruiter');
        Event_Staff__c supplierStaff = TestDataGenerator.createEventStaff(true,prod,supplierUser,'Salesperson');


        Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Event Active Opp',Date.today().addMonths(-4),a,'Closed Won',prod,'Renewal');

        Id execPartRecordType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c partRecord = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c, execPartRecordType);

        UserPermissions uPermissions = new UserPermissions('Executive',true);
        uPermissions.getEventStaff(prod.Id);

        //PageReference pageRef = Page.pdf_MeetingRequestConfirmation;

        Test.startTest();
        System.runAs(execCommUser){
            List<Contact> objConList = new List<Contact>();
            Set<String> objPrimaryMailId = new Set<String>();
            emailPdfReport pdfReport = new EmailPdfReport(prod,c,Site.getBaseUrl()+'apex/pdf_MeetingRequestConfirmation?', uPermissions,null,objConList,objPrimaryMailId);
            pdfReport.sendMeetingRequestEmail();
        }
        Test.stopTest();

    }
}