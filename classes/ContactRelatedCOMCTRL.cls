public class ContactRelatedCOMCTRL {
	@AuraEnabled 
    public static String showContactRecord(Id AcctId){
        Account objAcc = [Select id,RecordType.Name from Account where Id =:AcctId];
        System.debug('######RecordType.Name#objAcc####333 '+objAcc.RecordType.Name);
        String ContactRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(objAcc.RecordType.Name).getRecordTypeId();
        return ContactRecordTypeId;
    }
}