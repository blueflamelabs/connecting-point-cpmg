public class OpportunityRelatedCOMCTRL {
	@AuraEnabled 
    public static String showContactRecord(Id ContactsId){
        Contact objCon = [Select id,AccountId,RecordType.Name,Account.Name,Name from Contact where Id =:ContactsId];
        System.debug('######RecordType.Name#objAcc####333 '+objCon.RecordType.Name);
        String ContactRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(objCon.RecordType.Name).getRecordTypeId();
        String oppName = objCon.Account.Name+' - '+objCon.Name;
        String ConRecord = ContactRecordTypeId+','+objCon.AccountId+','+oppName+','+objCon.RecordType.Name;
        return ConRecord;
    }
}