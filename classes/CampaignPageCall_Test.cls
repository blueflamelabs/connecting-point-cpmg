@isTest
public class CampaignPageCall_Test{
        public static testMethod void CampaignPageCallTest(){
            Id execAccountRtype = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
            Id execConRtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
            Account a = TestDataGenerator.createTestAccount(true,'Test Account',execAccountRtype);
        
            Contact c = TestDataGenerator.createTestContact(true,'Test','Exec','t@t.com',a,(String) execConRtype);
            c.isCheckPage__c = true;
            update c;
            
            Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event',true,Date.today().addDays(2), Date.today().addDays(5), Date.today().addMonths(-3), Date.today().addMonths(-2));
            
            ApexPages.currentPage().getParameters().put('Id',c.id);
            ApexPages.StandardController sc = new ApexPages.StandardController(c);
            CampaignPageCall testAccPlan = new CampaignPageCall(sc);
     }
        
}