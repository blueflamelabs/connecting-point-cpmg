@isTest
private class CustomException_Test {

	@isTest
	private static void testName() {
		Test.startTest();
			String s ='THIS IS BAD' ;
			String extra ='New Info';
			CustomException custom = new CustomException(s);
			try{
				throw new CustomException(s,extra);
			}catch(Exception e){
				System.assertEquals(s, e.getMessage());
				CustomException myError = (CustomException) e;
				System.assertEquals(s, myError.getMessage());
				System.assertEquals(extra, myError.extra);
			}
			
		Test.stopTest();
	}
}