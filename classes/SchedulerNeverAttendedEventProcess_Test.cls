@isTest
public class SchedulerNeverAttendedEventProcess_Test{

    public static testMethod void neverAttendedEventProcessTest(){
       Id execAccountRtype = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
       Id execConRtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Account a = TestDataGenerator.createTestAccount(true,'Test Account',execAccountRtype);
        
        Contact c = TestDataGenerator.createTestContact(true,'Test','Exec','t@t.com',a,(String) execConRtype);
        
        
        
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event',true,Date.today().addDays(2), Date.today().addDays(-1), Date.today().addMonths(-3), Date.today().addMonths(-2));
        Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Event Active Opp',Date.today().addMonths(-4),a,'Closed Won',prod ,'Renewal');
        Id execPartRecordType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c partRecord = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c, execPartRecordType);
        Order conf = TestDataGenerator.createConfirmation(true,a,c,'Preliminary',opp,opp.CloseDate,partRecord,prod,true);
        
      SchedulerNeverAttendedEventProcess naep = new SchedulerNeverAttendedEventProcess();
      String sch = '0 0 8 13 2 ?'; 
      system.schedule('One Time Pro', sch, naep);
     
    }
 }