/*
    The purpose of this class is to run visualforce pages that are meant to be rendered as PDF documents.
    The sendEmailReport for event will:
    1.) Build an Email Message
    2.) Get the visualforce page as PDF based on the passed in pageURL variable.
    3.) Attach the visualforce page pdf to the email.
    4.) Send the email to the current user, and BCC the event staff based on the user permissions wrapper class.
    All of this is passed in through the EventRegistration2 Controller.
    
*/
public with sharing class emailPdfReport {
    private Product2 currentEvent {get;set;}
    private Contact currentContact {get;set;}
    private String pageUrl {get;set;}
    private UserPermissions permissions {get;set;}
    private string masterContactEmail{get;set;}
    private List<Contact> objConList{set;get;}
    private Set<String> objPrimaryMailId{set;get;}

    public emailPdfReport(Product2 currentEvent, Contact currentContact, String pageUrl, UserPermissions permissions,string masterContactEmail,List<Contact> objConListMail,Set<String> objPrimaryMailId) {
        this.currentEvent = currentEvent;    //The current portal event page
        this.currentContact = currentContact; //The logged in User's Contact record.
        this.pageUrl = pageUrl;   // The path so the Visualforce page will run whether its in the community or from within Salesforce
        this.permissions = permissions; //Passed to get the event staff associated with the logged in user.
        this.masterContactEmail = masterContactEmail;
        this.objConList = objConListMail;
        this.objPrimaryMailId = objPrimaryMailId;
    }
    

    public void sendEmailForEvent(){
        System.debug('#######objPrimaryMailIdobjPrimaryMailId######3 '+objPrimaryMailId);
        System.debug('#######objConList######3 '+objConList);
        EmailTemplate templateId = [Select id,HtmlValue,DeveloperName,Subject from EmailTemplate where DeveloperName = 'Registration_Summary_PDF'];
        String subjectName = templateId.Subject;
        if(subjectName.contains('{!Product2.Name}'))
            subjectName = subjectName.replace('{!Product2.Name}',currentEvent.Name);
        System.debug('%%%%%%%%%%%%%%%%%%%% '+templateId.HtmlValue);
        // Create email
        List<Messaging.SingleEmailMessage> objMassMailSend = new List<Messaging.SingleEmailMessage>();
        for(Contact objCon : objConList){
            String bodyName = templateId.HtmlValue;
            if(bodyName.contains('{!CurrentContact.Name}') && currentContact.Name != null)
                bodyName = bodyName.replace('{!CurrentContact.Name}',currentContact.Name);
            if(bodyName.contains('{!Product2.Id}'))
                bodyName = bodyName.replace('{!Product2.Id}',currentEvent.Id);
            if(bodyName.contains(']]>'))
                bodyName = bodyName.replace(']]>','');
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            //message.setToAddresses(new String[]{ currentContact.Email });
            message.setTargetObjectId(objCon.Id); //Sets the Who Id to the logged in User
          if(objPrimaryMailId.size() > 0){
              if(objPrimaryMailId.Contains(objCon.Id)){
                  if(permissions.eventStaff != null && permissions.eventStaff.staffEmail != null) message.setBccAddresses(new String[]{permissions.eventStaff.staffEmail});
              }
          }
           
            //if(masterContactEmail !=null) message.setCcAddresses(new String[]{masterContactEmail });
             if(bodyName.contains('{!Contact.FirstName}'))     
                 bodyName = bodyName.replace('{!Contact.FirstName}',objCon.FirstName);
             
             message.setSubject(subjectName);
             message.setHtmlBody(bodyName);
            /*message.setSubject(currentEvent.Name + ' Registration Summary attached in PDF');
            message.setHtmlBody('Hi '+objCon.FirstName+','
                                +'<br>A member of the team has updated Registration. The most recent edits were submitted by '+currentContact.Name
                                +'<br>Please see attached Registration Summary. '
                                +'<br>If you have difficulty viewing the attachment, please go to: '
                                +'<br><a href='+Site.getBaseUrl()+pageUrl+'eId='+currentEvent.Id +'>Link To Registration Summary</a>' );*/
            
            // Create PDF
                PageReference pdfPage = new PageReference(Site.getBaseUrl()+pageUrl);
                pdfPage.getParameters().put('eId', currentEvent.Id);
            Blob reportPdf;
            try {
                reportPdf = pdfPage.getContent();
            }
            catch (Exception e) {
                reportPdf = Blob.valueOf(e.getMessage());
                System.debug('Report PDF Error: '+reportPdf);
                System.debug(e.getCause());
                System.debug(e.getStackTraceString());
                System.debug(e.getTypeName());
                System.debug(e.getMessage());
            }
            
            // Attach PDF to email and send
            Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
            attachment.setContentType('application/pdf');
            attachment.setFileName('Registration Part 1 Summary.pdf');
            attachment.setInline(false);
            attachment.setBody(reportPdf);
            message.setFileAttachments(new Messaging.EmailFileAttachment[]{ attachment });
            objMassMailSend.add(message);
       } 
       try{
           if(objMassMailSend.size() > 0){
               Messaging.sendEmail(objMassMailSend,false);    
           }
       }catch(Exception ex){
           System.debug('$$$$$$$$$4 '+ex.getMessage());
       }
        
    }

    public void sendMeetingRequestEmail(){
        
        List<Messaging.SingleEmailMessage> objMassMailSend = new List<Messaging.SingleEmailMessage>();
        
        for(Contact objCon : objConList){
         Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
         String subjectHeader = (permissions.IsSupplier)?' Boardroom and One-on-One Selections':'  One-on-One Selections';
        message.setTargetObjectId(objCon.Id);
       // message.setTargetObjectId(currentContact.Id); //Sets the Who Id to the logged in User
       if(objPrimaryMailId.size() > 0){
           if(objPrimaryMailId.Contains(objCon.Id)){
               if(permissions.eventStaff != null && permissions.eventStaff.staffEmail != null) message.setBccAddresses(new String[]{permissions.eventStaff.staffEmail}); //
            }
       }
        message.setSubject(objCon.Name + subjectHeader);
        message.setHtmlBody('Hi '+objCon.FirstName+','
                            +'<br>You have successfully submitted your selections for the above event.  Thank you!'
                            +'<br>To view your selections click the link below: '
                            +'<br><a href='+Site.getBaseUrl()+pageUrl+'?eId='+currentEvent.Id +'>Link To '+subjectHeader+'</a>'
                            +'<br>'+'<br>'
                            +'<a href='+Site.getBaseUrl()+'CommunitiesHomePage?'+'?eId='+currentEvent.Id +'>Return to Registration</a>');

        
         // Create PDF
            PageReference pdfPage = new PageReference(Site.getBaseUrl()+pageUrl);
            pdfPage.getParameters().put('eId', currentEvent.Id);
            Blob reportPdf;
            try {
                reportPdf = pdfPage.getContent();
            }
            catch (Exception e) {
                reportPdf = Blob.valueOf(e.getMessage());
                System.debug('Report PDF Error: '+reportPdf);
                System.debug(e.getCause());
                System.debug(e.getStackTraceString());
                System.debug(e.getTypeName());
                System.debug(e.getMessage());
            }
            
            // Attach PDF to email and send
            Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
            attachment.setContentType('application/pdf');
            attachment.setFileName('Registration Part 2 Summary.pdf');
            attachment.setInline(false);
            attachment.setBody(reportPdf);
            message.setFileAttachments(new Messaging.EmailFileAttachment[]{ attachment });
            objMassMailSend.add(message);
          }  
       try{
          if(objMassMailSend.size() > 0){
              Messaging.sendEmail(objMassMailSend,false);  
          }
       }catch(Exception ex){
           System.debug('######## Part2 '+ex.getMessage());
       }   
         
            
    }

  
}