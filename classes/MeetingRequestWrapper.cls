public with sharing class MeetingRequestWrapper implements Comparable{

	public ProfileWrapper profile {get;set;}
	public Event_Ranking__c meetingRequest {get;set;}
	public boolean hasOneOnOne {get;set;}
	public boolean hasBoardRoom {get;set;}
	//public Integer index {get;set;}


	public MeetingRequestWrapper(ProfileWrapper profile, Event_Ranking__c meetingRequest) {
		this.profile= profile;
		this.meetingRequest = meetingRequest;
		hasOneOnOne = false;
		hasBoardRoom = false;
		determineCheckbox();
	}

	private void determineCheckbox(){
		if(String.isNotBlank(meetingRequest.Type__c)){
			if(meetingRequest.Type__c == 'Boardroom and One-on-One'){
				hasOneOnOne = true;
				hasBoardRoom = true;
			} else if(meetingRequest.Type__c == 'Boardroom'){
				hasBoardRoom = true;
			} else if(meetingRequest.Type__c == 'One-on-One'){
				hasOneOnOne = true;
			}
		}
	}

	public Integer compareTo(Object compareTo) {
      MeetingRequestWrapper compareToRequest = (MeetingRequestWrapper)compareTo;
      if (compareToRequest.meetingRequest.rank__c == NULL || meetingRequest == NULL) {
        return -1;
      } else {
        if (meetingRequest.Rank__c == compareToRequest.meetingRequest.Rank__c) {
          return 0;
        } else if (meetingRequest.Rank__c > compareToRequest.meetingRequest.Rank__c) {
          return 1;
        }
      }
      return -1;
    }
}