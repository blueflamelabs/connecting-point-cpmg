public class CreateConformation {
	@AuraEnabled 
    public static ListOfWrapperConformation showConformationRecord(Id oppId){
        String Badge_Add='';
    	Opportunity opp = [SELECT Id,Product__r.Start_Date__c,Product__r.Supplier_Renewal_Badge_Add_On_Rule__c,Account_Record_Type__c,Primary_Contact__r.Name,Primary_Contact__c,
                           AccountId,Returning_Customer__c,Account.Name, Product__c,Product__r.Name, Name FROM Opportunity WHERE Id =: oppId];
        OpportunityContactRole oppContactRole = [SELECT ContactId, Contact.RecordTypeId 
                                                 FROM OpportunityContactRole WHERE IsPrimary = true and OpportunityId = :oppId];
        Map<Id, RecordType> contactRecordTypes = new Map<Id, RecordType>([SELECT Id, Name FROM RecordType WHERE SobjectType='Contact']);
        Boolean isSupplier = contactRecordTypes.containsKey(oppContactRole.Contact.RecordTypeId) && contactRecordTypes.get(oppContactRole.Contact.RecordTypeId).Name == 'Supplier';
    	Event_Account_Details__c ead = NULL;
        try {
            List<Event_Account_Details__c> eads = [SELECT Id,Name, Account__c, Opportunity__c, Event__c FROM Event_Account_Details__c WHERE Opportunity__c =:oppId LIMIT 1];
            if (eads.size() > 0) {
                ead = eads.get(0);
            }
        } catch(Exception e) {
            System.debug(LoggingLevel.ERROR, e);
        }
        if (isSupplier && ead == NULL) {
            if(opp.Product__r.Supplier_Renewal_Badge_Add_On_Rule__c == 'Always Receive Renewal Badge Add On'){
                Badge_Add = '1';
            }
            if(opp.Product__r.Supplier_Renewal_Badge_Add_On_Rule__c == 'Must Attend Another Event'){
                List<Event_Account_Details__c> objEvenAcDetails = [Select id,Event__r.Start_Date__c from Event_Account_Details__c where Status__c != 'Cancelled' 
                                                                   and Account__c =:opp.AccountId];
                if(objEvenAcDetails.size() > 0){
                    Boolean isAvailable = false;
                    for(Event_Account_Details__c objEventAcc : objEvenAcDetails){
                        if(objEventAcc.Event__r.Start_Date__c.Year() == opp.Product__r.Start_Date__c.Year()){
                            isAvailable = true;
                            break;
                        }
                    }
                    if(isAvailable == true){
                        Badge_Add = '1'; 
                    }
                }
            }
        }else {
            if(!isSupplier && ead == NULL){ //if exec without ead - create ead record auto....
                 Id PRRecordTypeId = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
                ead = new Event_Account_Details__c();
                ead.Account__c = opp.AccountId;
                //ead.Badge_Add_Ons__c = 1;
                ead.Opportunity__c = opp.Id;
                ead.Event__c = opp.Product__c;
                ead.RecordTypeId = PRRecordTypeId;
                if(oppContactRole != null)ead.Primary_Contact__c = oppContactRole.ContactId;
                
                insert ead;
                
                ead = [SELECT Id, Name, Account__c, Opportunity__c, Event__c FROM Event_Account_Details__c WHERE Id =: ead.Id];
            } else {
                ead = [SELECT Id, Name, Account__c, Opportunity__c, Event__c FROM Event_Account_Details__c WHERE Id =: ead.Id];
            }
        }
        Id PRRecordTypeId = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Suppliers').getRecordTypeId();
       return new ListOfWrapperConformation(opp,oppContactRole,isSupplier,ead,Badge_Add,PRRecordTypeId);
    }  
    public class ListOfWrapperConformation{
        @AuraEnabled public Opportunity opp {get;set;}
        @AuraEnabled public OpportunityContactRole oppContactRole {get;set;}
        @AuraEnabled public Boolean isSupplier{get;set;}
        @AuraEnabled public Event_Account_Details__c ead{get;set;}
        @AuraEnabled public String Badge_Add{get;set;}
        @AuraEnabled public String RecTypeId{get;set;}
        public ListOfWrapperConformation(Opportunity opp,OpportunityContactRole oppContactRole, Boolean isSupplier,Event_Account_Details__c ead,String Badge_Add,String RecTypeId){
            this.opp = opp;
            this.oppContactRole= oppContactRole;
            this.isSupplier= isSupplier;
            this.ead= ead;
            this.Badge_Add = Badge_Add;
            this.RecTypeId = RecTypeId;
        }
    }
}