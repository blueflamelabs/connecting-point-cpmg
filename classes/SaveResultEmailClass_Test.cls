@isTest
private class SaveResultEmailClass_Test {

	@isTest
	private static void testName() {
		//Create Test Data
		Account[] accts = new List<Account>();
		accts.add(TestDataGenerator.createTestAccount(false,'Test Account'));
		accts.add(new Account()); //THIS WILL CAUSE A FAILURE

		Test.startTest();
			Database.SaveResult[] saveResults =Database.insert(accts,false);

			//Instantiate the SaveResultEmailClass
			
			String result = SaveResultEmailClass.processSaveResults(saveResults, accts);
			System.assertNotEquals(null,result);
		Test.stopTest();
	}
}