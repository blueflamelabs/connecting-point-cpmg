public class EventStaffDto {
	public String staffEmail { get; set; }
	public String staffName { get; set; }
	public String staffTitle { get; set; }
	public String staffPhone { get; set; }

	public EventStaffDto(String p_email, String p_name) {
		staffEmail = p_email;
		staffName = p_name;
	}
}