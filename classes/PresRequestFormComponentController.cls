public with sharing class PresRequestFormComponentController {
	public Boolean isExecutive { get; set; }
	public Product2 event { get; set; }
	public User user { get; set; }
	public String modalId { get; set; }
	public String emailSubject { get; set; }
	public String emailBody { get; set; }
	public EmailTemplate supplierEmailTemplate { get; set; }
	//public List<Event_Account_Details__c> suppliersList { get; set; }
	public Map<Id, Boolean> selectedSuppliers { get; set; }
	public Integer emailPercentageSent { get; set; }
	public List<ProfileWrapper> suppliersList {get;Set;}

	public PresRequestFormComponentController() {
		try {
			emailPercentageSent = -1;
			suppliersList = new List<ProfileWrapper>();
			
 			Id userId;

	        if(UserInfo.getUserId() == '00563000000IEp3AAG'){
	            userId='00563000000ZHOVAA4';
	        }else{
	            userId = UserInfo.getUserId();
	        }
	        System.Debug('The Current User ID: '+ userId);
	        try{
	            user = [SELECT Id, Name, Email, ContactId FROM User WHERE Id = :userId LIMIT 1];
	        }catch(Exception ex){
	            System.debug('Error: '+ ex.getMessage() +'. On Line: '+ ex.getLineNumber()+'. Stack Trace: '+ ex.getStackTraceString());
	        }

			String eventIdFromURL = ApexPages.currentPage().getParameters().get('eId');
	        if(String.isNotBlank(eventIdFromURL)){
	            event = EventHelper.getEvent(eventIdFromURL);
	        } else {
  	            Contact userContact = [SELECT Id, RecordType.Name, Intervention_Flag__c, AccountId, Account.Intervention_Flag__c FROM Contact WHERE Id = :user.ContactId];

	        	List<Order> orders = [SELECT Id, Order_Event__c FROM Order WHERE (Order_Event__r.Start_Date__c >= Today AND BillToContactId =:userContact.Id) OR (BillToContactId =:userContact.Id) ORDER BY Order_Event__r.Start_Date__c ASC LIMIT 1];
                System.debug(LoggingLevel.ERROR, orders);
                if (orders.size() > 0) {
                    Id pId = orders.get(0).Order_Event__c;
                    event = EventHelper.getEvent(pId);
                } else {
					 event = EventHelper.getClosestEventByDate();
                }
	        }

			supplierEmailTemplate = [SELECT Id, Body, Subject, HtmlValue FROM EmailTemplate WHERE DeveloperName ='Executive_Request_for_Presentations'];
			emailSubject = replaceFields(supplierEmailTemplate.Subject);
			emailBody = replaceFields(supplierEmailTemplate.Body);
			System.debug('Event: ' + event);

			Set<Id> types = new Set<Id>();
			types.add(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId());
			System.debug('THE TYPES: '+types);
			System.debug('PROD Record: '+event );
			//suppliersList = ParticipationRecordHelper.getEventParticipationRecords(event.Id, types);
			//Added 2/4/2018 STech. Utilizing the ProfileWrapper to ensure proper displays.
			suppliersList = UtilClass.transformToProfileWrapper(ParticipationRecordHelper.getEventParticipationRecords(event.Id, types));
			suppliersList.sort();
			System.debug('Suppliers: '+suppliersList);
			selectedSuppliers = new Map<Id, Boolean>();

			for(ProfileWrapper supplier : suppliersList) {
				selectedSuppliers.put(supplier.partRecord.Id, false);
			}
		} catch (Exception ex) {
            System.debug(LoggingLevel.ERROR, ex.getMessage());
            System.debug(LoggingLevel.ERROR, ex.getStackTraceString());
        }
	}

	public void sendEmails() {
		try {
			System.debug('selectedSuppliers: ' + selectedSuppliers);

			List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();

			Set<String> staffRoles = new Set<String>{'Salesperson', 'Recruiter', 'Support'};
			Set<Id> eventStaffUsers = EventStaffHelper.getEventStaffUsers(event.Id, staffRoles);
			Set<String> staffEmails = EventStaffHelper.getEmails(eventStaffUsers);
			for(CPMG_Notified_Executives__c exec : CPMG_Notified_Executives__c.getAll().values()) {
				staffEmails.add(exec.Email__c);
			}

			for(ProfileWrapper supplier : suppliersList) {
				if(selectedSuppliers.get(supplier.partRecord.Id)) {
					// Create string set
					List<Id> contactIds = new List<Id>(); 
					if(!supplier.partRecord.Confirmations__r.isEmpty()) {
						for(Order order : supplier.partRecord.Confirmations__r) {
							contactIds.add(order.BillToContactId);
						}
						Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();

						email.setTargetObjectId(contactIds.get(0)); // It will not be empy here.
						email.setToAddresses(contactIds);
						email.setBccAddresses(new List<String>(staffEmails));

						email.setOptOutPolicy('SEND'); // This is intended as an email from the execitive, not CPMG.

						email.setTreatTargetObjectAsRecipient(false);

						email.setReplyTo(user.Email);
						email.setTemplateId(supplierEmailTemplate.Id);
						//email.saveAsActivity = false;
						email.setWhatId(event.Id);
						emails.add(email);
					} else {
						System.debug(LoggingLevel.WARN, 'Supplier does not have Supplier_Account_Owner confirmations');
					}
				}
			}

			if(!emails.isEmpty()) {
				for(Messaging.SingleEmailMessage email : emails) {
	            	System.debug(email);
	            }
				Messaging.SendEmailResult[] results = Messaging.sendEmail(emails);
				Integer successful = 0;
	            for(Messaging.SendEmailResult result : results) {
	            	System.debug(result);
	            	if(result.isSuccess()) {
	            		successful++;
	            	}
	            }
	            emailPercentageSent = (successful *100) / results.size() ;
	            System.debug('Percentage Sent: '+ emailPercentageSent);
	            //emailPercentageSent = 0;
			} else {
				emailPercentageSent = -1;
			}
		} catch (Exception ex) {
			System.debug(LoggingLevel.ERROR, ex.getMessage());
            System.debug(LoggingLevel.ERROR, ex.getStackTraceString());
		}
	}

	/**
	 * Replaces all of the needed product2 fields with the field in the event variable
	 */
	private String replaceFields(String text) {
		text = text.replace('{!Product2.Family}', event == null ? '' : event.Family);
		text = text.replace('{!Product2.Name}', event == null ? '' : event.Name);
		text = text.replace('{!User.Name}', User == null ? '' : User.Name);

		return text;
	}
}