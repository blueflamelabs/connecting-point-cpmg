/*
*Created By     : 
*Created Date   : 19/07/2019 
*Description    : 
*Test Class Name: OpportunityRelateCTRL
Modification History :
============================================================================================
V.No    Date            By                  Description
============================================================================================
*/
@IsTest
public class OpportunityRelateCTRL_Test{
    static testMethod void OpportunityRelateCTRL_testMethod(){
        
         Id execAccountRtype = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
         Id execConRtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
         Account a = TestDataGenerator.createTestAccount(true,'Test Account',execAccountRtype);
         Contact objCon = TestDataGenerator.createTestContact(true,'Test','Exec','t@t.com',a,(String) execConRtype);
      
         Product2 objProd = TestDataGenerator.createTestProduct(true,'Test Name',True);
      
         Id execOpportunityRtype = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
         //Opportuntiy opp = TestDataGenerator.createTestOpp(true,'test',Date.today(),a,'Closed Won', prod,'')
         Opportunity objOpp = new Opportunity();
         objOpp.Name = 'TestOpp';
         objOpp.AccountId = a.Id;
         objOpp.Product__c = objProd.Id;
         objOpp.Primary_Contact__c = objCon.Id;
         objOpp.CloseDate = System.Today();
         objOpp.StageName = 'Closed Won';
         objOpp.RecordTypeId = execOpportunityRtype;
         objOpp.Returning_Customer__c = 'Onsite Renewal';
         insert objOpp;
         
         ApexPages.currentPage().getParameters().put('Id',objOpp.Id);
         ApexPages.currentPage().getParameters().put('ReTypeId','Executive');
         OpportunityRelateCTRL opprealte =new OpportunityRelateCTRL();
         
    }
}