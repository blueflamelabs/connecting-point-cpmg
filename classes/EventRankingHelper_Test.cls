@isTest
private class EventRankingHelper_Test {
	
	@isTest static void getEventRankingsForEvent() {
		// Prepare Test Data.
		TestDataGenerator.createInactiveTriggerSettings();

		//Event Setup
		Product2 prod = TestDataGenerator.createTestProduct(true,'Test Active Event',true,Date.today().addDays(40),Date.today().addDays(45),Date.today().addMonths(-2), Date.today().addMonths(-1));
		prod.Start_Date__c = Date.today().addDays(40);
		prod.Family ='BuildPoint';
		prod.Event_Navigation_Color__c = '90135d';
		prod.Event_Tint_Color__c = 'EEEEEE';
		update prod;

		Profile standardUser = [SELECT Id FROM Profile where Name ='Standard User' LIMIT 1];
		User execUserCPMG = TestDataGenerator.CreateUser(true,standardUser.Id,'Test','Exec',null,'testExec1234@cpmg.com',null);
		User supplierUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Supplier',null,'testSupplier1234@cpmg.com',null);
		User supportUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Support',null,'testSupport1234@cpmg.com',null);


		//Set Up Executive User
		Id execAccRType =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
		Id execConRType =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
		Id suppAccRType =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
		Id suppConRType =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();

		Account execAcc = TestDataGenerator.createTestAccount(true,'Test Exec Account', execAccRType);
		Account suppAcc = TestDataGenerator.createTestAccount(true,'Test Exec Account', suppAccRType);

		
		Contact execCon = TestDataGenerator.createTestContact(true,'Test Exec','Last Account','testexec@test.com',execAcc, execConRType);
		Contact suppCon = TestDataGenerator.createTestContact(true,'Test Exec','Last Account','testexec@test.com',suppAcc, suppConRType);

		Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
		User execUser = TestDataGenerator.CreateUser(true,commUserProfId.id,'Test Exec','Last Account','603-555-5555','testexec@test.com', execCon.Id);

		Opportunity execOpp = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),execAcc,'Closed Won',prod);
		Opportunity suppOpp = TestDataGenerator.createTestOpp(true,'Supplier Test Opp',Date.today(),suppAcc,'Closed Won',prod);

		Id execPRRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
		Id suppPRRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Suppliers').getRecordTypeId();
		Event_Account_Details__c execPRecod = TestDataGenerator.createParticipationRecord(true,prod,execAcc,execOpp,execCon,execPRRType);
		Event_Account_Details__c suppPRecod = TestDataGenerator.createParticipationRecord(true,prod,suppAcc,suppOpp,suppCon,suppPRRType);

		Event_Ranking__c ranking = TestDataGenerator.createEventRanking(true, prod,  suppPRecod, execPRecod, 1, suppCon, 'One-on-One', 'Test Reason Comment' );


		test.startTest();
			Map<String,Event_Ranking__c> eventRankingMap = EventRankingHelper.getEventRankingsForEvent(prod.Id,  suppPRecod.Id);
		test.stopTest();
		String tempKey = String.valueOf(suppPRecod.Id) + String.valueOf(execPRecod.Id);
		System.assertEquals(eventRankingMap.containsKey(tempKey),true);
	}
	
	@isTest static void createMeetingRequest_test() {
		// Prepare Test Data.
		TestDataGenerator.createInactiveTriggerSettings();

		//Event Setup
		Product2 prod = TestDataGenerator.createTestProduct(true,'Test Active Event',true,Date.today().addDays(40),Date.today().addDays(45),Date.today().addMonths(-2), Date.today().addMonths(-1));
		prod.Start_Date__c = Date.today().addDays(40);
		prod.Family ='BuildPoint';
		prod.Event_Navigation_Color__c = '90135d';
		prod.Event_Tint_Color__c = 'EEEEEE';
		update prod;

		Profile standardUser = [SELECT Id FROM Profile where Name ='Standard User' LIMIT 1];
		User execUserCPMG = TestDataGenerator.CreateUser(true,standardUser.Id,'Test','Exec',null,'testExec1234@cpmg.com',null);
		User supplierUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Supplier',null,'testSupplier1234@cpmg.com',null);
		User supportUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Support',null,'testSupport1234@cpmg.com',null);


		//Set Up Executive User
		Id execAccRType =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
		Id execConRType =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
		Id suppAccRType =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
		Id suppConRType =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();

		Account execAcc = TestDataGenerator.createTestAccount(true,'Test Exec Account', execAccRType);
		Account suppAcc = TestDataGenerator.createTestAccount(true,'Test Exec Account', suppAccRType);

		
		Contact execCon = TestDataGenerator.createTestContact(true,'Test Exec','Last Account','testexec@test.com',execAcc, execConRType);
		Contact suppCon = TestDataGenerator.createTestContact(true,'Test Exec','Last Account','testexec@test.com',suppAcc, suppConRType);

		Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
		User execUser = TestDataGenerator.CreateUser(true,commUserProfId.id,'Test Exec','Last Account','603-555-5555','testexec@test.com', execCon.Id);

		Opportunity execOpp = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),execAcc,'Closed Won',prod);
		Opportunity suppOpp = TestDataGenerator.createTestOpp(true,'Supplier Test Opp',Date.today(),suppAcc,'Closed Won',prod);

		Id execPRRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
		Id suppPRRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Suppliers').getRecordTypeId();
		Event_Account_Details__c execPRecod = TestDataGenerator.createParticipationRecord(true,prod,execAcc,execOpp,execCon,execPRRType);
		Event_Account_Details__c suppPRecod = TestDataGenerator.createParticipationRecord(true,prod,suppAcc,suppOpp,suppCon,suppPRRType);

		Test.startTest();
			Event_Ranking__c ranking = EventRankingHelper.createMeetingRequest(execPRecod, suppPRecod, prod, execCon);
		Test.stopTest();	
	}
	
}