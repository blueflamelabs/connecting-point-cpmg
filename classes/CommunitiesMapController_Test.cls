@isTest
private class CommunitiesMapController_Test {

	@isTest
	private static void CommunitiesMapController_Constructor_Test_w_eId() {
		//Prepare Data
		TestDataGenerator.createInactiveTriggerSettings();
		//Create CPMG_Notification Settings
		TestDataGenerator.createCPMGNotifiedUsers();
		TestDataGenerator.createOrderStatusCustomSetting();
			
		//Set Up Event
		Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event Active',true,Date.today().addDays(-10),Date.today().addDays(-5),Date.today().addMonths(-2), Date.today().addMonths(-1));
		prod.Family ='BuildPoint';
		update prod;
		//Set Up EventStaff
		Profile standardUser = [SELECT ID FROM Profile where Name ='Standard User' LIMIT 1];
		User execUserCPMG = TestDataGenerator.CreateUser(true,standardUser.Id,'Test','Exec',null,'testExec1234@cpmg.com',null);
		User supplierUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Supplier',null,'testSupplier1234@cpmg.com',null);
		User supportUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Support',null,'testSupport1234@cpmg.com',null);


		Event_Staff__c execStaff = TestDataGenerator.createEventStaff(true,prod,execUserCPMG,'Recruiter');
		Event_Staff__c supplierStaff = TestDataGenerator.createEventStaff(true,prod,supplierUserCPMG,'Salesperson');
		Event_Staff__c supportStaff = TestDataGenerator.createEventStaff(true,prod,supportUserCPMG,'Support');

		Event_Map_URL__c eMapUrl = TestDataGenerator.createEventMapUrl(true,'Test Map URL',1,'This Title Map','Supplier Locations','https://www.google.com',prod);

	

		//Set Up Executive User
		Id execAccRType =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
		Id execConRType =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
		Account execAcc = TestDataGenerator.createTestAccount(true,'Test Exec Account', execAccRType);
		Contact execCon = TestDataGenerator.createTestContact(true,'Test Exec','Last Account','testexec@test.com',execAcc, execConRType);

		Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
		User execUser = TestDataGenerator.CreateUser(true,commUserProfId.id,'Test Exec','Last Account','603-555-5555','testexec@test.com', execCon.Id);

		Opportunity execOpp = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),execAcc,'Closed Won',prod);

		Id execPRRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
		Event_Account_Details__c execPRecod = TestDataGenerator.createParticipationRecord(true,prod,execAcc,execOpp,execCon,execPRRType);

		Order execConf = TestDataGenerator.createConfirmation(true,execAcc,execCon,'Attending Event Contact',execOpp,execOpp.CloseDate,execPRecod,prod);
		

		//Set Up Suppliers
		Id supplierAccRType =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
		Id supplierConRType =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
		Account supAcc = TestDataGenerator.createTestAccount(true,'Test Sup Account', supplierAccRType);
		Contact supCon = TestDataGenerator.createTestContact(true,'Test Sup','Last Account','testsup123@test.com',supAcc,supplierConRType);

		User supUser = TestDataGenerator.CreateUser(true,commUserProfId.Id,'Test Sup','Last Account','603-555-5555',supCon.Email, supCon.Id);

		Opportunity supOpp = TestDataGenerator.createTestOpp(true,'Supp Test Opp',Date.today(),supAcc,'Closed Won',prod);

		Id supplierPRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Suppliers').getRecordTypeId();
		Event_Account_Details__c supPRecod = TestDataGenerator.createParticipationRecord(true,prod,supAcc,supOpp,supCon,supplierPRType);
		supPRecod.Location__c = String.valueOf(2);
		update supPRecod;
		Order supConf = TestDataGenerator.createConfirmation(true,supAcc,supCon,'Preliminary',supOpp,supOpp.CloseDate,supPRecod,prod);

		//Second Supplier
		Account supAcc2 = TestDataGenerator.createTestAccount(true,'Test Supplier Account2', supplierAccRType);
		Contact supCon2 = TestDataGenerator.createTestContact(true,'Test Supplier2','Last Account','testsupp2@test.com',supAcc2,supplierConRType);

		
		User supUser2 = TestDataGenerator.CreateUser(true,commUserProfId.Id,'Test Exec','Last Account','603-555-5555',supCon2.Email, supCon2.Id);

		Opportunity supOpp2 = TestDataGenerator.createTestOpp(true,'Supplier2 Test Opp',Date.today(),supAcc2,'Closed Won',prod);

		Event_Account_Details__c supPRecod2 = TestDataGenerator.createParticipationRecord(true,prod,supAcc2,supOpp2,supCon2,supplierPRType);
		supPRecod2.Location__c = String.valueOf(1);
		update supPRecod2;
		Order supConf2 = TestDataGenerator.createConfirmation(true,supAcc2,supCon2,'Preliminary',supOpp2,supOpp2.CloseDate,supPRecod2,prod);

		PageReference pageRef = Page.CommunitiesMap;
		pageRef.getParameters().put('eId', prod.Id);
		

		//PERFORM THE TESTING
		Test.startTest();
			System.runAs(execUser){
				Test.setCurrentPageReference(pageRef);
				CommunitiesMapController cMapController = new CommunitiesMapController();
			}
		Test.stopTest();
	}

	@isTest
	private static void CommunitiesMapController_Constructor_Test_No_eId() {
		//Prepare Data
		TestDataGenerator.createInactiveTriggerSettings();
		//Create CPMG_Notification Settings
		TestDataGenerator.createCPMGNotifiedUsers();
		TestDataGenerator.createOrderStatusCustomSetting();
			
		//Set Up Event
		Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event Active',true,Date.today().addDays(-10),Date.today().addDays(-5),Date.today().addMonths(-2), Date.today().addMonths(-1));
		prod.Family ='BuildPoint';
		update prod;
		//Set Up EventStaff
		Profile standardUser = [SELECT ID FROM Profile where Name ='Standard User' LIMIT 1];
		User execUserCPMG = TestDataGenerator.CreateUser(true,standardUser.Id,'Test','Exec',null,'testExec1234@cpmg.com',null);
		User supplierUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Supplier',null,'testSupplier1234@cpmg.com',null);
		User supportUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Support',null,'testSupport1234@cpmg.com',null);


		Event_Staff__c execStaff = TestDataGenerator.createEventStaff(true,prod,execUserCPMG,'Recruiter');
		Event_Staff__c supplierStaff = TestDataGenerator.createEventStaff(true,prod,supplierUserCPMG,'Salesperson');
		Event_Staff__c supportStaff = TestDataGenerator.createEventStaff(true,prod,supportUserCPMG,'Support');

		Event_Map_URL__c eMapUrl = TestDataGenerator.createEventMapUrl(true,'Test Map URL',1,'This Title Map','Supplier Locations','https://www.google.com',prod);

	

		//Set Up Executive User
		Id execAccRType =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
		Id execConRType =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
		Account execAcc = TestDataGenerator.createTestAccount(true,'Test Exec Account', execAccRType);
		Contact execCon = TestDataGenerator.createTestContact(true,'Test Exec','Last Account','testexec@test.com',execAcc, execConRType);

		Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
		User execUser = TestDataGenerator.CreateUser(true,commUserProfId.id,'Test Exec','Last Account','603-555-5555','testexec@test.com', execCon.Id);

		Opportunity execOpp = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),execAcc,'Closed Won',prod);

		Id execPRRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
		Event_Account_Details__c execPRecod = TestDataGenerator.createParticipationRecord(true,prod,execAcc,execOpp,execCon,execPRRType);

		Order execConf = TestDataGenerator.createConfirmation(true,execAcc,execCon,'Attending Event Contact',execOpp,execOpp.CloseDate,execPRecod,prod);
		

		//Set Up Suppliers
		Id supplierAccRType =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
		Id supplierConRType =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
		Account supAcc = TestDataGenerator.createTestAccount(true,'Test Sup Account', supplierAccRType);
		Contact supCon = TestDataGenerator.createTestContact(true,'Test Sup','Last Account','testsup123@test.com',supAcc,supplierConRType);

		User supUser = TestDataGenerator.CreateUser(true,commUserProfId.Id,'Test Sup','Last Account','603-555-5555',supCon.Email, supCon.Id);

		Opportunity supOpp = TestDataGenerator.createTestOpp(true,'Supp Test Opp',Date.today(),supAcc,'Closed Won',prod);

		Id supplierPRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Suppliers').getRecordTypeId();
		Event_Account_Details__c supPRecod = TestDataGenerator.createParticipationRecord(true,prod,supAcc,supOpp,supCon,supplierPRType);
		supPRecod.Location__c = String.valueOf(2);
		update supPRecod;
		Order supConf = TestDataGenerator.createConfirmation(true,supAcc,supCon,'Preliminary',supOpp,supOpp.CloseDate,supPRecod,prod);

		//Second Supplier
		Account supAcc2 = TestDataGenerator.createTestAccount(true,'Test Supplier Account2', supplierAccRType);
		Contact supCon2 = TestDataGenerator.createTestContact(true,'Test Supplier2','Last Account','testsupp2@test.com',supAcc2,supplierConRType);

		
		User supUser2 = TestDataGenerator.CreateUser(true,commUserProfId.Id,'Test Exec','Last Account','603-555-5555',supCon2.Email, supCon2.Id);

		Opportunity supOpp2 = TestDataGenerator.createTestOpp(true,'Supplier2 Test Opp',Date.today(),supAcc2,'Closed Won',prod);

		Event_Account_Details__c supPRecod2 = TestDataGenerator.createParticipationRecord(true,prod,supAcc2,supOpp2,supCon2,supplierPRType);
		supPRecod2.Location__c = String.valueOf(1);
		update supPRecod2;
		Order supConf2 = TestDataGenerator.createConfirmation(true,supAcc2,supCon2,'Preliminary',supOpp2,supOpp2.CloseDate,supPRecod2,prod);

		PageReference pageRef = Page.CommunitiesMap;
		//pageRef.getParameters().put('eId', prod.Id);
		

		//PERFORM THE TESTING
		Test.startTest();
			System.runAs(execUser){
				Test.setCurrentPageReference(pageRef);
				CommunitiesMapController cMapController = new CommunitiesMapController();
				cMapController.companiesSortField ='Location';
				cMapController.sortCompanies();
				cMapController.getMapURL();
				cMapController.upcomingProdEvent();
				cMapController.getIsError();
				cMapController.getErrorMessage();
			}
		Test.stopTest();
	}

	@isTest
	private static void CommunitiesMapController_Constructor_NOMapURLs_NOPRs_eId() {
		//Prepare Data
		TestDataGenerator.createInactiveTriggerSettings();
		//Create CPMG_Notification Settings
		TestDataGenerator.createCPMGNotifiedUsers();
		TestDataGenerator.createOrderStatusCustomSetting();
			
		//Set Up Event
		Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event Active',true,Date.today().addDays(-10),Date.today().addDays(-5),Date.today().addMonths(-2), Date.today().addMonths(-1));
		prod.Family ='BuildPoint';
		update prod;
		//Set Up EventStaff
		Profile standardUser = [SELECT ID FROM Profile where Name ='Standard User' LIMIT 1];
		User execUserCPMG = TestDataGenerator.CreateUser(true,standardUser.Id,'Test','Exec',null,'testExec1234@cpmg.com',null);
		User supplierUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Supplier',null,'testSupplier1234@cpmg.com',null);
		User supportUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Support',null,'testSupport1234@cpmg.com',null);


		Event_Staff__c execStaff = TestDataGenerator.createEventStaff(true,prod,execUserCPMG,'Recruiter');
		Event_Staff__c supplierStaff = TestDataGenerator.createEventStaff(true,prod,supplierUserCPMG,'Salesperson');
		Event_Staff__c supportStaff = TestDataGenerator.createEventStaff(true,prod,supportUserCPMG,'Support');

		//Event_Map_URL__c eMapUrl = TestDataGenerator.createEventMapUrl(true,'Test Map URL',1,'This Title Map','Supplier Locations','https://www.google.com',prod);

	

		//Set Up Executive User
		Id execAccRType =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
		Id execConRType =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
		Account execAcc = TestDataGenerator.createTestAccount(true,'Test Exec Account', execAccRType);
		Contact execCon = TestDataGenerator.createTestContact(true,'Test Exec','Last Account','testexec@test.com',execAcc, execConRType);

		Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
		User execUser = TestDataGenerator.CreateUser(true,commUserProfId.id,'Test Exec','Last Account','603-555-5555','testexec@test.com', execCon.Id);

		Opportunity execOpp = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),execAcc,'Closed Won',prod);

		Id execPRRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
		Event_Account_Details__c execPRecod = TestDataGenerator.createParticipationRecord(true,prod,execAcc,execOpp,execCon,execPRRType);

		Order execConf = TestDataGenerator.createConfirmation(true,execAcc,execCon,'Attending Event Contact',execOpp,execOpp.CloseDate,execPRecod,prod);
		

		//Set Up Suppliers
		Id supplierAccRType =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
		Id supplierConRType =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
		Account supAcc = TestDataGenerator.createTestAccount(true,'Test Sup Account', supplierAccRType);
		Contact supCon = TestDataGenerator.createTestContact(true,'Test Sup','Last Account','testsup123@test.com',supAcc,supplierConRType);

		User supUser = TestDataGenerator.CreateUser(true,commUserProfId.Id,'Test Sup','Last Account','603-555-5555',supCon.Email, supCon.Id);

		Opportunity supOpp = TestDataGenerator.createTestOpp(true,'Supp Test Opp',Date.today(),supAcc,'Closed Won',prod);

		Id supplierPRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Suppliers').getRecordTypeId();
		/* Event_Account_Details__c supPRecod = TestDataGenerator.createParticipationRecord(true,prod,supAcc,supOpp,supCon,supplierPRType);
		supPRecod.Location__c = String.valueOf(2);
		update supPRecod; 
		Order supConf = TestDataGenerator.createConfirmation(true,supAcc,supCon,'Preliminary',supOpp,supOpp.CloseDate,supPRecod,prod); */

		//Second Supplier
		Account supAcc2 = TestDataGenerator.createTestAccount(true,'Test Supplier Account2', supplierAccRType);
		Contact supCon2 = TestDataGenerator.createTestContact(true,'Test Supplier2','Last Account','testsupp2@test.com',supAcc2,supplierConRType);

		
		User supUser2 = TestDataGenerator.CreateUser(true,commUserProfId.Id,'Test Exec','Last Account','603-555-5555',supCon2.Email, supCon2.Id);

		Opportunity supOpp2 = TestDataGenerator.createTestOpp(true,'Supplier2 Test Opp',Date.today(),supAcc2,'Closed Won',prod);

		/*Event_Account_Details__c supPRecod2 = TestDataGenerator.createParticipationRecord(true,prod,supAcc2,supOpp2,supCon2,supplierPRType);
		supPRecod2.Location__c = String.valueOf(1);
		update supPRecod2; 
		Order supConf2 = TestDataGenerator.createConfirmation(true,supAcc2,supCon2,'Preliminary',supOpp2,supOpp2.CloseDate,supPRecod2,prod); */

		PageReference pageRef = Page.CommunitiesMap;
		pageRef.getParameters().put('eId', prod.Id);
		

		//PERFORM THE TESTING
		Test.startTest();
			System.runAs(execUser){
				Test.setCurrentPageReference(pageRef);
				CommunitiesMapController cMapController = new CommunitiesMapController();
				cMapController.companiesSortField ='Location';
				cMapController.sortCompanies();
				cMapController.getMapURL();
				cMapController.upcomingProdEvent();
				cMapController.getIsError();
				cMapController.getErrorMessage();
			}
		Test.stopTest();
	}
}