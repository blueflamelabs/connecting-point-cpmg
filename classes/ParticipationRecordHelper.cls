public without sharing class ParticipationRecordHelper {

    public static Map<String,Event_Account_Details__c> getEventAccountIdAndParticipationRecordMap(Set<Id> eventIds, Set<Id> accountIDs){
        
        Map<String,Event_Account_Details__c> eventAccountIdAndParticipationRecordMap = new Map<String,Event_Account_Details__c>();
        for(Event_Account_Details__c evd :  [SELECT Id,Personal_Section__c,Company_Information__c,
                                                     Category_Questions__c,Attendee_Information__c,Company_Logo__c,Boardroom_Ranking__c,
                                                     Location__c, Event__c, Account__c
                                                     FROM Event_Account_Details__c
                                                     WHERE Event__c IN :eventIds
                                                     AND Account__c IN :accountIds
                                                     ]){
            eventAccountIdAndParticipationRecordMap.put(String.valueOf(evd.Event__c)+String.valueOf(evd.Account__c), evd);                                           
        }
        return eventAccountIdAndParticipationRecordMap;
    }

    public static List<Event_Account_Details__c> getEventParticipationRecords(Id eventId, Set<Id> types){
        
        /*return [SELECT Id, Location__c, Event__c, Account__c, Account__r.Name,
                    (SELECT BillToContactId FROM Confirmations__r WHERE Supplier_Account_Owner__c = TRUE)
                 FROM Event_Account_Details__c
                 WHERE Event__c = :eventId
                 AND Account__r.RecordTypeId IN :types
                ]; */
        List<String> eventContactStatuses = Order_Statuses__c.getOrgDefaults().Event_Contact_picklist_values__c.split('\\|');
        return [SELECT Id, Location__c,Personal_Section__c,Company_Information__c,
                    Category_Questions__c,Attendee_Information__c,Company_Logo__c,Boardroom_Ranking__c,Event__c, Account__c, Account__r.Name, Company_Display_Name__c, Account__r.Website,
                    (SELECT BillToContactId FROM Confirmations__r WHERE Status IN :eventContactStatuses)
                 FROM Event_Account_Details__c
                 WHERE Event__c = :eventId
                 AND Account__r.RecordTypeId IN :types
                 AND Account__r.Type NOT IN ('Staff', 'Vendor')
                ];
    }

    //Used to get a single PR record based on the logged in User's Current Event and Account.
    public static Event_Account_Details__c getParticipationRecord(Id currentEventId, Id currentAccountId){
         return [SELECT Id, Badge_Total__c, One_on_One_Meetings__c,Personal_Section__c,Company_Information__c,
                    Category_Questions__c,Attendee_Information__c,Company_Logo__c,Boardroom_Ranking__c,
                    Registration_Reason__c, Package_Selection__r.Name, Package_Selection__r.Number_of_Board_Rooms__c, 
                    Package_Selection__r.Number_Of_One_on_Ones__c, Package_Selection__r.Number_of_Theatres__c,
                    Registration_Part_1_Extension__c, Registration_Part_1_Submitted__c, Registration_Part_1_Complete__c,
                    Registration_Part_2_Extension__c, Registration_Part_2_Submitted__c, Registration_Part_2_Complete__c,
                    Package_Selection__r.Number_of_Boardroom_Selections_Needed__c, Package_Selection__r.Number_of_One_On_One_Selections_Needed__c
                    ,Company_Display_Name__c, Boardrooms__c
                    FROM Event_Account_Details__c
                    WHERE Account__c = :currentAccountId AND Event__c = :currentEventId LIMIT 1];
    }
    public static Event_Account_Details__c getParticipationRecord(Id currentEventId, Contact currentContact){
         system.debug('**********'+currentEventId);
         system.debug('**********'+currentContact);
         return [SELECT Id, Badge_Total__c, One_on_One_Meetings__c,Personal_Section__c,Company_Information__c,
                    Category_Questions__c,Attendee_Information__c,Company_Logo__c,Boardroom_Ranking__c,
                    Registration_Reason__c, Package_Selection__r.Name, Package_Selection__r.Number_of_Board_Rooms__c, 
                    Package_Selection__r.Number_Of_One_on_Ones__c, Package_Selection__r.Number_of_Theatres__c,
                    Registration_Part_1_Extension__c, Registration_Part_1_Submitted__c, Registration_Part_1_Complete__c,
                    Registration_Part_2_Extension__c, Registration_Part_2_Submitted__c, Registration_Part_2_Complete__c,
                    Package_Selection__r.Number_of_Boardroom_Selections_Needed__c, Package_Selection__r.Number_of_One_On_One_Selections_Needed__c
                    ,Company_Display_Name__c, Boardrooms__c
                    FROM Event_Account_Details__c
                    WHERE Primary_Contact__c = :currentContact.Id AND Event__c = :currentEventId LIMIT 1];
    }

    /*public static Event_Account_Details__c getParticipationRecord(Id partRecordId){
        return [SELECT Id, Registration_part_1_Submitted__c,
    } */
    public static List<Event_Account_Details__c> getAllParticipationRecordsByEvent(Set<Id> eventIds){
        return [SELECT Id,Personal_Section__c,Company_Information__c,
                        Category_Questions__c,Attendee_Information__c,Company_Logo__c,Boardroom_Ranking__c, Account__c, Account__r.Type,Account__r.Name, Account__r.Description, Account__R.website, Event__c, Location__c, Primary_Contact__c,
                        Primary_Contact__r.Name, Primary_Contact__r.Title, Primary_Contact__r.Account.Name, Primary_Contact__r.RecordTypeID
                        ,Primary_Contact__r.RecordType.Name, Primary_Contact__r.Account.Website, Account__r.RecordTypeID, Account__r.RecordType.Name,
                        Company_Display_Name__c 
                        FROM Event_Account_Details__c WHERE Event__c IN : eventIds  and Account__r.Type <> 'Staff' ORDER BY Account__r.Name ASC];
    }

    public static List<Event_Account_Details__c> getAllParticipationRecordsByEventAndRType(Set<Id> eventIds, String recordTypeName){
        return [SELECT Id, Account__c, Account__r.Type,Account__r.Name, Personal_Section__c,Company_Information__c,
                    Category_Questions__c,Attendee_Information__c,Company_Logo__c,Boardroom_Ranking__c,Account__r.Description, Account__R.website, Event__c, Location__c, Primary_Contact__c,
                        Primary_Contact__r.Name, Primary_Contact__r.Title, Primary_Contact__r.Account.Name, Primary_Contact__r.RecordTypeID
                        ,Primary_Contact__r.RecordType.Name, Primary_Contact__r.Account.Website, Account__r.RecordTypeID, Account__r.RecordType.Name,
                        Company_Display_Name__c 
                        FROM Event_Account_Details__c WHERE Event__c IN : eventIds  and Account__r.Type <> 'Staff' and Account__r.RecordType.Name = :recordTypeName ORDER BY Account__r.Name ASC];
    }

    public static Map<Id,Event_Account_Details__c> getAllPRRecordsByEventForMeetingRequests(Set<Id> eventIds, String recordType){
        String query = 'SELECT Id, Account__r.Type,Account__c, Primary_Contact__c,  Primary_Contact__r.Name, Badge_Total__c, Boardrooms__c';
        query+= ', Company_Display_Name__c, Package_Selection__c, Package_Selection__r.Number_of_Boardroom_Selections_Needed__c, Package_Selection__r.Number_of_One_On_One_Selections_Needed__c';
        //query+= ', Primary_Contact__r.Title';
        //Add THE ACCOUNT FIELDS
        for(String s : ParticipationRecordHelper.getAccountUserSpecifiedFields()){
            query += ', Account__r.'+s;
        }
        //ADD THE CONTACT FIELDS
        for(String s: ParticipationRecordHelper.getContactUserSpecifiedFields()){
            query += ', Primary_Contact__r.'+s;
        }
        query+= ' FROM Event_Account_Details__c WHERE';
        query+= ' Event__c IN :eventIds AND Account__r.RecordType.Name =\''+recordType+'\''+ 'AND Account__r.Type<>\'Staff\'';
        return new Map <Id,Event_Account_Details__c>((List<Event_Account_Details__c>)database.query(query));
    }

    public static Set<String> getAccountUserSpecifiedFields() {
        Set<String> accountFields = new Set<String>();
        For(FieldSetMember fsm: SObjectType.Account.FieldSets.User_Specified_Fields.getFields()){
            accountFields.add(fsm.getFieldPath());
        }
        return accountFields;
    }

    public static Set<String> getContactUserSpecifiedFields() {
        Set<String> contactFields = new Set<String>();
        For(FieldSetMember fsm: SObjectType.Contact.FieldSets.Self_User_Specified_Fields.getFields()){
            contactFields.add(fsm.getFieldPath());
        }
        return contactFields;
    }
}