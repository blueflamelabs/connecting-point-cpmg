public with sharing class CommunitiesHomePageController {

    private final sObject mysObject;
    private String userId;
    private User currentUser;
    private Contact userContact;
   // private Set<Id> userEventIds {get; set;}
    private Product2 currentEvent;

    private String twitterName {get; set;}
    //private String imageURL {get;set;}
    public Static Integer count = 0;
    private Boolean noCharity = false;
    private String charityLogoUrl {get;set;}
    private Integer charityGoal {get;set;}
    private Integer charityRaised {get;set;}
    private String charityOrgURL {get;set;}

    private Boolean hasNoEventFeed = false;
    private String eventMessage {get;set;}
    //private String eventName {get;set;}
   // private Boolean intervention {get;set;}
   // private String interventionMessage {get;set;}
    private List<Order> eventOrders {get;set;}
    private Order eventOrder {get;set;}
    private String registrationErrorMessage {get;set;}
    private Boolean isRegistrationError {get;set;}
   // private Boolean reg1Complete {get;set;}
   // private Boolean reg2Complete {get;set;}
    public Boolean isSupplier {get;set;}
    public Boolean isExecutive {get;set;}
    private String supplierID {get;set;}
    private String executiveID {get;set;}
    private Community_Settings__c commSettings {get;Set;}
    public String ImportantMessage{set;get;}

    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    /* public CommunitiesHomePageController(ApexPages.StandardController stdController) {
        this.mysObject = (sObject)stdController.getRecord();
    } */
    public CommunitiesHomePageController() {
        //this.mysObject = (sObject)stdController.getRecord();
        commSettings = Community_Settings__c.getOrgDefaults();
        
        //if(UserInfo.getUserId() == '00563000000IEp3AAG'){
          //  userId='00563000000ZHOVAA4';
        //}else{
            userId = UserInfo.getUserId();
        //if(commSettings != null && String.isNotBlank(commSettings.Community_User_Profile__c) && commSettings.Community_User_Profile__c != UserInfo.getProfileId()){
          //  userId = commSettings.Test_User__c;
        //}
        //}
        System.Debug('The Current User ID: '+ userId);
        try{
            currentUser = [SELECT Id, Name, ContactId, Contact.RecordTypeId FROM User where Id = :userId];
        }catch(Exception ex){
            System.debug('Error: '+ ex.getMessage() +'. On Line: '+ ex.getLineNumber()+'. Stack Trace: '+ ex.getStackTraceString());
        }
        supplierId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        executiveID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();

        System.debug('The User Record with Contact Information: ' + currentUser);
        System.debug('Supplier Contact Record Type Id: '+supplierId);
        System.debug('Executive Contact Record Type Id: '+ executiveId);
        initializeHomePage();

    }

    public void initializeHomePage(){
      //  try{
        getUserContact();
        getUserOrderProducts();
       // getHeaderLogo();
       // getEventAccountDetails();
       // populateEventFeed();
        getAssociatedOrder();
        determineContactType();
        //}catch(Exception ex){
           // System.debug('Error: '+ ex.getStackTraceString());
        //}
    }

    private void getUserContact(){
        try{
          If(currentUser.ContactID != null)  userContact = [SELECT Id, RecordType.Name, Intervention_Flag__c, AccountId, Account.Intervention_Flag__c FROM Contact WHERE Id = :currentUser.ContactId];
        }catch(Exception ex){
            System.debug('Error: '+ ex.getMessage() +'. On Line: '+ ex.getLineNumber()+'. Stack Trace: '+ ex.getStackTraceString());
            return;
        }
        System.Debug('The User Contact: '+ userContact);
    }

    private void getUserOrderProducts(){
        //get all Orders associated with the logged in User
      /*  userEventIds = new Set<Id>();

        //Iterate through all orders associated with the User and Get the Event from the Order
        for(Order o:[SELECT Id, BillToContactId,Order_Event__c FROM Order WHERE BillToContactId = :currentUser.ContactId]){
            if(o.Order_Event__c !=null) userEventIds.add(o.Order_Event__c);
        }
        System.debug('The Event Ids from Orders associated with the Contact: ' + userEventIds); */
        //Get the Most Current Event for the Twitter Feed
        //if(userEventIds.size() > 0)
        try{
            String eventIdFromURL = ApexPages.currentPage().getParameters().get('eId');
            if(String.isNotBlank(eventIdFromURL)){
                
                currentEvent = EventHelper.getEvent(eventIdFromURL);
            } else {
                if(userContact != null){
                    List<Order> orders = [SELECT Id, Order_Event__c,Status FROM Order WHERE (Order_Event__r.Start_Date__c >= Today AND BillToContactId =:userContact.Id AND Status != 'Cancelled') ORDER BY Order_Event__r.Start_Date__c ASC LIMIT 1];
                    System.debug(LoggingLevel.ERROR, orders);
                    if (orders.size() > 0) {
                        Id pId = orders.get(0).Order_Event__c;

                        currentEvent = EventHelper.getEvent(pId);
                    } 
                }else {
 
                    currentEvent = EventHelper.getClosestEventByDate();
                }
        
            }
           // eventName = currentEvent.Family; 
            System.debug('The Current Event: '+ currentEvent);

            if(currentEvent == null){
                noCharity = true;
            }
            if(String.isBlank(currentEvent.Charity_Logo_URL__c)) {
                noCharity = true;
            }else{
                noCharity = false;
                charityLogoUrl = currentEvent.Charity_Logo_URL__c;
                charityOrgURL = currentEvent.Charity_Org_URL__c;
                charityGoal = (Integer)currentEvent.Charity_Goal__c;
                charityRaised = (Integer)currentEvent.Charity_Raised__c;
            }
            if(String.isBlank(currentEvent.Twitter__c)){
                twitterName = 'Twitter';
            }else{
                twitterName= currentEvent.Twitter__c;
            }
        }catch(Exception ex){
            System.debug('Error: '+ ex.getMessage() +'. On Line: '+ ex.getLineNumber()+'. Stack Trace: '+ ex.getStackTraceString());
        }
        System.debug('Twitter Name: '+ twitterName);
        System.debug('Charity Logo URL: '+ charityLogoUrl);
        System.debug('Charity Goal: '+ charityGoal);
        System.debug('Charity Raised: '+ charityRaised);

    }

   

    private void getAssociatedOrder(){
        //eventOrder = new Order();
        isRegistrationError = false;
        if(currentEvent != null){
            eventOrders =[SELECT Status,Opportunity.Message_Center__c,Id,OpportunityId,AccountId, Order_Event__c, Post_Event_Survey_Complete__c, // Registration_Part_1_Complete__c //Registration_Part_2_Complete__c, 
                            Event_Account_Detail__c, Event_Account_Detail__r.Registration_Part_1_Complete__c,Event_Account_Detail__r.Registration_Part_1_Submitted__c,
                            Event_Account_Detail__r.Registration_Part_2_Complete__c, Event_Account_Detail__r.Registration_Part_2_Submitted__c FROM Order where BillToContactId =: currentUser.ContactId and Order_Event__c = :currentEvent.Id and Status != 'Cancelled'];
        }
        if(eventOrders != null && eventOrders.size() > 0){
            eventOrder = eventOrders[0];
            System.debug('Event Order: '+ eventOrder);
            ImportantMessage = eventOrder.Opportunity.Message_Center__c;
        }
        if(eventOrders == null){
            registrationErrorMessage = 'There is an Error with the Registration Process. Please contact your Sales Manager.';
            isRegistrationError = true;
        }

    }

  
    private void determineContactType(){
        isSupplier = false;
        isExecutive = false;

        if(currentUser.Contact.RecordTypeID == supplierId){
            isSupplier = true;
        }
        if(currentUser.Contact.RecordTypeId == executiveID){
            isExecutive = true;
        }
        System.debug('Is Supplier User: '+isSupplier);
        System.debug('Is Executive User: '+ isExecutive);

    }

    //Getter Methods for Home Page Data
    public String getTwitterName(){
        return twitterName;
    }


    public String getCharityLogo(){

        return charityLogoUrl;
    }

    public Integer getCharityGoal(){
        return charityGoal;
    }
    public Integer getCharityRaised(){
        return charityRaised;
    }

    public Boolean getNoCharity(){
        return noCharity;
    }
    public String getCharityOrgURL(){
        return charityOrgURL;
    }
    public Product2 getCurrentEvent(){
        return currentEvent;
    }
   

    public Order getEventOrder(){
        return eventOrder;
    }

    public Boolean getIsRegistrationError(){
        return isRegistrationError;
    }
    public String getRegistrationErrorMessage(){
        return registrationErrorMessage;
    }

   
}