public with sharing class OrderTriggerHandler {
    private boolean m_isExecuting = false;
    private integer BatchSize = 0;
    public OrderHandler handler {get;set;}
    //public Community_Settings__c commSett {get;set;}
    
    public OrderTriggerHandler(boolean isExecuting, integer size) {
        m_isExecuting = isExecuting;
        BatchSize = size;
        //commSett = Community_Settings__c.getOrgDefaults();
        handler = new OrderHandler();
    }

    public void OnBeforeInsert(Order[] newOrders){
            handler.executeConfirmationOwnerProcess(newOrders);
    }
    
    public void OnAfterInsert(Order[] newOrders) {      
        handler.updateTotalRegistrants(newOrders);
        //handler.executeUserCreationForActiveEvents(newOrders);
    }
    public void OnAfterUpdate(Order[] newOrders){
        handler.updateTotalRegistrants(newOrders);
    }
}