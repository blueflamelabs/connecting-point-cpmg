@isTest
private class DirectoryWrapper_Test {
    /*
        Test Data Needed:
        1.) Participation_Record__C (Event_Account_Details__c) Executive
        2.)  Participation_Record__C (Event_Account_Details__c) Supplier
        3.) Event Staff__c Record
        4.) Session_Speaker__c record

    */
    @isTest static void DirectoryWrapper_Constructor_Test() {
        // Implement test code
        Test.startTest();
            DirectoryWrapper dirWrapper = new DirectoryWrapper();
        Test.stopTest();
        //Asser that all collections are empty;
        System.assertEquals(dirWrapper.executiveDirectoryList.isEmpty(),true);
        System.assertEquals(dirWrapper.supplierDirectoryList.isEmpty(),true);
        System.assertEquals(dirWrapper.cpmgStaffList.isEmpty(),true);
        System.assertEquals(dirWrapper.speakerList.isEmpty(),true);
    }
    
    @isTest static void addExecutive_Test() {
        // Prepare Test Data.
        TestDataGenerator.createInactiveTriggerSettings();

        //Event Setup
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Active Event',true,Date.today().addDays(-10),Date.today().addDays(-5),Date.today().addMonths(-2), Date.today().addMonths(-1));
        prod.Start_Date__c = Date.today().addDays(10);
        prod.Family ='BuildPoint';
        prod.Event_Navigation_Color__c = '90135d';
        prod.Event_Tint_Color__c = 'EEEEEE';
        update prod;

        Profile standardUser = [SELECT Id FROM Profile where Name ='Standard User' LIMIT 1];
        User execUserCPMG = TestDataGenerator.CreateUser(true,standardUser.Id,'Test','Exec',null,'testExec1234@cpmg.com',null);
        User supplierUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Supplier',null,'testSupplier1234@cpmg.com',null);
        User supportUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Support',null,'testSupport1234@cpmg.com',null);


        Event_Staff__c execStaff = TestDataGenerator.createEventStaff(true,prod,execUserCPMG,'Recruiter',true,1);
        Event_Staff__c supplierStaff = TestDataGenerator.createEventStaff(true,prod,supplierUserCPMG,'Salesperson',true,2);
        Event_Staff__c supportStaff = TestDataGenerator.createEventStaff(true,prod,supportUserCPMG,'Support',true,3);

        //Set Up Executive User
        Id execAccRType =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Id execConRType =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Account execAcc = TestDataGenerator.createTestAccount(true,'Test Exec Account', execAccRType);
        //Wrapper wants a website.
        execAcc.Website = 'https://www.google.com';
        update execAcc;
        Contact execCon = TestDataGenerator.createTestContact(true,'Test Exec','Last Account','testexec@test.com',execAcc, execConRType);

        Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        User execUser = TestDataGenerator.CreateUser(true,commUserProfId.id,'Test Exec','Last Account','603-555-5555','testexec@test.com', execCon.Id);

        Opportunity execOpp = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),execAcc,'Closed Won',prod);

        Id execPRRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c execPRecod = TestDataGenerator.createParticipationRecord(true,prod,execAcc,execOpp,execCon,execPRRType);

        Order execConf = TestDataGenerator.createConfirmation(true,execAcc,execCon,'Attending Event Contact',execOpp,execOpp.CloseDate,execPRecod,prod);

        //Create Question
        Map<String,RecordTypeInfo> questionRTypeMap = Schema.SObjectType.Question__c.getRecordTypeInfosByName();
        Question__c picklistQuestion = TestDataGenerator.createQuestion(true,'Test Category Pickilist',200,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
            Question_Answers_Values__c qav1Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option','Select');
            Question_Answers_Values__c qav2Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option2','Select2');
            Question_Answers_Values__c qav3Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option3','Select3');
        Question__c longQuestion = TestDataGenerator.createQuestion(true,'Long Description',2000,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
        Question__c shortQuestion = TestDataGenerator.createQuestion(true,'Short Description',200,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
            
        //Create the 3 Event Questions
        Event_Question__c categoryEQ = TestDataGenerator.createEventQuestion(true,'Category',prod,'Category',picklistQuestion,true,1,null);
        Event_Question__c longEQ = TestDataGenerator.createEventQuestion(true,'Company Long Description',prod,'Company',longQuestion,true,1,'Company Long Description');
        Event_Question__c shortEQ = TestDataGenerator.createEventQuestion(true,'Company Short Description',prod,'Company',shortQuestion,true,1,'Company Short Description');

        //Create Contact Answer Questions
        Contact_Event_Answers__c categoryAnswer = TestDataGenerator.createQuestionAnswers(true,execConf,execPRecod,categoryEQ,execCon,'Select2;Select3',null,null);
        Contact_Event_Answers__c longAnswer = TestDataGenerator.createQuestionAnswers(true,execConf,execPRecod,longEQ,execCon,'Long Answer',null,null);
        Contact_Event_Answers__c shortAnswer = TestDataGenerator.createQuestionAnswers(true,execConf,execPRecod,shortEQ,execCon,'Short Answer',null,null);

        DirectoryWrapper dirWrapper = new DirectoryWrapper();

        Test.startTest();
            dirWrapper.addExecutive(new ParticipationRecordWrapper(execPRecod, longAnswer, shortAnswer, categoryAnswer));
        Test.stopTest();
            System.assertEquals(dirWrapper.executiveDirectoryList.size(),1);
            System.assertEquals(dirWrapper.supplierDirectoryList.isEmpty(),true);
            System.assertEquals(dirWrapper.cpmgStaffList.isEmpty(),true);
            System.assertEquals(dirWrapper.speakerList.isEmpty(),true);
    }

    static testmethod void addSupplier_Test(){
        // Prepare Test Data.
        TestDataGenerator.createInactiveTriggerSettings();

        //Event Setup
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Active Event',true,Date.today().addDays(-10),Date.today().addDays(-5),Date.today().addMonths(-2), Date.today().addMonths(-1));
        prod.Start_Date__c = Date.today().addDays(10);
        prod.Family ='BuildPoint';
        prod.Event_Navigation_Color__c = '90135d';
        prod.Event_Tint_Color__c = 'EEEEEE';
        update prod;

        Profile standardUser = [SELECT Id FROM Profile where Name ='Standard User' LIMIT 1];
        User execUserCPMG = TestDataGenerator.CreateUser(true,standardUser.Id,'Test','Exec',null,'testExec1234@cpmg.com',null);
        User supplierUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Supplier',null,'testSupplier1234@cpmg.com',null);
        User supportUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Support',null,'testSupport1234@cpmg.com',null);


        Event_Staff__c execStaff = TestDataGenerator.createEventStaff(true,prod,execUserCPMG,'Recruiter',true,1);
        Event_Staff__c supplierStaff = TestDataGenerator.createEventStaff(true,prod,supplierUserCPMG,'Salesperson',true,2);
        Event_Staff__c supportStaff = TestDataGenerator.createEventStaff(true,prod,supportUserCPMG,'Support',true,3);

        //Set Up Executive User
        Id suppAccRType =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Id suppConRType =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Account suppAcc = TestDataGenerator.createTestAccount(true,'Test Exec Account', suppAccRType);
        //Wrapper wants a website.
        suppAcc.Website = 'https://www.google.com';
        update suppAcc;
        Contact suppCon = TestDataGenerator.createTestContact(true,'Test Exec','Last Account','testexec@test.com',suppAcc, suppConRType);

        Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        User execUser = TestDataGenerator.CreateUser(true,commUserProfId.id,'Test Supp','Last Account','603-555-5555','testexec@test.com', suppCon.Id);

        Opportunity suppOpp = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),suppAcc,'Closed Won',prod);

        Id execPRRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Suppliers').getRecordTypeId();
        Event_Account_Details__c suppPRecod = TestDataGenerator.createParticipationRecord(true,prod,suppAcc,suppOpp,suppCon,execPRRType);

        Order execConf = TestDataGenerator.createConfirmation(true,suppAcc,suppCon,'Attending Event Contact',suppOpp,suppOpp.CloseDate,suppPRecod,prod);

        //Create Question
        Map<String,RecordTypeInfo> questionRTypeMap = Schema.SObjectType.Question__c.getRecordTypeInfosByName();
        Question__c picklistQuestion = TestDataGenerator.createQuestion(true,'Test Category Pickilist',200,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
            Question_Answers_Values__c qav1Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option','Select');
            Question_Answers_Values__c qav2Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option2','Select2');
            Question_Answers_Values__c qav3Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option3','Select3');
        Question__c longQuestion = TestDataGenerator.createQuestion(true,'Long Description',2000,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
        Question__c shortQuestion = TestDataGenerator.createQuestion(true,'Short Description',200,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
            
        //Create the 3 Event Questions
        Event_Question__c categoryEQ = TestDataGenerator.createEventQuestion(true,'Category',prod,'Category',picklistQuestion,true,1,null);
        Event_Question__c longEQ = TestDataGenerator.createEventQuestion(true,'Company Long Description',prod,'Company',longQuestion,true,1,'Company Long Description');
        Event_Question__c shortEQ = TestDataGenerator.createEventQuestion(true,'Company Short Description',prod,'Company',shortQuestion,true,1,'Company Short Description');

        //Create Contact Answer Questions
        Contact_Event_Answers__c categoryAnswer = TestDataGenerator.createQuestionAnswers(true,execConf,suppPRecod,categoryEQ,suppCon,'Select2;Select3',null,null);
        Contact_Event_Answers__c longAnswer = TestDataGenerator.createQuestionAnswers(true,execConf,suppPRecod,longEQ,suppCon,'Long Answer',null,null);
        Contact_Event_Answers__c shortAnswer = TestDataGenerator.createQuestionAnswers(true,execConf,suppPRecod,shortEQ,suppCon,'Short Answer',null,null);

        DirectoryWrapper dirWrapper = new DirectoryWrapper();

        Test.startTest();
            dirWrapper.addSupplier(new ParticipationRecordWrapper(suppPRecod, longAnswer, shortAnswer, categoryAnswer));
        Test.stopTest();
            System.assertEquals(dirWrapper.supplierDirectoryList.size(),1);
            System.assertEquals(dirWrapper.executiveDirectoryList.isEmpty(),true);
            System.assertEquals(dirWrapper.cpmgStaffList.isEmpty(),true);
            System.assertEquals(dirWrapper.speakerList.isEmpty(),true);
    }

    static testmethod void addStaff_Test(){
        // Prepare Test Data.
        TestDataGenerator.createInactiveTriggerSettings();

        //Event Setup
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Active Event',true,Date.today().addDays(-10),Date.today().addDays(-5),Date.today().addMonths(-2), Date.today().addMonths(-1));
        prod.Start_Date__c = Date.today().addDays(10);
        prod.Family ='BuildPoint';
        prod.Event_Navigation_Color__c = '90135d';
        prod.Event_Tint_Color__c = 'EEEEEE';
        update prod;

        Profile standardUser = [SELECT Id FROM Profile where Name ='Standard User' LIMIT 1];
        User execUserCPMG = TestDataGenerator.CreateUser(true,standardUser.Id,'Test','Exec',null,'testExec1234@cpmg.com',null);
        User supplierUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Supplier',null,'testSupplier1234@cpmg.com',null);
        User supportUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Support',null,'testSupport1234@cpmg.com',null);


        Event_Staff__c execStaff = TestDataGenerator.createEventStaff(true,prod,execUserCPMG,'Recruiter',true,1);
        Event_Staff__c supplierStaff = TestDataGenerator.createEventStaff(true,prod,supplierUserCPMG,'Salesperson',true,2);
        Event_Staff__c supportStaff = TestDataGenerator.createEventStaff(true,prod,supportUserCPMG,'Support',true,3);

        DirectoryWrapper dirWrapper = new DirectoryWrapper();
        
        Test.startTest();
            dirWrapper.addStaff(execStaff);
            dirWrapper.addStaff(supplierStaff);
            dirWrapper.addStaff(supportStaff);
        Test.stopTest();
            System.assertEquals(dirWrapper.cpmgStaffList.size(),3);
            System.assertEquals(dirWrapper.supplierDirectoryList.isEmpty(),true);
            System.assertEquals(dirWrapper.executiveDirectoryList.isEmpty(),true);
            System.assertEquals(dirWrapper.speakerList.isEmpty(),true);
    }

    static testMethod void addSpeaker_Test(){
        //PRepare Test Data.
        TestDataGenerator.createInactiveTriggerSettings();

        //Event Setup
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Active Event',true,Date.today().addDays(-10),Date.today().addDays(-5),Date.today().addMonths(-2), Date.today().addMonths(-1));
        prod.Start_Date__c = Date.today().addDays(10);
        prod.Family ='BuildPoint';
        prod.Event_Navigation_Color__c = '90135d';
        prod.Event_Tint_Color__c = 'EEEEEE';
        update prod;

        //Create Speaker Sessions
        Speaker_Session__c speakSession1 = TestDataGenerator.createSpeakerSessions(true,String.valueOf( System.Now()), System.Now(),'Speaker Session 1', prod,'Overview Test','Http://www.google.com','Presenter 1','Presentation Title');
        Speaker_Session__c speakSession2 = TestDataGenerator.createSpeakerSessions(true,String.valueOf( System.Now().addDays(-11)), System.Now().addDays(-1),'Speaker Session 2', prod,'Overview Test','Http://www.google.com','Presenter 2','Presentation Title For Session 2');

        DirectoryWrapper dirWrapper = new DirectoryWrapper();

        Test.startTest();
            dirWrapper.addSpeaker(speakSession1);
            dirWrapper.addSpeaker(speakSession2);
        Test.stopTest();
            System.assertEquals(dirWrapper.speakerList.size(),2);
            System.assertEquals(dirWrapper.supplierDirectoryList.isEmpty(),true);
            System.assertEquals(dirWrapper.executiveDirectoryList.isEmpty(),true);
            System.assertEquals(dirWrapper.cpmgStaffList.isEmpty(),true);
    }

    static testMethod void JSONSerailizeAllCollections_test(){
        //Need all types of Data, supplier PR (And Questions), Executive PR (and questions), CPMG Staff, Speakers.
        // Prepare Test Data.
        TestDataGenerator.createInactiveTriggerSettings();

        //Event Setup
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Active Event',true,Date.today().addDays(-10),Date.today().addDays(-5),Date.today().addMonths(-2), Date.today().addMonths(-1));
        prod.Start_Date__c = Date.today().addDays(10);
        prod.Family ='BuildPoint';
        prod.Event_Navigation_Color__c = '90135d';
        prod.Event_Tint_Color__c = 'EEEEEE';
        update prod;

        Profile standardUser = [SELECT Id FROM Profile where Name ='Standard User' LIMIT 1];
        User execUserCPMG = TestDataGenerator.CreateUser(true,standardUser.Id,'Test','Exec',null,'testExec1234@cpmg.com',null);
        User supplierUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Supplier',null,'testSupplier1234@cpmg.com',null);
        User supportUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Support',null,'testSupport1234@cpmg.com',null);


        Event_Staff__c execStaff = TestDataGenerator.createEventStaff(true,prod,execUserCPMG,'Recruiter',true,1);
        Event_Staff__c supplierStaff = TestDataGenerator.createEventStaff(true,prod,supplierUserCPMG,'Salesperson',true,2);
        Event_Staff__c supportStaff = TestDataGenerator.createEventStaff(true,prod,supportUserCPMG,'Support',true,3);

        //Set Up Executive User
        Id execAccRType =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Id execConRType =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Id suppAccRType =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Id suppConRType =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Account execAcc = TestDataGenerator.createTestAccount(true,'Test Exec Account', execAccRType);
        //Wrapper wants a website.
        execAcc.Website = 'https://www.google.com';
        update execAcc;

        Account suppAcc = TestDataGenerator.createTestAccount(true,'Test Supplier Account', suppAccRType);
        //Wrapper wants a website.
        suppAcc.Website = 'https://www.cpmginc.com';
        update suppAcc;
        Contact execCon = TestDataGenerator.createTestContact(true,'Test Exec','Last Account','testexec@test.com',execAcc, execConRType);
        Contact suppCon = TestDataGenerator.createTestContact(true,'Test Supplier','Last Account','testsupplier@test.com',suppAcc, suppConRType);

        Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        User execUser = TestDataGenerator.CreateUser(true,commUserProfId.id,'Test Exec','Last Account','603-555-5555','testexec@test.com', execCon.Id);
        User suppUser = TestDataGenerator.CreateUser(true,commUserProfId.id,'Test Supp','Last Account','603-555-5555','testsupplier@test.com', suppCon.Id);

        Opportunity execOpp = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),execAcc,'Closed Won',prod);
        Opportunity suppOpp = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),suppAcc,'Closed Won',prod);

        Id execPRRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Id suppPRRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Suppliers').getRecordTypeId();
        Event_Account_Details__c execPRecod = TestDataGenerator.createParticipationRecord(true,prod,execAcc,execOpp,execCon,execPRRType);
        Event_Account_Details__c suppPRecod = TestDataGenerator.createParticipationRecord(true,prod,suppAcc,suppOpp,suppCon,suppPRRType);

        Order execConf = TestDataGenerator.createConfirmation(true,execAcc,execCon,'Attending Event Contact',execOpp,execOpp.CloseDate,execPRecod,prod);
        Order suppConf = TestDataGenerator.createConfirmation(true,suppAcc,suppCon,'Attending Event Contact',suppOpp,suppOpp.CloseDate,suppPRecod,prod);

        //Create Question
        Map<String,RecordTypeInfo> questionRTypeMap = Schema.SObjectType.Question__c.getRecordTypeInfosByName();
        Question__c picklistQuestion = TestDataGenerator.createQuestion(true,'Test Category Pickilist',200,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
            Question_Answers_Values__c qav1Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option','Select');
            Question_Answers_Values__c qav2Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option2','Select2');
            Question_Answers_Values__c qav3Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option3','Select3');
        Question__c longQuestion = TestDataGenerator.createQuestion(true,'Long Description',2000,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
        Question__c shortQuestion = TestDataGenerator.createQuestion(true,'Short Description',200,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
            
        //Create the 3 Event Questions
        Event_Question__c categoryEQ = TestDataGenerator.createEventQuestion(true,'Category',prod,'Category',picklistQuestion,true,1,null);
        Event_Question__c longEQ = TestDataGenerator.createEventQuestion(true,'Company Long Description',prod,'Company',longQuestion,true,1,'Company Long Description');
        Event_Question__c shortEQ = TestDataGenerator.createEventQuestion(true,'Company Short Description',prod,'Company',shortQuestion,true,1,'Company Short Description');

        //Create Contact Answer Questions
        Contact_Event_Answers__c categoryAnswer = TestDataGenerator.createQuestionAnswers(true,execConf,execPRecod,categoryEQ,execCon,'Select2;Select3',null,null);
        Contact_Event_Answers__c longAnswer = TestDataGenerator.createQuestionAnswers(true,execConf,execPRecod,longEQ,execCon,'Long Answer',null,null);
        Contact_Event_Answers__c shortAnswer = TestDataGenerator.createQuestionAnswers(true,execConf,execPRecod,shortEQ,execCon,'Short Answer',null,null);

        //Create SUpplier Contact Answer Questions
        Contact_Event_Answers__c suppCategoryAnswer = TestDataGenerator.createQuestionAnswers(true,suppConf,suppPRecod,categoryEQ,suppCon,'Select;Select3',null,null);
        Contact_Event_Answers__c suppLongAnswer = TestDataGenerator.createQuestionAnswers(true,suppConf,suppPRecod,longEQ,suppCon,'Long SUpplier Answer',null,null);
        Contact_Event_Answers__c suppShortAnswer = TestDataGenerator.createQuestionAnswers(true,suppConf,suppPRecod,shortEQ,suppCon,'Short Supplier Answer',null,null);

        //Create Speaker Sessions
        Speaker_Session__c speakSession1 = TestDataGenerator.createSpeakerSessions(true,String.valueOf( System.Now()), System.Now(),'Speaker Session 1', prod,'Overview Test','Http://www.google.com','Presenter 1','Presentation Title');
        Speaker_Session__c speakSession2 = TestDataGenerator.createSpeakerSessions(true,String.valueOf( System.Now().addDays(-11)), System.Now().addDays(-1),'Speaker Session 2', prod,'Overview Test','Http://www.cpmgInc.com','Presenter 2','Presentation Title For Session 2');

            DirectoryWrapper dirWrapper = new DirectoryWrapper();
            dirWrapper.addExecutive(new ParticipationRecordWrapper(execPRecod, longAnswer, shortAnswer, categoryAnswer));
            dirWrapper.addSupplier(new ParticipationRecordWrapper(suppPRecod, suppLongAnswer, suppShortAnswer, suppCategoryAnswer));
            dirWrapper.addStaff(execStaff);
            dirWrapper.addStaff(supplierStaff);
            dirWrapper.addStaff(supportStaff);
            dirWrapper.addSpeaker(speakSession1);
            dirWrapper.addSpeaker(speakSession2);

        Test.startTest();
            dirWrapper.JSONSerializeAllCollections();
        Test.stopTest();
    
    }
    
}