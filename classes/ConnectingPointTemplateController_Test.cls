@isTest
private class ConnectingPointTemplateController_Test {

	@isTest
	private static void test_constructor() {
		// Implement test code
		Id execConRtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();

		Account a = TestDataGenerator.createTestAccount(true,'Executive Test Account');

		Contact c = TestDataGenerator.createTestContact(true,'Test','Exec','t@t.com',a,(String) execConRtype);
		c.Has_Event__c = true;
		update c;
		Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event',true,Date.today(), Date.today().addDays(5), Date.today().addMonths(-3), Date.today().addMonths(-2));
		
		//Start Defining Event "Product" related info.
		Profile communityProfile = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
		
		User commUser = TestDataGenerator.CreateUser(true,communityProfile.Id,'Test','Exec',null,'testExec@cpmg.com',c.Id);

		PageReference pageRef = Page.ConnectingPointTemplate;
		pageRef.getParameters().put('eId',prod.Id);
		Test.setCurrentPageReference(pageRef);

		// create an instance of the controller

	
		Test.startTest();
			System.runAs(commUser){
				ConnectingPointTemplateController cpTController = new ConnectingPointTemplateController();
				}
		Test.stopTest();
		
	}
	@isTest
	private static void test_CommHomePage() {
		// Implement test code
		Id execConRtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();

		Account a = TestDataGenerator.createTestAccount(true,'Executive Test Account');

		Contact c = TestDataGenerator.createTestContact(true,'Test','Exec','t@t.com',a,(String) execConRtype);
		c.Has_Event__c = true;
		update c;
		Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event',true,Date.today(), Date.today().addDays(5), Date.today().addMonths(-3), Date.today().addMonths(-2));
		
		//Start Defining Event "Product" related info.
		Profile communityProfile = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
		
		User commUser = TestDataGenerator.CreateUser(true,communityProfile.Id,'Test','Exec',null,'testExec@cpmg.com',c.Id);

		PageReference pageRef = Page.CommunitiesHomePage;
		pageRef.getParameters().put('eId',prod.Id);
		Test.setCurrentPageReference(pageRef);

		// create an instance of the controller

	
		Test.startTest();
			System.runAs(commUser){
				ConnectingPointTemplateController cpTController = new ConnectingPointTemplateController();
				Integer x =cpTController.getCurrentPageIndex();
				System.assertEquals(1,x);
				}
		Test.stopTest();
		
	}
	@isTest
	private static void test_CommTravelPage() {
		// Implement test code
		Id execConRtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();

		Account a = TestDataGenerator.createTestAccount(true,'Executive Test Account');

		Contact c = TestDataGenerator.createTestContact(true,'Test','Exec','t@t.com',a,(String) execConRtype);
		c.Has_Event__c = true;
		update c;
		Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event',true,Date.today(), Date.today().addDays(5), Date.today().addMonths(-3), Date.today().addMonths(-2));
		
		//Start Defining Event "Product" related info.
		Profile communityProfile = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
		
		User commUser = TestDataGenerator.CreateUser(true,communityProfile.Id,'Test','Exec',null,'testExec@cpmg.com',c.Id);

		PageReference pageRef = Page.CommunitiesTravel;
		pageRef.getParameters().put('eId',prod.Id);
		Test.setCurrentPageReference(pageRef);

		// create an instance of the controller

	
		Test.startTest();
			System.runAs(commUser){
				ConnectingPointTemplateController cpTController = new ConnectingPointTemplateController();
				Integer x =cpTController.getCurrentPageIndex();
				System.assertEquals(2,x);
				}
		Test.stopTest();
		
	}
		@isTest
	private static void test_EventDirectoryPage() {
		// Implement test code
		Id execConRtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();

		Account a = TestDataGenerator.createTestAccount(true,'Executive Test Account');

		Contact c = TestDataGenerator.createTestContact(true,'Test','Exec','t@t.com',a,(String) execConRtype);
		c.Has_Event__c = true;
		update c;
		Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event',true,Date.today(), Date.today().addDays(5), Date.today().addMonths(-3), Date.today().addMonths(-2));
		
		//Start Defining Event "Product" related info.
		Profile communityProfile = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
		
		User commUser = TestDataGenerator.CreateUser(true,communityProfile.Id,'Test','Exec',null,'testExec@cpmg.com',c.Id);

		PageReference pageRef = Page.EventDirectory;
		pageRef.getParameters().put('eId',prod.Id);
		Test.setCurrentPageReference(pageRef);

		// create an instance of the controller

	
		Test.startTest();
			System.runAs(commUser){
				ConnectingPointTemplateController cpTController = new ConnectingPointTemplateController();
				Integer x =cpTController.getCurrentPageIndex();
				System.assertEquals(4,x);
				}
		Test.stopTest();
		
	}

		@isTest
	private static void test_CommMapPage() {
		// Implement test code
		Id execConRtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();

		Account a = TestDataGenerator.createTestAccount(true,'Executive Test Account');

		Contact c = TestDataGenerator.createTestContact(true,'Test','Exec','t@t.com',a,(String) execConRtype);
		c.Has_Event__c = true;
		update c;
		Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event',true,Date.today(), Date.today().addDays(5), Date.today().addMonths(-3), Date.today().addMonths(-2));
		
		//Start Defining Event "Product" related info.
		Profile communityProfile = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
		
		User commUser = TestDataGenerator.CreateUser(true,communityProfile.Id,'Test','Exec',null,'testExec@cpmg.com',c.Id);

		PageReference pageRef = Page.CommunitiesMap;
		pageRef.getParameters().put('eId',prod.Id);
		Test.setCurrentPageReference(pageRef);

		// create an instance of the controller

	
		Test.startTest();
			System.runAs(commUser){
				ConnectingPointTemplateController cpTController = new ConnectingPointTemplateController();
				Integer x =cpTController.getCurrentPageIndex();
				System.assertEquals(5,x);
				}
		Test.stopTest();
		
	}
		@isTest
	private static void test_EventRegistrationPage() {
		// Implement test code
		Id execConRtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();

		Account a = TestDataGenerator.createTestAccount(true,'Executive Test Account');

		Contact c = TestDataGenerator.createTestContact(true,'Test','Exec','t@t.com',a,(String) execConRtype);
		c.Has_Event__c = true;
		update c;
		Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event',true,Date.today(), Date.today().addDays(5), Date.today().addMonths(-3), Date.today().addMonths(-2));
		
		//Start Defining Event "Product" related info.
		Profile communityProfile = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
		
		User commUser = TestDataGenerator.CreateUser(true,communityProfile.Id,'Test','Exec',null,'testExec@cpmg.com',c.Id);

		PageReference pageRef = Page.EventRegistration2;
		pageRef.getParameters().put('eId',prod.Id);
		Test.setCurrentPageReference(pageRef);

		// create an instance of the controller

	
		Test.startTest();
			System.runAs(commUser){
				ConnectingPointTemplateController cpTController = new ConnectingPointTemplateController();
				Integer x =cpTController.getCurrentPageIndex();
				System.assertEquals(6,x);
				}
		Test.stopTest();
		
	}
		@isTest
	private static void test_EventsPage() {
		// Implement test code
		Id execConRtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();

		Account a = TestDataGenerator.createTestAccount(true,'Executive Test Account');

		Contact c = TestDataGenerator.createTestContact(true,'Test','Exec','t@t.com',a,(String) execConRtype);
		c.Has_Event__c = true;
		update c;
		Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event',true,Date.today(), Date.today().addDays(5), Date.today().addMonths(-3), Date.today().addMonths(-2));
		
		//Start Defining Event "Product" related info.
		Profile communityProfile = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
		
		User commUser = TestDataGenerator.CreateUser(true,communityProfile.Id,'Test','Exec',null,'testExec@cpmg.com',c.Id);

		PageReference pageRef = Page.Events;
		pageRef.getParameters().put('eId',prod.Id);
		Test.setCurrentPageReference(pageRef);

		// create an instance of the controller

	
		Test.startTest();
			System.runAs(commUser){
				ConnectingPointTemplateController cpTController = new ConnectingPointTemplateController();
				Integer x =cpTController.getCurrentPageIndex();
				System.assertEquals(7,x);
				}
		Test.stopTest();
		
	}
			@isTest
	private static void test_OtherPage() {
		// Implement test code
		Id execConRtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();

		Account a = TestDataGenerator.createTestAccount(true,'Executive Test Account');

		Contact c = TestDataGenerator.createTestContact(true,'Test','Exec','t@t.com',a,(String) execConRtype);
		c.Has_Event__c = true;
		update c;
		Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event',true,Date.today(), Date.today().addDays(5), Date.today().addMonths(-3), Date.today().addMonths(-2));
		
		//Start Defining Event "Product" related info.
		Profile communityProfile = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
		
		User commUser = TestDataGenerator.CreateUser(true,communityProfile.Id,'Test','Exec',null,'testExec@cpmg.com',c.Id);

		PageReference pageRef = Page.OpportunitiesWorkflow;
		pageRef.getParameters().put('eId',prod.Id);
		Test.setCurrentPageReference(pageRef);

		// create an instance of the controller

	
		Test.startTest();
			System.runAs(commUser){
				ConnectingPointTemplateController cpTController = new ConnectingPointTemplateController();
				Integer x =cpTController.getCurrentPageIndex();
				System.assertEquals(0,x);
				}
		Test.stopTest();
		
	}

	@isTest
	private static void test_constructor_noEid() {
		// Implement test code
		Id execConRtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();

		Account a = TestDataGenerator.createTestAccount(true,'Executive Test Account');

		Contact c = TestDataGenerator.createTestContact(true,'Test','Exec','t@t.com',a,(String) execConRtype);
		c.Has_Event__c = true;
		update c;
		Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event',true,Date.today(), Date.today().addDays(5), Date.today().addMonths(-3), Date.today().addMonths(-2));
		prod.Registration_Part_1_Open_For_Executive__c = Date.today().addDays(5);
		prod.Registration_Part_1_Open_For_Supplier__c = Date.today().addDays(5);
		prod.Start_Date__c = Date.today().addMonths(4);
		update prod;
		//Start Defining Event "Product" related info.
		Profile communityProfile = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
		
		User commUser = TestDataGenerator.CreateUser(true,communityProfile.Id,'Test','Exec',null,'testExec@cpmg.com',c.Id);
		System.assert(commUser.Id != null && commUser.ContactId != null);

		
		// create an instance of the controller

	
		Test.startTest();
			System.runAs(commUser){
				PageReference pageRef = Page.ConnectingPointTemplate;
				//pageRef.getParameters().put('eId',prod.Id);
				Test.setCurrentPageReference(pageRef);

				ConnectingPointTemplateController cpTController = new ConnectingPointTemplateController();
				}
		Test.stopTest();
		
	}
}