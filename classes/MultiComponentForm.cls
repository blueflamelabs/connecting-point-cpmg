public with sharing abstract class MultiComponentForm {
    private static Set<registrationQuestionsController> controllers = new Set<registrationQuestionsController>();
    private static Set<ImageComponentController> imageControllers  = new Set<ImageComponentController>();
    public String ansValue{set;get;}
    public MultiComponentForm() {
        //imageControllers  = new Set<ImageComponentController>();
    }

    public void registerQuestionsCompController(registrationQuestionsController controller) {
        controllers.add(controller);
        //System.debug('Registered component');
    }

    public void registerImageCompController(ImageComponentController controller){
        imageControllers.add(controller);
    }

    public void saveAllComponents() {
        System.debug('$$$$$$$ansValue^^^^^^^^^^^^ '+ansValue);
        //System.debug('saveAllComponents ' + controllers.size());
        for(registrationQuestionsController controller : controllers) {
            //System.debug('Saving answers');
            controller.ansValueSave = ansValue;
            controller.saveAnswers();
            //THis is the code to save the Image component

        }

    }
    public void saveAllImages(){
        System.debug('IMAGE CONTROLLERS:  '+ imageControllers);
        for(ImageComponentController controller : imageControllers){

                controller.upsertImages();
        } 

    }
    public Integer getNumControllers(){
        return controllers.size();
    }
    public Integer getImageControllersSize(){
        return imageControllers.size();
    }

    /*public List<QuestionWrapper> getAllRegQuestions(){
        for(registrationQuestionsController controller : controllers) {
            //System.debug('Saving answers');
            for(QuestionWrapper qw: controller.questionsList){
                System.debug('The Qw: '+ qw);
            }
        }
        return null;
    } */

    public static Set<registrationQuestionsController> getControllers(){
        return controllers;
    }

    public static Set<ImageComponentController> getImageControllers(){
        return imageControllers;
    }

}