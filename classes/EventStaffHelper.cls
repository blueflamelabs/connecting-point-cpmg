public without sharing class EventStaffHelper {
	//private Map<String,Event_Staff__c> eventStaffMap {get;set;}
	/**
	 * Gets a set of the IDs of the event staff's users.
	 */
	public static Set<Id> getEventStaffUsers(Id event, Set<String> roles) {
		Set<Id> eventStaffUsers = new Set<Id>();
		List<Event_Staff__c> allEventStaff = [SELECT Id, User__c FROM Event_Staff__c WHERE CPMG_Event__c = :event AND Role__c IN :roles];
		
		for(Event_Staff__c staff : allEventStaff) {
			eventStaffUsers.add(staff.User__c);
		}
		return eventStaffUsers;
	}

	/**
	 * Gets the emails of the users in the given set of IDs.
	 */
	public static Set<String> getEmails(Set<Id> userIDs) {
		Set<String> emails = new Set<String>();
		if(userIDs.size() > 0) {
			List<User> users = [SELECT id,email FROM User WHERE Id IN :userIDs];
			for(User user : users) {
				emails.add(user.email);
			}
		}
		return emails;
	}

	public static Map<String,Event_Staff__c> getEventStaffMapByRoles(Set<Id> eventIds){
		Map<String,Event_Staff__c> eventStaffMap = new Map<String, Event_Staff__c>();
		for(Event_Staff__c eventStaff : [SELECT Id, User__c, Role__c, CPMG_Event__c FROM Event_Staff__c WHERE CPMG_Event__c IN :eventIds ]){
			eventStaffMap.put(eventStaff.Role__c+eventStaff.CPMG_Event__c, eventStaff);
		}
		return eventStaffMap;
	}

	public static List<Event_Staff__c> getEventStaffByEvent(Set<Id> eventIDs){
		return [SELECT Id, CPMG_Event__c, Directory_Staff_Order__c, Display_on_Staff_Directory__c, Role__c, User__c, User__r.Title, User__r.FirstName, User__r.LastName, User__r.Email,
						User__r.FullPhotoUrl, User__r.Department 
			    FROM Event_Staff__c WHERE CPMG_Event__c in :eventIds  and Display_on_Staff_Directory__c = true ORDER BY Directory_Staff_Order__c, User__r.Name  ASC];
	}

	public static Map<String,Object> eventStaffJSON(Event_Staff__c eStaff){
		Map<String,Object> eStaffJson = new Map<String,Object>();
		eStaffJson.put('department',eStaff.User__r.Department);
		eStaffJson.put('title',eStaff.User__r.Title);
		eStaffJson.put('email',eStaff.User__r.Email);
		eStaffJson.put('name',eStaff.User__r.FirstName+' '+ eStaff.User__r.LastName);
		eStaffJson.put('picture',eStaff.User__r.FullPhotoUrl);
		eStaffJson.put('orderNumber', eStaff.Directory_Staff_Order__c);
		return eStaffJson;
	}
}