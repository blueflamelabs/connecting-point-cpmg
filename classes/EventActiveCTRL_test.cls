@isTest
 public class EventActiveCTRL_test{
      public static testmethod void testEventActiveCTRL(){
             
          Product2 prod = new Product2(Supplier_Users_Created__c = true,isActiveCheck__c=false,IsActive = true,Name = 'Test Event', User_Link_Expiration_for_Executives__c = 2, User_Link_Expiration_for_Suppliers__c = 2, Start_Date__c =System.Today(),  ProductCode =' TE 17'); //
         
         insert prod;
         prod.isActiveCheck__c = true;
         prod.IsActive = false;
         update prod;
          
        Test.startTest();
        ApexPages.StandardController prodStandardController = new ApexPages.StandardController(Prod);
        EventActiveCTRL objeac=new EventActiveCTRL(ProdStandardController);
        objeac.updateProduct();
        objeac.updateNotProduct();
        Test.stopTest();
      }
 }