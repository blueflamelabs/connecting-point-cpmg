public class ProductWrapper{
        public String event_Date_logo_path {get;set;}
        public String circular_Logo_path {get;set;}
        public String transparent_Logo_Path {get;set;}
        
        public String CPMG_Events_Page_Label {get;set;}
        public String CPMG_Events_Page_Hotel_Label {get;set;}
        public String CPMG_Header_Transparent_Label {get;set;}
        
        public Product2 prod {get;set;}
        public Boolean isSupplier {get;set;}
        public Boolean isCPMG_Events_Page_Label{get;set;}
        public Boolean isCPMG_Events_Page_Hotel_Label{get;set;}
        public Boolean isCPMG_Header_Transparent_Label{get;set;}
        public Boolean isActive{get;set;}

        public ProductWrapper(Product2 prod){
            this.prod = prod;
            this.isActive = true;
            event_date_logo_Path = prod.Year__c+'/'+prod.family.replace(' ','_')+'.png';
            circular_Logo_path = prod.Year__c+'/'+prod.family.replace(' ','_')+'.png';
            transparent_Logo_Path = prod.Year__c+'/'+prod.family.replace(' ','_')+'.png';
            CPMG_Events_Page_Label = 'CPMG_Events_Page_'+prod.Year__c;
            CPMG_Events_Page_Hotel_Label = 'CPMG_Events_Page_Hotel_'+prod.Year__c;
            CPMG_Header_Transparent_Label = 'CPMG_Header_Transparent_'+prod.Year__c;
            isCPMG_Events_Page_Label        = true;
            isCPMG_Events_Page_Hotel_Label  = true;
            isCPMG_Header_Transparent_Label = true;
            
        }

        public ProductWrapper(Product2 prod, Boolean isSupplier){
            this.prod = prod;
            this.isActive = true;
            this.isSupplier = isSupplier;
            event_date_logo_Path = prod.Year__c+'/'+prod.family.replace(' ','_')+'.png';
            circular_Logo_path = prod.Year__c+'/'+prod.family.replace(' ','_')+'.png';
            transparent_Logo_Path = prod.Year__c+'/'+prod.family.replace(' ','_')+'.png';
            CPMG_Events_Page_Label = 'CPMG_Events_Page_'+prod.Year__c;
            CPMG_Events_Page_Hotel_Label = 'CPMG_Events_Page_Hotel_'+prod.Year__c;
            CPMG_Header_Transparent_Label = 'CPMG_Header_Transparent_'+prod.Year__c;
            isCPMG_Events_Page_Label        = true;
            isCPMG_Events_Page_Hotel_Label  = true;
            isCPMG_Header_Transparent_Label = true;
        }
}