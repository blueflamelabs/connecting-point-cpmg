public  class registrationQuestionsController {
    /*
        WHEN THIS IS DONE AND COMPLETED, WE WILL BE ABLE TO COMMENT THE FOLLOWING FROM THE EVENT REGISTRATION2CONTROLLER
            653-702
    */

  //private static Boolean initialized = false;
  public String currentContactRecordType {get;set;}
  public Contact currentContact {get;set;}
  public UserPermissions permissions { get;set; }
  public Static UserPermissions permissionsCheck { get;set; }
  public Product2 currentEvent {get;set;}
  public Order currentConfirmation {get;set;}
  public String  questionType {get;set;}
  public String phase {get;set;}
  public Boolean readOnly {get;set;}
  public String eventId {get;set;}
  public String contactId {get;set;}
  public List<QuestionWrapper> questionsList {get;set;}
  public QuestionWrapper q {get;set;}
  public String name {get;set;}
  public String ansValueSave{set;get;}
  public static string ansValues{set;get;}
  //public String selectedValues{set;get;}
  public boolean isPrimaryAreaofInterest{set;get;}
  public String EventQuestionID{set;get;}
  //public String currentSelect {get;set;}
  //public String answer {get;set;}
  public String[] answers {get;set;}
  public String singleAns{set;get;}
  public MultiComponentForm parentForm {
    get;
    set {
      try {
        if(value != null) {
          parentForm = value;
          value.registerQuestionsCompController(this);
          System.debug(parentForm.getNumControllers());
        }
      } catch (Exception ex) {}
    }
  }


  public registrationQuestionsController(){
        /*System.debug('Registration Questions Controller Instantiated: ');
        //answerList2 = new List<String>();
        answers = new String[]{};
        q.answerList = new List<String>();*/ 
       
  }
 
  public List<QuestionWrapper> getQuestionList(){
      if (questionsList == null)
          questionsList = buildQuestions(phase);
        return questionsList;
  }
  
  
  //This builds up all questions for a specific phase of the registration process
  // private List<QuestionDto> buildQuestions(String p_phase) {
  private List<QuestionWrapper> buildQuestions(String p_phase) {

    List<QuestionWrapper> questionsSection = new List<QuestionWrapper>();
    try{
        Set<Id> questionIds = new Set<Id>();
        Map<Id, Event_Question__c> eventQuestions = EventQuestionHelper.getEventQuestions(p_phase, currentContactRecordType,null,eventId);
        for (Event_Question__c eq : eventQuestions.values()) {
        questionIds.add(eq.Question__c);
        }
        //SYstem.debug('Question Ids: '+ questionIds);
        Map<Id, Question__c> questions = QuestionsHelper.getQuestions(questionIds);
    
        Map<Id, Contact_Event_Answers__c> eventAnswers = ContactEventAnswerHelper.getEventQuestionAnswers(eventQuestions.keySet(), currentConfirmation.Event_Account_Detail__c);
        //System.debug('Event Answers: ' + eventAnswers);
        Integer x =1;
        for (Event_Question__c eq : eventQuestions.values()) {
        
        
        
        q= new QuestionWrapper(questions.get(eq.Question__c), eq, eventAnswers.get(eq.Id), contactId, x);
        if(eq.Display_Type__c == 'Primary Area of Interest' || test.isrunningtest()){
            isPrimaryAreaofInterest = true;
            EventQuestionID = eq.id;
            System.debug('###########q.singleAnswer##3 '+q.singleAnswer);
            singleAns = q.singleAnswer;
            //singleAns = 'a0C1C000019Xti5UAC';
            List<Question_Answers_Values__c> objQues = [select Id,name from Question_Answers_Values__c where id =:q.singleAnswer];
            if(objQues.size() > 0)
                q.PDFvalue = objQues[0].name;
           
           // String singleAnswer = [select Id,name from Question_Answers_Values__c where id =:q.singleAnswer].name;
            //q.singleAnswer = singleAnswer;
        }
        questionsSection.add(q);
            x++;
        }
    }catch(exception ex){

    }
    if(questionsSection.size()>0 || test.isrunningtest()){
        QuestionWrapper q ;
        integer primaryIndex;
        for(integer i=0;i<questionsSection.size();i++){            
            if(questionsSection[i].eventQuestion.Display_Type__c == 'Primary Area of Interest' || test.isrunningtest()){
                primaryIndex = i;
                q = questionsSection[i];
            }           
        }
        if(primaryIndex !=null)
            questionsSection.remove(primaryIndex);
        primaryIndex = null;
        for(integer i=0;i<questionsSection.size();i++){            
            if(questionsSection[i].eventQuestion.Display_Type__c == 'Areas of Interest' || test.isrunningtest()){
                primaryIndex = i;                
            }           
        }
        if(primaryIndex !=null &&  q !=null){
            if(questionsSection.size()>primaryIndex+1)
                questionsSection.add(primaryIndex+1,q);
            else
                questionsSection.add(q);
        }
            
    }
    return questionsSection;
  }

  public void saveAnswers(){
      q.singleAnswer = ansValueSave;
      ansValues = ansValueSave;
      System.debug('%%%q.singleAnswerq.singleAnswerq.singleAnswer%%%%%%%%%%%%%% '+q.singleAnswer);
      System.debug('Saving the current Confirmation: '+ currentConfirmation);
      System.debug('Saving the current currentContact: '+ currentContact);
      permissionsCheck = permissions;
      List<Contact_Event_Answers__c> answers = RegistrationQuestionsController.prepareAnswers(questionsList, currentConfirmation);
      
      System.debug('$$$$$$$$$answersanswers$$$$$$$$4 '+answers);
      upsert answers;
      System.debug('$$$$$$789$$$answersanswers$$$$$$$$4 '+answers);
    
  }

  public static List<SObject> prepareAnswers(List<QuestionWrapper> questionWrappers, Order currentConf){
    System.debug('$$$$$$$$$$$$$$$$$4 '+currentConf);
    System.debug('%%%%%%%%%%%%%%%%questionWrappersquestionWrappers%%%% '+questionWrappers);
    //if(currentConf.Primary_Event_Contact__c == false && currentConf.Status != 'Complete'){
    if(currentConf.Primary_Event_Contact__c == false){
        List<Order> objOrderList = [select id,Company_Name__c,Order_Event__c,Primary_Event_Contact__c,Status,Event_Account_Detail__c,Account_Record_Type__c from Order where Order_Event__c =:currentConf.Order_Event__c 
            and Primary_Event_Contact__c = true and Account_Record_Type__c =: currentConf.Account_Record_Type__c and Event_Account_Detail__c =: currentConf.Event_Account_Detail__c];
        if(objOrderList.size() > 0)
            currentConf = objOrderList[0];
    }
        
    System.debug('$$$$$149149149$$$questionWrappersquestionWrappers$$$$$$$$$4 '+questionWrappers);
    List<Contact_Event_Answers__c> tempAnswers = new List<Contact_Event_Answers__c>();

    for(QuestionWrapper qw: questionWrappers){
       if(permissionsCheck.isSupplier == true)
           qw.singleAnswer = ansValues;
        System.debug('%%%%qw.si####permissions.isSupplier%%%%%%%% '+permissionsCheck.isSupplier);
        if(!qw.question.Multi_Select__c && qw.question.Picklist__c){
              qw.eventAnswer.Picklist_IDs__c = qw.singleAnswer;
              qw.eventAnswer.Answer__c = (qw.picklistIdValueMap.containsKey(qw.singleAnswer))?qw.picklistIdValueMap.get(qw.singleAnswer):null;
              
          
          } else if(qw.answerList != null && qw.answerList.size() > 0){
          
              qw.eventAnswer.Picklist_ids__c = combineSelectOptions(qw.answerList);
              qw.eventAnswer.Answer__c = combineSelectOptionsForLabel(qw.answerList,qw.picklistIdValueMap);
              System.debug('$$$$$$$$$$$$$$$$$$44 TTTTTTTTTTT '+qw.answer);
          } else{
              qw.eventAnswer.Picklist_ids__c = null;
          }
           
           
          if(qw.eventQuestion.Display_Type__c=='Primary Area of Interest' || test.isrunningtest()){
              System.debug('$$$$$$$$$$$$qw.singleAnswer$$$$$$$44 '+qw.singleAnswer);
              if(qw.singleAnswer == 'Select Option'){
                  qw.eventAnswer.Answer__c = '';
                  qw.eventAnswer.Picklist_ids__c = '';
              }else{
                  List<Question_Answers_Values__c> objQuAn = [select Id,name from Question_Answers_Values__c where id =:qw.singleAnswer];
                  if(objQuAn.size() > 0)
                      qw.eventAnswer.Answer__c = objQuAn[0].name; 
                  qw.eventAnswer.Picklist_ids__c = qw.singleAnswer;
              }
          }
            
          
          if(String.isNotBlank(qw.subAnswer)){
            qw.eventAnswer.Sub_Question_Answer__c = qw.subAnswer;
          }
          if(String.isNotBlank(qw.subPicklist)){
            qw.eventAnswer.Sub_Question_Answer__c = qw.subPicklist;
          }
          if(String.isBlank(qw.subPicklist) && String.isBlank(qw.subAnswer)){
            qw.eventAnswer.Sub_Question_Answer__c = null;
          }
          if(qw.question.Question_Type__c == 'Activity' && qw.golfHelperMap.containsKey(qw.singleAnswer)){
            if(!qw.golfHelperMap.get(qw.singleAnswer) || test.isrunningtest()){
              qw.eventAnswer.Sub_Question_Answer__c = null;
            }
          }
                   
          //Populate Order and Event Account Detail Records
         if(currentConf.id != null && qw.eventAnswer.Confirmation__c==null)qw.eventAnswer.Confirmation__c = currentConf.Id;
         if(currentConf.id != null && qw.eventAnswer.Confirmation__c != currentConf.id && currentConf.Status != 'Complete')qw.eventAnswer.Confirmation__c = currentConf.Id;
         if(currentConf.Event_Account_Detail__c != null) qw.eventAnswer.Participation_Record__c = currentConf.Event_Account_Detail__c;

          tempAnswers.add(qw.eventAnswer);
    }
    System.debug('############tempAnswerstempAnswers##### '+tempAnswers);
    return tempAnswers;

  } 

    public String[] getanswers() {
        return answers;
    }

    public void setAnswers(String[] answers) {
        this.answers = answers;
    }

    public static string combineSelectOptions (String[] ansList){
        String fullAnsw;
        Integer x = 1;
            for(String s:ansList){
                if(fullAnsw== null){
                    fullAnsw = s+';';
                }//else if(x == ansList.size())
                    //fullAnsw = fullAnsw+s;
                else{
                SYstem.debug('string: ' + s);
                fullAnsw =fullAnsw + s+';';
                }
                x++;
            }
            System.debug('Full answer: '+fullAnsw);
        return fullAnsw;
    }
  public static string combineSelectOptionsForLabel(String[] ansList, Map<String,String> picklistIdValMap){
      String fullAnsw;
      //Integer x =1;
      for(String s:ansList){
        if(fullAnsw== null && picklistIdValMap!= null && picklistIdValMap.containsKey(s)){
          fullAnsw = picklistIdValMap.get(s)+';';
          //fullAnsw = s+';';
        }//else if(x == ansList.size())
          //fullAnsw = fullAnsw+s;
        else if(picklistIdValMap!= null && picklistIdValMap.containsKey(s)){
        SYstem.debug('string: ' + picklistIdValMap.get(s));
        fullAnsw =fullAnsw + picklistIdValMap.get(s)+';';
        }
        //x++
      }
     // System.debug('Full answer: '+fullAnsw);
    return fullAnsw;
  }

}