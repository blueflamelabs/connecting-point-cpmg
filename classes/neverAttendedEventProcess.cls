/*
*Created By     : 
*Created Date   : 16/07/2019 
*Description    : This Apex class run daily that checks the CPMG Event to see if the End date = yesterday.
*Test Class Name: 
Modification History :
============================================================================================
V.No    Date            By                  Description
============================================================================================
*/
public class neverAttendedEventProcess{

     public void cancelEvent(){
         Set<Id> setOfOrderId = new Set<Id>();
         
         List<Product2> productList = [SELECT ID, Name, End_Date__c FROM Product2 Where End_Date__c =:Date.Today().addDays(-1)];
         if(productList.size() > 0){
              Set<Id> objProId = new Set<Id>();
              for(Product2 objPro : productList){
                  objProId.add(objPro.Id);
              }
              Set<Id> ObjConId = new Set<Id>();
              list<ORDER> OrderList = [SELECT ID, Name, Status, Order_Event__c,BillToContactId,BillToContact.Never_Attended_Event__c FROM Order 
                                          Where Status !='Cancelled' AND BillToContact.Never_Attended_Event__c = True and Order_Event__c IN : objProId];
              System.debug('Orders@@@@@@'+OrderList);
              for(Order objOrd : OrderList){
                 ObjConId.add(objOrd.BillToContactId);
              } 
              if(ObjConId.size() > 0){
                  List<Contact> objCon = [Select id,Never_Attended_Event__c from Contact where Id IN : ObjConId];
                  for(Contact objContact : objCon){
                      objContact.Never_Attended_Event__c = false;
                  }
                  update objCon;
              }
          }
    } 
              
}