@isTest
private class UserCreationProcessScheduler_Test {

	static String Cron_EXP = '0 0 0 15 3 ? 2022';
	
	@isTest
	private static void userCreationProcessScheduler_Test() {
		TestDataGenerator.createInactiveTriggerSettings();
		Community_Settings__c commSett = TestDataGenerator.createCommSettings();
		
		Id suppAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
		Account a = TestDataGenerator.createTestAccount(true, 'Test Account',suppAccRtype);
		Id suppConRtype =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
		Contact c = TestDataGenerator.createTestContact(true,'Test','Last Name','testAcc@account.com',a,suppConRtype);

		Contact attendContact = TestDataGenerator.createTestContact(true,'Test','Attending Con', 'testAttendingCon@account.com',a,suppConRtype);
		
		Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event Active',true,Date.today().addDays(-10),Date.today().addDays(-5),Date.today().addMonths(-2), Date.today().addMonths(-1));

		Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Event Active Opp',Date.today().addMonths(-4),a,'Closed Won',prod);

		Id execPartRecordType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
		Event_Account_Details__c partRecord = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c, execPartRecordType);
		partRecord.Registration_Part_1_Submitted__c = true;
		update partRecord;

		Order conf = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod);
		Order attendingConf = TestDataGenerator.createConfirmation(true,a,attendContact,'Complete',opp,opp.CloseDate,partRecord,prod);
		List<Order> ordersJustCreated = new List<Order>{conf, attendingConf};

	Test.startTest();
		String jobId = System.schedule('Test Name',Cron_EXP, new UserCreationProcessScheduler());

		CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
	                      FROM CronTrigger 
	                      WHERE id = :jobId];

			//Execute the Batch
			Id batchProcessId = Database.executeBatch(new UserCreationProcessBatch(), 200);

		Test.stopTest();
	}
}