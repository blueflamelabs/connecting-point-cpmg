@isTest
private class ProductWrapper_test {
    
    @isTest static void test_method_one() {

        Product2 myEvent = new Product2(Name='testEvent', Year__c='2017', Family='HotelPoint',User_Link_Expiration_for_Executives__c=2,User_Link_Expiration_for_Suppliers__c=2);
        insert myEvent;

        ProductWrapper myProductWrapper = new ProductWrapper(myEvent);

        test.startTest();
        System.assertEquals(myEvent.Id, myProductWrapper.prod.Id);

        System.assertNotEquals(null, myProductWrapper.event_Date_logo_path);
        System.assertNotEquals(null, myProductWrapper.circular_Logo_path);
        System.assertNotEquals(null, myProductWrapper.transparent_Logo_Path);
        test.stopTest();

    }

    @IsTest
    static void product_wrapper_Constructor_with_type_TEST(){
        Product2 myEvent = new Product2(Name='testEvent', Year__c='2017', Family='HotelPoint',User_Link_Expiration_for_Executives__c=2,User_Link_Expiration_for_Suppliers__c=2);
        insert myEvent;

        ProductWrapper myProductWrapper = new ProductWrapper(myEvent, true);

        test.startTest();
        System.assertEquals(myEvent.Id, myProductWrapper.prod.Id);

        System.assertNotEquals(null, myProductWrapper.event_Date_logo_path);
        System.assertNotEquals(null, myProductWrapper.circular_Logo_path);
        System.assertNotEquals(null, myProductWrapper.transparent_Logo_Path);
        test.stopTest();
    }
    
    
}