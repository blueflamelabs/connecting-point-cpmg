public class EmailSeriesController {
    public Product2 targetEvent {get;set;}
    public String navColor {get; set;}
    private integer counter=0;  //keeps track of the offset
    private integer list_size=3; //sets the page size or number of rows
    public integer total_size=0; //used to show user the total size of the list
        
    public string selectedPage{get;set{selectedPage=value;}
    }
    
    public EmailSeriesController() {
        User currentUser = [SELECT Id, Name,ContactId FROM User where Id = :UserInfo.getUserId()];
        Contact objCon = [Select id,Name,RecordTypeId,RecordType.Name from Contact where Id =: currentUser.ContactId limit 1];
        Id recTypeId = Schema.getGlobalDescribe().get('Planning_Series__c').getDescribe().getRecordTypeInfosByName().get(objCon.RecordType.Name).getRecordTypeId();
        Id event_id_param = ApexPages.currentPage().getParameters().get('eId'); 
        targetEvent = EventHelper.getEvent(event_id_param);         
        total_size = [select count() from Planning_Series__c where 
                        RecordTypeId=:recTypeId and Posted_Date__c <=today and Event__c =:targetEvent.Id]; //set the total size in the constructor
        selectedPage='0';
    }
    
    public void updateOrderAlertNewPost(){
        Id event_id_param = ApexPages.currentPage().getParameters().get('eId'); 
        String orderUpdate = ApexPages.currentPage().getParameters().get('orderUpdate'); 
        if(String.isNotBlank(orderUpdate)){
            User currentUser = [SELECT Id, Name,ContactId FROM User where Id = :UserInfo.getUserId()];
            Contact objCon = [Select Id,Name,RecordTypeId,RecordType.Name from Contact where Id =: currentUser.ContactId limit 1];
            List<Order> objOrderList = [Select id,Alert_New_Post__c,Order_Event__c,BillToContactId from Order 
                                    where Order_Event__c =:event_id_param and BillToContactId =:objCon.Id and Alert_New_Post__c = true];
            if(objOrderList.size() > 0){
                for(Order objOrder : objOrderList){
                    objOrder.Alert_New_Post__c = false;
                }
                update objOrderList;
            }                        
            
        }
        
        
    }
    
    public class wrapperClass{
        public String bodyValue{set;get;}
        public String Title{set;get;}
        public String writtenBy{set;get;}
        public String LongDate{set;get;}
        public wrapperClass(String bodyValue,String Title,String writtenBy,String LongDate){
            this.bodyValue = bodyValue;
            this.Title = Title;
            this.writtenBy = writtenBy;
            this.LongDate = LongDate;
        }
    }
    
    public wrapperClass[] getNumbers() {
                
        if (selectedPage != '0') counter = list_size*integer.valueOf(selectedPage)-list_size;
        try { 
               System.debug('#@@@@1111111111@@@@@@@@@@ '+counter);
              List<wrapperClass>  objWraList = new List<wrapperClass>();
               Id event_id_param = ApexPages.currentPage().getParameters().get('eId');
               User currentUser = [SELECT Id, Name,ContactId FROM User where Id = :UserInfo.getUserId()];
               if(currentUser.ContactId != null){
                   Contact objCon = [Select id,Name,RecordTypeId,RecordType.Name from Contact where Id =: currentUser.ContactId limit 1];
                   Id recTypeId = Schema.getGlobalDescribe().get('Planning_Series__c').getDescribe().getRecordTypeInfosByName().get(objCon.RecordType.Name).getRecordTypeId();
                   targetEvent = EventHelper.getEvent(event_id_param);
                   navColor = targetEvent.Event_Navigation_Color__c;
                   List<Planning_Series__c> objListEmailSeries = [select Id,Title__c,Name,Written_By__c,Written_By__r.FirstName,Written_By__r.LastName,Posted_Date__c,Event__c,
                                                               Long_Date__c,Body__c,RecordTypeId from Planning_Series__c 
                                                               where RecordTypeId=:recTypeId and Posted_Date__c <=today and Event__c =:targetEvent.Id order by Posted_Date__c desc
                                                               limit :list_size offset :counter];
                   for(Planning_Series__c objEm : objListEmailSeries){
                       String Name = String.valueOf(objEm.Written_By__r.FirstName +' '+objEm.Written_By__r.LastName).toUpperCase();
                       objWraList.add(new wrapperClass(objEm.Body__c,objEm.Title__c,Name,String.valueOf(objEm.Long_Date__c)));
                   }
               } 
              return objWraList;  
        
        } catch (QueryException e) {                            
                ApexPages.addMessages(e);                   
                return null;
        }       
    }
    
    public Component.Apex.pageBlockButtons getMyCommandButtons() {
        
        //the reRender attribute is a set NOT a string
        Set<string> theSet = new Set<string>();
        theSet.add('myPanel');
        theSet.add('myButtons');
                
        integer totalPages;
        if (math.mod(total_size, list_size) > 0) {
        System.debug('######total_size###### '+total_size);
        System.debug('######list_size###### '+list_size);
            totalPages = total_size/list_size+1;
        } else {
            totalPages = (total_size/list_size);
        }
        
        integer currentPage;        
        if (selectedPage == '0') {
            currentPage = counter/list_size + 1;
        } else {
            currentPage = integer.valueOf(selectedPage);
        }
     
        Component.Apex.pageBlockButtons pbButtons = new Component.Apex.pageBlockButtons();        
        pbButtons.location = 'top';
        pbButtons.id = 'myPBButtons';
        
        Component.Apex.outputPanel opPanel = new Component.Apex.outputPanel();
        opPanel.id = 'myButtons';
                                
        //the Previous button will alway be displayed
        If(totalPages >1){
            Component.Apex.commandButton b1 = new Component.Apex.commandButton();
            b1.expressions.action = '{!Previous}';
            b1.title = 'Previous';
            b1.value = 'Previous';
            b1.expressions.disabled = '{!disablePrevious}'; 
           /* if(getdisablePrevious() == true)
            b1.style = 'background: #b1b1b1e0;cursor: not-allowed;';  
            if(getdisablePrevious() == false)
            b1.style = 'background: #2196F3;cursor: pointer';  */     
            b1.reRender = theSet;
            opPanel.childComponents.add(b1);        
        }                
        for (integer i=0;i<totalPages;i++) {
            Component.Apex.commandButton btn = new Component.Apex.commandButton();
            
            if (i+1==1) {
                btn.title = '1';
                btn.value = '1';
                btn.rendered = true;                                        
            } else if (i+1==totalPages) {
                btn.title = String.valueOf(totalPages);
                btn.value = String.valueOf(totalPages);
                btn.rendered = true;                            
            } 
            else{
                btn.title = 'Page ' + string.valueOf(i+1) + ' ';
                btn.value = '' + string.valueOf(i+1) + '';
                btn.rendered = false;             
           }
            if (   (i+1 <= 5 && currentPage < 5)
                || (i+1 >= totalPages-4 && currentPage > totalPages-4)
                || (i+1 >= currentPage-2 && i+1 <= currentPage+2))
            {
                btn.rendered = true;
            }
                                     
            if (i+1==currentPage) {
                btn.disabled = true; 
                btn.style = 'background: #647f8e;border: solid 1px #cae0ea66;';
            }  
            
            btn.onclick = 'queryByPage(\''+string.valueOf(i+1)+'\');return false;';
                 
            opPanel.childComponents.add(btn);
            
            if ((i+1 == 1 || i+1 == totalPages-1) && (totalPages >1)) { //put text after page 1 and before last page
                Component.Apex.outputText text = new Component.Apex.outputText();
                text.value = '...';        
                opPanel.childComponents.add(text);
            } 
             
        }
        
        //the Next button will alway be displayed
        If(totalPages >1){
            Component.Apex.commandButton b2 = new Component.Apex.commandButton();
            b2.expressions.action = '{!Next}';
            b2.title = 'Next';
            b2.value = 'Next';
            b2.expressions.disabled = '{!disableNext}';        
            b2.reRender = theSet;
            opPanel.childComponents.add(b2);
          }          
            //add all buttons as children of the outputPanel                
            pbButtons.childComponents.add(opPanel);  
       
        return pbButtons;

    }    
    
    public PageReference refreshGrid() { //user clicked a page number        
        system.debug('**** ' + selectedPage);
        return null;
    }
    
    public PageReference Previous() { //user clicked previous button
        selectedPage = '0';
        counter -= list_size;
        return null;
    }

    public PageReference Next() { //user clicked next button
        selectedPage = '0';
        counter += list_size;
        getNumbers();
        return null;
    }

    public PageReference End() { //user clicked end
        selectedPage = '0';
        counter = total_size - math.mod(total_size, list_size);
        return null;
    }
    
    public Boolean getDisablePrevious() { //this will disable the previous and beginning buttons
        if (counter>0) return false; else return true;
    }

    public Boolean getDisableNext() { //this will disable the next and end buttons
        if (counter + list_size < total_size) return false; else return true;
    }

    public Integer getTotal_size() {
        return total_size;
    }
    
    public Integer getPageNumber() {
        return counter/list_size + 1;
    }

    public Integer getTotalPages() {
        if (math.mod(total_size, list_size) > 0) {
            return total_size/list_size + 1;
        } else {
            return (total_size/list_size);
        }
    }
    
   
    
}