@isTest
private class ContactEventAnswerHelper_test {
    
    @isTest static void getEventQuestionAnswers_Test() {

        RecordType rt = [SELECT Id,Name FROM RecordType WHERE SobjectType='Question__c' AND Name='Question' LIMIT 1];

        System.assertNotEquals(rt, null);

        Set<Id> questionIds = new Set<Id>();

        Event myEvent = new Event(ActivityDate=Date.today().addDays(3), IsAllDayEvent = true);

        Question__c question = new Question__c(Name='testQuestion', RecordTypeId=rt.Id);
        insert question;

        Event_Question__c question2 = new Event_Question__c(Name='testQuestion2');
        insert question2;

        //Event_Category_Inventory__c category = new Event_Category_Inventory__c(Name='test', Total_Available_Spots__c=4 ,Category__c='Communications');
        //insert category;

        Opportunity opp = new Opportunity(Name='test', StageName ='stagetest', CloseDate = Date.today());
        insert opp;


        questionIds.add(question2.Id);
        questionIds.add(question.Id);

        Event_Account_Details__c record = new Event_Account_Details__c(Event__c=myEvent.Id, Opportunity__c=opp.Id);
        insert record;

        Contact_Event_Answers__c eventAnswers = new Contact_Event_Answers__c(Question__c = question2.Id, Participation_Record__c = record.Id);
        insert eventAnswers;

        Map<Id, Contact_Event_Answers__c> result =  ContactEventAnswerHelper.getEventQuestionAnswers(questionIds,  record.Id);

        System.assertEquals(result.get(question2.id).id, eventAnswers.id );

    }
    
    @isTest static void getLongAndShortProfiles() {
        // Implement test code
        // Prepare Test Data.
        TestDataGenerator.createInactiveTriggerSettings();

        //Event Setup
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Active Event',true,Date.today().addDays(-10),Date.today().addDays(-5),Date.today().addMonths(-2), Date.today().addMonths(-1));
        prod.Start_Date__c = Date.today().addDays(50);
        prod.Family ='BuildPoint';
        prod.Event_Navigation_Color__c = '90135d';
        prod.Event_Tint_Color__c = 'EEEEEE';
        update prod;

        Profile standardUser = [SELECT Id FROM Profile where Name ='Standard User' LIMIT 1];
        User execUserCPMG = TestDataGenerator.CreateUser(true,standardUser.Id,'Test','Exec',null,'testExec1234@cpmg.com',null);
        User supplierUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Supplier',null,'testSupplier1234@cpmg.com',null);
        User supportUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Support',null,'testSupport1234@cpmg.com',null);


        Event_Staff__c execStaff = TestDataGenerator.createEventStaff(true,prod,execUserCPMG,'Recruiter',true,1);
        Event_Staff__c supplierStaff = TestDataGenerator.createEventStaff(true,prod,supplierUserCPMG,'Salesperson',true,2);
        Event_Staff__c supportStaff = TestDataGenerator.createEventStaff(true,prod,supportUserCPMG,'Support',true,3);

        //Set Up Executive User
        Id execAccRType =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Id execConRType =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Account execAcc = TestDataGenerator.createTestAccount(true,'Test Exec Account', execAccRType);
        //Wrapper wants a website.
        execAcc.Website = 'https://www.google.com';
        update execAcc;
        Contact execCon = TestDataGenerator.createTestContact(true,'Test Exec','Last Account','testexec@test.com',execAcc, execConRType);

        Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        User execUser = TestDataGenerator.CreateUser(true,commUserProfId.id,'Test Exec','Last Account','603-555-5555','testexec@test.com', execCon.Id);

        Opportunity execOpp = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),execAcc,'Closed Won',prod);

        Id execPRRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c execPRecod = TestDataGenerator.createParticipationRecord(true,prod,execAcc,execOpp,execCon,execPRRType);

        Order execConf = TestDataGenerator.createConfirmation(true,execAcc,execCon,'Attending Event Contact',execOpp,execOpp.CloseDate,execPRecod,prod);

        //Create Question
        Map<String,RecordTypeInfo> questionRTypeMap = Schema.SObjectType.Question__c.getRecordTypeInfosByName();
        Question__c picklistQuestion = TestDataGenerator.createQuestion(true,'Test Category Pickilist',200,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
            Question_Answers_Values__c qav1Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option','Select');
            Question_Answers_Values__c qav2Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option2','Select2');
            Question_Answers_Values__c qav3Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option3','Select3');
        Question__c longQuestion = TestDataGenerator.createQuestion(true,'Long Description',2000,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
        Question__c shortQuestion = TestDataGenerator.createQuestion(true,'Short Description',200,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
            
        //Create the 3 Event Questions
        Event_Question__c categoryEQ = TestDataGenerator.createEventQuestion(true,'Category',prod,'Category',picklistQuestion,true,1,null);
        Event_Question__c longEQ = TestDataGenerator.createEventQuestion(true,'Company Long Description',prod,'Company',longQuestion,true,1,'Company Long Description');
        Event_Question__c shortEQ = TestDataGenerator.createEventQuestion(true,'Company Short Description',prod,'Company',shortQuestion,true,1,'Company Short Description');

        //Create Contact Answer Questions
        Contact_Event_Answers__c categoryAnswer = TestDataGenerator.createQuestionAnswers(true,execConf,execPRecod,categoryEQ,execCon,'Select2;Select3',null,null);
        Contact_Event_Answers__c longAnswer = TestDataGenerator.createQuestionAnswers(true,execConf,execPRecod,longEQ,execCon,'Long Answer',null,null);
        Contact_Event_Answers__c shortAnswer = TestDataGenerator.createQuestionAnswers(true,execConf,execPRecod,shortEQ,execCon,'Short Answer',null,null);

        //PErform Test
        Set<String> shortLongSet = New Set<String>{'Company Short Description','Company Long Description'};

        Test.startTest();
            Map<String, Contact_Event_Answers__c> answersByPRAndQType = ContactEventAnswerHelper.getLongAndShortProfiles(new Set<Id>{prod.Id}, shortLongSet);
        Test.stopTest();
    }

    static testmethod void getAnswerByQRandType(){
        // Implement test code
        // Prepare Test Data.
        TestDataGenerator.createInactiveTriggerSettings();

        //Event Setup
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Active Event',true,Date.today().addDays(-10),Date.today().addDays(-5),Date.today().addMonths(-2), Date.today().addMonths(-1));
        prod.Start_Date__c = Date.today().addDays(50);
        prod.Family ='BuildPoint';
        prod.Event_Navigation_Color__c = '90135d';
        prod.Event_Tint_Color__c = 'EEEEEE';
        update prod;

        Profile standardUser = [SELECT Id FROM Profile where Name ='Standard User' LIMIT 1];
        User execUserCPMG = TestDataGenerator.CreateUser(true,standardUser.Id,'Test','Exec',null,'testExec1234@cpmg.com',null);
        User supplierUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Supplier',null,'testSupplier1234@cpmg.com',null);
        User supportUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Support',null,'testSupport1234@cpmg.com',null);


        Event_Staff__c execStaff = TestDataGenerator.createEventStaff(true,prod,execUserCPMG,'Recruiter',true,1);
        Event_Staff__c supplierStaff = TestDataGenerator.createEventStaff(true,prod,supplierUserCPMG,'Salesperson',true,2);
        Event_Staff__c supportStaff = TestDataGenerator.createEventStaff(true,prod,supportUserCPMG,'Support',true,3);

        //Set Up Executive User
        Id execAccRType =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Id execConRType =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Account execAcc = TestDataGenerator.createTestAccount(true,'Test Exec Account', execAccRType);
        //Wrapper wants a website.
        execAcc.Website = 'https://www.google.com';
        update execAcc;
        Contact execCon = TestDataGenerator.createTestContact(true,'Test Exec','Last Account','testexec@test.com',execAcc, execConRType);

        Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        User execUser = TestDataGenerator.CreateUser(true,commUserProfId.id,'Test Exec','Last Account','603-555-5555','testexec@test.com', execCon.Id);

        Opportunity execOpp = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),execAcc,'Closed Won',prod);

        Id execPRRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c execPRecod = TestDataGenerator.createParticipationRecord(true,prod,execAcc,execOpp,execCon,execPRRType);

        Order execConf = TestDataGenerator.createConfirmation(true,execAcc,execCon,'Attending Event Contact',execOpp,execOpp.CloseDate,execPRecod,prod);

        //Create Question
        Map<String,RecordTypeInfo> questionRTypeMap = Schema.SObjectType.Question__c.getRecordTypeInfosByName();
        Question__c picklistQuestion = TestDataGenerator.createQuestion(true,'Test Category Pickilist',200,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
            Question_Answers_Values__c qav1Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option','Select');
            Question_Answers_Values__c qav2Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option2','Select2');
            Question_Answers_Values__c qav3Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option3','Select3');
        Question__c longQuestion = TestDataGenerator.createQuestion(true,'Long Description',2000,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
        Question__c shortQuestion = TestDataGenerator.createQuestion(true,'Short Description',200,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
            
        //Create the 3 Event Questions
        Event_Question__c categoryEQ = TestDataGenerator.createEventQuestion(true,'Category',prod,'Category',picklistQuestion,true,1,null);
        Event_Question__c longEQ = TestDataGenerator.createEventQuestion(true,'Company Long Description',prod,'Company',longQuestion,true,1,'Company Long Description');
        Event_Question__c shortEQ = TestDataGenerator.createEventQuestion(true,'Company Short Description',prod,'Company',shortQuestion,true,1,'Company Short Description');

        //Create Contact Answer Questions
        Contact_Event_Answers__c categoryAnswer = TestDataGenerator.createQuestionAnswers(true,execConf,execPRecod,categoryEQ,execCon,'Select2;Select3',null,null);
        Contact_Event_Answers__c longAnswer = TestDataGenerator.createQuestionAnswers(true,execConf,execPRecod,longEQ,execCon,'Long Answer',null,null);
        Contact_Event_Answers__c shortAnswer = TestDataGenerator.createQuestionAnswers(true,execConf,execPRecod,shortEQ,execCon,'Short Answer',null,null);

        //PErform Test
        Set<String> shortLongSet = New Set<String>{'Company Short Description','Company Long Description'};
        Map<String, Contact_Event_Answers__c> answersByPRAndQType = ContactEventAnswerHelper.getLongAndShortProfiles(new Set<Id>{prod.Id}, shortLongSet);

        Test.startTest();
            Contact_Event_Answers__c categoryAnswerMap = ContactEventAnswerHelper.getAnswerByPRandQType(execPRecod.Id,'Category',answersByPRAndQType);
            Contact_Event_Answers__c longAnswerMap = ContactEventAnswerHelper.getAnswerByPRandQType(execPRecod.Id,'Long',answersByPRAndQType);
            Contact_Event_Answers__c shortAnswerMap = ContactEventAnswerHelper.getAnswerByPRandQType(execPRecod.Id,'Short',answersByPRAndQType);
            Contact_Event_Answers__c NullAnswerMap = ContactEventAnswerHelper.getAnswerByPRandQType(execPRecod.Id,'',answersByPRAndQType);
        Test.stopTest();
            System.assertEquals(categoryAnswerMap.Id, categoryAnswer.Id);
            System.assertEquals(longAnswerMap.Id, longAnswer.Id);
            System.assertEquals(shortAnswerMap.Id, shortAnswer.Id);
            System.assertEquals(NullAnswerMap,new Contact_Event_Answers__c());
    }

    static testmethod void getProfileQuestionAnswers(){
        TestDataGenerator.createInactiveTriggerSettings();

        //Event Setup
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Active Event',true,Date.today().addDays(-10),Date.today().addDays(-5),Date.today().addMonths(-2), Date.today().addMonths(-1));
        prod.Start_Date__c = Date.today().addDays(50);
        prod.Family ='BuildPoint';
        prod.Event_Navigation_Color__c = '90135d';
        prod.Event_Tint_Color__c = 'EEEEEE';
        update prod;

        Profile standardUser = [SELECT Id FROM Profile where Name ='Standard User' LIMIT 1];
        User execUserCPMG = TestDataGenerator.CreateUser(true,standardUser.Id,'Test','Exec',null,'testExec1234@cpmg.com',null);
        User supplierUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Supplier',null,'testSupplier1234@cpmg.com',null);
        User supportUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Support',null,'testSupport1234@cpmg.com',null);


        Event_Staff__c execStaff = TestDataGenerator.createEventStaff(true,prod,execUserCPMG,'Recruiter',true,1);
        Event_Staff__c supplierStaff = TestDataGenerator.createEventStaff(true,prod,supplierUserCPMG,'Salesperson',true,2);
        Event_Staff__c supportStaff = TestDataGenerator.createEventStaff(true,prod,supportUserCPMG,'Support',true,3);

        //Set Up Executive User
        Id execAccRType =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Id execConRType =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Account execAcc = TestDataGenerator.createTestAccount(true,'Test Exec Account', execAccRType);
        //Wrapper wants a website.
        execAcc.Website = 'https://www.google.com';
        update execAcc;
        Contact execCon = TestDataGenerator.createTestContact(true,'Test Exec','Last Account','testexec@test.com',execAcc, execConRType);

        Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        User execUser = TestDataGenerator.CreateUser(true,commUserProfId.id,'Test Exec','Last Account','603-555-5555','testexec@test.com', execCon.Id);

        Opportunity execOpp = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),execAcc,'Closed Won',prod);

        Id execPRRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c execPRecod = TestDataGenerator.createParticipationRecord(true,prod,execAcc,execOpp,execCon,execPRRType);

        Order execConf = TestDataGenerator.createConfirmation(true,execAcc,execCon,'Attending Event Contact',execOpp,execOpp.CloseDate,execPRecod,prod);

        //Create Question
        Map<String,RecordTypeInfo> questionRTypeMap = Schema.SObjectType.Question__c.getRecordTypeInfosByName();
        Question__c picklistQuestion = TestDataGenerator.createQuestion(true,'Test Category Pickilist',200,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
            Question_Answers_Values__c qav1Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option','Select');
            Question_Answers_Values__c qav2Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option2','Select2');
            Question_Answers_Values__c qav3Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option3','Select3');
        Question__c longQuestion = TestDataGenerator.createQuestion(true,'Long Description',2000,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
        Question__c shortQuestion = TestDataGenerator.createQuestion(true,'Short Description',200,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
            
        //Create the 3 Event Questions
        Event_Question__c categoryEQ = TestDataGenerator.createEventQuestion(true,'Category',prod,'Category',picklistQuestion,true,1,null);
        Event_Question__c longEQ = TestDataGenerator.createEventQuestion(true,'Company Long Description',prod,'Company',longQuestion,true,1,'Company Long Description');
        Event_Question__c shortEQ = TestDataGenerator.createEventQuestion(true,'Company Short Description',prod,'Company',shortQuestion,true,1,'Company Short Description');
        shortEQ.Include_in_part_2_Profile__c = true;
        update shortEq;
        longEq.Include_in_part_2_Profile__c = true;
        update longEq;

        //Create Contact Answer Questions
        Contact_Event_Answers__c categoryAnswer = TestDataGenerator.createQuestionAnswers(true,execConf,execPRecod,categoryEQ,execCon,'Select2;Select3',null,null);
        Contact_Event_Answers__c longAnswer = TestDataGenerator.createQuestionAnswers(true,execConf,execPRecod,longEQ,execCon,'Long Answer',null,null);
        Contact_Event_Answers__c shortAnswer = TestDataGenerator.createQuestionAnswers(true,execConf,execPRecod,shortEQ,execCon,'Short Answer',null,null);

        test.startTest();
            ContactEventAnswerHelper.getProfileQuestionAnswers(new Set<Id>{prod.Id}, new Set<Id>{execPRecod.Id});
        Test.stopTest();
    }

    private static testmethod void getShortAnsweQuestionsByPr_Test(){
        TestDataGenerator.createInactiveTriggerSettings();

        //Event Setup
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Active Event',true,Date.today().addDays(-10),Date.today().addDays(-5),Date.today().addMonths(-2), Date.today().addMonths(-1));
        prod.Start_Date__c = Date.today().addDays(50);
        prod.Family ='BuildPoint';
        prod.Event_Navigation_Color__c = '90135d';
        prod.Event_Tint_Color__c = 'EEEEEE';
        update prod;

        Profile standardUser = [SELECT Id FROM Profile where Name ='Standard User' LIMIT 1];
        User execUserCPMG = TestDataGenerator.CreateUser(true,standardUser.Id,'Test','Exec',null,'testExec1234@cpmg.com',null);
        User supplierUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Supplier',null,'testSupplier1234@cpmg.com',null);
        User supportUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Support',null,'testSupport1234@cpmg.com',null);


        Event_Staff__c execStaff = TestDataGenerator.createEventStaff(true,prod,execUserCPMG,'Recruiter',true,1);
        Event_Staff__c supplierStaff = TestDataGenerator.createEventStaff(true,prod,supplierUserCPMG,'Salesperson',true,2);
        Event_Staff__c supportStaff = TestDataGenerator.createEventStaff(true,prod,supportUserCPMG,'Support',true,3);

        //Set Up Executive User
        Id execAccRType =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Id execConRType =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Account execAcc = TestDataGenerator.createTestAccount(true,'Test Exec Account', execAccRType);
        //Wrapper wants a website.
        execAcc.Website = 'https://www.google.com';
        update execAcc;
        Contact execCon = TestDataGenerator.createTestContact(true,'Test Exec','Last Account','testexec@test.com',execAcc, execConRType);

        Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        User execUser = TestDataGenerator.CreateUser(true,commUserProfId.id,'Test Exec','Last Account','603-555-5555','testexec@test.com', execCon.Id);

        Opportunity execOpp = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),execAcc,'Closed Won',prod);

        Id execPRRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c execPRecod = TestDataGenerator.createParticipationRecord(true,prod,execAcc,execOpp,execCon,execPRRType);

        Order execConf = TestDataGenerator.createConfirmation(true,execAcc,execCon,'Attending Event Contact',execOpp,execOpp.CloseDate,execPRecod,prod);

        //Create Question
        Map<String,RecordTypeInfo> questionRTypeMap = Schema.SObjectType.Question__c.getRecordTypeInfosByName();
        Question__c picklistQuestion = TestDataGenerator.createQuestion(true,'Test Category Pickilist',200,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
            Question_Answers_Values__c qav1Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option','Select');
            Question_Answers_Values__c qav2Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option2','Select2');
            Question_Answers_Values__c qav3Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option3','Select3');
        Question__c longQuestion = TestDataGenerator.createQuestion(true,'Long Description',2000,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
        Question__c shortQuestion = TestDataGenerator.createQuestion(true,'Short Description',200,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
            
        //Create the 3 Event Questions
        Event_Question__c categoryEQ = TestDataGenerator.createEventQuestion(true,'Category',prod,'Category',picklistQuestion,true,1,null);
        Event_Question__c longEQ = TestDataGenerator.createEventQuestion(true,'Company Long Description',prod,'Company',longQuestion,true,1,'Company Long Description');
        Event_Question__c shortEQ = TestDataGenerator.createEventQuestion(true,'Company Short Description',prod,'Company',shortQuestion,true,1,'Company Short Description');

        //Create Contact Answer Questions
        Contact_Event_Answers__c categoryAnswer = TestDataGenerator.createQuestionAnswers(true,execConf,execPRecod,categoryEQ,execCon,'Select2;Select3',null,null);
        Contact_Event_Answers__c longAnswer = TestDataGenerator.createQuestionAnswers(true,execConf,execPRecod,longEQ,execCon,'Long Answer',null,null);
        Contact_Event_Answers__c shortAnswer = TestDataGenerator.createQuestionAnswers(true,execConf,execPRecod,shortEQ,execCon,'Short Answer',null,null);

        Test.startTest();
            Map<String,Contact_Event_Answers__c> shortAnswerMap =  ContactEventAnswerHelper.getShortAnswerQuestionsByPR(new Set<Id>{prod.id});
            System.assertEquals(1,shortAnswerMap.size());
            System.assert(shortAnswerMap.containsKey(execPRecod.Id));
            System.assertEquals(shortAnswer.Id, shortAnswerMap.get(execPRecod.Id).Id);
        Test.stopTest();
    }
    
}