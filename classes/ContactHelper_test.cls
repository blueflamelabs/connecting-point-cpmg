@isTest
private class ContactHelper_test {

        @TestSetup
    private static void createTestData(){
        Id accSupplierRType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Id accExecRType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Id conSupplierRType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Id conExecRType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        
        Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];

        Account a = TestDataGenerator.createTestAccount(true,'Test Account',accSupplierRType);
        Account a2 = TestDataGenerator.createTestAccount(true,'Acc1',accExecRType);
        List<Contact> testCons = new List<Contact>();
        for(integer x =0; x < 100; ++x){
            Account conAcc = Math.mod(x,2) == 0?a:a2;
            Id conRType = Math.mod(x,2) == 0 ?conSupplierRType:conExecRType;
            testCons.add(TestDataGenerator.createTestContact(false,'First'+String.valueOf(x),'Last'+String.valueOf(x),'first.last'+String.valueOf(x)+'@testaccountSObjectHelper.com',conAcc,conRType));
        }
        
        insert testCons;
        List<User> users = new List<User>();
        for(Contact c : testCons)
        {
            users.add(TestDataGenerator.CreateUser(false,commUserProfId.Id,c.FirstName,c.LastName,c.Phone, c.Email,c.Id));

        }

        insert users;       
    }
    
    @isTest static void test_method_one() {
        test.startTest();
        Account acct = new Account(Name='SFDC Account');
        insert acct;

        // Once the account is inserted, the sObject will be 
        // populated with an ID.
        // Get this ID.
        ID acctID = acct.ID;

        // Add a contact to this account.
        Contact con = new Contact(
        FirstName='Joe',
        LastName='Smith',
        Phone='415.555.1212',
        AccountId=acctID);

        Contact committedContact = ContactHelper.commitCommunityContact(con);

        System.assertEquals(con, committedContact);
        test.stopTest();
    }
    static testMethod void communityContactUpsert_Test() {
        test.startTest();
        Account acct = new Account(Name='SFDC Account');
        insert acct;

        // Once the account is inserted, the sObject will be 
        // populated with an ID.
        // Get this ID.
        ID acctID = acct.ID;

        // Add a contact to this account.
        Contact con = new Contact(
        FirstName='Joe',
        LastName='Smith',
        Phone='415.555.1212',
        AccountId=acctID);

        Contact committedContact = ContactHelper.upsertCommunityContact(con);

        System.assertEquals(con, committedContact);
        test.stopTest();
    }

    static testmethod void getContactMap_Test(){
        //create test data.
        Id execAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Account a = TestDataGenerator.createTestAccount(true, 'tr',execAccRtype);
        Id execConRtype =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Contact c = TestDataGenerator.createTestContact(true,'Test','Last Name','testAcc@account.com',a,execConRtype);

        Set<Id> contactIds = new Set<Id>{c.id};

        Test.StartTest();
            Map<Id,Contact> contactMap = ContactHelper.getContactMap(contactIds);

            Contact testCon = contactMap.get(c.Id);
            System.assertEquals(c.Id,testCon.Id);
        Test.stopTest();
    }

    static testmethod void getContact_Test(){
        Id execAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Account a = TestDataGenerator.createTestAccount(true, 'SynTestAcc',execAccRtype);
        Id execConRtype =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Contact c = TestDataGenerator.createTestContact(true,'Test','Last Name','testAcc@account.com',a,execConRtype);

        Test.startTest();
            Contact testCon = ContactHelper.getContact(c.Id);
            System.assertEquals(c.Id, testCon.Id);
        Test.stopTest();
    }

    static testmethod void getContactByEmail_Test(){
        Id execAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Account a = TestDataGenerator.createTestAccount(true, 'TestSFDCTest',execAccRtype);
        Id execConRtype =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Contact c = TestDataGenerator.createTestContact(true,'Test','Last Name','testAcc@account.com',a,execConRtype);

        Test.startTest();
            Boolean testCon = ContactHelper.getContactByEmail(c.email);
            System.assertEquals(true, testCon);
            testCon = ContactHelper.getContactByEmail('notexist@nonexistant.org');
            System.assertEquals(false,testCon);
        Test.stopTest();
    }

    @IsTest
    private static void updateUserEmailAndUserNameHelper_Test()
    {
        integer count = 0;
        Map<Id,Contact> contactMap = new Map<Id,Contact>();
        
        
        for(Contact c : [SELECT Id, Community_User_Name__c, email FROM Contact where CreatedDate = Today and email like '%@testaccountSObjectHelper.com'])
        {
            c.put('Email',String.valueOf(count)+ c.Email);
            contactMap.put(c.Id,c);
            ++count;
        }

        Test.startTest();
            ContactHelper.updateUserEmailAndUserName(contactMap.values());
        Test.stopTest();
    }
    
}