public with sharing class CloneCampaignController {
   private final sObject mysObject;
   public Id eventId{get;set;}
   public Id  campaignId{get;set;}
   public boolean checked{get;set;}  
   public List<myWrapperClass> opportunityList {get;set;}
   public String campagianRecordId;
   Public Boolean isMultipleResponded{set;get;}
   
   public CloneCampaignController(ApexPages.StandardController stdController) {
        this.mysObject = (sObject)stdController.getRecord();
        this.campaignId= (Id)stdController.getId();
        campagianRecordId=ApexPages.currentPage().getParameters().get('id');
        List<CampaignMemberStatus> objCamMebStatus = [SELECT CampaignId,HasResponded,Label FROM CampaignMemberStatus where HasResponded = true and CampaignId =:campagianRecordId];
        if(objCamMebStatus.size() > 1)
            isMultipleResponded = true;
        else
            fetchData();
    }
    
     
    public PageReference createNewCampaign(){
      String eventId;
      String clonedCampaignId;
      List<CampaignMember> campMemberList=new  List<CampaignMember>();
      List<CampaignMember> oppCampMemberList=new List<CampaignMember>();
      List<Opportunity> campOpportunityList= new List<opportunity>();
      List<CampaignMemberStatus> newCampaignMemStatusList=new List<CampaignMemberStatus>();
      List<CampaignMemberStatus> newCampMemStatusListUpdate=new List<CampaignMemberStatus>();
      Map<String,String> mapOfContact=new Map<String,String>();
      Map<String,String> mapOfCampaignStatus=new Map<String,String>();
      Set<Id> primaryContact=new Set<Id>();
      Map<String,String> campMemberStatusLabel = new Map<String,String>();
      Campaign originalCamp= [SELECT Id,Name,Event__c,Event__r.name,Event__r.ProductCode,Type,StartDate FROM Campaign where id=:campagianRecordId];
      List<CampaignMember> campaignMember =[SELECT Id,Name,CampaignId,ContactId,LeadId FROM CampaignMember where CampaignId=:campagianRecordId];
      List<Product2> eventList=[Select id,name,ProductCode from Product2];
      List<CampaignMemberStatus> CampaignMemberStatusList=[SELECT Id,Label,IsDefault from CampaignMemberStatus where CampaignId=:campagianRecordId];     
                                                  
      system.debug('@@originalCamp' +originalCamp);
      system.debug('@@campaignMember' +campaignMember.size()+''+campaignMember);
      system.debug('@@campOpportunityList ' +campOpportunityList.size()+'' +campOpportunityList);
      for(Product2 pro:eventList){
          String originalEvent=originalCamp.Event__r.ProductCode;      
          String numericString = '';
                for(integer i =0;i<originalEvent.length();i++){
                    String s= originalEvent.mid(i,1);
                    if(s.isNumeric()){
                        numericString +=s;
                    }
                }
          Integer year=integer.valueof(numericString)+1;
          String eventName = String.valueof(year);
          String newEvent = originalEvent.replace(numericString, eventName);
          System.debug('Number is event: ' +newEvent );
          if(pro.ProductCode==newEvent){
              eventId=Pro.id;
           }     
      }
          
      sObject originalCampaign = (sObject) originalCamp;
      List<sObject> originalCampaigns = new List<sObject>{originalCampaign };           
      List<sObject> clonedCampaigns = CloneCampaignController.cloneObjects(originalCampaigns,originalCampaign.getsObjectType());
      Campaign clonedCampaign = (Campaign)clonedCampaigns.get(0);
         clonedCampaign.Status='Planned';
         clonedCampaign.Event__c=eventId;
         clonedCampaign.Name=originalCamp.Name+'_clone';
         insert clonedCampaign ;
         
       List<CampaignMemberStatus> cloneCampMemberStatusList=[SELECT Id,CampaignId,Label,IsDefault from CampaignMemberStatus where CampaignId=:clonedCampaign.id];
        if(cloneCampMemberStatusList.size()>0){
            for(CampaignMemberStatus camStatus :cloneCampMemberStatusList){
                if(camStatus.Label!=null){
                    campMemberStatusLabel.put(camStatus.Label,camStatus.Id);
                 }   
             }   
        } 
         
         if(CampaignMemberStatusList.size()>0){      
            for(CampaignMemberStatus campStatus : CampaignMemberStatusList){
                if(!campMemberStatusLabel.keySet().contains(campStatus.Label)){
                    CampaignMemberStatus campMemStatus =new CampaignMemberStatus();
                    campMemStatus.Label = campStatus.Label;
                    campMemStatus.IsDefault=campStatus.IsDefault;
                    campMemStatus.CampaignId=clonedCampaign.id;
                    newCampaignMemStatusList.add(campMemStatus);
                }else{
                    CampaignMemberStatus campMemStatus =new CampaignMemberStatus();
                    campMemStatus.Id=campMemberStatusLabel.get(campStatus.Label);
                    campMemStatus.Label = campStatus.Label;
                    campMemStatus.IsDefault=campStatus.IsDefault;
                    newCampMemStatusListUpdate.add(campMemStatus);
                }
             }   
         }
         
         if(newCampaignMemStatusList.size()>0){
            insert newCampaignMemStatusList;
         } 
         if(newCampMemStatusListUpdate.size()>0){
            update newCampMemStatusListUpdate;
         }     
        List<CampaignMemberStatus> cloneCampaignMemberStatusList=[SELECT Id,CampaignId,Label,IsDefault from CampaignMemberStatus where CampaignId=:clonedCampaign.id]; 
        if(cloneCampaignMemberStatusList.size()>0){
            for(CampaignMemberStatus camStstusObj : cloneCampaignMemberStatusList){
                if(camStstusObj.IsDefault==true){
                    mapOfCampaignStatus.put(camStstusObj.CampaignId,camStstusObj.Label);
                }     
            }
        
        }
      
        if(campaignMember.size()>0){ 
            if(clonedCampaign !=null){ 
                for(CampaignMember campMember: campaignMember){
                    if(campMember.contactid!=null){                                      
                        CampaignMember campMemb =new CampaignMember();
                        campMemb.CampaignId=clonedCampaign.Id;
                        campMemb.ContactId=campMember.ContactId;
                        campMemb.Status=mapOfCampaignStatus.get(clonedCampaign.Id);
                        campMemberList.add(campMemb); 
                        mapOfContact.put(campMemb.ContactId,clonedCampaignId);                      
                    }                   
                 }
            }         
        } 
       for(myWrapperClass wraoObj:opportunityList){
           if(wraoObj.selected==true){
              campOpportunityList.add(wraoObj.opp);
           }       
        }
        
        Set<Id> objConId = new Set<Id>();
        if(campOpportunityList.size()>0){
            for(Opportunity opp:campOpportunityList ){           
                if(!mapOfContact.keyset().contains(opp.Primary_Contact__c) && String.isNotBlank(opp.Primary_Contact__c)){
                        objConId.add(opp.Primary_Contact__c); 
                    }
            }
            Map<Id,Contact> objMapOfContact = new Map<Id,Contact>([select id from Contact where Id IN : objConId]);
            for(Id objId : objMapOfContact.keySet()){
                CampaignMember campMemb =new CampaignMember();
                campMemb.CampaignId=clonedCampaign.id;
                campMemb.ContactId=objId;
                oppCampMemberList.add(campMemb);
            }           
         }
           
         if(campMemberList.size()>0){
             insert campMemberList;
         }
          if(oppCampMemberList.size()>0){
             insert oppCampMemberList;
         }
         
         List<CampaignMember> deleteCampaignMember =[SELECT Id FROM CampaignMember where CampaignId=:clonedCampaign.id 
                                                  and (Contact.DoNotCall=true
                                                  OR Contact.HasOptedOutOfEmail=true
                                                  OR Contact.Deceased__c=true
                                                  OR Contact.Retired__c=true)];       
         if(deleteCampaignMember.size()>0){
             delete deleteCampaignMember;
         }
               
          PageReference pg = new PageReference('/'+clonedCampaign.Id);
          pg.setRedirect(true);
          return pg ;
     }
     
     public PageReference cloneCampaignNo(){
      String eventId;
      String clonedCampaignId;
      List<CampaignMember> campMemberList=new  List<CampaignMember>();
      List<CampaignMember> oppCampMemberList=new  List<CampaignMember>();
      List<CampaignMemberStatus> newCampaignMemStatusList=new List<CampaignMemberStatus>();
      List<CampaignMemberStatus> newCampMemStatusListUpdate=new List<CampaignMemberStatus>();
      Map<String,String> mapOfContact=new Map<String,String>();
      Map<String,String> mapOfCampaignStatus=new Map<String,String>();
      Set<Id> primaryContact=new Set<Id>();
      Set<Id> objConId = new Set<Id>();
      Map<String,String> campMemberStatusLabel = new Map<String,String>();
       
      Campaign originalCamp= [SELECT Id,Name,Event__c,Event__r.name,Event__r.ProductCode,Type,StartDate FROM Campaign where id=:campagianRecordId];
      List<CampaignMember> campaignMember =[SELECT Id,Name,CampaignId,ContactId,LeadId FROM CampaignMember where CampaignId=:campagianRecordId];
      List<Product2> eventList=[Select id,name,ProductCode from Product2];
      List<CampaignMemberStatus> CampaignMemberStatusList=[SELECT Id,Label,IsDefault from CampaignMemberStatus where CampaignId=:campagianRecordId];    
         
      sObject originalCampaign = (sObject) originalCamp;
      List<sObject> originalCampaigns = new List<sObject>{originalCampaign };           
      List<sObject> clonedCampaigns = CloneCampaignController.cloneObjects(originalCampaigns,originalCampaign.getsObjectType());
      Campaign clonedCampaign = (Campaign)clonedCampaigns.get(0);
         clonedCampaign.Status='Planned';
         clonedCampaign.Name=originalCamp.Name+'_clone';
         insert clonedCampaign ;
         
        List<CampaignMemberStatus> cloneCampMemberStatusList=[SELECT Id,CampaignId,Label,IsDefault from CampaignMemberStatus where CampaignId=:clonedCampaign.id];
        if(cloneCampMemberStatusList.size()>0){
            for(CampaignMemberStatus camStatus :cloneCampMemberStatusList){
                if(camStatus.Label!=null){
                    campMemberStatusLabel.put(camStatus.Label,camStatus.Id);
                 }   
             }   
        } 
        if(CampaignMemberStatusList.size()>0){      
            for(CampaignMemberStatus campStatus : CampaignMemberStatusList){
                if(!campMemberStatusLabel.keySet().contains(campStatus.Label)){
                    CampaignMemberStatus campMemStatus =new CampaignMemberStatus();
                    campMemStatus.Label = campStatus.Label;
                    campMemStatus.IsDefault=campStatus.IsDefault;
                    campMemStatus.CampaignId=clonedCampaign.id;
                    newCampaignMemStatusList.add(campMemStatus);
                 }else{
                    CampaignMemberStatus campMemStatus =new CampaignMemberStatus();
                    campMemStatus.Id=campMemberStatusLabel.get(campStatus.Label);
                    campMemStatus.Label = campStatus.Label;
                    campMemStatus.IsDefault=campStatus.IsDefault;
                    newCampMemStatusListUpdate.add(campMemStatus);
                 }   
             }
         }  
         
        if(newCampaignMemStatusList.size()>0){          
            insert newCampaignMemStatusList;
         }
         if(newCampMemStatusListUpdate.size()>0){
            update newCampMemStatusListUpdate;
         }      
         
        List<CampaignMemberStatus> cloneCampaignMemberStatusList=[SELECT Id,CampaignId,Label,IsDefault from CampaignMemberStatus where CampaignId=:clonedCampaign.id]; 
        if(cloneCampaignMemberStatusList.size()>0){
            for(CampaignMemberStatus camStstusObj : cloneCampaignMemberStatusList){
                if(camStstusObj.IsDefault==true){
                    mapOfCampaignStatus.put(camStstusObj.CampaignId,camStstusObj.Label);
                }     
            }
        
        }
        if(campaignMember.size()>0){ 
            if(clonedCampaign !=null){ 
                for(CampaignMember campMember: campaignMember){
                    if(campMember.contactid!=null){                                      
                        CampaignMember campMemb =new CampaignMember();
                        campMemb.CampaignId=clonedCampaign.Id;
                        campMemb.Status=mapOfCampaignStatus.get(clonedCampaign.Id);
                        campMemb.ContactId=campMember.ContactId;
                        campMemberList.add(campMemb);                                              
                    }                   
                 }
            }         
        }      
        if(campMemberList.size()>0){
             insert campMemberList;
         }
       
        List<CampaignMember> deleteCampaignMember =[SELECT Id FROM CampaignMember where CampaignId=:clonedCampaign.id 
                                                  and (Contact.DoNotCall=true
                                                  OR Contact.HasOptedOutOfEmail=true
                                                  OR Contact.Deceased__c=true
                                                  OR Contact.Retired__c=true)];
           
          if(deleteCampaignMember.size()>0){
             delete deleteCampaignMember;
         }
               
          PageReference pg = new PageReference('/'+clonedCampaign.Id);
          pg.setRedirect(true);
          return pg ;      
    }
    
   public void fetchData(){
      Campaign originalCamp= [SELECT Id,Name,Event__c,Event__r.name,RecordType.name,Event__r.ProductCode,Type,StartDate FROM Campaign where id=:campagianRecordId];
      List<Opportunity> campOpportunityList =[SELECT Id,StageName,Reach_Hygiene__c,account.name,Campaign.name,Primary_Contact__r.name,Name,Account_Record_Type__c,Product__c FROM Opportunity where StageName='Closed Won' and Product__c =: originalCamp.Event__c and Account_Record_Type__c=:originalCamp.RecordType.name];      
           opportunityList=new List<myWrapperClass>();
          if(campOpportunityList.size()>0){
              for(Opportunity opp:campOpportunityList){
                  myWrapperClass warpObj = null;
                  if(opp.StageName == 'Closed Won'){
                     warpObj = new myWrapperClass(opp,false);
                  }else{
                     warpObj = new myWrapperClass(opp,true);
                  }
                 if(warpObj != null){
                      opportunityList.add(warpObj);
                 }
               }
           }                        
    }

   public static List<sObject> cloneObjects(List<sObject> sObjects,Schema.SObjectType objectType){    
    List<Id> sObjectIds = new List<Id>{};
    List<String> sObjectFields = new List<String>{};
    List<sObject> clonedSObjects = new List<sObject>{};
    
        if(objectType != null){
          sObjectFields.addAll(objectType.getDescribe().fields.getMap().keySet());
        }  
        if(sObjects != null && !sObjects.isEmpty() && !sObjectFields.isEmpty()){
                for (sObject objectInstance: sObjects){
                    sObjectIds.add(objectInstance.Id);
                  }
    
            String allSObjectFieldsQuery = 'SELECT ' + sObjectFields.get(0); 
             for (Integer i=1 ; i < sObjectFields.size() ; i++){
                allSObjectFieldsQuery += ', ' + sObjectFields.get(i);
              }
            
            allSObjectFieldsQuery += ' FROM ' + 
                                   objectType.getDescribe().getName() + 
                                   ' WHERE ID IN (\'' + sObjectIds.get(0) + 
                                   '\'';
    
              for (Integer i=1 ; i < sObjectIds.size() ; i++){
                allSObjectFieldsQuery += ', \'' + sObjectIds.get(i) + '\'';
              }
        
           allSObjectFieldsQuery += ')';
        
          try{  
              for (SObject sObjectFromDatabase:
                  Database.query(allSObjectFieldsQuery)){
                  clonedSObjects.add(sObjectFromDatabase.clone(false,true));  
              }
        
            } catch (exception e){
          
          }
    }     
    return clonedSObjects;
  }
   public class myWrapperClass{
        public Opportunity opp{get;set;}
        public Boolean selected {get; set;} 
        public myWrapperClass(Opportunity oppt,Boolean checked) { 
            selected = checked; 
            opp=oppt;
        } 
    }
    
    Public PageReference cancelButton(){
        PageReference pageRef = new PageReference('/'+campagianRecordId);
        pageRef.setRedirect(true);
        return pageRef;  
    }
    public PageReference cloneCampaignYes(){
        PageReference pg = new PageReference('/Apex/CloneCampaignOpportunity?id='+campagianRecordId);
        pg.setRedirect(true);
        return pg ;   
    }

}