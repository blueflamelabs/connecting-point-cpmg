@isTest
private class MarkGoneContactCTRL_test {
    
    @isTest static void test_MarkGoneContactCTRL() {
      Id AccRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Account acc = new Account(); 
        acc.Name='test';
        acc.RecordTypeId = AccRecordTypeId;
        insert acc;
        
        Account accCreate = new Account(); 
        accCreate.Name='test1';
        
        
        Id execConRtype =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Contact c = TestDataGenerator.createTestContact(true,'Test','Last Name','testAcc@account.com',acc,execConRtype);
        Contact cCreate = TestDataGenerator.createTestContact(false,'Test','Last Name','testAcc@account.com',acc,execConRtype);
        
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event',true,Date.today().addDays(2), Date.today().addDays(-1), Date.today().addMonths(-3), Date.today().addMonths(-2));
        Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Event Active Opp',Date.today().addMonths(-4),acc,'Market Research',prod ,'Renewal');
        opp.Primary_Contact__c = c.id;
        update opp;
        Id execPRRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Suppliers').getRecordTypeId();
        Event_Account_Details__c execPRecord = TestDataGenerator.createParticipationRecord(true,prod,acc,opp,c,execPRRType);
        Order execConf = TestDataGenerator.createConfirmation(true,acc,c,'Attending Event Contact',opp,opp.CloseDate,execPRecord,prod,true);  
        test.startTest();
        
        MarkGoneContactCTRL.ContactsHyginRecord();
        MarkGoneContactCTRL.fetchLookUpValues('t','Account','Supplier');
        MarkGoneContactCTRL.ContactInfo(c.id);
        MarkGoneContactCTRL.addressOfAccount(acc.id,MarkGoneContactCTRL.ContactInfo(c.id));
        MarkGoneContactCTRL.allContact(MarkGoneContactCTRL.ContactInfo(c.id));
        MarkGoneContactCTRL.ContactRecTypeId(acc.id);
        MarkGoneContactCTRL.createContactRecord(cCreate,acc.id);
        MarkGoneContactCTRL.createAccountRecord(accCreate,'Supplier');
        MarkGoneContactCTRL.OpportunityInfo(c.id);
        MarkGoneContactCTRL.OrderInfo(c.id);
        MarkGoneContactCTRL.SaveAllUpdate(acc.id,MarkGoneContactCTRL.ContactInfo(c.id),
                    MarkGoneContactCTRL.OpportunityInfo(c.Id),MarkGoneContactCTRL.OrderInfo(c.id),true,New List<Id>{execConf.Id},New List<Id>{execConf.Id},'YesOrder');
        test.stopTest();
    }
   
    @isTest static void test_MarkGoneContactCTRLEx() {
      Id AccRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Account acc = new Account(); 
        acc.Name='test';
        acc.RecordTypeId = AccRecordTypeId;
        insert acc;
        
        Account accCreate = new Account(); 
        accCreate.Name='test1';
        
        
        Id execConRtype =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Contact c = TestDataGenerator.createTestContact(true,'Test','Last Name','testAcc@account.com',acc,execConRtype);
        Contact cCreate = TestDataGenerator.createTestContact(false,'Test','Last Name','testAcc@account.com',acc,execConRtype);
        
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event',true,Date.today().addDays(2), Date.today().addDays(-1), Date.today().addMonths(-3), Date.today().addMonths(-2));
        Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Event Active Opp',Date.today().addMonths(-4),acc,'Market Research',prod ,'Renewal');
        opp.Primary_Contact__c = c.id;
        update opp;
        Id execPRRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Suppliers').getRecordTypeId();
        Event_Account_Details__c execPRecord = TestDataGenerator.createParticipationRecord(true,prod,acc,opp,c,execPRRType);
        Order execConf = TestDataGenerator.createConfirmation(true,acc,c,'Attending Event Contact',opp,opp.CloseDate,execPRecord,prod,true);  
        test.startTest();
        
        MarkGoneContactCTRL.ContactsHyginRecord();
        MarkGoneContactCTRL.fetchLookUpValues('t','Account','Supplier');
        MarkGoneContactCTRL.ContactInfo(c.id);
        MarkGoneContactCTRL.addressOfAccount(acc.id,MarkGoneContactCTRL.ContactInfo(c.id));
        MarkGoneContactCTRL.allContact(MarkGoneContactCTRL.ContactInfo(c.id));
        MarkGoneContactCTRL.ContactRecTypeId(acc.id);
        MarkGoneContactCTRL.createContactRecord(cCreate,acc.id);
        MarkGoneContactCTRL.createAccountRecord(accCreate,'Supplier');
        MarkGoneContactCTRL.OpportunityInfo(c.id);
        MarkGoneContactCTRL.OrderInfo(c.id);
        MarkGoneContactCTRL.SaveAllUpdate(acc.id,MarkGoneContactCTRL.ContactInfo(c.id),
                    MarkGoneContactCTRL.OpportunityInfo(c.id),MarkGoneContactCTRL.OrderInfo(c.id),true,New List<Id>{execConf.Id},New List<Id>{execConf.Id},'YesOrder');
        MarkGoneContactCTRL.SaveAllUpdate(acc.id,MarkGoneContactCTRL.ContactInfo(c.id),
                    MarkGoneContactCTRL.OpportunityInfo(c.id),MarkGoneContactCTRL.OrderInfo(c.id),false,New List<Id>{execConf.Id},New List<Id>{execConf.Id},'YesOrder');
        
        test.stopTest();
    }
    
 }