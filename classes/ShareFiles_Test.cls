@isTest(seealldata=false)

 public class ShareFiles_Test {
      static testMethod void TestShareFiles() {

        //Create test account
          Account objAcc = TestDataGenerator.createTestAccount(true,'Test Supplier Account');
          
          ContentVersion contentVersion = new ContentVersion(
                                          Title = 'Penguins',
                                          PathOnClient = 'Penguins.jpg',
                                          VersionData = Blob.valueOf('Test Content'),
                                          IsMajorVersion = true
                                          );
              insert contentVersion;   
          system.Test.startTest();
             
            List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
            
            //create ContentDocumentLink  record 
            ContentDocumentLink cdl = New ContentDocumentLink();
            cdl.LinkedEntityId = objAcc.id;
            cdl.ContentDocumentId = documents[0].Id;
            cdl.shareType = 'V';
            cdl.Visibility = 'AllUsers';
            insert cdl;
          System.Test.stopTest();
      }
 }