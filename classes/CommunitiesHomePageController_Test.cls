@isTest
private class CommunitiesHomePageController_Test {

    @isTest
    private static void test_Executive_Event_Home_Page() {
        Id execConRtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        TestDataGenerator.createOrderStatusCustomSetting();
        Account a = TestDataGenerator.createTestAccount(true,'Executive Test Account');

        Contact c = TestDataGenerator.createTestContact(true,'Test','Exec','t@t.com',a,(String) execConRtype);
        c.Has_Event__c = true;
        update c;
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event',true,Date.today().addDays(-35), Date.today().addDays(-30), Date.today().addMonths(-3), Date.today().addMonths(-2),
            'Test Venue', 'This is a Venue and Location','Test Location','Test LinkedIn','Test Twitter',String.valueOf(Date.today().year()),'January 21 - February 24, 2019','BuildPoint','BuildPoint');
            prod.Charity_Logo_URL__c ='http://charity.org/image.png';
            prod.Charity_Org_URL__c = 'http://charity.org/';
            prod.Charity_Goal__c = 1000000.00;
            prod.Charity_Raised__c=10000.00;
            prod.Twitter__c = null;
            update prod;

    
        //Start Defining Event "Product" related info.
        Profile communityProfile = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        
        User commUser = TestDataGenerator.CreateUser(true,communityProfile.Id,'Test','Exec',null,'testExec@cpmg.com',c.Id);

        
        Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Opp',Date.today().addYears(-1),a, 'Closed Won', prod);

        Id chainExecRType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c ead = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c,chainExecRType);

        Order conf = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',opp,Date.today(),ead, prod,true);
        conf.Ground_Transportation__c = 'This is custom stuff';
        conf.Airline_and_flight__c ='1234';
        conf.Airline_and_flight_URL__c='www.delta.com';
        update conf;
        System.assertNotEquals(null,conf);

        PageReference pageRef = Page.CommunitiesHomePage;
        //pageRef.getParameters().put('eId',prod.Id);
        Test.setCurrentPageReference(pageRef);

        // create an instance of the controller

    
        Test.startTest();
            System.runAs(commUser){
                CommunitiesHomePageController homePage = new CommunitiesHomePageController();
                //comHeader.getHead();
                }
        Test.stopTest();
    }

    @isTest
    private static void test_Supplier_Event_Home_Page() {
        Id supplierAccRtype = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Id supplierConRtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        TestDataGenerator.createOrderStatusCustomSetting();
        Account a = TestDataGenerator.createTestAccount(true,'Executive Test Account', supplierAccRtype);

        Contact c = TestDataGenerator.createTestContact(true,'Test','Exec','t@t.com',a,(String) supplierConRtype);
        c.Has_Event__c = true;
        update c;
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event',true,Date.today().addDays(-35), Date.today().addDays(-30), Date.today().addMonths(-3), Date.today().addMonths(-2),
            'Test Venue', 'This is a Venue and Location','Test Location','Test LinkedIn','Test Twitter',String.valueOf(Date.today().year()),'January 21 - February 24, 2019','BuildPoint','BuildPoint');

    
        //Start Defining Event "Product" related info.
        Profile communityProfile = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        
        User commUser = TestDataGenerator.CreateUser(true,communityProfile.Id,'Test','Exec',null,'testExec@cpmg.com',c.Id);

        
        Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Opp',Date.today().addYears(-1),a, 'Closed Won', prod);

        Id chainExecRType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c ead = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c,chainExecRType);

        Order conf = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',opp,Date.today(),ead, prod,true);
        conf.Ground_Transportation__c = 'This is custom stuff';
        conf.Airline_and_flight__c ='1234';
        conf.Airline_and_flight_URL__c='www.delta.com';
        update conf;
        System.assertNotEquals(null,conf);

        PageReference pageRef = Page.CommunitiesHomePage;
        pageRef.getParameters().put('eId',prod.Id);
        Test.setCurrentPageReference(pageRef);

        // create an instance of the controller

    
        Test.startTest();
            System.runAs(commUser){
                CommunitiesHomePageController homePage = new CommunitiesHomePageController();
                //comHeader.getHead();
                }
        Test.stopTest();
    }
    @isTest
    private static void test_Supplier_Event_Home_Page_NO_ORder() {
        Id supplierAccRtype = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Id supplierConRtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        TestDataGenerator.createOrderStatusCustomSetting();
        Account a = TestDataGenerator.createTestAccount(true,'Executive Test Account', supplierAccRtype);

        Contact c = TestDataGenerator.createTestContact(true,'Test','Exec','t@t.com',a,(String) supplierConRtype);
        c.Has_Event__c = true;
        update c;
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event',true,Date.today().addDays(-35), Date.today().addDays(-30), Date.today().addMonths(-3), Date.today().addMonths(-2),
            'Test Venue', 'This is a Venue and Location','Test Location','Test LinkedIn','Test Twitter',String.valueOf(Date.today().year()),'January 21 - February 24, 2019','BuildPoint','BuildPoint');

    
        //Start Defining Event "Product" related info.
        Profile communityProfile = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        
        User commUser = TestDataGenerator.CreateUser(true,communityProfile.Id,'Test','Exec',null,'testExec@cpmg.com',c.Id);

        
        Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Opp',Date.today().addYears(-1),a, 'Closed Won', prod);

        Id chainExecRType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c ead = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c,chainExecRType);

        /*Order conf = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',opp,Date.today(),ead, prod);
        conf.Ground_Transportation__c = 'This is custom stuff';
        conf.Airline_and_flight__c ='1234';
        conf.Airline_and_flight_URL__c='www.delta.com';
        update conf;
        System.assertNotEquals(null,conf); */

        PageReference pageRef = Page.CommunitiesHomePage;
        pageRef.getParameters().put('eId',prod.Id);
        Test.setCurrentPageReference(pageRef);

        // create an instance of the controller

    
        Test.startTest();
            System.runAs(commUser){
                CommunitiesHomePageController homePage = new CommunitiesHomePageController();
                //comHeader.getHead();
                }
        Test.stopTest();
    }

    @isTest
    private static void test_Supplier_Event_Home_Page_NO_ORder_NO_eId() {
        Id supplierAccRtype = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Id supplierConRtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        TestDataGenerator.createOrderStatusCustomSetting();
        Account a = TestDataGenerator.createTestAccount(true,'Executive Test Account', supplierAccRtype);

        Contact c = TestDataGenerator.createTestContact(true,'Test','Exec','t@t.com',a,(String) supplierConRtype);
        c.Has_Event__c = true;
        update c;
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event',true,Date.today().addDays(-35), Date.today().addDays(-30), Date.today().addMonths(-3), Date.today().addMonths(-2),
            'Test Venue', 'This is a Venue and Location','Test Location','Test LinkedIn','Test Twitter',String.valueOf(Date.today().year()),'January 21 - February 24, 2019','BuildPoint','BuildPoint');

    
        //Start Defining Event "Product" related info.
        Profile communityProfile = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        
        User commUser = TestDataGenerator.CreateUser(true,communityProfile.Id,'Test','Exec',null,'testExec@cpmg.com',c.Id);

        
        Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Opp',Date.today().addYears(-1),a, 'Closed Won', prod);

        Id chainExecRType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c ead = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c,chainExecRType);

        /*Order conf = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',opp,Date.today(),ead, prod);
        conf.Ground_Transportation__c = 'This is custom stuff';
        conf.Airline_and_flight__c ='1234';
        conf.Airline_and_flight_URL__c='www.delta.com';
        update conf;
        System.assertNotEquals(null,conf); */

        PageReference pageRef = Page.CommunitiesHomePage;
        //pageRef.getParameters().put('eId',prod.Id);
        Test.setCurrentPageReference(pageRef);

        // create an instance of the controller

    
        Test.startTest();
            System.runAs(commUser){
                CommunitiesHomePageController homePage = new CommunitiesHomePageController();
                //comHeader.getHead();
                }
        Test.stopTest();
    }

    static testMethod void getTwitterName_Test(){
        Id supplierAccRtype = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Id supplierConRtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        TestDataGenerator.createOrderStatusCustomSetting();
        Account a = TestDataGenerator.createTestAccount(true,'Executive Test Account', supplierAccRtype);

        Contact c = TestDataGenerator.createTestContact(true,'Test','Exec','t@t.com',a,(String) supplierConRtype);
        c.Has_Event__c = true;
        update c;
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event',true,Date.today().addDays(-35), Date.today().addDays(-30), Date.today().addMonths(-3), Date.today().addMonths(-2),
            'Test Venue', 'This is a Venue and Location','Test Location','Test LinkedIn','Test Twitter',String.valueOf(Date.today().year()),'January 21 - February 24, 2019','BuildPoint','BuildPoint');

    
        //Start Defining Event "Product" related info.
        Profile communityProfile = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        
        User commUser = TestDataGenerator.CreateUser(true,communityProfile.Id,'Test','Exec',null,'testExec@cpmg.com',c.Id);

        
        Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Opp',Date.today().addYears(-1),a, 'Closed Won', prod);

        Id chainExecRType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c ead = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c,chainExecRType);

        Order conf = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',opp,Date.today(),ead, prod,true);
        conf.Ground_Transportation__c = 'This is custom stuff';
        conf.Airline_and_flight__c ='1234';
        conf.Airline_and_flight_URL__c='www.delta.com';
        update conf;
        System.assertNotEquals(null,conf);

        PageReference pageRef = Page.CommunitiesHomePage;
        pageRef.getParameters().put('eId',prod.Id);
        Test.setCurrentPageReference(pageRef);

        CommunitiesHomePageController homePage = new CommunitiesHomePageController();
        //homePage.
        Test.startTest();
            
            homePage.initializeHomePage();
            String s = homePage.getTwitterName();
            System.assertEquals(prod.Twitter__c, s);
        Test.stopTest();

    }

    static testMethod void getCharityFields_Test(){
        Id supplierAccRtype = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Id supplierConRtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        TestDataGenerator.createOrderStatusCustomSetting();
        Account a = TestDataGenerator.createTestAccount(true,'Executive Test Account', supplierAccRtype);

        Contact c = TestDataGenerator.createTestContact(true,'Test','Exec','t@t.com',a,(String) supplierConRtype);
        c.Has_Event__c = true;
        update c;
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event',true,Date.today().addDays(-35), Date.today().addDays(-30), Date.today().addMonths(-3), Date.today().addMonths(-2),
            'Test Venue', 'This is a Venue and Location','Test Location','Test LinkedIn','Test Twitter',String.valueOf(Date.today().year()),'January 21 - February 24, 2019','BuildPoint','BuildPoint');
            prod.Charity_Logo_URL__c ='http://charity.org/image.png';
            prod.Charity_Org_URL__c = 'http://charity.org/';
            prod.Charity_Goal__c = 1000000.00;
            prod.Charity_Raised__c=10000.00;
            prod.Twitter__c = null;
            update prod;
    
        //Start Defining Event "Product" related info.
        Profile communityProfile = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        
        User commUser = TestDataGenerator.CreateUser(true,communityProfile.Id,'Test','Exec',null,'testExec@cpmg.com',c.Id);

        
        Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Opp',Date.today().addYears(-1),a, 'Closed Won', prod);

        Id chainExecRType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c ead = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c,chainExecRType);

        Order conf = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',opp,Date.today(),ead, prod,true);
        conf.Ground_Transportation__c = 'This is custom stuff';
        conf.Airline_and_flight__c ='1234';
        conf.Airline_and_flight_URL__c='www.delta.com';
        update conf;
        System.assertNotEquals(null,conf);

        PageReference pageRef = Page.CommunitiesHomePage;
        pageRef.getParameters().put('eId',prod.Id);
        Test.setCurrentPageReference(pageRef);

        CommunitiesHomePageController homePage = new CommunitiesHomePageController();
        //homePage.
        Test.startTest();
            
            homePage.initializeHomePage();
            Integer charityGoal = homePage.getCharityGoal();
            System.assertEquals(Integer.valueOf(prod.Charity_Goal__c), charityGoal);

            String charityLogo = homePage.getCharityLogo();
            System.assertEquals(prod.Charity_Logo_URL__c, charityLogo);

            String charityOrg = homePage.getCharityOrgURL();
            System.assertEquals(prod.Charity_Org_URL__c, charityOrg);

            Integer charityRaised = homePage.getCharityRaised();
            System.assertEquals(Integer.valueOf(prod.Charity_Raised__c), charityRaised);

            Product2 curEvent = homePage.getCurrentEvent();
            System.assertEquals(prod.Id,curEvent.Id);

            Boolean noCharity = homePage.getNoCharity();
            System.assertEquals(false,noCharity);

            Order eventOrder = homePage.getEventOrder();

            Boolean regError = homePage.getIsRegistrationError();

            String regErrorMessage = homePage.getRegistrationErrorMessage();
        Test.stopTest();

    }

    @isTest
    private static void test_Internal_User_Access() {
        Id supplierAccRtype = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Id supplierConRtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        TestDataGenerator.createOrderStatusCustomSetting();
        TestDataGenerator.createCommSettings();
        Account a = TestDataGenerator.createTestAccount(true,'Executive Test Account', supplierAccRtype);

        Contact c = TestDataGenerator.createTestContact(true,'Test','Exec','t@t.com',a,(String) supplierConRtype);
        c.Has_Event__c = true;
        update c;
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event',true,Date.today().addDays(-35), Date.today().addDays(-30), Date.today().addMonths(-3), Date.today().addMonths(-2),
            'Test Venue', 'This is a Venue and Location','Test Location','Test LinkedIn','Test Twitter',String.valueOf(Date.today().year()),'January 21 - February 24, 2019','BuildPoint','BuildPoint');

    
        //Start Defining Event "Product" related info.
        Profile communityProfile = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        
        User commUser = TestDataGenerator.CreateUser(true,communityProfile.Id,'Test','Exec',null,'testExec@cpmg.com',c.Id);

        
        Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Opp',Date.today().addYears(-1),a, 'Closed Won', prod);

        Id chainExecRType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c ead = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c,chainExecRType);

        Order conf = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',opp,Date.today(),ead, prod,true);
        conf.Ground_Transportation__c = 'This is custom stuff';
        conf.Airline_and_flight__c ='1234';
        conf.Airline_and_flight_URL__c='www.delta.com';
        update conf;
        System.assertNotEquals(null,conf);

        PageReference pageRef = Page.CommunitiesHomePage;
        pageRef.getParameters().put('eId',prod.Id);
        Test.setCurrentPageReference(pageRef);

        // create an instance of the controller

    
        Test.startTest();
            //System.runAs(commUser){
                CommunitiesHomePageController homePage = new CommunitiesHomePageController();
                //comHeader.getHead();
            //  }
        Test.stopTest();
    }
}