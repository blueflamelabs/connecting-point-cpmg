@isTest
public class updateAccountContactRelationTrigger_Test{
    static testMethod void updateAccountContactRelationTriggertest(){
       Id execAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Account a = TestDataGenerator.createTestAccount(true, 'Test Account',execAccRtype);
       
         Account accRecord=new account();
         accRecord.Name='Test Acc1114';
         insert accRecord;
       
       Title_Reference_Table__c titleRef = new Title_Reference_Table__c();
        titleRef.Value_to_Replace__c = 'Test';
        titleRef.Replace_with_Value__c = 'Owner';
        titleRef.Add_Comma_After_Replaced_Value__c = true;
        titleRef.Review_Order__c = 5;
        insert titleRef;
       
        ControllingTrigger.isRunAgain = false;
        Id conRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();     
        Contact c = TestDataGenerator.createTestContact(true,'Test','Last Name','Acc@account.com',a,conRecordTypeId);
        c.Email = 'testAcc@account1.com';
        c.Title = 'Test';
        c.Seniority__c = 'Vice President';
        //c.Badge_Sort_Order__c =50;
        update c;
        
         updateAccountContactRelationCTRL.updateContactTitle(new List<Contact>{c});
        
        
        
      
        AccountContactRelation acr = new AccountContactRelation(); 
        acr.Accountid = accRecord.id;
        acr.Contactid = c.id;
        acr.Title__c = 'Test1';
        acr.Email_Address__c = 'Acc@account.com';
        acr.StartDate = date.today();
        acr.Employment_Status__c = 'Active';
        acr.IsActive = true;
        insert acr;
        acr.Title__c = 'Vice PresidentP1';
        update acr;
        
        
    }
   
}