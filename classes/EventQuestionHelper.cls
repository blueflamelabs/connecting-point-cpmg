public class EventQuestionHelper {
    

    public static  Map<Id,Event_Question__c> getEventQuestions(String p_phase, String recordTypeName, String questType, Id eventId) {
        System.debug('Get Event Questions called:');
        
         Map<Id,Event_Question__c> eventQuestionMap = new Map<Id,Event_Question__c>([SELECT Id,RecordType.Name,Phase__c, Question__c, Display_Order__c,Instructional_Text__c, 
                                                                Display_Type__c, Question_Display__c, Required__c
                                                  FROM Event_Question__c
                                                  WHERE Event__c = :eventId
                                                  AND Phase__c = :p_phase
                                                  AND RecordType.Name =: recordTypeName
                                                  ORDER BY Display_Order__c ASC]);

        return eventQuestionMap;
    }
}