public without sharing class ProfilePageController {

    public transient List<ProfileWrapper> profilesList {get;set;}
    public  List<Schema.FieldSetMember> accountFormFields{get;set;}
    public  Map<String, Account_Form_Display_Settings__c> accountUserSpecifiedFieldNames { get; set; }
    public  String eventId {get;set;}
    public  String partRecordId {get;set;}
    public  String currentPartRecordId {get;set;}
    public transient Map<Id,ProfileWrapper> prIdProfilesMap {get;set;}
    private  Map<String,Contact_Event_Answers__c> answersByPRAndQType {get;set;}
    public  UserPermissions permissions {get;set;}
    private  Map<Id,Event_Account_Details__c> allPRsForOppositeRType {get;set;}
    private  Map<Id,List<Contact_Event_Answers__c>> partRecordIdProfileQuestions {get;set;}
    public  Set<Id> categoryQuestionId {get;set;}
    public  List<SelectOption> categorySelectOptions {get;set;}
    //private  Map<String,Event_Ranking__c> partRecordIdEventRankingMap{get;set;}

    public ProfilePageController() {
        accountUserSpecifiedFieldNames = new Map<String, Account_Form_Display_Settings__c>(Account_Form_Display_Settings__c.getAll());
        accountFormFields = SObjectType.Account.FieldSets.User_Specified_Fields.getFields();
        
        for(Schema.FieldSetMember field :accountFormFields){
        
          if(!accountUserSpecifiedFieldNames.containsKey(field.getFieldPath())) {
            accountUserSpecifiedFieldNames.put(field.getFieldPath(), new Account_Form_Display_Settings__c(Read_Only__c = false, Field_Display_Name__c = field.getLabel()));
          }
            //System.debug('Field: ' + accountUserSpecifiedFieldNames.get(field.getFieldPath()));
        }
        
            if(ApexPages.currentPage().getParameters().containsKey('eId')){
          eventId =ApexPages.currentPage().getParameters().get('eId');
        }
        if(ApexPages.currentPage().getParameters().containsKey('prId')){
          partRecordId =ApexPages.currentPage().getParameters().get('prId');
        }
        if(ApexPages.currentPage().getParameters().containsKey('currentprId')){
          currentPartRecordId =ApexPages.currentPage().getParameters().get('currentprId');
        }
        if(eventId != null && currentPartRecordId != null){
          try{
          buildProfileWrappers(eventId,currentPartRecordId);
          }catch(Exception ex){
            System.debug('Error: '+ ex.getMessage());
            System.debug('Stack Trace: '+ ex.getStackTraceString());
          }
        }
    }

    public void buildProfileWrappers(Id eventId, Id currentPartRecord){
        profilesList = new List<ProfileWrapper>();
        getAllPRsForOppositeRType(new Set<Id>{eventId});
        getParticipationRecordQuestionAnswers(new Set<Id>{eventId});
        getAllIncludeInProfileQuestions(eventId, currentPartRecordId);
        prepareMeetingRequestWrappers();
        if(partRecordId != null){
          profilesList.add( prIdProfilesMap.get(partRecordId));
        }else{
          profilesList.addAll(prIdProfilesMap.values());
        }
        profilesList.sort();
        System.debug('Profiles List: '+ profilesList);
  }

  //Gets Short, and Category Category Answers
  private void getParticipationRecordQuestionAnswers(Set<Id> eventIds){
      Set<String> questionTypes = new Set<String>{'Company Short Description'};
      answersByPRAndQType = ContactEventAnswerHelper.getLongAndShortProfiles(EventIds, questionTypes);
  }

  private void getAllPRsForOppositeRType(Set<Id> eventIds){
      if(permissions == null){
        List<Id> eventIdsList = new List<Id>(eventIds);
        permissions = UtilClass.getUserPermissions();
        permissions.getEventStaff(eventIdsList[0]);
      }
      String rType = permissions.isSupplier ? 'Executive' : 'Supplier';
      if(rType == 'Executive'){
          Set<Id> objConId = new Set<Id>();
          allPRsForOppositeRType = new Map<Id,Event_Account_Details__c>();
          Map<Id,Event_Account_Details__c> objAllPREventAcctountSupplier = ParticipationRecordHelper.getAllPRRecordsByEventForMeetingRequests(eventIDs, rType);
          for(Event_Account_Details__c objEventAccount : objAllPREventAcctountSupplier.values()){
              objConId.add(objEventAccount.Primary_Contact__c);
          }
          List<Order> objOrderList = [select id,BillToContactId,Event_Account_Detail__c,Status from Order where BillToContactId IN : objConId and Event_Account_Detail__c IN : objAllPREventAcctountSupplier.keySet() and Status != 'Cancelled'];
          for(Order objOrder : objOrderList){
              allPRsForOppositeRType.put(objOrder.Event_Account_Detail__c,objAllPREventAcctountSupplier.get(objOrder.Event_Account_Detail__c));
          }
      }else{
          allPRsForOppositeRType = new Map<Id,Event_Account_Details__c>();
          Map<Id,Event_Account_Details__c> objAllPREventAcctountExecutive = ParticipationRecordHelper.getAllPRRecordsByEventForMeetingRequests(eventIDs, rType);
          Map<String,List<Event_Account_Details__c>> objMapAccIdEventAccList = new Map<String,List<Event_Account_Details__c>>();
          Map<String,List<Event_Account_Details__c>> objMapCompanyDisEventAccList = new Map<String,List<Event_Account_Details__c>>();
          for(Event_Account_Details__c objEventAccount : objAllPREventAcctountExecutive.values()){
             if(String.isBlank(objEventAccount.Company_Display_Name__c)){
                      List<Event_Account_Details__c> objListEvent = new List<Event_Account_Details__c>();
                      objListEvent.add(objEventAccount);
                      objMapAccIdEventAccList.put(objEventAccount.Account__c,objListEvent);
              }else{
                      List<Event_Account_Details__c> objListEvent = new List<Event_Account_Details__c>();
                      objListEvent.add(objEventAccount);
                      objMapCompanyDisEventAccList.put(objEventAccount.Company_Display_Name__c,objListEvent);
              }
          }
          // Account Name
          List<Event_Account_Details__c> objListEventAccountINFO = new List<Event_Account_Details__c>();
          List<Account> objAccList = [select id,(select id,Status,Order_Event__c from Orders where Status != 'Cancelled' and Order_Event__c =:eventId) from Account where id IN : objMapAccIdEventAccList.keySet()];
          for(Account objAcc : objAccList){
              List<Order> objOrder = objAcc.Orders;
              if(objOrder.size() > 0){
                  objListEventAccountINFO.addAll(objMapAccIdEventAccList.get(objAcc.id));
              }
          }
          for(Event_Account_Details__c objEventAcc : objListEventAccountINFO){
              allPRsForOppositeRType.put(objEventAcc.id,objEventAcc);
          }
          
          //Company Display Name
          List<Event_Account_Details__c> objListEventDisplayINFO = new List<Event_Account_Details__c>();
          List<Order> objOrderList = [select id,Status,Order_Event__c,Company_Display_Name__c,Primary_Event_Contact__c from Order where Company_Display_Name__c IN :objMapCompanyDisEventAccList.keySet() and Status != 'Cancelled' and Order_Event__c =:eventId and Primary_Event_Contact__c = true];
          for(Order objOrd : objOrderList){
             if(objMapCompanyDisEventAccList.keySet().Contains(objOrd.Company_Display_Name__c)){
                  objListEventDisplayINFO.addAll(objMapCompanyDisEventAccList.get(objOrd.Company_Display_Name__c));
                  objMapCompanyDisEventAccList.remove(objOrd.Company_Display_Name__c);
             }
          }
          for(Event_Account_Details__c objEventAcc : objListEventDisplayINFO){
              allPRsForOppositeRType.put(objEventAcc.id,objEventAcc);
          }
           
      }
  }

 /* private void getAllEventRankingsForPR(Id currentEventId, Id partRecordId){
      partRecordIdEventRankingMap = EventRankingHelper.getEventRankingsForEvent(currentEventId, partRecordId);
  } */
  private void getAllIncludeInProfileQuestions(Id currentEventId, Id partRecordId){
      partRecordIdProfileQuestions = ContactEventAnswerHelper.getProfileQuestionAnswers(new Set<Id>{currentEventId}, new Set<Id>{partRecordId});
  }
  private void prepareMeetingRequestWrappers(){
     categorySelectOptions = new List<SelectOption>{new SelectOption('All','All')};
     // selectedMeetingRequests = new List<MeetingRequestWrapper>();
      //availableMeetingRequests = new List<MeetingRequestWrapper>();
      prIdProfilesMap = new Map<Id,ProfileWrapper>();
      categoryQuestionId = new Set<Id>();
      //selectedOneOnOne = 0;
     // selectedBoardroom=0;



      for(Event_Account_Details__c partRecord: allPRsForOppositeRType.values()){
          //MeetingRequestWrapper meetingRequest;
          Contact_Event_Answers__c shortAnswer = ContactEventAnswerHelper.getAnswerByPRandQType(partRecord.Id, 'Short',  answersByPRAndQType);
          Contact_Event_Answers__c category = ContactEventAnswerHelper.getAnswerByPRandQType(partRecord.Id, 'Category',  answersByPRAndQType);
          categoryQuestionId.add(category.Question__r.Question__c);
          List<Contact_Event_Answers__c> profileQuestions = partRecordIdProfileQuestions.get(partRecord.Id);
          ProfileWrapper profile = new ProfileWrapper(partRecord, shortAnswer, category, profileQuestions);
          prIdProfilesMap.put(partRecord.Id,profile);
          //System.debug('Part Record Event Ranking: '+ partRecordIdEventRankingMap);
         /* if(partRecordIdEventRankingMap != null){
            if(partRecordIdEventRankingMap.containsKey(String.valueOf(currentPartRecord.Id)+String.valueOf(partRecord.Id))){
              meetingRequest = new MeetingRequestWrapper(profile,partRecordIdEventRankingMap.get(String.valueOf(currentPartRecord.Id)+String.valueOf(partRecord.Id)));
             // selectedMeetingRequests.add(meetingRequest);
              incrementCounts(meetingRequest);
            }else{
               meetingRequest = new MeetingRequestWrapper(profile,EventRankingHelper.createMeetingRequest(currentPartRecord, partRecord, currentEvent, currentContact));
               availableMeetingRequests.add(meetingRequest);
             }
          }  */
      }
      if(categoryQuestionId.size() > 0) categorySelectOptions.addAll(QuestionsHelper.getCategoryOptions(categoryQuestionId));
      //if(selectedMeetingRequests.size() > 0) selectedMeetingRequests.sort();
     // if(availableMeetingRequests.size() > 0) availableMeetingRequests.sort();
      System.debug('prIdProfilesMap' + prIdProfilesMap);
      System.debug('Select Options : '+ categorySelectOptions);
  }
}