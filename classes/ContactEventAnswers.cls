public class ContactEventAnswers{
    public void CreateExecutiveEventAnswers(){
       Date todaydate = Date.today().adddays(1);
       List<Product2> objProduct = [select id,Default_Executive_Event_Answers_Created__c,Registration_Part_1_Open_For_Executive__c from product2 where 
                                        Registration_Part_1_Open_For_Executive__c =: todaydate and Default_Executive_Event_Answers_Created__c= false];
       ContactEventAnswersOnConfirmations objConEvent = new ContactEventAnswersOnConfirmations();
       set<id> eventId = new set<id>();
       set<id> orderId = new set<id>();
       for(Product2 prod2 : objProduct){
           prod2.Default_Executive_Event_Answers_Created__c= true;
           eventId.add(prod2.id);
       }
       for(order ord : [select id,Status,Order_Event__c,Order_Event__r.Default_Executive_Event_Answers_Created__c,Opportunity.Account_Record_Type__c from Order 
                  where Order_Event__c IN : eventId and (Status = 'Preliminary' OR Status = 'Attending Event Contact') and Primary_Event_Contact__c = true 
                  and Opportunity.Account_Record_Type__c = 'Executive']){
           orderId.add(ord.id);
       }
       System.debug('#############orderIdorderId##3 '+orderId);
       if(orderId.size()>0)
           objConEvent.CreateExecutiveEventAnswers(orderId);
       CreateSupplierEventAnswers();
       
        if(objProduct.size()>0)
           update objProduct;
    }
    
    public void CreateSupplierEventAnswers(){
       Date todaydate = Date.today().adddays(1);
       List<Product2> objProduct = [select id,Registration_Part_1_Open_For_Supplier__c,Default_Supplier_Event_Answers_Created__c,Registration_Part_1_Open_For_Executive__c from product2 where 
                                        Registration_Part_1_Open_For_Supplier__c =: todaydate and Default_Supplier_Event_Answers_Created__c= false];
       ContactEventAnswersOnConfirmations objConEvent = new ContactEventAnswersOnConfirmations();
       set<id> eventId = new set<id>();
       set<id> orderId = new set<id>();
       for(Product2 prod2 : objProduct){
           prod2.Default_Supplier_Event_Answers_Created__c= true;
           eventId.add(prod2.id);
       }
       for(order ord  :[select id,Primary_Event_Contact__c,Company_Display_Name__c,Event_Account_Detail__c,Order_Event__c,Order_Event__r.Family,Order_Event__r.Parent_Event__c,Status,BillToContactId from Order 
                    where Order_Event__c IN : eventId  and (Status = 'Preliminary' OR Status = 'Attending Event Contact') and Primary_Event_Contact__c = true and Opportunity.Account_Record_Type__c = 'Supplier']){       
           orderId.add(ord.id);
       }
       
       if(orderId.size()>0)
           objConEvent.CreateSupplierEventAnswers(orderId);  
       
       if(objProduct.size() > 0)   
           update objProduct;          
    }
    
    public void CreateEventAnswers(Map<Id,Order> objMapOrder){
        Set<Id> objExecutiveProId = new Set<Id>();
        Set<Id> objSupplierProId = new Set<Id>();
        List<Order> objOrderList = [select id,Status,Order_Event__c,Order_Event__r.Default_Executive_Event_Answers_Created__c,Order_Event__r.Default_Supplier_Event_Answers_Created__c,Opportunity.Account_Record_Type__c from Order 
                  where Id IN : objMapOrder.Keyset() and (Status = 'Preliminary' OR Status = 'Attending Event Contact') and Primary_Event_Contact__c = true];
        for(Order objOrder : objOrderList){
            if(objOrder.Opportunity.Account_Record_Type__c == 'Executive' && objOrder.Order_Event__r.Default_Executive_Event_Answers_Created__c == true)
                objExecutiveProId.add(objOrder.Id);
            else if(objOrder.Opportunity.Account_Record_Type__c == 'Supplier' && objOrder.Order_Event__r.Default_Supplier_Event_Answers_Created__c== true)
                objSupplierProId.add(objOrder.Id);
        }
        
        ContactEventAnswersOnConfirmations objConEvent = new ContactEventAnswersOnConfirmations();
        if(objExecutiveProId.size() > 0)
            objConEvent.CreateExecutiveEventAnswers(objExecutiveProId);       
     
        if(objSupplierProId.size() > 0)
            objConEvent.CreateSupplierEventAnswers(objSupplierProId);        
        
    }
    public void CreateUserUpdateLink(Map<Id,Order> objMapOrder){
        System.debug('@@@@@objMapOrder@@@@@@@@ '+objMapOrder);
        Blob cryptoKey = Blob.valueOf(system.label.cryptoKey);
        String ChangePassword = Label.ChangePassword;
        Set<Id> objOrderId = new Set<Id>();
        Set<Id> billToContactIds = new Set<Id>();
        Community_Settings__c commSettings = Community_Settings__c.getOrgDefaults();
        List<Order> objOrderListRecord = [select id,BillToContactId,Status,Order_Event__c,Order_Event__r.Default_Executive_Event_Answers_Created__c,Order_Event__r.Default_Supplier_Event_Answers_Created__c,
                                    Opportunity.Account_Record_Type__c from Order where Id IN : objMapOrder.keySet()];
        for(Order objOrder : objOrderListRecord){
            if(objOrder.Opportunity.Account_Record_Type__c == 'Executive'){
                objOrderId.add(objOrder.Id);
                billToContactIds.add(objOrder.BillToContactId);
            }
        }
        System.debug('@@@@@@@@ billToContactIds '+billToContactIds);
        if(billToContactIds.size() > 0){
          Map<Id,User> objUserMap = new Map<Id,User>();
          List<User> usersToCreate =new List<User>();
          List<User> objUserList = [Select id,ContactId,LastLoginDate from User where ContactId IN : billToContactIds];
          for(User objUser : objUserList){
              objUserMap.put(objUser.ContactId,objUser);
          }
          Map<Id,User> objUserMapExist = new Map<Id,User>();
          List<User> objUserListUser = [Select id,ContactId,LastLoginDate from User where ContactId IN : billToContactIds and LastLoginDate != null];
          for(User objUser : objUserListUser){
              objUserMapExist.put(objUser.ContactId,objUser);
          }
          
          Set<Id> objConUserExist = new Set<Id>();
          Map<Id,Contact> contactMap =new Map<Id,Contact>([SELECT Id, User_Create_Confirmation__c,Email, FirstName, LastName,Phone, AccountId, Account.Type,RecordTypeId FROM Contact Where Id IN :billToContactIds]);
          for(Contact objCon : contactMap.values()){
              if(objUserMap.KeySet().Contains(objCon.id) == false){
                  usersToCreate.add(UserHelper.CreateUser(commSettings.Community_User_Profile__c,
                                        (objCon.firstName != null)?objCon.firstName:null, 
                                         objCon.LastName, 
                                        (objCon.Phone != null)?objCon.Phone: null,
                                         objCon.Email,
                                         objCon.Id));
              }else if(objUserMapExist.KeySet().Contains(objCon.id) == true){
                  objConUserExist.add(objCon.id);
              }
          } 
          if(usersToCreate.size() > 0){
              try{
                  Database.insert(usersToCreate,false);
                 //insert usersToCreate;
                 System.debug('@@@@@@@@@@@@@@@@@############### '+usersToCreate);
                 
              }catch(Exception ex){
                  System.debug('@@@@@@@@@@@@@@@@@@ '+ex.getMessage());
              }
          }
          System.debug('@@@@objConUserExist@@@@@@@@@ '+objConUserExist);
          List<Order> objOrderList = [Select id,BillToContactId,Welcome_Email_Link__c,Welcome_Email_Link_Expiration__c,
                                      Order_Event__r.User_Link_Expiration_for_Executives__c from Order where Id IN : objOrderId];
                                      
          for(Order ord : objOrderList){
              if(ord.Order_Event__r.User_Link_Expiration_for_Executives__c != null){
                 ord.Welcome_Email_Link_Expiration__c = Date.today().adddays(Integer.valueOf(ord.Order_Event__r.User_Link_Expiration_for_Executives__c));
                 String LinkExpir='';
                 if(ord.Welcome_Email_Link_Expiration__c !=null)
                    LinkExpir = String.valueOf(ord.Welcome_Email_Link_Expiration__c).replace('-','');
                    
                  Blob tokenData = Blob.valueOf('{"contactId":"'+ord.BillToContactId+'","confirmationId":"'+ord.Id+'","token":"'+ LinkExpir+'"}');
                  Blob encryptedData = Crypto.encryptWithManagedIV('AES128',cryptoKey,tokenData);
                  String token = EncodingUtil.base64Encode(encryptedData);
                  token  = EncodingUtil.urlEncode(token, 'UTF-8');
                  ord.Welcome_Email_Link__c = ChangePassword+token;// ChangePassword+ord.BillToContactId+'&ConfirmationId='+ord.Id+'&Token='+LinkExpir;   
                  
              }
              System.debug('####ord.BillToContactId####### '+ord.BillToContactId);
              if(objConUserExist.Contains(ord.BillToContactId) == true){
                      String LoginUserPage = Label.LoginPage;
                      ord.Welcome_Email_Link_Expiration__c = null;
                      ord.Welcome_Email_Link__c = LoginUserPage;
              }
          }                           
         if(objOrderList.size() > 0){
             update objOrderList;
         }                             
        }
    }
    
    public Static void ContactUpdateBadgeSortOrder(List<Contact> objConList){
        Set<Id> objConId = new Set<Id>();
        Id conRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId(); 
        for(Contact objCon : objConList){
           if(objCon.RecordTypeId == conRecordTypeId){
                objConId.add(objCon.Id);
            }
        }
        if(objConId.size() > 0){
            List<Order> objOrderList = [Select id,BillToContactId,Event_Account_Detail__c,BillToContact.Badge_Sort_Order__c from Order where BillToContactId IN : objConId and Status != 'Cancelled' and Status != 'Preliminary'];
            if(objOrderList.size() > 0){
                Map<Id,Decimal> objEveIdBadgeValue = new Map<Id,Decimal>();
                Set<Id> objPRID = new Set<Id>();
                Set<Id> objConIdBill = new Set<Id>();
                for(Order objOrPr : objOrderList){
                    objPRID.add(objOrPr.Event_Account_Detail__c);
                    objEveIdBadgeValue.put(objOrPr.Event_Account_Detail__c,objOrPr.BillToContact.Badge_Sort_Order__c);
                    objConIdBill.add(objOrPr.BillToContactId);
                }
                if(objPRID.size() > 0){
                    List<Order> objOrderListUpdate = [Select id,Order_Event__r.End_Date__c,Badge_Order__c,Event_Account_Detail__c,BillToContact.Badge_Sort_Order__c from Order 
                                                        where BillToContactId IN : objConIdBill and Order_Event__r.End_Date__c >=today and Status != 'Cancelled' and Status != 'Preliminary'];
                    for(Order objOrd : objOrderListUpdate){
                        objOrd.Badge_Order__c = objEveIdBadgeValue.get(objOrd.Event_Account_Detail__c);
                    }
                    if(objOrderListUpdate.size() > 0){
                        update objOrderListUpdate;
                    }
                }
            }
        }
    }
    
    public static void updateConfo(List<Order> objOrderListInUpdate){
    
        System.debug('$$$$$$$$$$$$ 189$$$');
        Set<Id> objOrdId = new Set<Id>();
        for(Order objOrd : objOrderListInUpdate){
            objOrdId.add(objOrd.id);
        }
        List<Order> objOrderList = [Select Id,Status,BillToContact.RecordTypeId,BillToContact.Seniority__c from Order 
                                        where Id IN : objOrdId];
        if(objOrderList.size() > 0){
            Set<Id> objSetIdConCanEvent = new Set<Id>();
            Set<Id> objSetIdConAttEvnCon = new Set<Id>();
            Id conRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();   
            for(Order objOrd : objOrderList){
                if(objOrd.BillToContact.RecordTypeId == conRecordTypeId){
                    if(objOrd.Status == 'Cancelled' || objOrd.Status == 'Preliminary'){
                        objSetIdConCanEvent.add(objOrd.id);
                    }
                    if(objOrd.Status == 'Complete' || objOrd.Status == 'Attending Event Contact'){
                        objSetIdConAttEvnCon.add(objOrd.id);
                    }
                }
            }
            if(objSetIdConAttEvnCon.size() > 0){
                Map<Id,Decimal> objEveIdBadgeValue = new Map<Id,Decimal>();
                Set<Id> objPRID = new Set<Id>();
                Set<Id> objConId = new Set<Id>();
                List<Order> objOrderRelatedPR = [Select id,Event_Account_Detail__c,BillToContactId,BillToContact.Badge_Sort_Order__c from Order 
                                                    where Id IN : objSetIdConAttEvnCon and BillToContact.Seniority__c != null];
                for(Order objOrPr : objOrderRelatedPR){
                    objPRID.add(objOrPr.Event_Account_Detail__c);
                    objEveIdBadgeValue.put(objOrPr.Event_Account_Detail__c,objOrPr.BillToContact.Badge_Sort_Order__c);
                    objConId.add(objOrPr.BillToContactId);
                }
                if(objPRID.size() > 0){
                    List<Order> objOrderListUpdate = [Select id,Order_Event__r.End_Date__c,Badge_Order__c,Event_Account_Detail__c,BillToContact.Badge_Sort_Order__c from Order 
                                                        where BillToContactId IN : objConId and Order_Event__r.End_Date__c >=today];
                    for(Order objOrd : objOrderListUpdate){
                        objOrd.Badge_Order__c = objEveIdBadgeValue.get(objOrd.Event_Account_Detail__c);
                    }
                    if(objOrderListUpdate.size() > 0){
                        update objOrderListUpdate;
                    }
                }
            }
            
            if(objSetIdConCanEvent.size() > 0){
                Set<Id> objPRID = new Set<Id>();
                Set<Id> objConId = new Set<Id>();
                List<Order> objOrderRelatedPR = [Select id,Event_Account_Detail__c,BillToContactId from Order where Id IN : objSetIdConCanEvent];
                for(Order objOrPr : objOrderRelatedPR){
                    objPRID.add(objOrPr.Event_Account_Detail__c);
                    objConId.add(objOrPr.BillToContactId);
                }
                if(objPRID.size() > 0){
                    List<Order> objOrderListUpdate = [Select id,Order_Event__r.End_Date__c,Badge_Order__c,Event_Account_Detail__c from Order 
                        where BillToContactId IN : objConId and Order_Event__r.End_Date__c >=today];
                    for(Order objOr : objOrderListUpdate){
                        objOr.Badge_Order__c = null;
                    }
                    if(objOrderListUpdate.size() > 0){
                        update objOrderListUpdate;
                    }
                }
            }
        } 
    }
    
}