@isTest
private class SponsorshipController_Test {

    @isTest
    private static void test_SponsorImage_Instantiation() {
        TestDataGenerator.createInactiveTriggerSettings();
        Community_Settings__c commSett = TestDataGenerator.createCommSettings();
        
        Id execAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Account a = TestDataGenerator.createTestAccount(true, 'Test Account',execAccRtype);
        Id execConRtype =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Contact c = TestDataGenerator.createTestContact(true,'Test','Last Name','testAcc@account.com',a,execConRtype);

        Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        
        User execUser = TestDataGenerator.CreateUser(true,commUserProfId.Id,'Test','Supplier',null,'testExec@cpmg.com',c.Id);
        
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event Active',true,Date.today().addDays(-10),Date.today().addDays(-5),Date.today().addMonths(-2), Date.today().addMonths(-1));
        Event_Sponsorships__c eSponsor = TestDataGenerator.createEventSponsorship(true,'Test Sponsorship',prod,100000.00,false,true);

        Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Event Active Opp',Date.today().addMonths(-4),a,'Closed Won',prod,'Renewal');

        Id execPartRecordType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Suppliers').getRecordTypeId();
        Event_Account_Details__c partRecord = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c, execPartRecordType);

        Purchased_Sponsorship__c purcSponsor = TestDataGenerator.createPurchasedSponsorship(true,prod,partRecord,50000.00,eSponsor);

        Order conf = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod);
        List<Order> ordersJustCreated = new List<Order>{conf};

        Document d1 = TestDataGenerator.createDocument('test1','text/plain','test1');
        Document d2 = TestDataGenerator.createDocument('test2','text/plain','test2');
        Document d3 = TestDataGenerator.createDocument('test3','text/plain','test3');
        Document d4 = TestDataGenerator.createDocument('test4','text/plain','test4');

        Image__c img1 = TestDataGenerator.createImage(true,a,c,conf,prod,(String) d1.Id,partRecord,true,true,false);
        Image__c img2 = TestDataGenerator.createImage(true,a,c,conf,prod,(String) d2.Id,partRecord,true,false,false);
        //MAKE IMG3 the sponsorship image.
        Image__c img3 = TestDataGenerator.createImage(true,a,c,conf,prod,(String) d3.Id,partRecord,true,true,true);
        img3.Sponsorship_Image__c = true;
        update img3;
        Image__c img4= TestDataGenerator.createImage(true,a,c,conf,prod,(String) d4.Id,partRecord,true,false,true);

        Test.startTest();
            System.runAs(execUser){
                SponsorshipController sponsorController = new SponsorshipController();
                sponsorController.currentEvent = prod;
                List<SponsorshipController.ImageWrapper> sponsorImages = sponsorController.docIds;
            /*  ImageComponentController imgController = new ImageComponentController();
                imgController.imageSection = 'Logo';
                imgController.currentContact = c;
                imgController.currentEvent = prod;
                imgController.currentConfirmation = conf;
                imgController.userID = execUser.Id;
            //imgController.currentContact = c.Id;
        
                imgController.getCurrentUploadPhotos(); */
            }
        Test.stopTest();
    }
}