public without sharing class SObjectHelper implements Queueable {
	
	public string SobjType {get;set;}
	public List<SObject> sobjectsToUpdate {get;set;}

	public SObjectHelper(string SobjType, List<SObject> sobjectsToUpdate){
		this.SobjType = SobjType;
		this.sobjectsToUpdate = sobjectsToUpdate;
	}
	
	/* public static void updateRecords(List<User> usersToUpdate)
	{
		System.debug('Updating Users '+ usersToUpdate);
		Database.update(usersToUpdate,true);
	}

	public static void updateRecords(List<Contact> consToUpdate)
	{
		//System.debug('Contacts to Update: '+ consToUpdate);
		Database.update(consToUpdate,false);
	} */

	public void execute(QueueableContext context){
		System.debug('SobjectType: ' + SobjType);
		System.debug('SobjectsToUpdate: ' + sobjectsToUpdate);
		if(SobjType == 'Contact'){
			Database.update((List<Contact>) sobjectsToUpdate);
		}else if (SobjType == 'User'){
			Database.update((List<User>) sobjectsToUpdate);
		}
		
	}
}