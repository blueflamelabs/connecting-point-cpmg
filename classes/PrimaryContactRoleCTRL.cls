public class PrimaryContactRoleCTRL{
    public void PrimaryContactRoleDelete(List<Opportunity> oppListDelete){
        List<Id> objOppId = new List<Id>();
        for(Opportunity objOPP : oppListDelete){
            objOppId.add(objOPP.id);
        }
        List<OpportunityContactRole> objOppContactRoleDelete = [select id,OpportunityId from OpportunityContactRole where OpportunityId IN : objOppId];
        if(objOppContactRoleDelete != null && objOppContactRoleDelete.size() > 0)
            delete objOppContactRoleDelete;
    }
    public void PrimaryContactRoleCreate(List<Opportunity> oppList){
        List<OpportunityContactRole> list_opptyContactRolesCreate = new List<OpportunityContactRole>();
        for(Opportunity objOpp : oppList){
            if(objOpp.Primary_Contact__c != null){
                OpportunityContactRole objConRole = new OpportunityContactRole();
                objConRole.IsPrimary = true;
                if(objOpp.Account_Record_Type__c == 'Supplier')
                    objConRole.Role = 'Sales Rep';
                if(objOpp.Account_Record_Type__c == 'Executive')
                    objConRole.Role = 'Event Contact'; 
                objConRole.ContactId = objOpp.Primary_Contact__c;
                objConRole.OpportunityId = objOpp.id;
                list_opptyContactRolesCreate.add(objConRole);
            }
        }
        if(list_opptyContactRolesCreate.size() > 0)
            insert list_opptyContactRolesCreate;
    }
    public void PrimaryContactRoleUpdate(Map<Id,Opportunity> objOppMap){
        List<Opportunity> objOppConRolCreate = new List<Opportunity>();
        List<OpportunityContactRole> objUpdateOppConRole = new List<OpportunityContactRole>();
        List<OpportunityContactRole> objUpdateNotPrime = new List<OpportunityContactRole>();
        Map<Id,List<OpportunityContactRole>> objMapOppConId = new Map<Id,List<OpportunityContactRole>>();
        List<OpportunityContactRole> list_opptyContactRolesUpdate = [select Id,IsPrimary,Role,ContactId,OpportunityId from OpportunityContactRole where OpportunityId in :objOppMap.keySet() limit 50000];
        for(OpportunityContactRole objOppConRoleUpdate : list_opptyContactRolesUpdate){
           if(!objMapOppConId.keyset().contains(objOppConRoleUpdate.OpportunityId)){
                objMapOppConId.put(objOppConRoleUpdate.OpportunityId,new List<OpportunityContactRole>{objOppConRoleUpdate});
           }else{
               List<OpportunityContactRole> objoppCon = objMapOppConId.get(objOppConRoleUpdate.OpportunityId);
               objoppCon.add(objOppConRoleUpdate);
               objMapOppConId.put(objOppConRoleUpdate.OpportunityId,objoppCon);
           } 
        }
        for(Opportunity objOpp : objOppMap.values()){
            Boolean isConRoleexists = false;
            List<OpportunityContactRole> objConRole = objMapOppConId.get(objOpp.id);
            if(!objMapOppConId.keyset().contains(objOpp.id)){
                   objOppConRolCreate.add(objOpp);
            }else{
               
               if(objOpp.Primary_Contact__c == null)
                   objUpdateNotPrime.addAll(objConRole);
               for(OpportunityContactRole objOppContact : objConRole){
                   if(objOpp.Primary_Contact__c == objOppContact.ContactId){
                       isConRoleexists = true;
                       objOppContact.IsPrimary = true;
                       if(objOpp.Account_Record_Type__c == 'Supplier')
                            objOppContact.Role = 'Sales Rep';
                       if(objOpp.Account_Record_Type__c == 'Executive')
                            objOppContact.Role = 'Event Contact';
                       
                       objUpdateOppConRole.add(objOppContact);
                   }   
               }
               if(isConRoleexists == false)
                   objOppConRolCreate.add(objOpp);
            }
            if(objConRole !=null)
            for(OpportunityContactRole objOppContact : objConRole){
               if(((map<Id,opportunity>)trigger.oldmap).get(objOpp.id).Primary_Contact__c == objOppContact.ContactId){                       
                   objOppContact.role = 'Other';
                   objOppContact.IsPrimary = false;
                   objUpdateOppConRole.add(objOppContact);
               }        
           }   
                    
        }
        if(objUpdateNotPrime.size() > 0){
            for(OpportunityContactRole objConRolePrime : objUpdateNotPrime){
                objConRolePrime.IsPrimary = false;
            }
            update objUpdateNotPrime;
        }
        if(objUpdateOppConRole.size() > 0)
            update objUpdateOppConRole;
        if(objOppConRolCreate.size() > 0)
            PrimaryContactRoleCreate(objOppConRolCreate);
    }
}