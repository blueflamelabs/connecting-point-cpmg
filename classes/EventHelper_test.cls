@isTest
private class EventHelper_test {
    
    @isTest static void test_method_one() {
        test.startTest();

        Product2 testProduct = new Product2(Name='testProduct',User_Link_Expiration_for_Executives__c = 1, User_Link_Expiration_for_Suppliers__c = 1);

        insert testProduct;

        Product2 p = EventHelper.getEvent(testProduct.Id);

        System.assertEquals(p.Id, testProduct.Id);

        test.stopTest();
    }
    
    @isTest static void test_method_two() {
        test.startTest();

        Product2 closestByDate = new Product2(Name='ClosestDateTest', User_Link_Expiration_for_Executives__c = 1, User_Link_Expiration_for_Suppliers__c = 1 , Start_Date__c = Datetime.now().addDays(2).date());
        Product2 testProduct2 = new Product2(Name='SecondClosestDateTest', User_Link_Expiration_for_Executives__c = 1, User_Link_Expiration_for_Suppliers__c = 1 ,Start_Date__c = Datetime.now().addDays(3).date());
        Product2 testProduct3 = new Product2(Name='ThirdClosestDateTest', User_Link_Expiration_for_Executives__c = 1, User_Link_Expiration_for_Suppliers__c = 1 , Start_Date__c = Datetime.now().addDays(4).date());
        List<Product2> events = new List<Product2>();

        events.add(closestByDate);
        events.add(testProduct2);
        events.add(testProduct3);


        insert events;

        Product2 p = EventHelper.getClosestEventByDate();

        System.assertEquals(p.Id, closestByDate.Id);

        test.stopTest();
    }


    @isTest static void test_method_three(){
        test.startTest();

        Product2 testProduct = new Product2(Name='testProduct', User_Link_Expiration_for_Executives__c = 1, User_Link_Expiration_for_Suppliers__c = 1 , Start_Date__c = Datetime.now().addDays(2).date(), End_Date__c = Datetime.now().addDays(12).date());
        Product2 testProduct2 = new Product2(Name='testProduct2', User_Link_Expiration_for_Executives__c = 1, User_Link_Expiration_for_Suppliers__c = 1 , Start_Date__c = Datetime.now().addDays(3).date(), End_Date__c = Datetime.now().addDays(13).date());
        Product2 testProduct3 = new Product2(Name='testProduct3', User_Link_Expiration_for_Executives__c = 1, User_Link_Expiration_for_Suppliers__c = 1 , Start_Date__c = Datetime.now().addDays(4).date(), End_Date__c = Datetime.now().addDays(14).date());

        List<Product2> events = new List<Product2>();

        events.add(testProduct);
        events.add(testProduct2);
        events.add(testProduct3);

        insert events;

        Product2 testProduct4 = new Product2(Name='ProductThatShouldNotBeSelected', User_Link_Expiration_for_Executives__c = 1, User_Link_Expiration_for_Suppliers__c = 1);

        insert testProduct4;

        Set<Id> eventIds = new Set<Id>();

        for(Integer i = 0; i < events.size(); i++){
            eventIds.add(events[i].Id);
        }


        List<Product2> pList = EventHelper.getAllCurrentEventsForCommunity(eventIds);

        System.assertEquals(pList.size(), eventIds.size());

        test.stopTest();
    }

    @isTest static void getEventMap_Test(){
        test.startTest();
        Product2 testProduct = new Product2(Name='testProduct',User_Link_Expiration_for_Executives__c = 1, User_Link_Expiration_for_Suppliers__c = 1 , Start_Date__c = Datetime.now().addDays(2).date(), End_Date__c = Datetime.now().addDays(12).date());
        Product2 testProduct2 = new Product2(Name='testProduct2', User_Link_Expiration_for_Executives__c = 1, User_Link_Expiration_for_Suppliers__c = 1 , Start_Date__c = Datetime.now().addDays(3).date(), End_Date__c = Datetime.now().addDays(13).date());
        Product2 testProduct3 = new Product2(Name='testProduct3', User_Link_Expiration_for_Executives__c = 1, User_Link_Expiration_for_Suppliers__c = 1 , Start_Date__c = Datetime.now().addDays(4).date(), End_Date__c = Datetime.now().addDays(14).date());

        List<Product2> events = new List<Product2>();

        events.add(testProduct);
        events.add(testProduct2);
        events.add(testProduct3);

        insert events;

        Product2 testProduct4 = new Product2(Name='ProductThatShouldNotBeSelected' , User_Link_Expiration_for_Executives__c = 1, User_Link_Expiration_for_Suppliers__c = 1);

        insert testProduct4;

        Set<Id> eventIds = new Set<Id>();

        for(Integer i = 0; i < events.size(); i++){
            eventIds.add(events[i].Id);
        }


        Map<Id,Product2> pList = EventHelper.getEventMap(eventIds);

        System.assertEquals(pList.size(), eventIds.size());

        test.stopTest();
    }

    @isTest static void getPreviousEvents_Test(){
        test.startTest();

        Product2 testProduct = new Product2(Name='testProduct', User_Link_Expiration_for_Executives__c = 1, User_Link_Expiration_for_Suppliers__c = 1 , Start_Date__c = Datetime.now().addDays(-12).date(), End_Date__c = Datetime.now().addDays(-2).date());
        Product2 testProduct2 = new Product2(Name='testProduct2', User_Link_Expiration_for_Executives__c = 1, User_Link_Expiration_for_Suppliers__c = 1 , Start_Date__c = Datetime.now().addDays(-13).date(), End_Date__c = Datetime.now().addDays(-3).date());
        Product2 testProduct3 = new Product2(Name='testProduct3', User_Link_Expiration_for_Executives__c = 1, User_Link_Expiration_for_Suppliers__c = 1 , Start_Date__c = Datetime.now().addDays(-14).date(), End_Date__c = Datetime.now().addDays(-4).date());

        List<Product2> events = new List<Product2>();

        events.add(testProduct);
        events.add(testProduct2);
        events.add(testProduct3);

        insert events;

        Product2 testProduct4 = new Product2(Name='ProductThatShouldNotBeSelected', User_Link_Expiration_for_Executives__c = 1, User_Link_Expiration_for_Suppliers__c = 1);

        insert testProduct4;

        Set<Id> eventIds = new Set<Id>();

        for(Integer i = 0; i < events.size(); i++){
            eventIds.add(events[i].Id);
        }


        List<Product2> pList = EventHelper.getAllPreviousEventsForCommunity(eventIds);

        System.assertEquals(pList.size(), eventIds.size());

        test.stopTest();
    }
    
}