public without sharing class UserPermissions {
	public Boolean isSupplier { get; set; }
	public Boolean isExecutive { get; set; }
	public Boolean isAccountOwner { get; set; }
	public Boolean canEditAccountFields { get; set; }
	public Boolean canEditContactFields { get; set; }
	public Boolean canManageContacts { get; set; }

	public Boolean part1IsOpen { get; set; }
	public Boolean part1IsReadOnly { get; set; }

	public Boolean part2IsOpen { get; set; }
	public Boolean part2IsReadOnly { get; set; }

	public Boolean isReadOnly { get; set; }
	public EventStaffDto eventStaff { get; set; }
	public List<EventStaffDto> eventStaffList { get; set; }
	
	public UserPermissions() {
		isReadOnly = false;

		isSupplier = false;
		isExecutive = false;
		isAccountOwner = false;

		canEditAccountFields = false;
		canEditContactFields = false;

		canManageContacts = false;
		//instantiate Part1 and Part2 readOnly as false;
		part1IsReadOnly = false;
		part2IsReadOnly = false;
	}

	public UserPermissions(String p_recordType, Boolean p_isAccountOwner) {
		isReadOnly = false;
		// 'Supplier'
		// 'Executive'
		isSupplier = (p_recordType == 'Supplier');
		isExecutive = (p_recordType == 'Executive');
		isAccountOwner = p_isAccountOwner;

		canEditAccountFields = (isSupplier && isAccountOwner) || isExecutive;
		canEditContactFields = true;

		canManageContacts = (isSupplier && isAccountOwner);
	}

	public Folder getPublicPhotoFolder(String p_publicFolderName) {
		Folder publicFolderResult;
		List<Folder> publicFolders = [SELECT Id, Name FROM Folder Where Name =:p_publicFolderName];
			if(publicFolders.size() != 0){
				publicFolderResult = publicFolders[0];
			}

			return publicFolderResult;
	}
	public void getEventStaff(Id p_eventId) {
		System.debug('Get Event Staff Called:' );
		if (eventStaff == NULL) {
			String defaultStaffRole = 'Salesperson';
			if (isExecutive) {
			defaultStaffRole = 'Recruiter';
			} else if (isSupplier) {
			//			- If Executive get record from event staff object where display to Executive is true and the role is Recruiter
			//- If Supplier get record from event staff object where display to Supplier is true and the role is Support
			defaultStaffRole = 'Support';
			}
			System.debug('THE TYPE OF CONTACT IS:  '+ defaultStaffRole);

			List<Event_Staff__c> eventStaffRecords = [SELECT Id, User__c, User__r.Email,User__r.Name, User__r.FirstName, User__r.LastName FROM Event_Staff__c WHERE CPMG_Event__c = :p_eventId AND Role__c =:defaultStaffRole LIMIT 1];
			if (!eventStaffRecords.isEmpty()) {
			Event_Staff__c eventStaffR	= eventStaffRecords.get(0);
			eventStaff = new EventStaffDto(eventStaffR.User__r.Email, eventStaffR.User__r.FirstName+' '+eventStaffR.User__r.LastName);
			}
		}

		if (eventStaffList == NULL) {
			eventStaffList = new List<EventStaffDto>();
			List<Event_Staff__c> eventStaffRecords = [SELECT Id, User__c, User__r.Email, User__r.Name, User__r.Title, User__r.Phone,	User__r.FirstName, User__r.LastName FROM Event_Staff__c WHERE CPMG_Event__c = :p_eventId];
			if (!eventStaffRecords.isEmpty()) {
			for (Event_Staff__c eventStaffR : eventStaffRecords) {
				EventStaffDto eventStaffD = new EventStaffDto(eventStaffR.User__r.Email, eventStaffR.User__r.FirstName+' '+ eventStaffR.User__r.LastName);
				eventStaffD.staffTitle = eventStaffR.User__r.Title;
				eventStaffD.staffPhone = eventStaffR.User__r.Phone;
				eventStaffList.add(eventStaffD);
			}

			}
		}
		System.debug('Event Staff: '+ eventStaff);
		System.debug('Event Staff List: '+ eventStaffList);
	}
	}