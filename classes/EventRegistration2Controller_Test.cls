/*
*Created By     : 
*Created Date   : 22/07/2019 
*Description    : 
*Class Name     : CampaignController
Modification History :
============================================================================================
V.No    Date            By                  Description
============================================================================================
*/
@isTest
private class EventRegistration2Controller_Test {

    @isTest
    private static void registrationController_Instantiation_Supplier_attendee() {
        TestDataGenerator.createInactiveTriggerSettings();
        Community_Settings__c commSett = TestDataGenerator.createCommSettings();

        //AccountFormSettings
        TestDataGenerator.createAccountFormSettings();

        //Contact Form Settings:
        TestDataGenerator.createContactFormSettings();

        //Order Form Display settings
        TestDataGenerator.createOrderFormSettings();

        //Registration Settings
        TestDataGenerator.createRegistrationSettings();

        //Need the Order Status Custom Setting
        TestDataGenerator.createOrderStatusCustomSetting();

        TestDataGenerator.createOtheContactFormSettings();
        
        Id execAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Account a = TestDataGenerator.createTestAccount(false, 'Test Account',execAccRtype);
        a.Phone='123-555-5555';
        insert a;
        Id execConRtype =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Contact c = TestDataGenerator.createTestContact(false,'Test','Last Name','testAcc@account.com',a,execConRtype);
        c.Phone = '603-555-1234';
        c.No_Longer_at_Company__c = true;
        insert c;

        Contact potentialAttendee = TestDataGenerator.createTestContact(true,'Test Attendee','Last Name','testAttendee@account.com',a,execConRtype);

        Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        
        User execCommUser = TestDataGenerator.CreateUser(true,commUserProfId.Id,'Test','Exec',null,'testExecUser@cpmg.com',c.Id);
        
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event Active',true,Date.today().addDays(40),Date.today().addDays(45),Date.today().addMonths(-2), Date.today().addMonths(-1));

        Profile standUserProfId = [SELECT ID FROM Profile where Name ='Standard User' LIMIT 1];
        
        User execUser = TestDataGenerator.CreateUser(true,standUserProfId.Id,'Test','Exec',null,'testExec1234@cpmg.com',null);
        User supplierUser = TestDataGenerator.CreateUser(true, standUserProfId.Id,'Test','Supplier',null,'testSupplier1234@cpmg.com',null);

        Event_Staff__c execStaff = TestDataGenerator.createEventStaff(true,prod,execUser,'Recruiter');
        Event_Staff__c supplierStaff = TestDataGenerator.createEventStaff(true,prod,supplierUser,'Support');

        User salespersonUser = TestDataGenerator.CreateUser(true, standUserProfId.Id,'Test','Supplier',null,'testsalespersonUser1234@cpmg.com',null);
        Event_Staff__c salespersonStaff = TestDataGenerator.createEventStaff(true,prod,supplierUser,'Salesperson');

        //Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Event Active Opp',Date.today().addMonths(-4),a,'Closed Won',prod);
        Opportunity opp = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),a,'Closed Won',prod,'Onsite Renewal');
        
        //Opportunity opp1 = TestDataGenerator.createTestOpp(true,'Test Event Active Opp',Date.today().addMonths(-4),a,'Closed Won',prod);
        Opportunity opp1 = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),a,'Closed Won',prod,'Onsite Renewal');
        
        Id execPartRecordType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Suppliers').getRecordTypeId();
        Event_Account_Details__c partRecord = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c, execPartRecordType);

        Order conf = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod,true);
        conf.Grant_Edit_Permission__c = true;
        update conf;
       
        Order conf2 = TestDataGenerator.createConfirmation(true,a,potentialAttendee ,'Attending Event Contact',opp1,opp1.CloseDate,partRecord,prod,true);
        //Order conf1 = TestDataGenerator.createConfirmation(true,a,c,'Cancelled',opp,opp.CloseDate,partRecord,prod,false);
        List<Order> ordersJustCreated = new List<Order>{conf};

        Document d1 = TestDataGenerator.createDocument('test1','text/plain','test1');
        Document d2 = TestDataGenerator.createDocument('test2','text/plain','test2');
        Document d3 = TestDataGenerator.createDocument('test3','text/plain','test3');
        Document d4 = TestDataGenerator.createDocument('test4','text/plain','test4');

        Image__c img1 = TestDataGenerator.createImage(true,a,c,conf,prod,(String) d1.Id,partRecord,true,false,true);
        Image__c img2 = TestDataGenerator.createImage(true,a,c,conf,prod,(String) d2.Id,partRecord,true,false,true);
        Image__c img3 = TestDataGenerator.createImage(true,a,c,conf,prod,(String) d3.Id,partRecord,true,false,true);
        Image__c img4= TestDataGenerator.createImage(true,a,c,conf,prod,(String) d4.Id,partRecord,true,false,true);

        Map<String,RecordTypeInfo> questionRTypeMap = Schema.SObjectType.Question__c.getRecordTypeInfosByName();
        Question__c picklistQuestion = TestDataGenerator.createQuestion(true,'Test Pickilist',200,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),false,false);
            Question_Answers_Values__c qav1Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option','Select');
            Question_Answers_Values__c qav2Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option2','Select2');
            Question_Answers_Values__c qav3Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option3','Select3');
        Question__c mulitPicklistQuestion = TestDataGenerator.createQuestion(true,'Test Multi Pickilist',200,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
            Question_Answers_Values__c qav4Picklist = TestDataGenerator.createQuestionOption(true,mulitPicklistQuestion,'Select Option4','Select4');
            Question_Answers_Values__c qav5Picklist = TestDataGenerator.createQuestionOption(true,mulitPicklistQuestion,'Select Option5','Select5');
            Question_Answers_Values__c qav6Picklist = TestDataGenerator.createQuestionOption(true,mulitPicklistQuestion,'Select Option6','Select6');
        //Supplier
        Question__c supplierQuestion = TestDataGenerator.createQuestion(true,'Test Supplier Question',2000,false,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);

        //Executive Golf
        Question__c execActivityQuestion = TestDataGenerator.createQuestion(true,'Test Golf Pickilist',200,true,'Activity',questionRTypeMap.get('Question').getRecordTypeId(),false,true);
        
        //Create Event Questions
        Event_Question__c eQuestion = TestDataGenerator.createEventQuestion(true,'The Picklist Event Quest',prod,'Category',mulitPicklistQuestion,true,null,null);

        PageReference pageRef = Page.EventRegistration2;
        pageRef.getParameters().put('eId', prod.Id);
        pageRef.getParameters().put('oId', conf.Id);
        pageRef.getParameters().put('p1', 'test');


        Test.startTest();
            System.runAs(execCommUser){
                Test.setCurrentPageReference(pageRef);
                EventRegistration2Controller eRegController = new EventRegistration2Controller();
                eRegController.currentContact = c;
                
                String anchorPart = eRegController.anchorPart;
                String subAnchorPart = eRegController.subAnchorPart;
                String siteURL = eRegController.getSiteURL();
                UserPermissions uPermissions = eRegController.permissions;
                Boolean displayContact = eRegController.displayEditContactModal;
                //Boolean displayAccount = eRegController.displayProfileAnswersModal;
                Boolean hasErrors = eRegController.pageHasErrors;

                eRegController.getContactModal(potentialAttendee.Id);
                eRegController.getOrderModal(c.Id);
                eRegController.saveAsDraft();
                eRegController.goBack();
                Test.setCurrentPageReference(pageRef);
                eRegController.goToEvent();
                Test.setCurrentPageReference(pageRef);
                eRegController.submit();
                eRegController.saveAllComponents();
                eRegController.saveAndSubmit();
                eRegController.saveAndSubmitQuestionAns();
                eRegController.verifyPart1Reg();
                eRegController.sendMailToUser();
                eRegController.getThis();
                eRegController.updateContactList();

                /*Methods Not Called*/
                eRegController.saveAllImageComponents();
                eRegController.validatePhoneNumbers();
                eRegController.contactModalId = potentialAttendee.Id;
                eRegController.contactModal = c;
                eRegController.orderModal = conf;
                eRegController.selectedAccountRegistrantAvailableOption  = potentialAttendee.Id;
                 
                //eRegController.saveEditContactModal();
                eRegController.openEditContactModal();
                eRegController.contactModalId = conf.Id;
                eRegController.removeEditContactModal();
                EventRegistration2Controller.getContactById1(c.Id,prod.Id,a.Id,partRecord.Id);
                EventRegistration2Controller.getOrderByContactId(c.Id,prod.Id,a.Id,partRecord.Id);
                EventRegistration2Controller.getContactById(potentialAttendee.Id);
                EventRegistration2Controller.getContactModalReadOnlyFields();

                Boolean conExists = EventRegistration2Controller.checkNewContactEmail((String) c.email);
                System.assertEquals(true, conExists);
                System.assertEquals(false,EventRegistration2Controller.checkNewContactEmail('testNoEmail@test.com'));
                
            }
        Test.stopTest();
        }

        @isTest
    private static void registrationController_Instantiation_Supplier_attendee2() {
        TestDataGenerator.createInactiveTriggerSettings();
        Community_Settings__c commSett = TestDataGenerator.createCommSettings();

        //AccountFormSettings
        TestDataGenerator.createAccountFormSettings();

        //Contact Form Settings:
        TestDataGenerator.createContactFormSettings();

        //Order Form Display settings
        TestDataGenerator.createOrderFormSettings();

        //Registration Settings
        TestDataGenerator.createRegistrationSettings();

        //Need the Order Status Custom Setting
        TestDataGenerator.createOrderStatusCustomSetting();

        TestDataGenerator.createOtheContactFormSettings();
        
        Id execAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Account a = TestDataGenerator.createTestAccount(false, 'Test Account',execAccRtype);
        a.Phone='123-555-5555';
        insert a;
        Id execConRtype =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Contact c = TestDataGenerator.createTestContact(false,'Test','Last Name','testAcc@account.com',a,execConRtype);
        c.Phone = '603-555-1234';
        insert c;

        Contact potentialAttendee = TestDataGenerator.createTestContact(true,'Test Attendee','Last Name','testAttendee@account.com',a,execConRtype);

        Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        
        User execCommUser = TestDataGenerator.CreateUser(true,commUserProfId.Id,'Test','Exec',null,'testExecUser@cpmg.com',c.Id);
        
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event Active',true,Date.today().addDays(40),Date.today().addDays(45),Date.today().addMonths(-2), Date.today().addMonths(-1));

        Profile standUserProfId = [SELECT ID FROM Profile where Name ='Standard User' LIMIT 1];
        
        User execUser = TestDataGenerator.CreateUser(true,standUserProfId.Id,'Test','Exec',null,'testExec1234@cpmg.com',null);
        User supplierUser = TestDataGenerator.CreateUser(true, standUserProfId.Id,'Test','Supplier',null,'testSupplier1234@cpmg.com',null);

        Event_Staff__c execStaff = TestDataGenerator.createEventStaff(true,prod,execUser,'Recruiter');
        Event_Staff__c supplierStaff = TestDataGenerator.createEventStaff(true,prod,supplierUser,'Support');

        User salespersonUser = TestDataGenerator.CreateUser(true, standUserProfId.Id,'Test','Supplier',null,'testsalespersonUser1234@cpmg.com',null);
        Event_Staff__c salespersonStaff = TestDataGenerator.createEventStaff(true,prod,supplierUser,'Salesperson');

        //Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Event Active Opp',Date.today().addMonths(-4),a,'Closed Won',prod);
        Opportunity opp = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),a,'Closed Won',prod,'Onsite Renewal');
        
        Id execPartRecordType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Suppliers').getRecordTypeId();
        Event_Account_Details__c partRecord = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c, execPartRecordType);

        Order conf = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod,true);
        //Order conf1 = TestDataGenerator.createConfirmation(true,a,c,'Cancelled',opp,opp.CloseDate,partRecord,prod,false);
        List<Order> ordersJustCreated = new List<Order>{conf};

        Document d1 = TestDataGenerator.createDocument('test1','text/plain','test1');
        Document d2 = TestDataGenerator.createDocument('test2','text/plain','test2');
        Document d3 = TestDataGenerator.createDocument('test3','text/plain','test3');
        Document d4 = TestDataGenerator.createDocument('test4','text/plain','test4');

        Image__c img1 = TestDataGenerator.createImage(true,a,c,conf,prod,(String) d1.Id,partRecord,true,false,true);
        Image__c img2 = TestDataGenerator.createImage(true,a,c,conf,prod,(String) d2.Id,partRecord,true,false,true);
        Image__c img3 = TestDataGenerator.createImage(true,a,c,conf,prod,(String) d3.Id,partRecord,true,false,true);
        Image__c img4= TestDataGenerator.createImage(true,a,c,conf,prod,(String) d4.Id,partRecord,true,false,true);

        Map<String,RecordTypeInfo> questionRTypeMap = Schema.SObjectType.Question__c.getRecordTypeInfosByName();
        Question__c picklistQuestion = TestDataGenerator.createQuestion(true,'Test Pickilist',200,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),false,false);
            Question_Answers_Values__c qav1Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option','Select');
            Question_Answers_Values__c qav2Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option2','Select2');
            Question_Answers_Values__c qav3Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option3','Select3');
        Question__c mulitPicklistQuestion = TestDataGenerator.createQuestion(true,'Test Multi Pickilist',200,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
            Question_Answers_Values__c qav4Picklist = TestDataGenerator.createQuestionOption(true,mulitPicklistQuestion,'Select Option4','Select4');
            Question_Answers_Values__c qav5Picklist = TestDataGenerator.createQuestionOption(true,mulitPicklistQuestion,'Select Option5','Select5');
            Question_Answers_Values__c qav6Picklist = TestDataGenerator.createQuestionOption(true,mulitPicklistQuestion,'Select Option6','Select6');
        //Supplier
        Question__c supplierQuestion = TestDataGenerator.createQuestion(true,'Test Supplier Question',2000,false,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);

        //Executive Golf
        Question__c execActivityQuestion = TestDataGenerator.createQuestion(true,'Test Golf Pickilist',200,true,'Activity',questionRTypeMap.get('Question').getRecordTypeId(),false,true);
        
        //Create Event Questions
        Event_Question__c eQuestion = TestDataGenerator.createEventQuestion(true,'The Picklist Event Quest',prod,'Category',mulitPicklistQuestion,true,null,null);

        PageReference pageRef = Page.EventRegistration2;
        pageRef.getParameters().put('eId', prod.Id);
       // pageRef.getParameters().put('oId', conf.Id);
        pageRef.getParameters().put('section', 'test');
        pageRef.getParameters().put('p2', 'test');


        Test.startTest();
            System.runAs(execCommUser){
                Test.setCurrentPageReference(pageRef);
                EventRegistration2Controller eRegController = new EventRegistration2Controller();
                String anchorPart = eRegController.anchorPart;
                String subAnchorPart = eRegController.subAnchorPart;
                String siteURL = eRegController.getSiteURL();
                UserPermissions uPermissions = eRegController.permissions;
                Boolean displayContact = eRegController.displayEditContactModal;
                //Boolean displayAccount = eRegController.displayProfileAnswersModal;
                Boolean hasErrors = eRegController.pageHasErrors;

                eRegController.getContactModal(potentialAttendee.Id);
                eRegController.getOrderModal(c.Id);
                eRegController.saveAsDraft();
                eRegController.goBack();
                Test.setCurrentPageReference(pageRef);
                eRegController.goToEvent();
                Test.setCurrentPageReference(pageRef);
                eRegController.submit();
                eRegController.saveAllComponents();
                eRegController.saveAndSubmit();
                eRegController.saveAndSubmitQuestionAns();
                eRegController.updateContactList();

                /*Methods Not Called*/
                eRegController.saveAllImageComponents();
                eRegController.validatePhoneNumbers();
                eRegController.contactModalId = potentialAttendee.Id;
                eRegController.contactModal = new Contact(FirstName='Test New Attendee', LastName ='New Attendee Now', email ='newattendeenow@supplieraccount.com',Title='New Test Title');

                //Opportunity opp2 = TestDataGenerator.createTestOpp(true,'Test Event Active Opp',Date.today().addMonths(-4),a,'Closed Won',prod);
                Opportunity opp2 = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),a,'Closed Won',prod,'Onsite Renewal');
                eRegController.orderModal = new Order(AccountId =a.Id ,BillToContactId =eRegController.contactModal.Id ,Status ='Attending Event Contact' ,OpportunityId = opp2.Id,effectiveDate = opp2.CloseDate,Event_Account_Detail__c = partRecord.Id ,Order_Event__c = prod.Id ,Primary_Event_Contact__c = true,Grant_Edit_Permission__c = true);
                // eRegController.selectedAccountRegistrantAvailableOption  = potentialAttendee.Id;
                eRegController.selectedAccountRegistrantAvailableOption  = null;

                eRegController.saveEditContactModal();
                eRegController.openEditContactModal();
                eRegController.contactModalId = null;
                eRegController.removeEditContactModal();

                EventRegistration2Controller.getContactById(potentialAttendee.Id);
                EventRegistration2Controller.getContactModalReadOnlyFields();

                Boolean conExists = EventRegistration2Controller.checkNewContactEmail((String) c.email);
                System.assertEquals(true, conExists);
                System.assertEquals(false,EventRegistration2Controller.checkNewContactEmail('testNoEmail@test.com'));
                
            }
        Test.stopTest();
        }

}