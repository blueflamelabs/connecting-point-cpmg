@isTest
private class SObjectHelper_Test {
	
	@TestSetup
	private static void createTestData(){
		Id accSupplierRType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
		Id accExecRType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
		Id conSupplierRType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
		Id conExecRType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
		
		Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];

		Account a = TestDataGenerator.createTestAccount(true,'Test Account',accSupplierRType);
		Account a2 = TestDataGenerator.createTestAccount(true,'Test Account2',accExecRType);
		List<Contact> testCons = new List<Contact>();
		for(integer x =0; x < 200; ++x){
			Account conAcc = Math.mod(x,2) == 0?a:a2;
			Id conRType = Math.mod(x,2) == 0 ?conSupplierRType:conExecRType;
			testCons.add(TestDataGenerator.createTestContact(false,'First'+String.valueOf(x),'Last'+String.valueOf(x),'first.last'+String.valueOf(x)+'@testaccountSObjectHelper.com',conAcc,conRType));
		}
		
		insert testCons;
		List<User> users = new List<User>();
		for(Contact c : testCons)
		{
			users.add(TestDataGenerator.CreateUser(false,commUserProfId.Id,c.FirstName,c.LastName,c.Phone, c.Email,c.Id));

		}

		insert users;		
	}

	@isTest
	private static void execute_test_User_PB_Test() {
		//Get all test Users, and change their username

		//Set<Id> conIds = new Set<Id>();
		Map<Id,User> contactIDUserMap = new Map<Id,User>();
		integer count = 0;
		for(User u : [SELECT Id, UserName, ContactId, Contact.COmmunity_User_Name__c FROM User where ContactId <> null and userName like '%@testaccountSObjectHelper.com']){
			//System.assertEquals(u.UserName,u.Contact.Community_User_Name__c);
			
			u.put('Username',u.Username + String.valueOf(count));
			contactIDUserMap.put(u.ContactId,u);
			++count;
			
			//conIds.add(u.ContactId);
		}
		Test.startTest();
			update contactIDUserMap.values();
		Test.stopTest();
		for(Contact c : [SELECT Id, Community_User_Name__c FROM Contact where Id IN :contactIDUserMap.keySet()])
		{
			//System.assertEquals(contactIDUserMap.get(c.Id).Username, c.Community_User_Name__c);
		}


	}

	@isTest
	private static void execute_test_Contact_PB_Test() {
		//Get all test Users, and change their username

		integer count = 0;
		Map<Id,Contact> contactMap = new Map<Id,Contact>();
		
		
		for(Contact c : [SELECT Id, Community_User_Name__c, email FROM Contact where CreatedDate = Today and email like '%@testaccountSObjectHelper.com'])
		{
			c.put('Email',String.valueOf(count)+ c.Email);
			contactMap.put(c.Id,c);
			++count;
		}

		Test.startTest();
			update contactMap.values();
		Test.stopTest();

		for(User u: [SELECT Id,UserName,Email, ContactId FROM User where ContactID in : contactMap.keySet()])
		{
		//	System.assertEquals(contactMap.get(u.ContactId).Email, u.Email);
		//	System.assertEquals(contactMap.get(u.ContactId).Email, u.Username);
		}
	}

	@isTest
	private static void SObjectHelper_execute_Class_Queuable_Contact(){
		integer count = 0;
		Map<Id,Contact> contactMap = new Map<Id,Contact>();
		
		
		for(Contact c : [SELECT Id, Community_User_Name__c, email FROM Contact where CreatedDate = Today and email like '%@testaccountSObjectHelper.com'])
		{
			c.put('Email',String.valueOf(count)+ c.Email);
			contactMap.put(c.Id,c);
			++count;
		}

		Test.startTest();
			SObjectHelper sobHelper = new SObjectHelper('Contact', contactMap.values());
			Id jobId = System.enqueueJob(sobHelper);
		Test.stopTest();
	}

	@isTest
	private static void SObjectHelper_execute_Class_Queuable_User(){
		Map<Id,User> contactIDUserMap = new Map<Id,User>();
		integer count = 0;
		for(User u : [SELECT Id, UserName, ContactId, Contact.COmmunity_User_Name__c FROM User where ContactId <> null and userName like '%@testaccountSObjectHelper.com']){
			//System.assertEquals(u.UserName,u.Contact.Community_User_Name__c);
			//testUserUpdate.add(new User(Id = u.Id, UserName = u.Username + String.valueOf(count)));
			u.put('Username',u.Username + String.valueOf(count));
			contactIDUserMap.put(u.ContactId,u);
			++count;
			
			//conIds.add(u.ContactId);
		}

		Test.startTest();
			SObjectHelper sobHelper = new SObjectHelper('User', contactIDUserMap.values());
			Id jobId = System.enqueueJob(sobHelper);
		Test.stopTest();
	}
}