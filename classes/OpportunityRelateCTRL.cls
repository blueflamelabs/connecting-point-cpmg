public class OpportunityRelateCTRL{

    Public String ConId{set;get;}
    Public String AccId{set;get;}
    Public String ConRecId{set;get;}
    Public String OppName{set;get;}
    Public String RecordTypeName{set;get;}
    public OpportunityRelateCTRL(){
        ConId = ApexPages.currentPage().getParameters().get('Id');
        AccId = ApexPages.currentPage().getParameters().get('AccountsId');
        OppName = ApexPages.currentPage().getParameters().get('AccName') +' - '+ ApexPages.currentPage().getParameters().get('ConName');
        RecordTypeName = ApexPages.currentPage().getParameters().get('ReTypeId');
        ConRecId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(ApexPages.currentPage().getParameters().get('ReTypeId')).getRecordTypeId();
    }
}