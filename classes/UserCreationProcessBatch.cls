global class UserCreationProcessBatch implements Database.Batchable<sObject> {
    
    String query;
    
    global UserCreationProcessBatch() {
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        Community_Settings__c commSettings = Community_Settings__c.getOrgDefaults();
        Date todaydate = Date.valueOf(System.now());
        Date startDate = commSettings.First_Event_date__c;
        String oStatus = String.escapeSingleQuotes(commSettings.Attendee_Confirmation_Status__c);
        string query = 'SELECT Order_Event__r.User_Link_Expiration_for_Suppliers__c,Order_Event__r.User_Link_Expiration_for_Executives__c,Welcome_Email_Link_Expiration__c,Welcome_Email_Link__c,Id,Order_Event__r.Registration_Part_1_Open_For_Supplier__c,Order_Event__r.Registration_Part_1_Open_For_Executive__c,Opportunity.Account_Record_Type__c,Status, Send_Email__c, Order_Event__c, Order_Event__r.isActive, BillToContactId,'
                + 'BillToContact.Account.Type, Order_Event__r.Start_Date__c, Event_Account_Detail__c, BillToContact.RecordTypeId,'
                + 'Email_Notification_User__c, Event_Account_Detail__r.Registration_Part_1_Submitted__c,'
                + 'Event_Account_Detail__r.Registration_Part_1_Complete__c FROM Order where'
                + ' Send_Email__c = FALSE and Order_Event__r.isActive=TRUE'
                + ' AND Order_Event__r.Start_Date__c >='+String.valueOf(startDate)
                + ' and(( BillToContact.recordType.name = \'Supplier\' and Order_Event__r.Supplier_Users_Created__c=true)'
                + ' OR ( BillToContact.recordType.name = \'Executive\' and Order_Event__r.Executive_Users_Created__c=true))';
        
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        System.debug('THe value of Scope: '+ scope);
        if(scope.size() > 0){
            UserAndEmailCreationProcess userProcess = new UserAndEmailCreationProcess((List<Order>)scope);
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
    
}