({
	createRecord : function(component, event, helper) {
       var AccId = component.get("v.recordId");
       var action = component.get('c.showContactRecord');
       action.setParams({'AccsId': AccId});
       action.setCallback(this, function(response) {            
           var state = response.getState();
           if (state === "SUCCESS") {
               var objRecord = response.getReturnValue();
               if(objRecord != null){
                   var createRecordEvent = $A.get("e.force:createRecord");
                   if(objRecord.split(',')[3] == 'Supplier'){
                       createRecordEvent.setParams({
                           "entityApiName": "Opportunity",
                           "recordTypeId": objRecord.split(',')[0],
                           'defaultFieldValues': {
                               'Name' : objRecord.split(',')[2],
                               'AccountId' : objRecord.split(',')[1],
                               'Amount' : '18500',
                           }
                       });
                   }else{
                       createRecordEvent.setParams({
                           "entityApiName": "Opportunity",
                           "recordTypeId": objRecord.split(',')[0],
                           'defaultFieldValues': {
                               'Name' : objRecord.split(',')[2],
                               'AccountId' : objRecord.split(',')[1],
                           }
                       });
                   }
                   createRecordEvent.fire();
               }
           }
       });
       $A.enqueueAction(action);
	}
})