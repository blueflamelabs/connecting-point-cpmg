({
    handleClickAdd : function(component, event, helper){
        component.set('v.showDetail',false);
        var indexvar =component.get("v.value");
        console.log("indexvar:::" + indexvar);
        var c = component.get("v.paginationListIndex")[indexvar].objCampaign.Id;
        var cmpEvnt=component.getEvent("campaignSelectedRecord");
        cmpEvnt.setParams({campaignsSelect:c,campaignAdd:true});
        cmpEvnt.fire();
    },
    handleClickCheck : function(component, event, helper){
        component.set('v.showDetail',true);
        var indexvar =component.get("v.value");
        console.log("indexvar:::" + indexvar);
        var c = component.get("v.paginationListIndex")[indexvar].objCampaign.Id;
        var cmpEvnt=component.getEvent("campaignSelectedRecord");
        cmpEvnt.setParams({campaignsSelect:c,campaignAdd:false});
        cmpEvnt.fire();
        
    },
    handleClickRemove : function(component, event, helper){
        component.set('v.showDetailRemove',false);
        var indexvar =component.get("v.value");
        console.log("indexvar:::" + indexvar);
        var c = component.get("v.paginationListIndex")[indexvar].objCampaign.Id;
        var cmpEvnt=component.getEvent("campaignSelectedRecordRemove");
        cmpEvnt.setParams({campaignsSelect:c,campaignAdd:false});
        cmpEvnt.fire();
    },
    handleClickCheckRemove : function(component, event, helper){
        component.set('v.showDetailRemove',true);
        var indexvar =component.get("v.value");
        console.log("indexvar:::" + indexvar);
        var c = component.get("v.paginationListIndex")[indexvar].objCampaign.Id;
        var cmpEvnt=component.getEvent("campaignSelectedRecordRemove");
        cmpEvnt.setParams({campaignsSelect:c,campaignAdd:true});
        cmpEvnt.fire();
        
    }
})