({
    /* doInitHelper funcation to fetch all records, and set attributes value on component load */
    
    doInitHelper : function(component,event,sortField){ 
        var action = component.get("c.fetchCampaignWrapper");
        var contactsId = component.get("v.recordId");
        var SelectCampaign = component.get("v.SelectedCampaign");
        var SelectCampaignEvent = component.get("v.SelectedCamEvent");
        var tabId= component.get("v.selTabId"); 
        action.setParams({contactId:contactsId,
                          SelectedCampaign:SelectCampaign,
                          SelectedCampaignEvent:SelectCampaignEvent,
                          sortField: sortField,
                          isAsc: component.get("v.isAsc"),
                          tabID:tabId,
                         });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS"){
                var oRes = response.getReturnValue();
                if(tabId=='tab1'){
                    component.set('v.listOfAllCampaigns', null);
                    if(oRes.length > 0){
                        component.set('v.listOfAllCampaigns', oRes);
                        component.set("v.bNoRecordsFound" , false);
                     }else{
                        component.set("v.bNoRecordsFound" , true);
                    } 
                }  
             
            }else{
                alert('Error...');
            }
        });
        $A.enqueueAction(action);
        
        var action = component.get('c.getBaseUrl')
          action.setCallback(this, function (response) {
            var state = response.getState()
            if (component.isValid() && state === 'SUCCESS') {
              var result = response.getReturnValue();
                //alert(result);
              component.set('v.baseUrl', result)
            }
          });
          $A.enqueueAction(action);
        
           var action = component.get('c.getUIThemeDescription')
          action.setCallback(this, function (response) {
            var state = response.getState()
            if (component.isValid() && state === 'SUCCESS') {
              var result = response.getReturnValue();
               // alert(result);
              component.set('v.theme_ui', result)
            }
          });
          $A.enqueueAction(action);
        
         var actionRemove = component.get("c.fetchRemovefromCampaigns");
         actionRemove.setParams({contactId:contactsId});
         actionRemove.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS"){
                var oRes = response.getReturnValue();
                    if(oRes.length > 0){
                        component.set('v.listOfAllSelectCampaigns', oRes);
                        component.set("v.bNoRecordsFoundRemoveCapmaign" , false);
                    }else{
                        // if there is no records then display message
                        component.set("v.bNoRecordsFoundRemoveCapmaign" , true);
                }    
            }
         });
         $A.enqueueAction(actionRemove);
        
         var actionName = component.get("c.RecName");
         actionName.setParams({contactId:contactsId});
         actionName.setCallback(this, function (responseName) {
            var state = responseName.getState()
            if (component.isValid() && state === 'SUCCESS') {
              	component.set('v.recordName', responseName.getReturnValue());
            }
          });
          $A.enqueueAction(actionName);
        
    },   
     SelectedHelper: function(component, event, RecordsIds) {
        
        var contactsId = component.get("v.recordId"); 
        var action = component.get('c.insertRecord');
         action.setParams({lstRecordId: RecordsIds,contactId:contactsId});
      
        action.setCallback(this, function(response) {            
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(state);
           		var contactsId = component.get("v.recordId");
                var basUrl=component.get('v.baseUrl');
                 if (component.get('v.theme_ui') === 'Theme3') {
                  // salesforce classic
                   window.location.href = basUrl + '/' + contactsId                 
                  } else {
                  // lightning experience        
                    var lightUrl= basUrl.replace('my.salesforce.com','lightning.force.com/lightning/r/Contact');
                     // window.location.href = lightUrl + '/' + contactsId+ '/view' ; 
                     window.location.href = '/lightning/r/Contact/'+contactsId+ '/view'; 
                    
                      var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            title : 'Success Message',
                            message: 'Contact is added to the Campaign successfully',
                            messageTemplate: 'Record {0} created! See it {1}!',
                            duration:' 30000',
                            key: 'info_alt',
                            type: 'success',
                            mode: 'pester'
                        });
                        toastEvent.fire();
                }
            }
        });
        $A.enqueueAction(action);
    },
   DeSelectedHelper: function(component, event, RecordsIds) {
        
        var contactsId = component.get("v.recordId"); 
        var action = component.get('c.removeRecord');
         action.setParams({lstRecordId: RecordsIds,contactId:contactsId});
      
        action.setCallback(this, function(response) {            
            var state = response.getState();
            if (state === "SUCCESS") {
                var contactsId = component.get("v.recordId");
                var basUrl=component.get('v.baseUrl');
                 if (component.get('v.theme_ui') === 'Theme3') {
                  // salesforce classic
                   window.location.href = basUrl + '/' + contactsId                 
                  } else { 
                  // lightning experience                    
                     var lightUrl= basUrl.replace('my.salesforce.com','lightning.force.com/lightning/r/Contact');
                   // window.location.href = lightUrl + '/' + contactsId+ '/view' ;
                      window.location.href = '/lightning/r/Contact/'+contactsId+'/view'; 
                      var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            title : 'Success Message',
                            message: 'Contact is Removed from the Campaign successfully',
                            messageTemplate: 'Record {0} created! See it {1}!',
                            duration:' 30000',
                            key: 'info_alt',
                            type: 'success',
                            mode: 'pester'
                        });
                        toastEvent.fire();
                }
            }    
        });
        $A.enqueueAction(action);
    },
    SearchlHelper: function(component, event) {
    var searchKey;
    var SelectCampaign = component.get("v.SelectedCampaign");
    var SelectCampaignEvent = component.get("v.SelectedCamEvent");
    var tabId= component.get("v.selTabId"); 
    // alert(tabId);
    var action = component.get("c.findByName");
    var contactsId = component.get("v.recordId");
    if (event.currentTarget) {
        searchKey = event.currentTarget.value;
        console.log('searchKey' + searchKey);
        console.log('contactsId' + contactsId);
        if (action) {
            action.setParams({
                "searchKey": searchKey,
    			contactId:contactsId,
                SelectedCampaign : SelectCampaign,
                SelectedCampaignEvent : SelectCampaignEvent,
                tabId:tabId
            });
            action.setCallback(this, function(response) {
                  var state = response.getState();
                if (state === "SUCCESS") {
                var oRes = response.getReturnValue();
                    if(tabId=='tab1'){ 
                        if(oRes.length > 0){
                               component.set('v.listOfAllCampaigns', oRes);
                               component.set("v.bNoRecordsFound" , false);                        
                             }else{
                                // if there is no records then display message
                                component.set("v.bNoRecordsFound" , true);
                             } 
                    }else{
                          if(oRes.length > 0){
                               component.set('v.listOfAllSelectCampaigns', oRes);
                               component.set("v.bNoRecordsFoundRemoveCapmaign" , false);                        
                             }else{
                                // if there is no records then display message
                                component.set("v.bNoRecordsFoundRemoveCapmaign" , true);
                             }        
                     }
                               
                    }else{
                    alert('Error...');
                }   
             });
            $A.enqueueAction(action);
        }
     }
   },  
    sortHelper: function(component, event, sortFieldName) {
      var currentDir = component.get("v.arrowDirection");
     //  alert(sortFieldName);
      if (currentDir == 'arrowdown') {
         component.set("v.arrowDirection", 'arrowup');
         component.set("v.isAsc", true);
      } else {
         component.set("v.arrowDirection", 'arrowdown');
         component.set("v.isAsc", false);
      }
      this.doInitHelper(component, event, sortFieldName);
   },
    sortHelperRemove: function(component, event, sortField) {
      var currentDir = component.get("v.arrowDirection");
      if (currentDir == 'arrowdown') {
         component.set("v.arrowDirection", 'arrowup');
         component.set("v.isAsc", true);
      } else {
         component.set("v.arrowDirection", 'arrowdown');
         component.set("v.isAsc", false);
      }
        
        var action = component.get("c.fetchCampaignWrapper");
        var contactsId = component.get("v.recordId");
        var SelectCampaign = component.get("v.SelectedCampaign");
        var SelectCampaignEvent = component.get("v.SelectedCamEvent");
        var tabId= component.get("v.selTabId"); 
    
        action.setParams({contactId:contactsId,
                          SelectedCampaign:SelectCampaign,
                          SelectedCampaignEvent:SelectCampaignEvent,
                          sortField: sortField,
                          isAsc: component.get("v.isAsc"),
                          tabID:tabId,
                         });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS"){
                var oRes = response.getReturnValue();
                if(tabId=='tab2'){                 
                      //component.set('v.listOfAllSelectCampaigns', null);
                     if(oRes.length > 0){
                        component.set('v.listOfAllSelectCampaigns', oRes);
                        component.set("v.bNoRecordsFoundRemoveCapmaign" , false);
                     }else{
                        component.set("v.bNoRecordsFoundRemoveCapmaign" , true);
                    }   
              }   
            }
            else{
                alert('Error...');
            }
             });
            $A.enqueueAction(action);
   },   
                                                   
})