({
	ConfirmationRecord : function(component, event, helper) {
       var ConfId = component.get("v.recordId");
       var action = component.get('c.showConfirmationRecord');
       action.setParams({'ConfId': ConfId});
       action.setCallback(this, function(response) {            
           var state = response.getState();
           if (state === "SUCCESS") {
               var objRecord = response.getReturnValue();
               if(objRecord != null){
                   component.set("v.OrderRecord",objRecord);
                   component.set("v.OrderPrimary",objRecord.Primary_Event_Contact__c);
               }
               
               if(objRecord.Primary_Event_Contact__c === false){
               		objRecord.Primary_Event_Contact__c = true;
               }else{
                   objRecord.Primary_Event_Contact__c = false;               
               }
                   
               var actionUpdate = component.get('c.ConfirmationRecordUpdate');
               actionUpdate.setParams({'objOrder': objRecord});
               actionUpdate.setCallback(this, function(responseError) {            
                   var stateMess = responseError.getState();
                   component.set("v.isShow",true);
                   if (stateMess === "SUCCESS") {
                       var objRecord = responseError.getReturnValue();
                       if(objRecord != null && objRecord.includes('There is already another confirmation record currently')){
                           component.set("v.onLoadMessageCheck",false);
                           component.set("v.ErrorMessage",objRecord);
                       }else if(objRecord != null && objRecord.includes('You must have a Primary Event Contact for this Event. Please ')){
                           component.set("v.onLoadMessageCheck",true);
                           component.set("v.ErrorMessage",objRecord);
                       }else if(objRecord != null && objRecord.includes('You must have a Primary Event Contact for this Event and')){
                           component.set("v.onLoadMessageCheck",false);
                           component.set("v.ErrorMessage",objRecord);
                       }else{
                           component.set("v.onLoadMessageCheck",false);
                       }
                       
                   }
               });
               $A.enqueueAction(actionUpdate);
               
           }
       });
       $A.enqueueAction(action);
	},
    UpdateConfirmationRecord : function(component, event, helper) {
        component.set("v.onLoadMessageCheck",false);
        var ConfObject = component.get("v.OrderRecord");
        var oldPrimary = component.get("v.OrderPrimary");
        if(ConfObject.Primary_Event_Contact__c != oldPrimary){
            var action = component.get('c.ConfirmationRecordUpdate');
            action.setParams({'objOrder': ConfObject});
            action.setCallback(this, function(response) {            
                var state = response.getState();
                if (state === "SUCCESS") {
                    var objRecord = response.getReturnValue();
                    if(objRecord != null){
                        component.set("v.ErrorMessage",objRecord);
                    }else{
                        alert('Error');
                    }
                }
            });
            $A.enqueueAction(action);
        }
    }
})