({
    openAccountModal: function(component, event, helper) {
        var AccCreate = component.get('v.account');
        AccCreate.Name = component.get('v.searchString');
        AccCreate.BillingStreet = '';
        AccCreate.BillingCity = '';
        AccCreate.BillingState = '';
        AccCreate.BillingPostalCode = '';
        AccCreate.BillingCountry = '';
        component.set('v.account',AccCreate);
        var modal = component.find("AccountModal");
        var modalBackdrop = component.find("AccountModalBackdrop");
        $A.util.addClass(modal,"slds-fade-in-open");
        $A.util.addClass(modalBackdrop,"slds-backdrop_open");
    },
    closeAccountModal: function(component, event, helper) {
        var AccCreate = component.get('v.account');
        AccCreate.Name = '';
        AccCreate.BillingStreet = '';
        AccCreate.BillingCity = '';
        AccCreate.BillingState = '';
        AccCreate.BillingPostalCode = '';
        AccCreate.BillingCountry = '';
        component.set('v.account',AccCreate);
        var modal = component.find("AccountModal");
        var modalBackdrop = component.find("AccountModalBackdrop");
        $A.util.removeClass(modal,"slds-fade-in-open");
        $A.util.removeClass(modalBackdrop,"slds-backdrop_open");
    },
    createAccount: function(component, event, helper) {
        var inputAccName = component.find("NameAccId");
        var Namevalue = inputAccName.get("v.value");
        var isvalid = true;
        if(Namevalue == ''){
            isvalid = false;
            inputAccName.setCustomValidity("You need to enter Name");
            inputAccName.reportValidity();
        }
        if(isvalid == true){
            var wraListCon = component.get('v.wrapperList');
            var CreateAccount = component.get('v.account');
            var createAction = component.get('c.createAccountRecord');
            createAction.setParams({
                'newAcc': CreateAccount,
                'RecTypeName':wraListCon.objContact.RecordType.Name,
            });
            createAction.setCallback(this, function(responseOPP) {
                var state = responseOPP.getState();
                if (state === "SUCCESS"){
                    var AccCreate = component.get('v.account');
                    component.set('v.searchString',AccCreate.Name);
                    component.set('v.selectedRecord',responseOPP.getReturnValue());
                    var modal = component.find("AccountModal");
                    var modalBackdrop = component.find("AccountModalBackdrop");
                    $A.util.removeClass(modal,"slds-fade-in-open");
                    $A.util.removeClass(modalBackdrop,"slds-backdrop_open");
                   // $A.util.removeClass(component.find('resultsDiv'),'slds-is-open');
                }
            });
            $A.enqueueAction(createAction);
        }
    },
    primaryContactPhoneORD :function(component, helper, event) {
        var phoneNo = component.find("phoneOrdId");
        var phoneNumber = phoneNo.get('v.value');
        var s = (""+phoneNumber).replace(/\D/g, '');
        var m = s.match(/^(\d{3})(\d{3})(\d{4})$/);
        var formattedPhone = (!m) ? null : "(" + m[1] + ") " + m[2] + "-" + m[3];
        phoneNo.set('v.value',formattedPhone);
    },
    primaryContactPhoneOPP :function(component, helper, event) {
        var phoneNo = component.find("phoneOppId");
        var phoneNumber = phoneNo.get('v.value');
        var s = (""+phoneNumber).replace(/\D/g, '');
        var m = s.match(/^(\d{3})(\d{3})(\d{4})$/);
        var formattedPhone = (!m) ? null : "(" + m[1] + ") " + m[2] + "-" + m[3];
        phoneNo.set('v.value',formattedPhone);
    },
    primaryContactPhone :function(component, helper, event) {
        var phoneNo = component.find("primaryContactPhone");
        var phoneNumber = phoneNo.get('v.value');
        var s = (""+phoneNumber).replace(/\D/g, '');
        var m = s.match(/^(\d{3})(\d{3})(\d{4})$/);
        var formattedPhone = (!m) ? null : "(" + m[1] + ") " + m[2] + "-" + m[3];
        phoneNo.set('v.value',formattedPhone);
    },
    primaryContactMobile: function(component, helper, event) {
        var phoneNo = component.find("primaryContactMobile");
        var phoneNumber = phoneNo.get('v.value');
        var s = (""+phoneNumber).replace(/\D/g, '');
        var m = s.match(/^(\d{3})(\d{3})(\d{4})$/);
        var formattedPhone = (!m) ? null : "(" + m[1] + ") " + m[2] + "-" + m[3];
        phoneNo.set('v.value',formattedPhone);
    },
    
  
    showSpinner: function(component, event, helper) {
        var pageNo = component.get('v.SelectedPage');
        var checkCmp = component.find("checkbox");
        var isCheck = false;
        if(checkCmp)
        	isCheck = checkCmp.get("v.value");
        if(pageNo != '1' || isCheck == true){
        	component.set("v.Spinner", true);     
        }
        
    },
    hideSpinner : function(component,event,helper){
     component.set("v.Spinner", false);
    },
    YesAction : function(component, event, helper) {
        component.set('v.SelectedPage','1');
        helper.YesActionHelper(component, event, helper,'yes');
        
	},
    NoActionCon : function(component, event, helper) {
        component.set('v.SelectedPage','2');
        helper.YesActionHelper(component, event, helper,'noAction');
    },
    NoAction : function(component, event, helper) {
        component.set('v.SelectedPage','2');
	},
    NoActionCheckOpp : function(component, event, helper) {
        var AccIdvalue = component.get("v.selectedRecord").Id;
        var inputEmail = component.find("emailId");
        var Emailvalue = inputEmail.get("v.value");
        var inputTitle = component.find("titileId");
        var titlevalue = inputTitle.get("v.value");
        var isvalid = true;
        if(Emailvalue == '' && titlevalue == '' && AccIdvalue == undefined){
            isvalid = false;
            inputEmail.setCustomValidity("You need to enter Email Id");
            inputEmail.reportValidity();
            inputTitle.setCustomValidity("You need to enter Title");
            inputTitle.reportValidity();
            component.set("v.messageAccount", "You need to select Account");
        }else if(AccIdvalue == undefined){
            isvalid = false;
            inputEmail.setCustomValidity("");
            inputEmail.reportValidity();
            inputTitle.setCustomValidity("");
            inputTitle.reportValidity();
            component.set("v.messageAccount", "You need to select Account");
        }else if(Emailvalue == ''){
            isvalid = false;
            inputTitle.setCustomValidity("");
            inputTitle.reportValidity();
            inputEmail.setCustomValidity("You need to enter EmailId");
            inputEmail.reportValidity();
            component.set("v.message", "");
        }else if(titlevalue == ''){
            isvalid = false;
            inputEmail.setCustomValidity("");
            inputEmail.reportValidity();
            inputTitle.setCustomValidity("You need to enter Title");
            inputTitle.reportValidity();
            component.set("v.message", "");
        }
        if(isvalid == true){
           component.set('v.SelectedPage','2');
            helper.OpportunityInfo(component, event, helper,'Yes');
        }
	},
    goneContactConf : function(component, event, helper) {
        component.set('v.SelectedPage','5');
    },
    goneContact : function(component, event, helper) {
        var oppList = component.get("v.wrapperOpportunityList");
        var oppMasterMap = component.get("v.wrapperOpportunityMapMaster");
        var opts = [];
        for(var i=0;i< oppList.length;i++){
            if(oppList[i].objOpp.Primary_Contact__c === oppMasterMap.get(oppList[i].objOppMap.Id).Primary_Contact__c){
                opts.push(oppList[i].objOpp.Id);
            }
        }
        var optsChange = [];
        for(var i=0;i< oppList.length;i++){
            if(oppList[i].objOpp.Primary_Contact__c != oppMasterMap.get(oppList[i].objOppMap.Id).Primary_Contact__c){
                optsChange.push(oppList[i].objOpp.Id);
            }
        }
        var isCl = component.get("v.isCloseLost");
        if(opts.length == 0 || isCl == true){
            for(var i=0;i< oppList.length;i++){
                for(var j=0;j< opts.length;j++){
                    if(opts[j] == oppList[i].objOpp.Id){
                        oppList[i].objOpp.StageName = 'Closed Lost';
                    }
                }
            }
            component.set("v.wrapperOpportunityList",oppList);
            component.set("v.SelectedPage",'5');
        }else{
        	component.set("v.isCloseLost",true);
            component.set("v.oppNtChangeRec",opts);
            component.set("v.oppChangeRec",optsChange);
        }
    },
    YesOpportunity : function(component, event, helper) {
        component.set('v.SelectedPage','3');
        var selectContact =  component.find("ContactNames"); 
        var wraList = component.get("v.wrapperList"); 
        var actionConApex = component.get("c.allContact");
        actionConApex.setParams({'objwrap':wraList});
        actionConApex.setCallback(this, function(response1) {
            var state = response1.getState();
            if(state === "SUCCESS"){
                var conList =  response1.getReturnValue();
                var opts = [];
                opts.push({"class": "optionClass", label: "None", value: ""});
                for(var i=0;i< conList.length;i++){
                    if(wraList.objContact.Id != conList[i].Id){
                        opts.push({"class": "optionClass", label: conList[i].Name, value: conList[i].Id});
                    }
                }
                selectContact.set("v.options", opts); 
                component.set('v.Contacts',conList);
                helper.OpportunityInfo(component, event, helper,'Yes');
                //var getInputkeyWord = '';
         		//helper.searchHelper(component,event,getInputkeyWord);
            }
        });
        $A.enqueueAction(actionConApex);
        helper.ContactsHyginRecordInfo(component, event, helper);
	},
    NoOpportunity : function(component, event, helper) {
		component.set('v.SelectedPage','4');
        helper.OpportunityInfo(component, event, helper,'No');
       // var getInputkeyWord = '';
       // helper.searchHelper(component,event,getInputkeyWord);
	},
    onSelectContact : function(component, event, helper) {
        var selected = component.find("ContactNames").get("v.value");
		component.set("v.ContactNameSingle",selected); 
        component.set("v.ApplyAllOppShow",true); 
	},
    onSelectOrder : function(component, event, helper) { 
        component.set("v.ApplyAllOrderShow",true); 
	},
    ApplytoAll : function(component, event, helper) {
        var selected = component.find("ContactNames").get("v.value");
        if(selected !=''){
            var opts = [];
            var wraListOpportunity = component.get("v.wrapperOpportunityList"); 
            for(var i=0;i< wraListOpportunity.length;i++){
          		wraListOpportunity[i].objOpp.Primary_Contact__c = selected;
                opts.push(wraListOpportunity[i]);
            }
            component.set("v.wrapperOpportunityList",opts);
        }
	},
    CloseLost : function(component, event, helper) {
        var opts = [];
        var wraListOpportunity = component.get("v.wrapperOpportunityList"); 
        for(var i=0;i< wraListOpportunity.length;i++){
            wraListOpportunity[i].objOpp.StageName = 'Closed Lost';
            opts.push(wraListOpportunity[i]);
            console.log('#########3 0+wraListOpportun '+wraListOpportunity[i].objOpp.StageName);
        }
        component.set("v.wrapperOpportunityList",opts);
        component.set('v.SelectedPage','5');
	},
    openModal: function(component, event, helper) {
        var modal = component.find("contactModal");
        var modalBackdrop = component.find("contactModalBackdrop");
        $A.util.addClass(modal,"slds-fade-in-open");
        $A.util.addClass(modalBackdrop,"slds-backdrop_open");
    },
    closeModal: function(component, event, helper) {
        var modal = component.find("contactModal");
        var modalBackdrop = component.find("contactModalBackdrop");
        $A.util.removeClass(modal,"slds-fade-in-open");
        $A.util.removeClass(modalBackdrop,"slds-backdrop_open");
    },
    openModalGone: function(component, event, helper) {
        var conCreate = component.get('v.contact');
        conCreate.FirstName = '';
        conCreate.LastName = '';
        conCreate.Email = '';
        conCreate.Phone = '';
        conCreate.Reach_Hygiene__c = '';
        component.set('v.contact',conCreate);
        var modal = component.find("contactModalGone");
        var modalBackdrop = component.find("contactModalBackdropGone");
        $A.util.addClass(modal,"slds-fade-in-open");
        $A.util.addClass(modalBackdrop,"slds-backdrop_open");
    },
    closeModalGone: function(component, event, helper) {
        var modal = component.find("contactModalGone");
        var modalBackdrop = component.find("contactModalBackdropGone");
        $A.util.removeClass(modal,"slds-fade-in-open");
        $A.util.removeClass(modalBackdrop,"slds-backdrop_open");
    },
     createContact: function(component, event, helper) {
         var inputEmail = component.find("emailOppId");
         var Emailvalue = inputEmail.get("v.value");
         var inputTitle = component.find("LastNameOppId");
         var titlevalue = inputTitle.get("v.value");
         var HygieneOpp = component.get("v.contact.Reach_Hygiene__c");
         var isvalid = true;
         if(Emailvalue == '' && titlevalue == '' && HygieneOpp == ''){
             isvalid = false;
             inputEmail.setCustomValidity("You need to enter Email Id");
             inputEmail.reportValidity();
             inputTitle.setCustomValidity("You need to enter Last Name");
             inputTitle.reportValidity();
             component.set("v.messageHygiOpp", "You need to select hygiene Value");
         }else if(HygieneOpp == ''){
             isvalid = false;
             inputTitle.setCustomValidity("");
             inputTitle.reportValidity();
             inputEmail.setCustomValidity("");
             inputEmail.reportValidity();
             component.set("v.messageHygiOpp", "You need to select Hygiene Value");
         }else if(Emailvalue == ''){
             isvalid = false;
             inputTitle.setCustomValidity("");
             inputTitle.reportValidity();
             inputEmail.setCustomValidity("You need to enter Email Id");
             inputEmail.reportValidity();
             component.set("v.messageHygiOpp", "");
         }else if(titlevalue == ''){
             isvalid = false;
             inputEmail.setCustomValidity("");
             inputEmail.reportValidity();
             inputTitle.setCustomValidity("You need to enter Last Name");
             inputTitle.reportValidity();
             component.set("v.messageHygiOpp", "");
         }
         if(isvalid == true){
          	helper.insertContact(component, event, helper);   
         }
    },
    createContactGone: function(component, event, helper) {
        var inputEmail = component.find("emailOrdId");
        var Emailvalue = inputEmail.get("v.value");
        var inputTitle = component.find("LastNameOrdId");
        var titlevalue = inputTitle.get("v.value");
        var HygieneOrd = component.get("v.contact.Reach_Hygiene__c");
        var isvalid = true;
        if(Emailvalue == '' && titlevalue == '' && HygieneOrd == ''){
            isvalid = false;
            inputEmail.setCustomValidity("You need to enter Email Id");
            inputEmail.reportValidity();
            inputTitle.setCustomValidity("You need to enter Last Name");
            inputTitle.reportValidity();
            component.set("v.messageHygiOrd", "You need to select hygiene Value");
        }else if(HygieneOrd == ''){
            isvalid = false;
            inputTitle.setCustomValidity("");
            inputTitle.reportValidity();
            inputEmail.setCustomValidity("");
            inputEmail.reportValidity();
            component.set("v.messageHygiOrd", "You need to select Hygiene Value");
        }else if(Emailvalue == ''){
            isvalid = false;
            inputTitle.setCustomValidity("");
            inputTitle.reportValidity();
            inputEmail.setCustomValidity("You need to enter Email Id");
            inputEmail.reportValidity();
            component.set("v.messageHygiOrd", "");
        }else if(titlevalue == ''){
            isvalid = false;
            inputEmail.setCustomValidity("");
            inputEmail.reportValidity();
            inputTitle.setCustomValidity("You need to enter Last Name");
            inputTitle.reportValidity();
            component.set("v.messageHygiOrd", "");
        }
        if(isvalid == true){
            helper.createContactGone(component, event, helper);
        }
    },
    YesConformation : function(component, event, helper) {
        component.set('v.SelectedPage','6');
        var selectContact =  component.find("ContactNameGones"); 
        var wraList = component.get("v.wrapperList"); 
        var actionConApex = component.get("c.allContact");
        actionConApex.setParams({'objwrap':wraList});
        actionConApex.setCallback(this, function(response1) {
            var state = response1.getState();
            if (state === "SUCCESS"){
                var conList =  response1.getReturnValue();
                var opts = [];
                opts.push({"class": "optionClass", label: "None", value: ""});
                for(var i=0;i< conList.length;i++){
                    if(wraList.objContact.Id != conList[i].Id){
                        opts.push({"class": "optionClass", label: conList[i].Name, value: conList[i].Id});
                    }
                }
                selectContact.set("v.options", opts); 
                component.set('v.Contacts',conList);
                helper.ConformationInfo(component, event, helper,'Yes');
            }
        });
        $A.enqueueAction(actionConApex);
        helper.ContactsHyginRecordInfo(component, event, helper);
	},
    ApplytoAllConfo : function(component, event, helper) {
        var selected = component.find("ContactNameGones").get("v.value");
        if(selected !=''){
            var opts = [];
            var wraListOrd = component.get("v.wrapperOrderList"); 
            for(var i=0;i< wraListOrd.length;i++){
          		wraListOrd[i].objOrd.BillToContactId = selected;
                opts.push(wraListOrd[i]);
            }
            component.set("v.wrapperOrderList",opts);
        }
	},
    NoConformation : function(component, event, helper) {
        component.set('v.SelectedPage','7');
        helper.ConformationInfo(component, event, helper,'No');
    },
    okandSave : function(component, event, helper) {
        var wraListOrd = component.get("v.wrapperOrderList");
        var opts = [];
        for(var i=0;i< wraListOrd.length;i++){
            wraListOrd[i].objOrd.Status = 'Cancelled';
            opts.push(wraListOrd[i]);
        }
        component.set("v.wrapperOrderList",opts);
        helper.SaveAllUpdateRecordHelper(component, event, helper,'NoOrder'); 
    },
    SaveAllUpdateRecord : function(component, event, helper) {
        
        var oppList = component.get("v.wrapperOrderList");
        var oppMasterMap = component.get("v.wrapperOrderMapMaster");
        var opts = [];
        for(var i=0;i< oppList.length;i++){
            if(oppList[i].objOrd.BillToContactId === oppMasterMap.get(oppList[i].objMasOrd.Id).BillToContactId){
                opts.push(oppList[i].objOrd.Id);
            }
        }
        
        var optsChange = [];
        for(var i=0;i< oppList.length;i++){
            if(oppList[i].objOrd.BillToContactId != oppMasterMap.get(oppList[i].objMasOrd.Id).BillToContactId){
                optsChange.push(oppList[i].objOrd.Id);
            }
        }
        var isCl = component.get('v.isCloseLostOrder');
        var isCan = false;
        if(opts.length == 0 || isCl == true){
            for(var i=0;i< oppList.length;i++){
                for(var j=0;j< opts.length;j++){
                    if(opts[j] == oppList[i].objOrd.Id){
                        oppList[i].objOrd.Status = 'Cancelled';
                        isCan = true;
                    }
                }
            }
            component.set("v.isCloseLostOrder",false);
            if(isCan === true){
            	component.set("v.wrapperOrderList",oppList);    
            }else{
                for(var i=0;i< oppList.length;i++){
                    if(oppList[i].objOrd.Status == 'Cancelled'){
                        oppList[i].objOrd.Status = oppMasterMap.get(oppList[i].objMasOrd.Id).Status;
                    }
                }
            	component.set("v.wrapperOrderList",oppList); 
            }
            component.set("v.oppNtChangeRecOrder",opts);
            component.set("v.oppChangeRecOrder",optsChange);
            helper.SaveAllUpdateRecordHelper(component, event, helper,'YesOrder'); 
        }else{
        	//component.set("v.wrapperOrderList",oppListOld);
            component.set("v.isCloseLostOrder",true);
            component.set("v.oppNtChangeRecOrder",opts);
            component.set("v.oppChangeRecOrder",optsChange);
        } 
    },
    onSelectChange: function(component, event, helper) {
        var checkCmp = component.find("checkbox");
        var isCheck = checkCmp.get("v.value");
        var AccOrCon = component.get("v.wrapperList");
        var AccIdUpdate = component.get("v.selectedRecord").Id;
        if(isCheck == true){
            var actionConApex = component.get("c.addressOfAccount");
            actionConApex.setParams({'AccId':AccIdUpdate,'objwrap':AccOrCon});
            actionConApex.setCallback(this, function(response1) {
                var state = response1.getState();
                if (state === "SUCCESS"){
                    component.set("v.wrapperList",response1.getReturnValue());
                }else{
                    alert('Error');
                }
            });
            $A.enqueueAction(actionConApex); 
        }
    },
	//Lookup Functionlity
	removeItem : function( component, event, helper ){
        component.set('v.selectedRecord','');
        component.set('v.value','');
        component.set('v.searchString','');
        setTimeout( function() {
            component.find( 'inputLookup' ).focus();
        }, 250);
    },  
    searchRecords : function( component, event, helper ) {
        component.set("v.messageAccount", "");
        if( !$A.util.isEmpty(component.get('v.searchString')) ) {
            $A.util.addClass(component.find('resultsDiv'),'slds-is-open');
		    helper.searchRecordsHelper( component, event, helper,component.get('v.searchString'));
        } else {
            $A.util.removeClass(component.find('resultsDiv'),'slds-is-open');
        }
	},
    selectItem : function( component, event, helper ) {
        if(!$A.util.isEmpty(event.currentTarget.id)) {
    		var recordsList = component.get('v.listOfSearchRecords');
    		var index = recordsList.findIndex(x => x.Id === event.currentTarget.id)
            if(index != -1) {
                var selectedRecord = recordsList[index];
            }
            component.set('v.selectedRecord',selectedRecord);
            component.set('v.value',selectedRecord.Id);
            $A.util.removeClass(component.find('resultsDiv'),'slds-is-open');
        }
	},
    blurEvent : function( component, event, helper ){
    	$A.util.removeClass(component.find('resultsDiv'),'slds-is-open');
    },
    doInit : function( component, event, helper ) {
        $A.util.removeClass(component.find('resultsDiv'),'slds-is-open');
	},
})