({
    myAction : function(component, event, helper) {
        
    },
    createRecord: function(component, event, helper) {
        
        var OppId = component.get("v.recordId"); 
        var action = component.get('c.showConformationRecord');
        action.setParams({'oppId': OppId});
        action.setCallback(this, function(response) {            
            var state = response.getState();
            if (state === "SUCCESS") {
                var objRecord = response.getReturnValue();
                if(objRecord != null){
                    //alert('hello'+objRecord.opp.Returning_Customer__c);
                    
                    if(objRecord.isSupplier == false){
                        var createRecordEvent = $A.get("e.force:createRecord");
                        createRecordEvent.setParams({
                            "entityApiName": "Order",
                            'defaultFieldValues': {
                                'Status' : 'Attending Event Contact',
                                'Order_Event__c' : objRecord.opp.Product__c,
                                'AccountId' : objRecord.opp.AccountId,
                                'BillToContactId' : objRecord.oppContactRole.ContactId,
                                'EffectiveDate' : new Date(),
                                'Primary_Event_Contact__c' : true,
                                'Event_Account_Detail__c' : objRecord.ead.Id,
                                'OpportunityId' : objRecord.opp.Id,
                                'Hotel_Confirmation__c' : 'Pending',
                            }
                        });
                        createRecordEvent.fire();
                    }
                    else if(objRecord.isSupplier == true && objRecord.opp.Returning_Customer__c!=null){
                        if(objRecord.ead != 'undefined' && objRecord.ead != '' && objRecord.ead != null){
                            var createRecordEvent = $A.get("e.force:createRecord");
                            createRecordEvent.setParams({
                                "entityApiName": "Order",
                                'defaultFieldValues': {
                                    'Status' : 'Preliminary',
                                    'Order_Event__c' : objRecord.opp.Product__c,
                                    'AccountId' : objRecord.opp.AccountId,
                                    'EffectiveDate' : new Date(),
                                    'Event_Account_Detail__c' : objRecord.ead.Id,
                                    'OpportunityId' : objRecord.opp.Id
                                }
                            });
                            createRecordEvent.fire();
                        }else{
                            if(objRecord.opp.Returning_Customer__c=='Renewal' || objRecord.opp.Returning_Customer__c=='Onsite Renewal'){
                                var createRecordEvent = $A.get("e.force:createRecord");
                                createRecordEvent.setParams({
                                    "entityApiName": "Event_Account_Details__c",
                                    "recordTypeId": objRecord.RecTypeId,
                                    'defaultFieldValues': {
                                        'Event__c' : objRecord.opp.Product__c,
                                        'Account__c' : objRecord.opp.AccountId,
                                        'Opportunity__c' : objRecord.opp.Id,
                                        'Badge_Add_Ons__c' : objRecord.Badge_Add,
                                        'Registration_Reason__c' : 'Renewal',                                          
                                    }
                                });
                                createRecordEvent.fire();
                            }else{
                                var createRecordEvent = $A.get("e.force:createRecord");
                                createRecordEvent.setParams({
                                    "entityApiName": "Event_Account_Details__c",
                                    "recordTypeId": objRecord.RecTypeId,
                                    'defaultFieldValues': {
                                        'Event__c' : objRecord.opp.Product__c,
                                        'Account__c' : objRecord.opp.AccountId, 
                                        'Opportunity__c' : objRecord.opp.Id,
                                    }
                                });
                                createRecordEvent.fire();
                            }     
                            
                        }
                        
                    }else{
                        component.set("v.isError",true);
                    }
                    
                }
                
            }
            
        });
        $A.enqueueAction(action);
        
        
    }
})