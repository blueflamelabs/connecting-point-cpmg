trigger preventEventQuestion on Event_Question__c (before insert,before update) {
    PreventEventQuestionCTRL objPrevent = new PreventEventQuestionCTRL();
    if(Trigger.isBefore && Trigger.isInsert){
        List<Event_Question__c> objEventQuestionList = new List<Event_Question__c>();
        for(Event_Question__c objEventQu : trigger.new){
            if(objEventQu.Display_Type__c != null){
               objEventQuestionList.add(objEventQu);
           }
        }
        if(objEventQuestionList.size() > 0){
            objPrevent.preventInsert(objEventQuestionList);
        }
    }
    if(Trigger.isBefore && Trigger.isUpdate){
        List<Event_Question__c> objEventQuestionList = new List<Event_Question__c>();
        for(Event_Question__c objEventQu : trigger.new){
            if(objEventQu.Display_Type__c != Trigger.oldMap.get(objEventQu.Id).Display_Type__c && objEventQu.Display_Type__c != null){
               objEventQuestionList.add(objEventQu);
           }
        }
        if(objEventQuestionList.size() > 0){
            objPrevent.preventInsert(objEventQuestionList);
        }
    }
}