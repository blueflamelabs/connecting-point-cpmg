trigger OrderTrigger on Order (before insert, before update, after insert, after update, 
                               before delete, after delete, after undelete) {
    OrderTriggerHandler handler = new OrderTriggerHandler(Trigger.isExecuting, Trigger.size);
    
    Trigger_Settings__c trigControl = Trigger_Settings__c.getOrgDefaults();
    if(Trigger.isBefore && RecursiveVariable.orderActiveVariable){
        /*if(Trigger.isUpdate)
            OrderPrimaryContactTriggerHandler.onUpdateEvent(Trigger.new);
        if(Trigger.isInsert )
            OrderPrimaryContactTriggerHandler.onInsertEvent(Trigger.new);*/
        if(Trigger.isDelete )
            OrderPrimaryContactTriggerHandler.onDeleteEvent(Trigger.Old); 
        
    }
    if(!trigControl.Order_Trigger__c){
        if(Trigger.isBefore && Trigger.isInsert){
            handler.onBeforeInsert(Trigger.new);
        }
        if(Trigger.isAfter && Trigger.isInsert) {
             handler.OnAfterInsert(Trigger.new);
        }
        if(Trigger.isAfter && Trigger.isUpdate){
            handler.OnAfterUpdate(Trigger.new);
        }
        if(Trigger.isAfter && Trigger.isDelete) {
            System.debug('USER___ Trigger.old: ' + Trigger.old);
            handler.OnAfterUpdate(Trigger.old);
        }
    }
    if(Trigger.isAfter && Trigger.isInsert) {
      ContactEventAnswers objContactEvent = new ContactEventAnswers();
      objContactEvent.CreateEventAnswers(Trigger.newmap);
      objContactEvent.CreateUserUpdateLink(Trigger.newmap);
      ContactEventAnswers.updateConfo(Trigger.New);
    }
    
    if(Trigger.isUpdate && Trigger.isAfter){
       List<Order> objOrderUpdate = new List<Order>();
       for(Order objOrd : Trigger.new){
           if(objOrd.Status != Trigger.oldMap.get(objOrd.Id).Status){
               objOrderUpdate.add(objOrd);
           }
       }     
       if(objOrderUpdate.size() > 0){
           ContactEventAnswers.updateConfo(objOrderUpdate);
       }
    }
    
    if(Trigger.isUpdate){
       List<Order> objOrderList = new List<Order>();
       for(Order objOrd : Trigger.new){
           if(objOrd.Status == 'Cancelled' && objOrd.Status != Trigger.oldMap.get(objOrd.Id).Status){
               objOrderList.add(objOrd);
           }
       } 
       if(objOrderList.size() > 0){
           OrderPrimaryContactTriggerHandler.onUpdateConfirmation(objOrderList);  
       }
   }
    
}