trigger EventActive on Product2 (before update) {
    if(Trigger.isBefore && Trigger.isUpdate){
        for(Product2 objPro : Trigger.new){
            if((objPro.Supplier_Users_Created__c == true || objPro.Executive_Users_Created__c == true) 
                    && objPro.IsActive == false && objPro.IsActive != Trigger.oldMap.get(objPro.Id).IsActive){
                objPro.isActiveCheck__c = true;
            }
        }
    }
}