trigger updateAccountContactRelationTrigger on Contact (after update,after insert) {
if(Trigger.isUpdate && Trigger.isAfter){
        Set<Id> objAccId = new Set<Id>();
        Map<Id,Contact> objMapContact = new Map<Id,Contact>();
        for(Contact objCon : Trigger.new){
            if(String.isNotBlank(objCon.Email) && objCon.Email != Trigger.oldMap.get(objCon.id).Email
                || String.isNotBlank(objCon.Title) && objCon.Title != Trigger.oldMap.get(objCon.id).Title
                || String.isNotBlank(objCon.AccountId) && objCon.AccountId != Trigger.oldMap.get(objCon.id).AccountId){
                objAccId.add(objCon.AccountId);
                objMapContact.put(objCon.Id,objCon);
            }
        }
        if(objAccId.size() > 0 && objMapContact.size() > 0){
           updateAccountContactRelationCTRL.updateAccConRela(objAccId,objMapContact,Trigger.OldMap);
        }
    }
    if(Trigger.isInsert && Trigger.isAfter){
        updateAccountContactRelationCTRL.updateContactTitle(Trigger.New);
        ContactEventAnswers.ContactUpdateBadgeSortOrder(Trigger.New);
    }
    if(Trigger.isUpdate && Trigger.isAfter){
        List<Contact> objConList = new List<Contact>();
        for(Contact objCon : trigger.new){
            if(objCon.Title != Trigger.oldMap.get(objCon.id).Title){
                objConList.add(objCon);
            }
        }
        if(objConList.size() > 0 && ControllingTrigger.isRunAgain == false){
            updateAccountContactRelationCTRL.updateContactTitle(objConList);
        }
        
        List<Contact> objConListOrderSort = new List<Contact>();
        for(Contact objCon : Trigger.new){
            if(objCon.Badge_Sort_Order__c != Trigger.oldMap.get(objCon.id).Badge_Sort_Order__c){
                objConListOrderSort.add(objCon);
            }
        }
        if(objConListOrderSort.size() > 0){
            ContactEventAnswers.ContactUpdateBadgeSortOrder(objConListOrderSort);
        }
        }
}