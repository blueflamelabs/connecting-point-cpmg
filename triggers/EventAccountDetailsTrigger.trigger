trigger EventAccountDetailsTrigger on Event_Account_Details__c (before insert, before update, after insert, 
                                                                after update, before delete, after delete, after undelete) {
    Trigger_Settings__c tSettings = new Trigger_Settings__c();
    tSettings = Trigger_Settings__c.getOrgDefaults();

    if(!tSettings.Event_Account_Detail_Trigger__c){
        EventAccountDetailsTriggerHandler handler = new EventAccountDetailsTriggerHandler(Trigger.isExecuting, Trigger.size);
        if(Trigger.isAfter && Trigger.isInsert) {
            handler.OnAfterInsert(Trigger.newMap);
        }
    
        if(Trigger.isAfter && Trigger.isDelete) {
            handler.OnAfterDelete(Trigger.oldMap);
        }

        if(Trigger.isBefore && Trigger.isUpdate) {
            handler.OnBeforeUpdate(Trigger.new, Trigger.oldMap);
        }

        if(Trigger.isAfter && Trigger.isUpdate) {
            handler.OnAfterUpdate(Trigger.newMap);
        }
    }
    
    if(Trigger.isAfter && (Trigger.isUpdate || Trigger.isInsert)){
        Map<Id,Event_Account_Details__c> objMapPR = new Map<Id,Event_Account_Details__c>();
        for(Event_Account_Details__c objPR : Trigger.New){
            if(Trigger.isUpdate){
                if(objPR.Status__c == 'Cancelled' && objPR.Status__c != Trigger.oldMap.get(objPR.Id).Status__c){
                    objMapPR.put(objPR.id,objPR);
                }
            }
            if(Trigger.isInsert){
               if(objPR.Status__c == 'Cancelled'){
                    objMapPR.put(objPR.id,objPR);
                }
            }
        }
        if(objMapPR.size() > 0){
            CancellationsOrderPRCTRL.canOrder(objMapPR);
        }
    }
    
}