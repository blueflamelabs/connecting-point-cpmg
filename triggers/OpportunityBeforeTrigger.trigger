/*******************************/
/***
    Who  : VSCIDev
    What : Add validation for duplicate opportunity based on account's record type 
***/
/*******************************/
trigger OpportunityBeforeTrigger on Opportunity (before insert,before update,after update,after insert) {
    if(trigger.isBefore && RecursiveVariable.duplicateVariable){
         RecursiveVariable.duplicateVariable = false;
        AddValidationOnOpportunity.addValidation(trigger.new);
    }
    Set<Id> objSetOppId = new Set<Id>();
    Set<Id> objSetOppIdforCam = new Set<Id>();
    if(Trigger.isAfter && (Trigger.isUpdate || Trigger.isInsert)){
        for(Opportunity objOpp : Trigger.New){
           if(Trigger.isUpdate){
                if(objOpp.StageName == 'Closed Lost' && objOpp.StageName != Trigger.oldMap.get(objOpp.Id).StageName){
                    objSetOppId.add(objOpp.id);
                }
                if(objOpp.StageName == 'Closed Won' && objOpp.StageName != Trigger.oldMap.get(objOpp.Id).StageName){
                    objSetOppIdforCam.add(objOpp.id);
                }
            }
            if(Trigger.isInsert){
                if(objOpp.StageName == 'Closed Lost'){
                    objSetOppId.add(objOpp.id);
                }
                if(objOpp.StageName == 'Closed Won'){
                    objSetOppIdforCam.add(objOpp.id);
                }
            }
        }
        if(objSetOppId.size() > 0){
            CancellationsOrderCTRL.canOrder(objSetOppId);
        }
        if(objSetOppIdforCam.size() > 0){
            CancellationsOrderCTRL.RespondedCamMemberStatus(objSetOppIdforCam);
        }
        
    }
}