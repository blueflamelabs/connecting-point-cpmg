<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>true</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>A custom junction object will need to be created to associate contacts for the purposes of scheduling one on one meetings during the event.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>Private</externalSharingModel>
    <fields>
        <fullName>Comment__c</fullName>
        <externalId>false</externalId>
        <label>Comment</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Event__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Event</label>
        <referenceTo>Product2</referenceTo>
        <relationshipLabel>Event Ranking</relationshipLabel>
        <relationshipName>Event_Ranking</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Participation_Record_Registrant_Match__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Participation of contact/account chosen by the Registrant.</description>
        <externalId>false</externalId>
        <label>Participation Record Registrant Match</label>
        <referenceTo>Event_Account_Details__c</referenceTo>
        <relationshipLabel>Event Ranking (Registrant Match)</relationshipLabel>
        <relationshipName>Event_Ranking1</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Participation_Record_Registrant__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Lookup to Participation Record that the contact choosing the ranking exists one.</description>
        <externalId>false</externalId>
        <label>Participation Record Registrant</label>
        <referenceTo>Event_Account_Details__c</referenceTo>
        <relationshipLabel>Event Ranking</relationshipLabel>
        <relationshipName>Event_Ranking</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Rank__c</fullName>
        <externalId>false</externalId>
        <label>Rank</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Registrant_Match_Account__c</fullName>
        <externalId>false</externalId>
        <formula>Participation_Record_Registrant_Match__r.Account__r.Name</formula>
        <label>Registrant Match Account</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Registrant__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Registrant</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Event Ranking</relationshipLabel>
        <relationshipName>Event_Ranking</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Removed_Ranking__c</fullName>
        <description>Checks to see if the ranking has been removed</description>
        <externalId>false</externalId>
        <formula>ISBLANK( Registrant_Match_Account__c )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Checks to see if the ranking has been removed</inlineHelpText>
        <label>Removed Ranking?</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Type__c</fullName>
        <externalId>false</externalId>
        <label>Type</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Boardroom</fullName>
                    <default>false</default>
                    <label>Boardroom</label>
                </value>
                <value>
                    <fullName>One-on-One</fullName>
                    <default>false</default>
                    <label>One-on-One</label>
                </value>
                <value>
                    <fullName>Boardroom and One-on-One</fullName>
                    <default>false</default>
                    <label>Boardroom and One-on-One</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Event Ranking</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Event Ranking Name</label>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Event Ranking</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Registrant__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Rank__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Registrant__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Rank__c</lookupDialogsAdditionalFields>
        <searchFilterFields>Rank__c</searchFilterFields>
        <searchFilterFields>Registrant__c</searchFilterFields>
        <searchResultsAdditionalFields>Registrant__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Rank__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
