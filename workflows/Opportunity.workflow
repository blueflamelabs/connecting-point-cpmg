<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Opp_Name</fullName>
        <description>Update the Opportunity Name to the standard naming convention.</description>
        <field>Name</field>
        <formula>IF( 
OR(Account_Record_Type__c = &quot;Executive&quot;, AND(Account_Record_Type__c = &quot;Supplier&quot;, Supplier_Opp_Count__c &lt;=1)),
Event_Code__c
&amp;&quot; - &quot;
&amp; Account.Name  
&amp; &quot; - &quot; 
&amp; Primary_Contact__r.FirstName
&amp; &quot; &quot;
&amp; Primary_Contact__r.LastName,

Event_Code__c
&amp;&quot; - &quot;
&amp; Account.Name  
&amp; &quot; - &quot; 
&amp; Primary_Contact__r.FirstName
&amp; &quot; &quot;
&amp; Primary_Contact__r.LastName
&amp; &quot; &quot;
&amp; TEXT(Supplier_Opp_Count__c)
)</formula>
        <name>Update Opp Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Opportunity Naming Convention</fullName>
        <actions>
            <name>Update_Opp_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Update the Opportunity Name to the standard naming convention</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
